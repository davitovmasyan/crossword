<?php
namespace app\components;
 

use Yii;

class Alert
{        
    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];
    public static function init($lang)
    {                
        if($lang == 'en') {
            $text = 'notification';
        } else if($lang == 'ru') {
            $text = 'оповещание';
        } else {
            $text = 'ծանուցում';
        }
        $session = \Yii::$app->getSession();
        $flashes = $session->getAllFlashes();        
        foreach ($flashes as $type => $data) {            
                $data = (array) $data;
                echo '<div class="notifWrap"></div>';
                foreach ($data as $i => $message) {                    
                    echo '<div class="notifMain"><h1 class="notifTitle">#'.$text.'</h1><p>' . $message . '</p></div>';
                $session->removeFlash($type);                
        }                
    }
}
}