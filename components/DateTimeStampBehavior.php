<?php
namespace app\components;

use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use Yii;

class DateTimeStampBehavior extends AttributeBehavior {
    public $attributes = [
        BaseActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],
        BaseActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
    ];

    public $value;

    protected function getValue($event)
    {
        if ($this->value instanceof Expression) {
            return $this->value;
        } else {
            return $this->value !== null ? call_user_func($this->value, $event) : Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i:s');
        }
    }
    public function touch($attribute)
    {
        $this->owner->updateAttributes(array_fill_keys((array) $attribute, $this->getValue(null)));
    }
}