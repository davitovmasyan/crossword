<?php

namespace app\components ;

use Yii ;
use yii\caching\MemCache as MemCacheLib ;

/**
 * 
 */
class MemCache extends MemCacheLib
{
    /**
     * 
     */
    public function init() {
        parent::init() ;
    }
    
    /**
     * Stores a value identified by a key into cache
     * 
     * @param mixed $key A key identifying the value to be cached. 
     *  This can be a simple string or a complex data structure consisting of factors representing the key.
     * @param mixed $value The value to be cached
     * @param integer $duration The number of seconds in which the cached value will expire. 0 means never expire.
     * @return boolean If no error happens
     */
    public function  add( $key, $value, $duration = 0) {
        return parent::addValue($key, $value, $duration) ;
    }
    
    /**
     * 
     * @param mixed $key A key identifying the value to be cached. 
     *  This can be a simple string or a complex data structure consisting of factors representing the key.
     * @param mixed $value The value to be cached
     * @param integer $duration The number of seconds in which the cached value will expire. 0 means never expire.
     * @return boolean If no error happens
     */
    public function update( $key, $value, $duration = 0) {
        return parent::setValue($key, $value, $duration) ;
    }
    
    /**
     * 
     * @param mixed $key A key identifying the cached value. This can be a simple string or a complex data structure consisting of factors representing the key.
     * @return mixed The value stored in cache, false if the value is not in the cache, expired, or the dependency associated with the cached data has changed.
     */
    public function get( $key ) {
        return parent::getValue($key) ;
    }

    /**
     * Deletes a value with the specified key from cache
     * 
     * @param mixed $key A key identifying the value to be deleted from cache. This can be a simple string or a complex data structure consisting of factors representing the key.
     * @return boolean If no error happens
     */
    public function  delete( $key ) {        
        return parent::deleteValue($key);
    }
    
    
    /**
     * Checks whether a specified key exists in the cache.
     * 
     * @param mixed $key A key identifying the cached value. 
     *  This can be a simple string or a complex data structure consisting of factors representing the key.
     * @return boolean True if a value exists in cache, false if the value is not in the cache or expired.
     */
    public function exists( $key ) {
        return parent::exists($key) ;
    }
    
    /**
     * Deletes all values from cache.
     * 
     * @return boolean Whether the flush operation was successful.
     */
    public function flush( ) {
        return parent::flush() ;
    }
    
}