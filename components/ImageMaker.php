<?php

namespace app\components;

use yii\base\Component;
use Yii;

/**
 * 
 */
class ImageMaker extends Component
{
	public $path = '';
    public $background_colors = [
        ['132', '75', '96'],
        ['150', '177', '179'],
        ['84', '128', '132'],
        ['91', '87', '107'],
        ['169', '132', '165'],
        ['214', '120', '65'],
        ['57', '98', '158'],
        ['133', '160', '87'],
        ['70', '165', '126'],
        ['142', '51', '129'],
        ['95', '101', '152'],
        ['95', '148', '152'],
        ['45', '101', '105'],
        ['146', '57', '57'],
        ['189', '86', '86'],
        ['150', '179', '152'],
        ['99', '111', '138'],
        ['138', '95', '144'],
        ['117', '115', '105'],
        ['89', '174', '183'],
        ['206', '190', '120'],
        ['148', '121', '28'],
        ['115', '34', '23'],
        ['175', '119', '58'],
        ['0', '131', '145'],
    ];
    /**
     * 
     */
    public function init() {
        parent::init();
        set_time_limit(0);
    }

    public function create_blank($width, $height) {
        //create image with specified sizes
        $image = imagecreatetruecolor($width, $height);
        //saving all full alpha channel information
        imagesavealpha($image, true);
        //setting completely transparent color
        $transparent = imagecolorallocatealpha($image, 0, 0, 0, 127);
        //filling created image with transparent color
        imagefill($image, 0, 0, $transparent);
        return $image;
    }

    public function generate_image($text, $path, $username) {

        $size = 640;
        $centre = $size / 2;

        //create image resource
        $image = $this->create_blank($size, $size);
        //colors
        $background_color = $this->background_colors[rand(0, 23)];
        $color = imagecolorallocate($image, $background_color[0], $background_color[1], $background_color[2]);
        $text_color = imagecolorallocate($image, 255, 255, 255);

        //font
        $font_file = './fonts/TopModern.ttf';

        //generate circle
        imagefilledarc($image, $centre, $centre, $size, $size, 0, 360, $color, IMG_ARC_PIE);



        //$img_width, $img_height
        // find font-size for $txt_width = 80% of $img_width...
        $font_size = 200;
        $txt_max_width = intval(0.7 * $size);
        $txt_max_height = intval(0.7 * $size);



        do {

            $font_size++;
            $p = imagettfbbox($font_size, 0, $font_file, $text);

            $bug_width = 0;
            //bad symbols
            if ($text[1] == 'L') {
                $bug_width = $size / 15;
            }

            $txt_width = $p[2] - $p[0] + $bug_width;
            $txt_height = $p[7] - $p[1]; // just in case you need it
        } while (abs($txt_width) < $txt_max_width and abs($txt_height) < $txt_max_height);

        $font_size_original = $font_size - 1;

        $bug_height = 0;    
        //bad symbols        
        if ($text[1] == 'j' or $text[1] == 'p' or $text[1] == 'q' or $text[1] == 'y' or $text[1] == 'g') {
            $bug_height = $size / 5;
        }


        $bug_width = 0;

        $I_width = 0;
        //bad symbols
        if ($text[0] == 'I' or $text[0] == 'i') {
            $bug_width = $size / 20;
            if ($text[1] == 'j' or $text[1] == 'p' or $text[1] == 'q' or $text[1] == 'y' or $text[1] == 'g') {
                $bug_height = 0;
                $bug_height = $size / 4.5;
            }

            if ($text[1] == 'a' or $text[1] == 'c' or $text[1] == 'e' or $text[1] == 'o') {
                $bug_height = 0;
                $bug_height = $size / 7;
            }
        }

        //change font size
        if ($text[1] == 'f' or $text[1] == 'j' or $text[1] == 't') {
            $font_size = 0.90 * $font_size;
            $font_size = round($font_size);
        } elseif ($text[0] == 'I') {
            $font_size = 0.85 * $font_size;
            $font_size = round($font_size);
            $I_width = 300;
        } elseif ($text[1] == 'i' or $text[1] == 'l') {
            $font_size = 0.95 * $font_size;
            $font_size = round($font_size);
        }


        // now center text...
        $y = ($size - $txt_height - $bug_height) / 2;

        $x = ($size - $txt_width - $bug_width + $I_width) / 2;

        //write text in image
        imagettftext($image, $font_size, 0, $x, $y, $text_color, $font_file, $text);

        ///to change text box
        /*
          $border_color = imagecolorallocate ( $image , 0 , 0 , 0 ) ;
          imageline($image, $x,$y,$x,$y+$txt_height,$border_color); //left
          imageline($image, $x,$y+$txt_height,$x+$txt_width,$y+$txt_height,$border_color); //top
          imageline($image, $x+$txt_width,$y+$txt_height,$x+$txt_width,$y,$border_color); //right
          imageline($image, $x+$txt_width,$y,$x,$y,$border_color);

         */


        $image_new = $this->create_blank($size / 10, $size / 10);
        imagecopyresampled($image_new, $image, 0, 0, 0, 0, $size / 10, $size / 10, $size, $size);


        //output to file
        imagepng($image_new, 'images/users/' . $username . '.png');
        imagedestroy($image);
        imagedestroy($image_new);

        return strtolower($username) . '.png';
    }

    public function generate($username)
    {
        $path = __DIR__;
        $path = str_replace('components', '', $path);
        $path .= 'web/images/users';
        $text = strtoupper($username[0]) . strtolower($username[1]);
        $image = $this->generate_image($text, $path, $username);

        return $image;
    }

    public function regenerate($username, $oldUsername)
    {
        $path = __DIR__;
        $path = str_replace('components', '', $path);
        $path .= 'web/images/users';
        $text = strtoupper($username[0]) . strtolower($username[1]);
        $image = $this->generate_image($text, $path, $username);

        if(file_exists($path.'/'.$oldUsername.'.png'))
        {
            unlink($path.'/'.$oldUsername.'.png');
        }

        return $image;
    }
}