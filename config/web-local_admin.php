<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '00ec146eb74ae6c90fc51f1dc6abb25d',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\Admins',
            'loginUrl' => '/admin/login',
            'enableAutoLogin' => true,
        ],        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        
        'Messages' => [
            'class' => 'app\components\Messages',
        ],
        'MemCache' => [
            'class' => 'app\components\MemCache',
        ],
        'ImageMaker' => [
            'class' => 'app\components\ImageMaker',
        ],

        'session' => [
          'class' => 'yii\web\CacheSession',
        ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db-local.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,            
            'rules' => [                            
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<key:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<param:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['admin', 'user'],
        ],
        'assetManager' => [
        'bundles' => [
            // turn off Yii2 default jquery
             'yii\web\JqueryAsset' => [
                'sourcePath' => null,
                'js' => [] 
            ],        
        ],
    ],
],    
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
