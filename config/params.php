<?php

return [
    'supportEmail' => 'support@crossword.am',
    'noReplyEmail' => 'no-reply@crossword.am',
    'davitEmail' => 'davit@crossword.am',
    'infoEmail' => 'info@crossword.am',
    'users.passwordResetTokenExpire' => 3600*24*30,
    'languages'  =>  [
        'en' => 'English', 
        'ru' => 'Русский', 	    
	],
];
