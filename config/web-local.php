<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'tnK_JxkURRKSBQoxXf1f73kT4eZvjcZH',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\Users',
            'loginUrl' => '/site/login',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        
        'Messages' => [
            'class' => 'app\components\Messages',
        ],
        'MemCache' => [
            'class' => 'app\components\MemCache',
        ],
        'ImageMaker' => [
            'class' => 'app\components\ImageMaker',
        ],

        'session' => [
          'class' => 'yii\web\CacheSession',
        ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db-local.php'),
        'urlManager' => [
            'class' => 'app\components\MyUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<controller>/<action>/<id:\d+>/<title>' => '<controller>/<action>',
                '<controller>/<id:\d+>/<title>' => '<controller>/index',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
                '<controller>' => '<controller>',
                '/'=>'site/index',
                '<language:\w+>/<controller>/<action>/<id:\d+>/<title>' => '<controller>/<action>',
                '<language:\w+>/<controller>/<id:\d+>/<title>' => '<controller>/index',
                '<language:\w+>/<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<language:\w+>/<controller>/<action>' => '<controller>/<action>',
                '<language:\w+>/<controller>' => '<controller>',
                '<language:\w+>/'=>'site/index',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['admin', 'user'],
        ],
        'assetManager' => [
        'bundles' => [
            // turn off Yii2 default jquery
             'yii\web\JqueryAsset' => [
                'sourcePath' => null,
                'js' => [] 
            ],        
        ],
    ],
],    
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
