<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use app\models\SignUpForm;
use app\models\LoginForm;
use app\models\ResetPasswordForm;
use app\models\PasswordResetRequestForm;
use app\components\Alert;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$actions = ['play', 'post', 'index', 'list', 'blog', 'am', 'us'];
$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"
];

?>
<?php $this->beginPage() ?>

<!doctype html>
<html lang="en_US">
<head>

    <meta charset="UTF-8">

    <!-- jquery -->
    <script src="/js/jquery-1.11.2.js"></script>

    <!-- magnific popup -->
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <script src="/js/jquery.magnific-popup.js"></script>

    <!-- mCustomScrollBar -->
    <link rel="stylesheet" href="/css/customScrollbar.css">
    <script src="/js/jquery.customScrollbar.js"></script>

    <!-- js files -->        
    <script src="/js/jquery.easing.js"></script>        
    <script src="/js/jquery.validate.min.js"></script>    
    <script src="/js/validations_en.js"></script>
    <script src="/js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- css files -->
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">    
    <meta name='wmail-verification' content='594e8c48efa789805b6c05ee33ff84e1' />
    <meta name="author" content="TD Lab.">
    <meta property="fb:app_id" content="966242223397117">
    <meta property="og:type" content="website">
    <?php if(Yii::$app->controller->action->id == 'play') { ?>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script src="/js/crossword.js"></script>    
    <link rel="stylesheet" type="text/css" href="/css/crossword.css">
    <?php } else if(Yii::$app->controller->action->id == 'discussions') { ?>
    <link rel="stylesheet" type="text/css" href="/css/discussion.css">
    <?php } ?>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <?php if(in_array(Yii::$app->controller->action->id, $actions)) { ?>
        <?php echo $this->render( 'head', [
        'action'=> Yii::$app->controller->action->id, 
        'data' => Yii::$app->controller->socialData, 
        'current' => Url::current(),
        'baseUrl' => Url::base(true),
        'title' => $this->title] ); ?>
    <?php } else { ?>
    <?php
        echo $this->render( 'head', ['action'=> Yii::$app->controller->action->id] );
    } ?>
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <style type="text/css">
        .g-recaptcha div {
            margin: 0 auto;
        }
    </style>
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60524243-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter34105675 = new Ya.Metrika({
                    id:34105675,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34105675" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php 
Alert::init('en');
?>    
<div class="mainWrapper">   
    <div class="rightBarFb" data-url="https://www.facebook.com/crossword.am" title="facebook">
    </div>
    <div class="rightBarLb" data-url="<?php echo Url::base(true); ?>/en/site/leaderboard"  title="leaders">
    </div>
    <header>
        <div class="logoSection">
            <a href="/en/site/index" class="logoLink"  title="Home">Crossword.am</a>
            <?php if(Url::to() == '/') { ?>
            <a href="/ru/site/index" class="langIcon russian" title="Русский"></a>
            <a href="/site/index" class="langIcon armenian" title="Հայերեն"></a>
            <?php } else { ?>
            <a href="<?php echo str_replace('/en/', '/ru/', Url::to());?>" class="langIcon russian" title="Русский"></a>
            <a href="<?php echo str_replace('/en/', '/', Url::to());?>" class="langIcon armenian" title="Հայերեն"></a>
            <?php } ?>
        </div>
        <div class="navSection">
            <nav class="headerMenu">
                <li>
                    <a href="/en/site/index"  title="home">#home</a>
                </li>
                <li class="invertMenu">
                    <a href="/en/site/games"  title="games">#games</a>
                </li>
                <li>
                    <a href="/en/site/championship"  title="championship">#championship</a>
                </li>
                <li>
                    <a href="/en/site/blog"  title="blog">#blog</a>
                </li>
                <li>
                    <a href="/en/site/about"  title="about">#about</a>
                </li>
                <li>
                    |
                </li>
                <?php if(Yii::$app->user->isGuest) { ?>
                <li class="invertMenu">
                    <a href="#loginPopup" class="mgnPopupBtn"  title="login">#login</a>
                </li>
                <li>
                    <a href="/en/site/signup"  title="singup">#signup</a> 
                </li>
                <?php } else { ?>
                <li class="invertMenu">
                    <a  title="my page" href="/en/user/account">#<?php echo substr(Yii::$app->user->identity->attributes['username'], 0, 8); ?><?php echo strlen(Yii::$app->user->identity->attributes['username']) > 8 ? '...' : ''; ?></a>
                </li>
                <li  title="my logo" class="userImage">
                    <a href="/en/user/account"><img src="/images/users/<?php echo Yii::$app->user->identity->attributes['username']; ?>.png" alt=""></a>
                </li>
                <?php } ?>
            </nav>
        </div>
    </header>    
    <div class="mainSearchBox">
        <input type="text" id="mainSearchBoxItem" placeholder="logic flash crossword game japan...">
        <div class="addTop" style="width:728px; height:90px; margin: 10px auto; text-align:center">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- top -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-4896911810089666"
             data-ad-slot="9224972438"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        </div>
    </div>
    <?php echo $content; ?>

<div class="addFooter" style="width:728px; height:90px; margin: 10px auto; text-align:center">
    <!-- footer -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:728px;height:90px"
         data-ad-client="ca-pub-4896911810089666"
         data-ad-slot="5992304431"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<footer>
    <div class="image">
    </div>
    <div class="section">
        <ul>            
            <li>
                <ul>
                    <li class="heading" title="map">
                        #map _
                    </li>
                    <li>
                        <a title="home" href="/en/site/index">#home</a>
                    </li>
                    <li>
                        <a title="games" href="/en/site/games">#games</a>
                    </li>
                    <li>
                        <a title="blog" href="/en/site/blog">#blog</a>
                    </li>
                    <li>
                        <a title="contact us" href="/en/site/contact-us">#contact</a>
                    </li>
                    <li>
                        <a title="about" href="/en/site/about">#about</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="heading" title="users">
                        #users _
                    </li>
                    <li>
                        <?php if(Yii::$app->user->isGuest) { ?>
                        <a href="#loginPopup" class="mgnPopupBtn" title="login">#login</a>
                        <?php } else { ?>
                        <a href="/en/user/account" title="my page">#my page</a>
                        <?php } ?>
                    </li>
                    <li>
                        <?php if(Yii::$app->user->isGuest) { ?>
                        <a href="/en/site/signup" title="signup">#signup</a>
                        <?php } else { ?>
                        <a href="/en/site/logout" title="exit">#log out</a>
                        <?php } ?>
                    </li>
                    <li>
                        <a href="/en/site/leaderboard" title="leaders">#leaders</a>
                    </li>
                    <li>
                        <a href="/en/site/faq" title="faq">#faq</a>
                    </li>
                    <li>
                        <a href="/en/site/privacy-policy" title="privacy policy">#policy</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="heading" title="games">
                        #games _
                    </li>
                    <li>
                        <a title="crosswords" href="/en/cross-words/list">#crosswords</a>
                    </li>
                    <li>
                        <a title="sudoku" href="/en/sudoku/list">#sudoku</a>
                    </li>
                    <li>
                        <a title="quiz" href="/en/quiz/list">#quiz</a>
                    </li>
                    <li>
                        <a title="other" href="/en/site/other">#other</a>
                    </li>
                    <li>
                        <a title="help" href="/en/site/how-to-use">#help</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="heading" title="contact">
                        #contact _
                    </li>
                    <li>
                        <a title="facebook" href="https://www.facebook.com/crossword.am" target="_blank">#facebook</a>
                    </li>
                    <li>
                        <a title="discussions" href="/en/site/discussions">#discussions</a>
                    </li>
                    <li>
                        <a title="championship" href="/en/site/championship">#championship</a>
                    </li>
                    <li>
                        <span title="email" class="footerInfo">info@crossword.am</span>
                    </li>
                    <li>
                        <span title="address" class="footerInfo">RA, Yerevan</span>
                    </li>
                </ul>
            </li>
        </ul>   
    </div>
</footer>
<div id="loginPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#login</h5>
    </div>
    <div class="popupCont">
        <?php $login = new LoginForm(); ?>
        <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'action' => '/en/site/login',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
                    ]); ?>
        <div class="formRow">
            <label for="loginform-username">#username</label>
            <?php echo $form->field($login, 'username')->textInput(); ?>
        </div>
        <div class="formRow">
            <label for="loginform-password">#password</label>
            <?php echo $form->field($login, 'password')->passwordInput(); ?>
        </div>
        <div class="formRow keepMeSigned">
            <?php echo $form->field($login, 'rememberMe')->checkbox(['template' => '{input}']); ?>            
            <label for="loginform-rememberme">remember me!</label>
        </div>
        <div class="formRow loginButtons">
            <input type="hidden" value="/en<?php $url = explode('?', Url::current()); echo $url[0]; ?>" name="redirect">
            <input type="submit" value="login" class="borderBtn orangeBorderBtn"> 
            <!-- #or
            <a href="#" class="borderBtn facebookBorderBtn">facebook</a> -->
        </div>
        <?php ActiveForm::end(); ?>
        <div class="formRow">
            <a href="#forgotPopup" class="fullWidthBtn grayBtn mgnPopupBtn">forgot password</a>          
        </div>
        <div class="formRow">
            <a href="/en/site/signup" class="fullWidthBtn orangeBtn">register</a>
        </div>      
    </div>
</div>
<div id="forgotPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#forgot password</h5>
    </div>
    <div class="popupCont">
        <?php $password_request = new PasswordResetRequestForm();?>
        <?php $form = ActiveForm::begin([
            'id' => 'request-password-reset-form',
            'action' => '/en/site/request-password-reset',
            'method' => 'post']); ?>
        <div class="formRow">
            <label for="passwordresetrequestform-email">#email</label>
            <?php echo $form->field($password_request, 'email')->label(false); ?>
        </div>
        <div class="formRow ">
            <input type="submit" value="request" class="fullWidthBtn orangeFullWidthBtn">
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div id="smileys" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#smileys</h5>
    </div>
    <div class="popupCont">
        <ul class="customScroll">
        <?php foreach($smileys as $smiley) { ?>
            <li style="margin-top: 2px;">
                (<?php echo $smiley; ?>) - <img src="<?php echo Url::base(true); ?>/images/smileys/smiley-<?php echo $smiley;?>.gif">
                <button data-smiley="(<?php echo $smiley; ?>)" class="addSmiley standartBtn orangeBtn">add</button>
            </li>
        <?php } ?>
        </ul>
    </div>
</div>
<div id="reportAboutProblem" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#get a problem</h5>
    </div>
    <div class="popupCont">
        <?php $form = ActiveForm::begin([
        'id' => 'problem-form',
        'action' => '/en/site/problem',
        'fieldConfig' => [
            'template' => '{input}'
            ],
        ]); ?>
        <div class="formRow">
            <label for="problem_email">#email</label>
            <?php echo Html::textInput('email', '', ['id' => 'problem_email']); ?>
        </div>
        <div class="formRow">
            <label for="problem_subject">#subject</label>
            <?php echo Html::textInput('subject', '', ['id' => 'problem_subject']); ?>
        </div>
        <div class="formRow">
            <label for="problem_message">#message</label>
            <?php echo Html::textArea('message', '', ['id' => 'problem_message']); ?>
        </div>
        <div class="formRow" style="text-align:center!important;">
            <div class="g-recaptcha" data-sitekey="6LcZzg8TAAAAAA7FvgA2v9Qv4LKi8WAUB-UueYq3"></div>
        </div>
        <div class="formRow">
            <input type="hidden" name="redirect" value="<?php echo Url::current(); ?>">
            <input type="submit" value="send" class="borderBtn orangeBorderBtn">
        </div>      
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#request-password-reset-form').on('sumbit', function()
        {
            var submit = true;
            if($('#forgotPopup .help-block-error').val() == '' &&  $('#passwordresetrequestform-email').val() != '')
            {

            }
            return false;
        });

        $('.rightBarLb, .rightBarFb').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.mainQuiz, .mainCross, .mainSudoku, .mainFb, .mainDisc, .mainBlog, .mainGamesBox, .mainLeaders, .mainCity, #myCanvas').on('click', function(){
            if($(this).hasClass('mainFb') || $(this).attr('id') == 'myCanvas') {
                window.open($(this).attr('data-url'));
            } else {
                window.location = $(this).attr('data-url');
            }
        });

        $('#mainSearchBoxItem').on('keypress', function(e){
            var code = (e.keyCode ? e.keyCode : e.which);
            var key = String.fromCharCode(code);
            var value = $(this).val();
            value = value.trim();            
            if(code == 13 && value != '') {
                window.location = '/en/site/search?id='+value;
            }
        });
    });
</script>
</body>
</html>

    

<?php $this->endPage() ?>
