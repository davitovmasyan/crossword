<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use app\models\SignUpForm;
use app\models\LoginForm;
use app\models\ResetPasswordForm;
use app\models\PasswordResetRequestForm;
use app\components\Alert;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$actions = ['play', 'post', 'index', 'list', 'blog', 'am', 'us'];
$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"
];

?>
<?php $this->beginPage() ?>

<!doctype html>
<html lang="ru_RU">
<head>

    <meta charset="UTF-8">

    <!-- jquery -->
    <script src="/js/jquery-1.11.2.js"></script>

    <!-- magnific popup -->
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <script src="/js/jquery.magnific-popup.js"></script>

    <!-- mCustomScrollBar -->
    <link rel="stylesheet" href="/css/customScrollbar.css">
    <script src="/js/jquery.customScrollbar.js"></script>

    <!-- js files -->        
    <script src="/js/jquery.easing.js"></script>        
    <script src="/js/jquery.validate.min.js"></script>    
    <script src="/js/validations_ru.js"></script>	 
    <script src="/js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- css files -->
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">    
    <meta name='wmail-verification' content='594e8c48efa789805b6c05ee33ff84e1' />
    <meta name="author" content="TD Lab.">
    <meta property="fb:app_id" content="966242223397117">
    <meta property="og:type" content="website">
    <?php if(Yii::$app->controller->action->id == 'play') { ?>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script src="/js/crossword.js"></script>    
    <link rel="stylesheet" type="text/css" href="/css/crossword.css">
    <?php } else if(Yii::$app->controller->action->id == 'discussions') { ?>
    <link rel="stylesheet" type="text/css" href="/css/discussion.css">
    <?php } ?>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <?php if(in_array(Yii::$app->controller->action->id, $actions)) { ?>
        <?php echo $this->render( 'head', [
        'action'=> Yii::$app->controller->action->id, 
        'data' => Yii::$app->controller->socialData, 
        'current' => Url::current(),
        'baseUrl' => Url::base(true),
        'title' => $this->title] ); ?>
    <?php } else { ?>
    <?php
        echo $this->render( 'head', ['action'=> Yii::$app->controller->action->id] );
    } ?>
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <style type="text/css">
        .g-recaptcha div {
            margin: 0 auto;
        }
    </style>
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60524243-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter34105675 = new Ya.Metrika({
                    id:34105675,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34105675" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php 
Alert::init('ru');
?>    
<div class="mainWrapper">   
    <div class="rightBarFb" data-url="https://www.facebook.com/crossword.am" title="фейсбук">
    </div>
    <div class="rightBarLb" data-url="<?php echo Url::base(true); ?>/ru/site/leaderboard"  title="лидеры">
    </div>
    <header>
        <div class="logoSection">
            <a href="/ru/site/index" class="logoLink"  title="главная">Crossword.am</a>
            <?php if(Url::to() == '/') { ?>
            <a href="/en/site/index" class="langIcon english" title="English"></a>
            <a href="/site/index" class="langIcon armenian" title="Հայերեն"></a>
            <?php } else { ?>
            <a href="<?php echo str_replace('/ru/', '/en/', Url::to());?>" class="langIcon english" title="English"></a>
            <a href="<?php echo str_replace('/ru/', '/', Url::to());?>" class="langIcon armenian" title="Հայերեն"></a>
            <?php } ?>            
        </div>
        <div class="navSection">
            <nav class="headerMenu">
                <li>
                    <a href="/ru/site/index"  title="главная">#главная</a>
                </li>
                <li class="invertMenu">
                    <a href="/ru/site/games"  title="игры">#игры</a>
                </li>
                <li>
                    <a href="/ru/site/championship"  title="соревнования">#соревнования</a>
                </li>
                <li>
                    <a href="/ru/site/blog"  title="блог">#блог</a>
                </li>
                <li>
                    <a href="/ru/site/about"  title="о нас">#о нас</a>
                </li>
                <li>
                    |
                </li>
                <?php if(Yii::$app->user->isGuest) { ?>
                <li class="invertMenu">
                    <a href="#loginPopup" class="mgnPopupBtn"  title="вход">#вход</a>
                </li>
                <li>
                    <a href="/ru/site/signup"  title="регистрация">#регистрация</a>
                </li>
                <?php } else { ?>
                <li class="invertMenu">
                    <a  title="моя страница" href="/ru/user/account">#<?php echo substr(Yii::$app->user->identity->attributes['username'], 0, 8); ?><?php echo strlen(Yii::$app->user->identity->attributes['username']) > 8 ? '...' : ''; ?></a>
                </li>
                <li  title="мое лого" class="userImage">
                    <a href="/ru/user/account"><img src="/images/users/<?php echo Yii::$app->user->identity->attributes['username']; ?>.png" alt=""></a>
                </li>
                <?php } ?>
            </nav>
        </div>
    </header>    
    <div class="mainSearchBox">
        <input type="text" id="mainSearchBoxItem" placeholder="задача флеш кроссворд игра японский...">
        <div class="addTop" style="width:728px; height:90px; margin: 10px auto; text-align:center">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- top -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-4896911810089666"
             data-ad-slot="9224972438"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        </div>
    </div>
    <?php echo $content; ?>
<div class="addFooter" style="width:728px; height:90px; margin: 10px auto; text-align:center">
    <!-- footer -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:728px;height:90px"
         data-ad-client="ca-pub-4896911810089666"
         data-ad-slot="5992304431"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<footer>
    <div class="image">
    </div>
    <div class="section">
        <ul>            
            <li>
                <ul>
                    <li class="heading" title="карта">
                        #карта _
                    </li>
                    <li>
                        <a title="главная" href="/ru/site/index">#главная</a>
                    </li>
                    <li>
                        <a title="игры" href="/ru/site/games">#игры</a>
                    </li>
                    <li>
                        <a title="блог" href="/ru/site/blog">#блог</a>
                    </li>
                    <li>
                        <a title="контакт" href="/ru/site/contact-us">#контакт</a>
                    </li>
                    <li>
                        <a title="о нас" href="/ru/site/about">#о нас</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="heading" title="игроки">
                        #игроки _
                    </li>
                    <li>
                        <?php if(Yii::$app->user->isGuest) { ?>
                        <a href="#loginPopup" class="mgnPopupBtn" title="вход">#вход</a>
                        <?php } else { ?>
                        <a href="/ru/user/account" title="моя страница">#моя страница</a>
                        <?php } ?>
                    </li>
                    <li>
                        <?php if(Yii::$app->user->isGuest) { ?>
                        <a href="/ru/site/signup" title="регистрация">#регистрация</a>
                        <?php } else { ?>
                        <a href="/ru/site/logout" title="выход">#выйти</a>
                        <?php } ?>
                    </li>
                    <li>
                        <a href="/ru/site/leaderboard" title="лидеры">#лидеры</a>
                    </li>
                    <li>
                        <a href="/ru/site/faq" title="чзв">#чзв</a>
                    </li>
                    <li>
                        <a href="/ru/site/privacy-policy" title="правила">#правила</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="heading" title="игры">
                        #игры _
                    </li>
                    <li>
                        <a title="кроссворды" href="/ru/cross-words/list">#кроссворды</a>
                    </li>
                    <li>
                        <a title="судоку" href="/ru/sudoku/list">#судоку</a>
                    </li>
                    <li>
                        <a title="викторина" href="/ru/quiz/list">#викторина</a>
                    </li>
                    <li>
                        <a title="другое" href="/ru/site/other">#другое</a>
                    </li>
                    <li>
                        <a title="помощь" href="/ru/site/how-to-use">#помощь</a>
                    </li>
                </ul>
            </li>
            <li>
                <ul>
                    <li class="heading" title="контакт">
                        #контакт _
                    </li>
                    <li>
                        <a title="фейсбук" href="https://www.facebook.com/crossword.am" target="_blank">#фейсбук</a>
                    </li>
                    <li>
                        <a title="обсуждения" href="/ru/site/discussions">#обсуждения</a>
                    </li>
                    <li>
                        <a title="соревнования" href="/ru/site/championship">#соревнования</a>
                    </li>
                    <li>
                        <span title="почта" class="footerInfo">info@crossword.am</span>
                    </li>
                    <li>
                        <span title="адрес" class="footerInfo">РА, г. Ереван</span>
                    </li>
                </ul>
            </li>
        </ul>   
    </div>
</footer>
<div id="loginPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#вход</h5>
    </div>
    <div class="popupCont">
        <?php $login = new LoginForm(); ?>
        <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'action' => '/ru/site/login',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
                    ]); ?>
        <div class="formRow">
            <label for="loginform-username">#логин</label>
            <?php echo $form->field($login, 'username')->textInput(); ?>
        </div>
        <div class="formRow">
            <label for="loginform-password">#пароль</label>
            <?php echo $form->field($login, 'password')->passwordInput(); ?>
        </div>
        <div class="formRow keepMeSigned">
            <?php echo $form->field($login, 'rememberMe')->checkbox(['template' => '{input}']); ?>            
            <label for="loginform-rememberme">помни меня!</label>
        </div>
        <div class="formRow loginButtons">
            <input type="hidden" value="/ru<?php $url = explode('?', Url::current()); echo $url[0]; ?>" name="redirect">
            <input type="submit" value="вход" class="borderBtn orangeBorderBtn"> 
            <!-- #или
            <a href="#" class="borderBtn facebookBorderBtn">facebook</a> -->
        </div>
        <?php ActiveForm::end(); ?>
        <div class="formRow">
            <a href="#forgotPopup" class="fullWidthBtn grayBtn mgnPopupBtn">не помню пароль</a>
        </div>
        <div class="formRow">
            <a href="/ru/site/signup" class="fullWidthBtn orangeBtn">регистрация</a>
        </div>      
    </div>
</div>
<div id="forgotPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#восстановить пароль</h5>
    </div>
    <div class="popupCont">
        <?php $password_request = new PasswordResetRequestForm();?>
        <?php $form = ActiveForm::begin([
            'id' => 'request-password-reset-form',
            'action' => '/ru/site/request-password-reset',
            'method' => 'post']); ?>
        <div class="formRow">
            <label for="passwordresetrequestform-email">#почта</label>
            <?php echo $form->field($password_request, 'email')->label(false); ?>
        </div>
        <div class="formRow ">
            <input type="submit" value="восстановить" class="fullWidthBtn orangeFullWidthBtn">
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div id="smileys" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#смайлики</h5>
    </div>
    <div class="popupCont">
        <ul class="customScroll">
        <?php foreach($smileys as $smiley) { ?>
            <li style="margin-top: 2px;">
                (<?php echo $smiley; ?>) - <img src="<?php echo Url::base(true); ?>/images/smileys/smiley-<?php echo $smiley;?>.gif">
                <button data-smiley="(<?php echo $smiley; ?>)" class="addSmiley standartBtn orangeBtn">вставить</button>
            </li>
        <?php } ?>
        </ul>
    </div>
</div>
<div id="reportAboutProblem" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#сообшить и проблеме</h5>
    </div>
    <div class="popupCont">
        <?php $form = ActiveForm::begin([
        'id' => 'problem-form',
        'action' => '/ru/site/problem',
        'fieldConfig' => [
            'template' => '{input}'
            ],
        ]); ?>
        <div class="formRow">
            <label for="problem_email">#почта</label>
            <?php echo Html::textInput('email', '', ['id' => 'problem_email']); ?>
        </div>
        <div class="formRow">
            <label for="problem_subject">#тема</label>
            <?php echo Html::textInput('subject', '', ['id' => 'problem_subject']); ?>
        </div>
        <div class="formRow">
            <label for="problem_message">#текст</label>
            <?php echo Html::textArea('message', '', ['id' => 'problem_message']); ?>
        </div>
        <div class="formRow" style="text-align:center!important;">
            <div class="g-recaptcha" data-sitekey="6LcZzg8TAAAAAA7FvgA2v9Qv4LKi8WAUB-UueYq3"></div>
        </div>
        <div class="formRow">
            <input type="hidden" name="redirect" value="<?php echo Url::current(); ?>">
            <input type="submit" value="отправить" class="borderBtn orangeBorderBtn">
        </div>      
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#request-password-reset-form').on('sumbit', function()
        {
            var submit = true;
            if($('#forgotPopup .help-block-error').val() == '' &&  $('#passwordresetrequestform-email').val() != '')
            {

            }
            return false;
        });

        $('.rightBarLb, .rightBarFb').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.mainQuiz, .mainCross, .mainSudoku, .mainFb, .mainDisc, .mainBlog, .mainGamesBox, .mainLeaders, .mainCity, #myCanvas').on('click', function(){
            if($(this).hasClass('mainFb') || $(this).attr('id') == 'myCanvas') {
                window.open($(this).attr('data-url'));
            } else {
                window.location = $(this).attr('data-url');
            }
        });

        $('#mainSearchBoxItem').on('keypress', function(e){
            var code = (e.keyCode ? e.keyCode : e.which);
            var key = String.fromCharCode(code);
            var value = $(this).val();
            value = value.trim();            
            if(code == 13 && value != '') {
                window.location = '/ru/site/search?id='+value;
            }
        });
    });
</script>
</body>
</html>

    

<?php $this->endPage() ?>
