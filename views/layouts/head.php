<?php
$keywords = [
	'faq' => 'crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական, faq, հտհ, հաճախակի տրվող հարցեր',
	'about' => 'crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական, խաչբառի մասին, օնլայն խաչբառի մասին, about crossword',
	'contact-us' => 'կապ, կապ մեզ հետ, հարցեր, օգնություն, info@crossword.am, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
	'privacy-policy' => 'կանոններ, pirvacy, policy, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
	'discussions' => 'մեկնաբանություններ, քննարկումներ, առաջարկներ, հարցեր, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
	'leaderboard' => 'առաջատարներ, քարտեզ, լիդերներ, թոփ, top, leaders, leaderboard, map-leaders, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
	'how-to-use' => 'օգնություն, ինչպես օգտվել, help, support, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
	'sign-up' => 'signup, registration, գրանցվել, արագ գրանցում, գրանցում, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
	'games' => 'games, խաղեր, տեսակներ, crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական',
];

if(isset($keywords[$action])) {
	echo '<meta name="keywords" content="'.$keywords[$action].'"></meta>';
}

if(isset($data)) { ?>
    <meta property="og:url" content="<?php echo $baseUrl.$current; ?>" /> 
    <meta property="og:title" content="<?php echo $title; ?>" /> 
    <meta property="og:description" content="<?php echo $data['description']; ?>" />
    <meta property="og:image" content="<?php echo $baseUrl . $data['image']; ?>" />
    <meta name="twitter:card" content="photo" />
    <meta name="twitter:site" content="@crossword" />
    <meta name="twitter:title" content="<?php echo $title; ?>" />
    <meta name="twitter:image" content="<?php echo $baseUrl . $data['image']; ?>" />
    <meta name="twitter:url" content="<?php echo $baseUrl.$current; ?>" />
    <?php if(isset($data['keywords'])) { ?>
    <meta name="keywords" content="<?php echo $data['keywords'];?>"></meta>
    <?php } ?>
<?php } ?>