<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use app\components\Alert;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>    
    <meta charset="UTF-8">

    <!-- jQuery -->
    <script src="/js/jquery-1.11.2.js"></script>        
    
    <!-- validations js file -->
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/adminValidations.js"></script>

    <!-- bootstrap js file -->
    <script src="/js/bootstrap.min.js"></script>

    <!-- magnific-popup js file -->
    <script src="/js/jquery.magnific-popup.js"></script>

    <!-- customScrollbar js file -->
    <script src="/js/jquery.customScrollbar.js"></script>
    
    <!-- main js file -->
    <script src="/js/jquery.easing.js"></script>        
    <script src="/js/admin.js"></script>

    <!-- main css file -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/customScrollbar.css">
    <link rel="stylesheet" href="/css/admin.css"> 
    <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>    
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    
</head>
<body>
    <?php 
    Alert::init('hy'); 
    ?>
    <div class="maincontainer">
            <nav class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                    <div class="navbar-header">                        
                        <a class="navbar-brand" href="/site/index">Crossword.am</a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse navbar-right">
                        <?php if(!Yii::$app->user->isGuest) { ?>
                        <ul class="nav navbar-nav">
                            <li <?php echo Yii::$app->requestedAction->id == 'index' ? 'class="active"' : ''; ?>><a href="/admin/index">Home</a></li>
                            <li <?php echo Yii::$app->requestedAction->id == 'discussions' ? 'class="active"' : ''; ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Discussions <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="/admin/discussions">Discussions</a></li>
                                    <li><a href="/admin/add-discussion">Add Discussion</a></li>
                                    <li><a href="/admin/all-discussions">All Discussions</a></li>
                                  </ul>
                            </li>
                            <li <?php echo Yii::$app->requestedAction->id == 'comments' ? 'class="active"' : ''; ?>><a href="/admin/comments">Comments</a></li>
                            <li <?php echo Yii::$app->requestedAction->id == 'contact' ? 'class="active"' : ''; ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="/admin/championship">Championship</a></li>
                                    <li><a href="/admin/images">Images</a></li>
                                    <li><a href="/admin/contact">Contact</a></li>
                                    <li><a href="/admin/blog">Blog</a></li>
                                    <li><a href="/admin/add-post">Add Post</a></li>                                    
                                  </ul>
                            </li>
                            <li <?php echo Yii::$app->requestedAction->id == 'users' ? 'class="active"' : ''; ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="/admin/users">Users</a></li>
                                    <li><a href="/admin/pluses">Pluses</a></li>
                                    <li><a href="/admin/notifications">Notifications</a></li>
                                  </ul>
                              </li>
                            </li>
                            <li <?php echo Yii::$app->requestedAction->id == 'crosswords' ? 'class="active"' : ''; ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Crosswords <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="/admin/crosswords">Crosswords</a></li>
                                    <li><a href="/admin/add-crossword">Add Crosswords</a></li>
                                    <li><a href="/admin/saved-crosswords">Saved Crosswords</a></li>
                                  </ul>
                              </li>
                            <li <?php echo Yii::$app->requestedAction->id == 'words' ? 'class="active"' : ''; ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Words <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="/admin/words">Words</a></li>
                                    <li><a href="/admin/add-word">Add Word</a></li>
                                    <li><a href="/admin/saved-words">Saved Words</a></li>
                                    <li><a href="/admin/categories">Categories</a></li>
                                  </ul>
                              </li>
                            <li <?php echo Yii::$app->requestedAction->id == 'admins' ? 'class="active"' : ''; ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admins <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="/admin/admins">Admins</a></li>
                                    <li><a href="/admin/add-admin">Add Admin</a></li>
                                  </ul>
                            </li>
                            <li><a href="/admin/logout">Logout</a></li>
                        </ul>
                        <?php } ?>
                    </div>
                </div>
            </nav>  
        
    <?php echo $content; ?>
        
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', 'a, button', function(){
            if($(this).text() == 'Delete' || $(this).val() == 'Delete') {
                return confirm('Are you sure?');
            }
        })

        $(document).on('click', 'button.scrapBtn', function(){
            var url = $(this).attr('data-url');
            $.post(
                'https://graph.facebook.com',
                {
                    id: url,
                    scrape: true
                },
                function(response){
                    console.log(response);
                }
            );
        })
    });
</script>
</body>
</html>
<?php $this->endPage() ?>