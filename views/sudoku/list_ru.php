<?php

use yii\widgets\LinkPager;
$this->title="Crossword.am | Судоку";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#судоку</h1>
    <div class="box">
        <h1 class="commingSoon">#скоро</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_ru')); ?>
    <div class="box">
        <div class="mainBlog" data-url="/ru/site/blog">
            <h1>блог</h1>
            <p>интересные публикации</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>