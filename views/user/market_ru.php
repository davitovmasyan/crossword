<?php

use yii\widgets\LinkPager;
$this->title="Crossword.am | Магазин";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#магазин</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <h1 class="commingSoon">#скоро</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_ru')); ?>   
</div>
<div class="box">
    <div class="mainLeaders" data-url="/ru/site/leaderboard">
        <h1>лидеры</h1>
        <p>получайте очки и поднимайтесь в топе</p>
    </div>
</div>
    