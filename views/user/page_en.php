<?php
$this->title="Crossword.am | My page";
$months = [
	'01' => 'january',
	'02' => 'february',
	'03' => 'march',
	'04' => 'april',
	'05' => 'may',
	'06' => 'june',
	'07' => 'jule',
	'08' => 'august',
	'09' => 'september',
	'10' => 'october',
	'11' => 'november',
	'12' => 'december',	
];
?>

<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#my page</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_en'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <ul class="myPage">
            <li>
                <img src="/images/users/<?php echo $user['username']; ?>.png" alt="">
            </li>
            <li class="username">
                <?php echo $user['username']; ?>
            </li>
            <li class="points">
                <?php echo $user['points']; ?> points
            </li>
        </ul>
        <ul class="myPage2">
            <li class="pluses">
                <h3 class="plusesHeading">#pluses <button class="plusesNotButton">+<?php echo $user['pluses']; ?></button></h3>
                <?php if(!empty($data['pluses'])) { ?>
                <ul class="plusesList customScroll">
                	<?php foreach($data['pluses'] as $plus) { ?>
                    <li>
                        <ul class="plusSection">
                        	<li><img src="/images/users/<?php echo $plus['username'];?>.png"</li>
                        	<li>#<?php echo $plus['username'];?></li>
                        	<li><?php echo substr($plus['created'], 8, 2),' ',$months[substr($plus['created'], 5, 2)],' ',substr($plus['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
            <li class="medals">
                <h3 class="medalsHeading">#medals</h3>
                <?php if(!empty($data['medals'])) { ?>
                <ul class="medalsList customScroll">
                	<?php foreach($data['medals'] as $medal) { ?>
                    <li>
                        <ul class="medalSection">
                        	<li><img src="/images/users/<?php echo $medal['username'];?>.png"</li>
                        	<li>#<?php echo $medal['username'];?></li>
                        	<li><?php echo substr($medal['created'], 8, 2),' ',$months[substr($medal['created'], 5, 2)],' ',substr($medal['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
        </ul>
    </div>
</div>
<div class="box">
     <div class="mainLeaders" data-url="/en/site/leaderboard">
        <h1>leaderboard</h1>
        <p>get points and be on top</p>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>