<?php

use yii\helpers\Url;
$this->title="Crossword.am | Оповещания";
$months = [
    '01' => 'январь',
    '02' => 'февраль',
    '03' => 'март',
    '04' => 'апрель',
    '05' => 'май',
    '06' => 'июнь',
    '07' => 'июль',
    '08' => 'август',
    '09' => 'сентябрь',
    '10' => 'октябрь',
    '11' => 'ноябрь',
    '12' => 'декабрь',  
];
?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#оповещания</h1>
	<?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>
	<div class="box">    
        <?php if(!empty($notifications)) { ?>
        <ul class="myNotifications" style="padding: 10px; max-height: 600px;">
            <?php foreach ($notifications as $n) { ?>                
            <li class="singleNotification <?php echo $n['status'] == 'unreaded' ? 'unread' : '';?>" data-url="<?php echo $n['url'];?>">
                <span style="display:inline; font-family: TopModern; color:#222;">
                    <?php echo substr($n['created'], 10, 6),', ',substr($n['created'], 8, 2),' ',$months[substr($n['created'], 5, 2)],' ',substr($n['created'], 0, 4);?>
                </span>
                <span style="display:inline; color:#f7931e; font-size: 18px;">
                    (<?php echo $n['notification'];?>)
                </span>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <h4 class="noInfSt">оповещаний нет</h4>
        <?php } ?>
    </div>
</div>
<div class="box">
    <div class="mainBlog" data-url="/ru/site/blog">
        <h1>блог</h1>
        <p>все самое интересное</p>
    </div>
</div>
<div class="box">
    <div class="mainDisc" data-url="/ru/site/discussions">
        <h1>обсуждения</h1>
        <p>участвуйте в обсуждениях</p>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $(".myNotifications").mCustomScrollbar({
            scrollbarPosition: 'outside',
            scrollInertia: 1000,
            autoHideScrollbar: true
        });        

        $('.singleNotification').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
	});
</script>