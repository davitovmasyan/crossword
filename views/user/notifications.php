<?php

use yii\helpers\Url;
$this->title="Crossword.am | Ծանուցումներ";
$months = [
    '01' => 'հունվար',
    '02' => 'փետրվար',
    '03' => 'մարտ',
    '04' => 'ապրիլ',
    '05' => 'մայիս',
    '06' => 'հունիս',
    '07' => 'հուլիս',
    '08' => 'օգոստոս',
    '09' => 'սեպտեմբեր',
    '10' => 'հոկտեմբեր',
    '11' => 'նոյեմբեր',
    '12' => 'դեկտեմբեր',    
];
?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#ծանուցումներ</h1>
	<?php echo $this->renderFile($this->findViewFile('userMenuTabs'), ['action' => Yii::$app->controller->action->id]); ?>
	<div class="box">    
        <?php if(!empty($notifications)) { ?>
        <ul class="myNotifications" style="padding: 10px; max-height: 600px;">
            <?php foreach ($notifications as $n) { ?>                
            <li class="singleNotification <?php echo $n['status'] == 'unreaded' ? 'unread' : '';?>" data-url="<?php echo $n['url'];?>">
                <span style="display:inline; font-family: TopModern; color:#222;">
                    <?php echo substr($n['created'], 10, 6),', ',substr($n['created'], 8, 2),' ',$months[substr($n['created'], 5, 2)],' ',substr($n['created'], 0, 4);?>
                </span>
                <span style="display:inline; color:#f7931e; font-size: 18px;">
                    (<?php echo $n['notification'];?>)
                </span>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <h4 class="noInfSt">ծանուցումներ չկան</h4>
        <?php } ?>
    </div>
</div>
<div class="box">
    <div class="mainBlog" data-url="/site/blog">
        <h1>բլոգ</h1>
        <p>հետաքրքիր տեղեկատվություն</p>
    </div>
</div>
<div class="box">
    <div class="mainDisc" data-url="/site/discussions">
        <h1>քննարկումներ</h1>
        <p>մասնակցեք մեր քննարկումներին</p>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $(".myNotifications").mCustomScrollbar({
            scrollbarPosition: 'outside',
            scrollInertia: 1000,
            autoHideScrollbar: true
        });        

        $('.singleNotification').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
	});
</script>