<!-- put it in cointainer div -->
<div class="boxUserTabs">
	<ul>
		<li>
			<a title="իմ տվյալները" href="/user/account" class="<?php echo $action == 'account' ? 'active' : ''; ?>">#իմ տվյալները</a>
		</li>
		<li>
			<a title="իմ էջը" href="/user/page" class="<?php echo $action == 'page' ? 'active' : ''; ?>">#իմ էջը</a>
		</li>
		<li>
			<a title="իմ խաղերը" href="/user/games" class="<?php echo $action == 'games' ? 'active' : ''; ?>">#իմ խաղերը</a>
		</li>
		<li>
			<a title="ծանուցումներ" href="/user/notifications" class="<?php echo $action == 'notifications' ? 'active' : ''; ?>">#ծանուցումներ</a>
		</li>
		<li>
			<a title="խանութ" href="/user/market" class="<?php echo $action == 'market' ? 'active' : ''; ?>">#խանութ</a>
		</li>
		<li>
			<a title="ելք" href="/site/logout">#ելք</a>
		</li>
	</ul>
</div>