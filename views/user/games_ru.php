<?php
$this->title="Crossword.am | Мои игры";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#мои игры</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <ul class="userGames">
            <li>
                <a href="/ru/user/crosswords" title="мои кроссворды" style="background-color: #F7931E;">мои кроссворды</a>
            </li>
            <li>
                <a href="/ru/user/words" title="мои викторины" style="background-color: #FF5454;">мои викторины</a>
            </li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="mainLeaders" data-url="/ru/site/leaderboard">
        <h1>лидеры</h1>
        <p>получайте очки и поднимайтесь в топе</p>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>