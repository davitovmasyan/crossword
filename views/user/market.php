<?php

use yii\widgets\LinkPager;
$this->title="Crossword.am | Խանութ";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#խանութ</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <h1 class="commingSoon">#շուտով</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
</div>
<div class="box">
        <div class="mainLeaders" data-url="/site/leaderboard">
            <h1>առաջատարներ</h1>
            <p>կուտակեք միավորներ և լրացրեք առաջատարների շարքերը</p>
        </div>
    </div>
    