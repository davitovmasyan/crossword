<!-- put it in cointainer div -->
<div class="boxUserTabs">
	<ul>
		<li>
			<a title="my account" href="/en/user/account" class="<?php echo $action == 'account' ? 'active' : ''; ?>">#my account</a>
		</li>
		<li>
			<a title="my page" href="/en/user/page" class="<?php echo $action == 'page' ? 'active' : ''; ?>">#my page</a>
		</li>
		<li>
			<a title="my games" href="/en/user/games" class="<?php echo $action == 'games' ? 'active' : ''; ?>">#my games</a>
		</li>
		<li>
			<a title="notifications" href="/en/user/notifications" class="<?php echo $action == 'notifications' ? 'active' : ''; ?>">#notifications</a>
		</li>
		<li>
			<a title="market" href="/en/user/market" class="<?php echo $action == 'market' ? 'active' : ''; ?>">#market</a>
		</li>
		<li>
			<a title="exit" href="/en/site/logout">#exit</a>
		</li>
	</ul>
</div>