<?php

use yii\helpers\Url;
$this->title="Crossword.am | My crosswords";

?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#my crosswords</h1>
	<?php echo $this->renderFile($this->findViewFile('userMenuTabs_en'), ['action' => Yii::$app->controller->action->id]); ?>
	<div class="box">    
        <?php if(!empty($crosswords)) { ?>
        <div class="listNames">
            <ul>
                <li style="width: 6%; text-align:center;">#number</li>
                <li style="width: 20%; text-align:center;">#image</li>
                <li style="width: 21%; text-align:center;">#who solve</li>
                <li style="width: 16%; text-align:center;">#name</li>
                <li style="width: 15%; text-align:center;">#status</li>
                <li style="width: 12%; text-align:center;">#button</li>
            </ul>
        </div>        
        <ul class="myCrosswordsList">                
            <?php foreach ($crosswords as $crossword) { ?>
            <li class="singleCrossword <?php echo $crossword['status'] == 'all' ? 'completed' : '';?>">
                <ul>
                    <li style="width: 6%; text-align:center;">#<?php echo $crossword['public_number'];?></li>
                    <li style="width: 20%; text-align:center;"><a class="img" href="/images/crosswords/<?php echo $crossword['image'];?>"><img src="/images/crosswords/<?php echo $crossword['image'];?>" alt=""  style="top:10px;"></a></li>
                    <li style="width: 21%; text-align:center;">
                        <a href="#whoSolvedList" class="mgnPopupBtn whoSolvedCrossword" data-id="<?php echo $crossword['cid'];?>">#show</a>
                    </li>
                    <li style="width: 16%; text-align:center;">#<?php echo $crossword['crossword_name'];?></li>
                    <li style="width: 15%; text-align:center;"><?php echo $crossword['status'] == 'all' ? '100%' : '#part';?></li>
                    <li style="width: 12%; text-align:center;">
                        <?php if($crossword['status'] == 'all') { ?>
                        <button class="crosswordViewBtn" data-url="<?php echo Url::base(true).'/en/cross-words/play?id='.$crossword['cid'];?>">#view</button>
                        <?php } else { ?>
                        <button class="crosswordContinueBtn" data-url="<?php echo Url::base(true).'/en/cross-words/play?id='.$crossword['cid'];?>">#play</button></li>
                        <?php } ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
    <?php } else { ?>
    <h4 class="noInfSt">no crosswords</h4>
    <?php } ?>
</div>
<div class="box">
    <div class="mainLeaders" data-url="/en/site/leaderboard">
        <h1>leaders</h1>
        <p>get points and be on top</p>
    </div>
</div>
<div id="whoSolvedList" class="mfp-hide mgnPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#who solve this crossword</h5>
    </div>
    <div class="popupCont">
        <div class="whoSolvedListScrollableDiv">
            <ul id="whoSolvedListScrollable" data-language="en">
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){        

		$(".myCrosswordsList").mCustomScrollbar({
            scrollbarPosition: 'outside',
            scrollInertia: 1000,
            autoHideScrollbar: true
        });

        $('.myCrosswordsList').magnificPopup({
            delegate: 'a.img',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element.find('img');
                }
            }
        });

        $('.crosswordContinueBtn, .crosswordViewBtn').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
	});
</script>