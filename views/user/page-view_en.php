<?php
use yii\bootstrap\ActiveForm;
use app\models\Pluses;
$this->title="Crossword.am | Player";
$months = [
    '01' => 'january',
    '02' => 'february',
    '03' => 'march',
    '04' => 'april',
    '05' => 'may',
    '06' => 'june',
    '07' => 'jule',
    '08' => 'august',
    '09' => 'september',
    '10' => 'october',
    '11' => 'november',
    '12' => 'december', 
];
?>

<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#player</h1>
    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->id == $user['id']) { ?>
    <h3 class="noteInfo">other players see your profile...</h3>
    <?php } ?>
    <div class="box">
        <ul class="myPage">
            <li>
                <img src="/images/users/<?php echo $user['username']; ?>.png" alt="">
            </li>
            <li class="username">
                <?php echo $user['username']; ?>
            </li>
            <li class="points">
                <?php echo $user['points']; ?> points
            </li>
        </ul>
        <ul class="myPage2">
            <li class="pluses">
                <h3 class="plusesHeading">#pluses <button>+<?php echo $user['pluses']; ?></button>
                <?php   
                echo (!Yii::$app->user->isGuest && Yii::$app->user->id != $user['id'] && !$data['plused']) ? '<button class="plusActiveBtn" title="հավանել">+1</button>' : ''; ?></h3>
                <ul class="plusesList customScroll">
                    <?php if(!empty($data['pluses'])) { ?>
                	<?php foreach($data['pluses'] as $plus) { ?>
                    <li>
                        <ul class="plusSection">
                        	<li><img src="/images/users/<?php echo $plus['username'];?>.png"</li>
                        	<li>#<?php echo $plus['username'];?></li>
                        	<li><?php echo substr($plus['created'], 8, 2),' ',$months[substr($plus['created'], 5, 2)],' ',substr($plus['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>
            </li>
            <?php if(!empty($data['medals'])) { ?>
            <li class="medals">
                <h3 class="medalsHeading">#medals</h3>
                <ul class="medalsList customScroll">
                	<?php foreach($data['medals'] as $medal) { ?>
                    <li>
                        <ul class="medalSection">
                        	<li><img src="/images/users/<?php echo $medal['username'];?>.png"</li>
                        	<li>#<?php echo $medal['username'];?></li>
                        	<li><?php echo substr($medal['created'], 8, 2),' ',$months[substr($medal['created'], 5, 2)],' ',substr($medal['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php if(!Yii::$app->user->isGuest && Yii::$app->user->id != $user['id'] && !$data['plused']) { ?>
<?php 
    $pluse = new Pluses(); 
    $pluse->from_user_id = Yii::$app->user->id;
    $pluse->to_user_id = $user['id'];
?>
<?php $form = ActiveForm::begin([
'id' => 'add-pluse-form',
'action' => '/user/pluse',
        'fieldConfig' => [
            'template' => '{input}'
            ],
]);?>    

<?php echo $form->field($pluse, 'from_user_id')->hiddenInput(); ?>
<?php echo $form->field($pluse, 'to_user_id')->hiddenInput(); ?>

<?php ActiveForm::end();?>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php if(!Yii::$app->user->isGuest && Yii::$app->user->id != $user['id'] && !$data['plused']) { ?>
        $(document).on('click', '.plusActiveBtn', function(){
            var $this = $(this);
            $.ajax({
                async: false,
                url: '/user/pluse',
                type: 'POST',
                data: $('#add-pluse-form').serialize(),
                dataType: 'JSON',
                success: function(data) {
                    if(data) {
                        var html = '<li><ul class="plusSection"><li><img src="/images/users/'+data.username+'.png"</li>'
                        +'<li>#'+data.username+'</li><li>'+data.date+months[parseInt(data.month)]+data.dateEnd+'</li></ul></li>';

                        var count = parseInt($('.plusesHeading button').first().html()) + 1;

                        $('.plusesHeading button').first().html('+'+count).end().last().remove();

                        $('.plusesList').prepend(html);
                    }
                }
            });
        });

         var months = [
                '', 'january', 'february', 'march', 'april', 'may', 'june', 'jule', 'august', 'september', 'october', 'november', 'december',
            ];
        <?php } ?>
    });
</script>