<?php
$this->title="Crossword.am | Իմ խաղերը";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#իմ խաղերը</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <ul class="userGames">
            <li>
                <a href="/user/crosswords" title="իմ խաչբառերը" style="background-color: #F7931E;">իմ խաչբառերը</a>
            </li>
            <li>
                <a href="/user/words" title="իմ վիկտորինաները" style="background-color: #FF5454;">իմ վիկտորինաները</a>
            </li>
        </ul>
    </div>
</div>
<div class="box">
        <div class="mainLeaders" data-url="/site/leaderboard">
            <h1>առաջատարներ</h1>
            <p>կուտակեք միավորներ և լրացրեք առաջատարների շարքերը</p>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>