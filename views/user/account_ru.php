<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\EditProfileForm;
$this->title="Crossword.am | Мои данные";
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> 
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#мои данные</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>

    <?php if(strpos($user['email'], '@crossword.am')) { ?>
    <h3 class="noteWarning">исползуйте существующий адрес электронной почты...</h3>
    <?php } ?>
    <div class="box">
        <?php 
        $editProfile = new EditProfileForm(); 
        $editProfile->facebook_id = $user->facebook_id;
        $editProfile->first_name = $user->first_name;
        $editProfile->last_name = $user->last_name;
        $editProfile->email = $user->email;                
        $editProfile->username = $user->username;        
        $editProfile->address = $user->address;
        $editProfile->city = $user->city;
        $editProfile->state = $user->state;
        $editProfile->age = $user->age;
        $editProfile->sex = $user->sex;
        $editProfile->country = $user->country;
        $editProfile->lat_lng = $user->lat_lng;
        $editProfile->street = $user->street;
        $editProfile->zip_code = $user->zip_code;
        $editProfile->password = '';
        $editProfile->password_repeat = '';                
        ?>
        <?php $form = ActiveForm::begin([
                    'id' => 'editprofileform',
                    'action' => '/ru/user/account',
                    'fieldConfig' => [
                        'template' => '{label}{input}<p class="help-block"></p>'
                        ],
                    ]); ?>                    
                    <div class="formRow twoCol">
                        <?php echo $form->field($editProfile, 'facebook_id')->hiddenInput()->label(false); ?>
                        <?php echo $form->field($editProfile, 'username')->textInput()->label('#логин'); ?>
                        <?php echo $form->field($editProfile, 'email')->textInput()->label('#почта'); ?>
                    </div>
                    <div class="formRow twoCol">
                        <?php echo $form->field($editProfile, 'password')->passwordInput()->label('#пароль'); ?>
                        <?php echo $form->field($editProfile, 'password_repeat')->passwordInput()->label('#подтверждение паролья'); ?>
                    </div>
                    <div class="formRow twoCol">
                        <?php echo $form->field($editProfile, 'first_name')->textInput()->label('#имя'); ?>
                        <?php echo $form->field($editProfile, 'last_name')->textInput()->label('#фамилья'); ?>
                    </div>
                    <div class="formRow twoCol">
                        <?php echo $form->field($editProfile, 'address')->textInput()->label('#адрес'); ?>
                        <?php echo $form->field($editProfile, 'city', ['inputOptions' => ['id' => 'city']])->hiddenInput()->label(false); ?>
                        <?php echo $form->field($editProfile, 'country', ['inputOptions' => ['id' => 'country']])->hiddenInput()->label(false); ?>
                        <?php echo $form->field($editProfile, 'street', ['inputOptions' => ['id' => 'street']])->hiddenInput()->label(false); ?>
                        <?php echo $form->field($editProfile, 'state', ['inputOptions' => ['id' => 'state']])->hiddenInput()->label(false); ?>
                        <?php echo $form->field($editProfile, 'zip_code', ['inputOptions' => ['id' => 'zip_code']])->hiddenInput()->label(false); ?>
                        <?php echo $form->field($editProfile, 'lat_lng', ['inputOptions' => ['id' => 'lat_lng']])->hiddenInput()->label(false); ?>
                    </div>
                    <div class="formRow twoCol">
                            <?php $ages = array(); ?>
                            <?php 
                            for($i=7; $i<=90; $i++) { 
                                $ages[$i] = $i;
                            } ?>
                            <?php
                            echo $form->field($editProfile, 'age')->dropDownList(
                                    $ages, [])->label(false);
                            echo $form->field($editProfile, 'sex')->radioList(array('boy'=>'#мужской','girl'=>'#женский'))->label('#пол'); 
                            ?>
                    </div>
              <div class="submitSect">
                <?php echo Html::submitButton('Обновить', ['class' => 'borderBtn orangeBorderBtn', 'name' => 'signup-button']); ?>                
                <!-- <a href="" class="btn fbBtn">Facebook</a> -->
              </div>
              <div class="clear"></div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
<script>
var autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};


function initialize() {
    
    

   autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('editprofileform-address')),
                {types: ['geocode'],
        });
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
        });
 
}

google.maps.event.addDomListener(window, 'load', initialize);


function getLatLngFromAddress(address) {
           var geocoder = new google.maps.Geocoder();
           geocoder.geocode({'address': address}, function(results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                   $('#lat_lng').val(results[0].geometry.location.lat()+','+results[0].geometry.location.lng())                   
               } else {
                   console.log("Geocode was not successful for the following reason: " + status);
               }
           });
       }

       function fillInAddress() {
           
            $("#city").val('');
            $("#street").val('');
            $("#country").val('');
            $("#zip_code").val('');
            $("#state").val('');
            $("#street").val('');
           // Get the place details from the autocomplete object.
           var place = autocomplete.getPlace();
           for (var component in componentForm) {
               if (document.getElementById(component)) {
                   document.getElementById(component).value = '';
                   document.getElementById(component).disabled = false;
               }

           }

           // Get each component of the address from the place details
           // and fill the corresponding field on the form.
           var address = '';
           var city="";
           for (var i = 0; i < place.address_components.length; i++) {
               var addressType = place.address_components[i].types[0];

               if (componentForm[addressType]) {
                   var val = place.address_components[i][componentForm[addressType]];
                   if (addressType === "locality") {
                    $("#city").val(val);
                   }                   
                   if (addressType === "street_number") {
                    $("#street").val(val);
                   }
                   if (addressType === "administrative_area_level_1") {
                    $("#state").val(val);
                   }                    
                   
                    if (addressType === "country") {
                    $("#country").val(val);
                   }
                    if (addressType === "postal_code") {
                    $("#zip_code").val(val);
                   }                             
                   address = address + ' ' + val;
                  
                   if (document.getElementById(addressType)) {
                       document.getElementById(addressType).value = val;
                   }
               }
           }
           getLatLngFromAddress(address);
       }
</script>