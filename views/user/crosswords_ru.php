<?php

use yii\helpers\Url;
$this->title="Crossword.am | Мои кроссворды";

?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#мои кроссворды</h1>
	<?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>
	<div class="box">    
        <?php if(!empty($crosswords)) { ?>
        <div class="listNames">
            <ul>
                <li style="width: 6%; text-align:center;">#номер</li>
                <li style="width: 20%; text-align:center;">#фото</li>
                <li style="width: 21%; text-align:center;">#кто решил</li>
                <li style="width: 16%; text-align:center;">#имя</li>
                <li style="width: 15%; text-align:center;">#статус</li>
                <li style="width: 12%; text-align:center;">#кнопка</li>
            </ul>
        </div>        
        <ul class="myCrosswordsList">                
            <?php foreach ($crosswords as $crossword) { ?>
            <li class="singleCrossword <?php echo $crossword['status'] == 'all' ? 'completed' : '';?>">
                <ul>
                    <li style="width: 6%; text-align:center;">#<?php echo $crossword['public_number'];?></li>
                    <li style="width: 20%; text-align:center;"><a class="img" href="/images/crosswords/<?php echo $crossword['image'];?>"><img src="/images/crosswords/<?php echo $crossword['image'];?>" alt=""  style="top:10px;"></a></li>
                    <li style="width: 21%; text-align:center;">
                        <a href="#whoSolvedList" class="mgnPopupBtn whoSolvedCrossword" data-id="<?php echo $crossword['cid'];?>">#показать</a>
                    </li>
                    <li style="width: 16%; text-align:center;">#<?php echo $crossword['crossword_name'];?></li>
                    <li style="width: 15%; text-align:center;"><?php echo $crossword['status'] == 'all' ? '100%' : '#доля';?></li>
                    <li style="width: 12%; text-align:center;">
                        <?php if($crossword['status'] == 'all') { ?>
                        <button class="crosswordViewBtn" data-url="<?php echo Url::base(true).'/en/cross-words/play?id='.$crossword['cid'];?>">#открыть</button>
                        <?php } else { ?>
                        <button class="crosswordContinueBtn" data-url="<?php echo Url::base(true).'/en/cross-words/play?id='.$crossword['cid'];?>">#играть</button></li>
                        <?php } ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
    <?php } else { ?>
    <h4 class="noInfSt">кроссвордов нет</h4>
    <?php } ?>
</div>
<div class="box">
    <div class="mainLeaders" data-url="/ru/site/leaderboard">
        <h1>лидеры</h1>
        <p>получайте очки и поднимайтесь в топе</p>
    </div>
</div>
<div id="whoSolvedList" class="mfp-hide mgnPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#кто решил этот кроссворд</h5>
    </div>
    <div class="popupCont">
        <div class="whoSolvedListScrollableDiv">
            <ul id="whoSolvedListScrollable" data-language="ru">
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){        

		$(".myCrosswordsList").mCustomScrollbar({
            scrollbarPosition: 'outside',
            scrollInertia: 1000,
            autoHideScrollbar: true
        });

        $('.myCrosswordsList').magnificPopup({
            delegate: 'a.img',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element.find('img');
                }
            }
        });

        $('.crosswordContinueBtn, .crosswordViewBtn').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
	});
</script>