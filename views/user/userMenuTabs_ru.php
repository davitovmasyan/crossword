<!-- put it in cointainer div -->
<div class="boxUserTabs">
	<ul>
		<li>
			<a title="мои данные" href="/ru/user/account" class="<?php echo $action == 'account' ? 'active' : ''; ?>">#мои данные</a>
		</li>
		<li>
			<a title="моя страница" href="/ru/user/page" class="<?php echo $action == 'page' ? 'active' : ''; ?>">#моя страница</a>
		</li>
		<li>
			<a title="мои игры" href="/ru/user/games" class="<?php echo $action == 'games' ? 'active' : ''; ?>">#мои игры</a>
		</li>
		<li>
			<a title="оповещания" href="/ru/user/notifications" class="<?php echo $action == 'notifications' ? 'active' : ''; ?>">#оповещания</a>
		</li>
		<li>
			<a title="магазин" href="/ru/user/market" class="<?php echo $action == 'market' ? 'active' : ''; ?>">#магазин</a>
		</li>
		<li>
			<a title="выход" href="/ru/site/logout">#выход</a>
		</li>
	</ul>
</div>