<?php
$this->title="Crossword.am | My games";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#my games</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_en'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <ul class="userGames">
            <li>
                <a href="/en/user/crosswords" title="my crosswords" style="background-color: #F7931E;">my crosswords</a>
            </li>
            <li>
                <a href="/en/user/words" title="my quizes" style="background-color: #FF5454;">my quizes</a>
            </li>
        </ul>
    </div>
</div>
<div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>