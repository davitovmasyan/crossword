<?php

use yii\helpers\Url;
$this->title="Crossword.am | Notifications";
$months = [
    '01' => 'january',
    '02' => 'february',
    '03' => 'march',
    '04' => 'april',
    '05' => 'may',
    '06' => 'june',
    '07' => 'jule',
    '08' => 'august',
    '09' => 'september',
    '10' => 'october',
    '11' => 'november',
    '12' => 'december', 
];
?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#notifications</h1>
	<?php echo $this->renderFile($this->findViewFile('userMenuTabs_en'), ['action' => Yii::$app->controller->action->id]); ?>
	<div class="box">    
        <?php if(!empty($notifications)) { ?>
        <ul class="myNotifications" style="padding: 10px; max-height: 600px;">
            <?php foreach ($notifications as $n) { ?>                
            <li class="singleNotification <?php echo $n['status'] == 'unreaded' ? 'unread' : '';?>" data-url="<?php echo $n['url'];?>">
                <span style="display:inline; font-family: TopModern; color:#222;">
                    <?php echo substr($n['created'], 10, 6),', ',substr($n['created'], 8, 2),' ',$months[substr($n['created'], 5, 2)],' ',substr($n['created'], 0, 4);?>
                </span>
                <span style="display:inline; color:#f7931e; font-size: 18px;">
                    (<?php echo $n['notification'];?>)
                </span>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <h4 class="noInfSt">no notifications</h4>
        <?php } ?>
    </div>
</div>
<div class="box">
    <div class="mainBlog" data-url="/en/site/blog">
        <h1>blog</h1>
        <p>awsome news</p>
    </div>
</div>
<div class="box">
    <div class="mainDisc" data-url="/en/site/discussions">
        <h1>discussions</h1>
        <p>participation in our discussions</p>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $(".myNotifications").mCustomScrollbar({
            scrollbarPosition: 'outside',
            scrollInertia: 1000,
            autoHideScrollbar: true
        });        

        $('.singleNotification').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
	});
</script>