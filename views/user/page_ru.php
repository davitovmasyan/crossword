<?php
$this->title="Crossword.am | Моя страница";
$months = [
    '01' => 'январь',
    '02' => 'февраль',
    '03' => 'март',
    '04' => 'апрель',
    '05' => 'май',
    '06' => 'июнь',
    '07' => 'июль',
    '08' => 'август',
    '09' => 'сентябрь',
    '10' => 'октябрь',
    '11' => 'ноябрь',
    '12' => 'декабрь',  
];
?>

<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#моя страница</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <ul class="myPage">
            <li>
                <img src="/images/users/<?php echo $user['username']; ?>.png" alt="">
            </li>
            <li class="username">
                <?php echo $user['username']; ?>
            </li>
            <li class="points">
                <?php echo $user['points']; ?> очки
            </li>
        </ul>
        <ul class="myPage2">
            <li class="pluses">
                <h3 class="plusesHeading">#плюсы <button class="plusesNotButton">+<?php echo $user['pluses']; ?></button></h3>
                <?php if(!empty($data['pluses'])) { ?>
                <ul class="plusesList customScroll">
                	<?php foreach($data['pluses'] as $plus) { ?>
                    <li>
                        <ul class="plusSection">
                        	<li><img src="/images/users/<?php echo $plus['username'];?>.png"</li>
                        	<li>#<?php echo $plus['username'];?></li>
                        	<li><?php echo substr($plus['created'], 8, 2),' ',$months[substr($plus['created'], 5, 2)],' ',substr($plus['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
            <li class="medals">
                <h3 class="medalsHeading">#медали</h3>
                <?php if(!empty($data['medals'])) { ?>
                <ul class="medalsList customScroll">
                	<?php foreach($data['medals'] as $medal) { ?>
                    <li>
                        <ul class="medalSection">
                        	<li><img src="/images/users/<?php echo $medal['username'];?>.png"</li>
                        	<li>#<?php echo $medal['username'];?></li>
                        	<li><?php echo substr($medal['created'], 8, 2),' ',$months[substr($medal['created'], 5, 2)],' ',substr($medal['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
        </ul>
    </div>
</div>
<div class="box">
    <div class="mainLeaders" data-url="/ru/site/leaderboard">
        <h1>лидеры</h1>
        <p>получайте очки и поднимайтесь в топе</p>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>