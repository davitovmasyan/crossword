<?php

use yii\helpers\Url;
$this->title="Crossword.am | Мои викторины";

?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#мои викторины</h1>
	<?php echo $this->renderFile($this->findViewFile('userMenuTabs_ru'), ['action' => Yii::$app->controller->action->id]); ?>
	<div class="box">    
        <?php if(!empty($allWords)) { ?>
        <div class="listNames">
            <ul>
                <li style="width: 6%; text-align:center;">#номер</li>
                <li style="width: 10%; text-align:center;">#фото</li>
                <li style="width: 26%; text-align:center;">#имя</li>
                <li style="width: 30%; text-align:center;">#статус</li>
                <li style="width: 12%; text-align:center;">#кнопка</li>
            </ul>
        </div>        
        <ul class="myCrosswordsList">                
            <?php 
                foreach ($allWords as $allWord) {             
                    $myPercent = 1;
                    if(!empty($myWords)) {
                        foreach ($myWords as $myWord) {
                            if($myWord['category_id'] == $allWord['category_id']) {
                                if($myWord['count'] == $allWord['count']) {
                                    $myPercent = 100; 
                                } else {
                                    $myPercent = ($myWord['count']*100)/$allWord['count'];
                                    $myPercent = ceil($myPercent);
                                }
                            }
                        }
                    } ?>
                    <li class="singleCrossword">
                        <ul>
                            <li style="width: 6%; text-align:center;">#<?php echo $allWord['category_id'];?></li>
                            <li style="width: 10%; text-align:center;"><a class="img" href="/images/uploads/<?php echo $allWord['image'];?>"><img src="/images/uploads/<?php echo $allWord['image'];?>" alt="" style="top:10px;"></a></li>
                            <li style="width: 26%; text-align:center;"><?php echo $allWord['category_name'];?>(<?php echo $allWord['count'];?> вопрос)</li>
                            <li style="width: 30%; text-align:left;">
                                <?php echo $myPercent != 1 ? $myPercent : 0;?>%
                                <span style="background-color: #F7931E; width:<?php echo $myPercent * 2;?>px; height:20px; margin-left: 10px; display:inline-block;"></span>
                            </li>
                            <li style="width: 12%; text-align:center;">
                                <button class="crosswordViewBtn" data-url="<?php echo Url::base(true).'/ru/quiz/play?id='.$allWord['category_id'];?>">#играть</button>
                            </li>
                        </ul>
                    </li><?php 
                }
            ?>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="box">
    <div class="mainLeaders" data-url="/ru/site/leaderboard">
        <h1>лидеры</h1>
        <p>получайте очки и поднимайтесь в топе</p>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $(".myCrosswordsList").mCustomScrollbar({
            scrollbarPosition: 'outside',
            scrollInertia: 1000,
            autoHideScrollbar: true
        });

        $('.myCrosswordsList').magnificPopup({
            delegate: 'a.img',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element.find('img');
                }
            }
        });

        $('.crosswordContinueBtn, .crosswordViewBtn').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
	});
</script>