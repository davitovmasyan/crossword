<?php
use yii\bootstrap\ActiveForm;
use app\models\Pluses;
$this->title="Crossword.am | Пользователь";
$months = [
    '01' => 'январь',
    '02' => 'февраль',
    '03' => 'март',
    '04' => 'апрель',
    '05' => 'май',
    '06' => 'июнь',
    '07' => 'июль',
    '08' => 'август',
    '09' => 'сентябрь',
    '10' => 'октябрь',
    '11' => 'ноябрь',
    '12' => 'декабрь',  
];
?>

<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#пользователь</h1>
    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->id == $user['id']) { ?>
    <h3 class="noteInfo">так видят вашу страницу другие пользователи...</h3>
    <?php } ?>
    <div class="box">
        <ul class="myPage">
            <li>
                <img src="/images/users/<?php echo $user['username']; ?>.png" alt="">
            </li>
            <li class="username">
                <?php echo $user['username']; ?>
            </li>
            <li class="points">
                <?php echo $user['points']; ?> очки
            </li>
        </ul>
        <ul class="myPage2">
            <li class="pluses">
                <h3 class="plusesHeading">#плюсы <button>+<?php echo $user['pluses']; ?></button>
                <?php   
                echo (!Yii::$app->user->isGuest && Yii::$app->user->id != $user['id'] && !$data['plused']) ? '<button class="plusActiveBtn" title="հավանել">+1</button>' : ''; ?></h3>
                <ul class="plusesList customScroll">
                    <?php if(!empty($data['pluses'])) { ?>
                	<?php foreach($data['pluses'] as $plus) { ?>
                    <li>
                        <ul class="plusSection">
                        	<li><img src="/images/users/<?php echo $plus['username'];?>.png"</li>
                        	<li>#<?php echo $plus['username'];?></li>
                        	<li><?php echo substr($plus['created'], 8, 2),' ',$months[substr($plus['created'], 5, 2)],' ',substr($plus['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>
            </li>
            <?php if(!empty($data['medals'])) { ?>
            <li class="medals">
                <h3 class="medalsHeading">#медали</h3>
                <ul class="medalsList customScroll">
                	<?php foreach($data['medals'] as $medal) { ?>
                    <li>
                        <ul class="medalSection">
                        	<li><img src="/images/users/<?php echo $medal['username'];?>.png"</li>
                        	<li>#<?php echo $medal['username'];?></li>
                        	<li><?php echo substr($medal['created'], 8, 2),' ',$months[substr($medal['created'], 5, 2)],' ',substr($medal['created'], 0, 4);?></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php if(!Yii::$app->user->isGuest && Yii::$app->user->id != $user['id'] && !$data['plused']) { ?>
<?php 
    $pluse = new Pluses(); 
    $pluse->from_user_id = Yii::$app->user->id;
    $pluse->to_user_id = $user['id'];
?>
<?php $form = ActiveForm::begin([
'id' => 'add-pluse-form',
'action' => '/user/pluse',
        'fieldConfig' => [
            'template' => '{input}'
            ],
]);?>    

<?php echo $form->field($pluse, 'from_user_id')->hiddenInput(); ?>
<?php echo $form->field($pluse, 'to_user_id')->hiddenInput(); ?>

<?php ActiveForm::end();?>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php if(!Yii::$app->user->isGuest && Yii::$app->user->id != $user['id'] && !$data['plused']) { ?>
        $(document).on('click', '.plusActiveBtn', function(){
            var $this = $(this);
            $.ajax({
                async: false,
                url: '/user/pluse',
                type: 'POST',
                data: $('#add-pluse-form').serialize(),
                dataType: 'JSON',
                success: function(data) {
                    if(data) {
                        var html = '<li><ul class="plusSection"><li><img src="/images/users/'+data.username+'.png"</li>'
                        +'<li>#'+data.username+'</li><li>'+data.date+months[parseInt(data.month)]+data.dateEnd+'</li></ul></li>';

                        var count = parseInt($('.plusesHeading button').first().html()) + 1;

                        $('.plusesHeading button').first().html('+'+count).end().last().remove();

                        $('.plusesList').prepend(html);
                    }
                }
            });
        });

         var months = [
                '', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь',
            ];
        <?php } ?>
    });
</script>