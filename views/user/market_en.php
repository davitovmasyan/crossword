<?php

use yii\widgets\LinkPager;
$this->title="Crossword.am | Market";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#market</h1>
    <?php echo $this->renderFile($this->findViewFile('userMenuTabs_en'), ['action' => Yii::$app->controller->action->id]); ?>
    <div class="box">
        <h1 class="commingSoon">#coming soon</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_en')); ?>   
</div>
<div class="box">
    <div class="mainLeaders" data-url="/en/site/leaderboard">
        <h1>leaders</h1>
        <p>get points and be on top</p>
    </div>
</div>