<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Comments;



$this->title="Crossword.am | ".$quizes[0]['category_name'];
$months = [
    '01' => 'январь',
    '02' => 'февраль',
    '03' => 'март',
    '04' => 'апрель',
    '05' => 'май',
    '06' => 'июнь',
    '07' => 'июль',
    '08' => 'август',
    '09' => 'сентябрь',
    '10' => 'октябрь',
    '11' => 'ноябрь',
    '12' => 'декабрь',  
];

$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"    
];

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<link rel="stylesheet" type="text/css" href="/css/quiz.css">
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#викторина <a href="/ru/quiz/list" class="backBtn">#викторины</a></h1>
    <div class="boxQuizPlayInfo">
    	<ul class="quizInfo">
    		<li>
    			<h3 class="quizCategory">#<?php echo isset($quizes[0]['category_name']) ? $quizes[0]['category_name'] : 'викторина';?></h3>
    		</li>
    		<li>
    			<a href="#" class="commentsBtnInfo">коментарии #<span class="commentCount"><?php echo count($comments); ?></span></a>
    		</li>
    		<li>
    			<a href="/images/uploads/<?php echo isset($quizes[0]['image']) ? $quizes[0]['image'] : 'no-image.jpg';?>" class="imgPopup">
    				<img src="/images/uploads/<?php echo isset($quizes[0]['image']) ? $quizes[0]['image'] : 'no-image.jpg';?>">
    			</a>
    		</li>
    		<li>
    			<div class="addthis_sharing_toolbox"></div>
    		</li>
    	</ul>
    </div>
    <div class="boxQuizPlay">    	
    	<div id="quizTime">
    		10
    	</div>

    	<button id="startQuiz" class="startQuiz">начать</button>
    	<ul class="finishedButtons" style="text-align:center; display:none;">
    		<li style="display:inline-block;">
    			<button class="startQuiz endQuiz" data-url="<?php echo Url::current();?>">снова</button>	
    		</li>
    		<li style="display:inline-block;">
    			<button class="startQuiz endQuiz" data-url="<?php echo Url::base(true);?>/ru/quiz/list">викторины</button>
    		</li>
    	</ul>
    	<div class="quizGamePlay">
    		<h1 id="questionHeading" class="quizQuestion"></h1>
    		<ul class="first">
    			<li>
    				<button class="buttonOne quizAnswerBtn"></button>
    			</li>    			
    			<li>
    				<button class="buttonTwo quizAnswerBtn"></button>
    			</li>
    		</ul>
    		<ul class="second">
    			<li>
    				<button class="buttonThree quizAnswerBtn"></button>
    			</li>    			
    			<li>
    				<button class="buttonFour quizAnswerBtn"></button>
    			</li>
    		</ul>
    	</div>
    </div>
    <div class="boxComments">
    	<h4 class="commentsCount">коментарии <span id="commentsCount">#<?php echo count($comments); ?></span></h4>
		<div class="crossComments customScroll">
			<ul>
				<?php if(!empty($comments)) { ?>
					<?php foreach($comments as $comment) { ?>
                        <?php 
                            foreach($smileys as $smiley) { 
                                $comment['comment'] = str_replace('('.$smiley.')', '<img src="'.Url::base(true).'/images/smileys/smiley-'.$smiley.'.gif">', $comment['comment']);
                            } 
                        ?>
						<li class="commentRow" id="commentNumber<?php echo $comment['id'];?>">
		                    <div class="commentContainer">
		                            <p class="commentText"><?php echo $comment['comment'];?></p>
		                            <p class="commentDate"><span class="commenter" data-url="<?php echo Url::base(true);?>/ru/user/page-view?id=<?php echo $comment['uid'];?>"><?php echo $comment['username'];?></span> <?php echo substr($comment['created'], 10, 6),', ',substr($comment['created'], 8, 2),' ',$months[substr($comment['created'], 5, 2)],' ',substr($comment['created'], 0, 4);?>
		                            	<?php if(!Yii::$app->User->isGuest && $comment['user_id'] == Yii::$app->User->id) { ?>
		                            	<button class="deleteButton" data-id="<?php echo $comment['id'];?>">x</button>
		                            	<?php } ?>
		                            </p>
		                    </div>
		                </li>
	                <?php } ?>
                <?php } ?>
			</ul>
		</div>				
		<?php if(!Yii::$app->User->isGuest) { ?>
		<div>
			<?php 
                $comment = new Comments(); 
                $comment->game_id = $quizes[0]['category_id'];
                $comment->game_type = 'word';
            ?>
        	<?php $form = ActiveForm::begin([
            'id' => 'add-comment-form',
            'action' => '/ru/user/comment',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
            ]);?>
	            <div class="formRow">
	            	<?php echo $form->field($comment, 'game_id')->hiddenInput(); ?>
	            	<?php echo $form->field($comment, 'game_type')->hiddenInput(); ?>
	                <?php echo $form->field($comment, 'comment')->textArea(['id' => 'comment-text', 'rows' => 3]); ?>
                    <p id="commentErrorMessages" class="help-block-error"></p>
	            </div> 
	            <div class="formRow">
	                <?php echo Html::submitButton('добавить', ['id' => 'add-comment-button',  'class' => 'standartBtn orangeBtn', 'name' => 'add-comment-button', 'style' => 'cursor:pointer;']) ?>
                    <a href="#smileys" class="borderBtn orangeBorderBtn mgnPopupBtn">#смайлики</a>
	            </div>
            <?php ActiveForm::end();?>
        </div>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<div id="finishpop" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#поздравляю</h5>
    </div>
    <div class="popupCont">
        <p style="color: #222; font-size:20px; padding: 10px;">из 7 вопросов вы ответили на <span id="resultsCount"></span></p>
        <a href="#" class="closePop standartBtn orangeBtn">закрыть</a>
        <a href="#finishpop" id="showQuizResults" class="mgnPopupBtn" hidden></a>
    </div>
</div>
<?php if(!Yii::$app->user->isGuest) { 
$form = ActiveForm::begin([
	'id' => 'save-form',
	'action' => '/quiz/save',
	]);
echo Html::textArea('answers', '', ['id' => 'answers', 'style' => 'display:none;']);
ActiveForm::end(); } ?>
<script type="text/javascript">

	$('#startQuiz').on('click', function(){
		$(this).hide();
		$('.quizGamePlay').fadeIn(800);
		start();
	});

	$('.commentsBtnInfo').on('click', function(){
		$('html, body').animate({
                scrollTop: $(".boxComments").offset().top
            }, 500);
	});

	var timeID;

	var questions = [

	<?php foreach($quizes as $quiz) { $answers = explode('|', $quiz['answers']); ?>
		{
			"idNumber": "<?php echo trim($quiz['id']);?>",
          	"question": "<?php echo trim($quiz['question']);?>",
          	"answer1": "<?php echo trim($answers[0]);?>",
          	"answer2": "<?php echo trim($answers[1]);?>",
          	"answer3": "<?php echo trim($answers[2]);?>",
          	"answer4": "<?php echo trim($answers[3]);?>",
          	"answer": "<?php echo trim($quiz['answer']);?>",
    	},
	<?php } ?>

	];

	var showed = false;
	var nextTimeout = 0;
	var typeTimeOut = 0;
	var timingTimeOut = 0;
	var questionStep = 0;
    var showTimeOut = 0;
	var step = 0;
	var stoped = false;
    <?php if(!Yii::$app->user->isGuest) { ?>
	var answers = [];
    <?php } else { ?>
    var answers = 0;
    <?php } ?>
	var $first = $('.first');
	var $second = $('.second');
	var $buttonOne = $('.buttonOne');
	var $buttonTwo = $('.buttonTwo');
	var $buttonThree = $('.buttonThree');
	var $buttonFour = $('.buttonFour');
	var $quizTime = $('#quizTime');

	$('.quizAnswerBtn').on('click', function(){
		if(stoped == false)
		{
			stoped = true;
			if(typeof questions[step] != 'undefined' && $(this).text() == questions[step].answer)
			{
				$(this).addClass('correct');
				<?php if(!Yii::$app->user->isGuest) { ?>
					answers.push({
						'id' : questions[step]['idNumber'],
						'answer' : $(this).html()
					});
				<?php } else { ?>
                answers++;
                <?php } ?>
			} else {
				$(this).addClass('incorrect');

                $('.quizAnswerBtn').each(function(){
                    if($(this).text() == questions[step].answer) {
                        $(this).addClass('correct');
                    }
                });
			}
			step++;
			clearTimeout(nextTimeout);
            if(typeof questions[step] != 'undefined')
            {
                nextTimeout = setTimeout(nextQuestion, 2000);
            } else {
                finishedTime();
            }
		}
	});

	function nextQuestion(){
		$first.hide();
		$second.hide();
		$quizTime.html(10);
		stoped = false;
		$('.quizQuestion').html('');
		$('.correct').removeClass('correct');
		$('.incorrect').removeClass('incorrect');
		if(typeof questions[step] != 'undefined')
		{
			$buttonOne.html(questions[step].answer1);
			$buttonTwo.html(questions[step].answer2);
			$buttonThree.html(questions[step].answer3);
			$buttonFour.html(questions[step].answer4);
			showQuestion(questions[step].question);
		} else {
			finishedTime();
		}
	}

	var finishedTimeOut = 0;
	var finished = false;

    var count = 0;

	function finishedTime()
	{
        
       if(typeof answers == 'object') {
            count = answers.length;
       } else {
            count = answers;
       }
        <?php if(!Yii::$app->user->isGuest) { ?>
            save(answers);
        <?php } ?>
		clearTimeout(finishedTimeOut);
		finishedTimeOut = setTimeout(finishedStart, 2000);
	}

	function finishedStart()
	{
        $('#showQuizResults').trigger('click');
        if(count) {
            $('#resultsCount').text(count);
        } else {
            $('#resultsCount').text('ноль');
        }
		$first.hide();
		$second.hide();
		$quizTime.html(10);
		stoped = false;
		$('.quizQuestion').html('');
		$('.correct').removeClass('correct');
		$('.incorrect').removeClass('incorrect');
		$('.finishedButtons').fadeIn(800);
	}

	<?php if(!Yii::$app->user->isGuest) { ?>
		function save(answers)
		{
			var $answers = $('#answers');
			$.each(answers, function(i, v){
				$answers.val($answers.val()+'|'+v.id+'?_?'+v.answer);
			});
			$.ajax({
				url : '/quiz/save',
				type : 'POST',
				data : $('#save-form').serialize(),
				success: function(data){
					console.log(data);
				}
			});
		}
	<?php } ?>

	function start()
	{
		$('.quizQuestion').html('');
		$quizTime.html(10);
		$first.hide();
		$second.hide();
		$buttonOne.html(questions[step].answer1);
		$buttonTwo.html(questions[step].answer2);
		$buttonThree.html(questions[step].answer3);
		$buttonFour.html(questions[step].answer4);
		showQuestion(questions[step].question);
	}

    function printLetterByLetter(message){
        var i = 0;
        if(message.length > 100){
            $('#questionHeading').css({
                fontSize: '22px',
            });
        } else {
            $('#questionHeading').css({
                fontSize: '32px',
            });
        }
        typeTimeOut = setInterval(function(){
            document.getElementById('questionHeading').innerHTML += message.charAt(i);
            i++;
            if (i > message.length){
                clearInterval(typeTimeOut);
                timing();
                $first.fadeIn(400);
                $second.fadeIn(800);
            }
        }, 50);
    }

	function showQuestion(message){
	    var time = 2;
        $('#questionHeading').hide();
        if(message.length > 80){
            $('#questionHeading').css({
                fontSize: '22px',
            });
            time = 4;
        } else {
            $('#questionHeading').css({
                fontSize: '32px',
            });
        }        
        document.getElementById('questionHeading').innerHTML = message;
        $('#questionHeading').fadeToggle(200);
        clearTimeout(showTimeOut);
        showTimeOut = setTimeout(showButtons, time*1000);
	}

    function showButtons()
    {
        timing();
        $first.fadeIn(400);
        $second.fadeIn(800);
    }

	function timing(){
	    var i = 0;
	    timingTimeOut = setInterval(function(){
	        document.getElementById('quizTime').innerHTML = parseInt(document.getElementById('quizTime').innerHTML) - 1;
	        i++;
	        if (parseInt(document.getElementById('quizTime').innerHTML) == 0 || stoped == true){
	            clearInterval(timingTimeOut);
	            if(!stoped) {
	            	step++;
	            	if(typeof questions[step] != 'undefined')
	            	{
	            		nextQuestion();
	            	}
	            	else {
	            		finishedTime();
	            	}
            	}
	        }
	    }, 1000);
	}
$(document).ready(function(){

    $('.closePop').on('click', function(){
        $.magnificPopup.close();
    });

    $('html, body').animate({
                scrollTop: $(".boxQuizPlay").offset().top-50,
            }, 1500);

	$('.endQuiz').on('click', function(){
		window.location = $(this).attr('data-url');
	});

    $(document).on('click', '.commenter', function(){
        window.location = $(this).attr('data-url');
    });

    
	<?php if(!Yii::$app->User->isGuest) { ?>
            var user_id = <?php echo Yii::$app->user->id; ?>;

            var smileys = [
                "baby",
                "black_eye",
                "blink",
                "blush",
                "boredom",
                "clapping",
                "cray",
                "eye",
                "fool",
                "friends",
                "goblin",
                "good",
                "hi",
                "kiss",
                "lazy",
                "lol",
                "lol2",
                "love",
                "mda",
                "not_i",
                "ok",
                "pioneer",
                "rolleyes",
                "scare",
                "scenic",
                "sclerosis",
                "secret",
                "shok",
                "shout",
                "sorry",
                "sos",
                "stop",
                "sing",
                "tease",
                "telephone",
                "this",
                "vayreni",
                "victory",
                "wink",
                "yahoo",
                "zagar"    
            ];


			$('#add-comment-button').on('click', function(){
                $('#commentErrorMessages').text('');
                var commentText = $('#comment-text').val();
               if(commentText == '')
                {
                    $('#commentErrorMessages').text('Напишите коментарий');
                    return false;
                }
                else if(commentText.length < 2 || commentText.length > 200)
                {
                    $('#commentErrorMessages').text('Коментарий должен быть с 2-ух до 200 букв');
                    return false;
                }
                else {
                    $.ajax({
                        async: false,
                        url: '/ru/user/comment',
                        type: 'POST',
                        data: $('#add-comment-form').serialize(),
                        dataType: 'JSON',
                        success: function(data)
                        {
                            if(data)
                            {
                                commentText = strip_tags(commentText);
                                var index = 0;
                                for(index; index<40; index++)
                                {
                                    commentText = str_replace('('+smileys[index]+')', '<img src="<?php echo Url::base(true);?>/images/smileys/smiley-'+smileys[index]+'.gif">', commentText);
                                }
                                var html = '<li class="commentRow" id="commentNumber'+data.id+'"><div class="commentContainer">'
                                +'<p class="commentText">'+commentText+'</p><p class="commentDate">'+'<span class="commenter" data-url="<?php echo Url::base(true);?>/ru/user/page-view?id='+user_id+'">'+data.username+'</span>'
                                +data.date+months[parseInt(data.month)]+data.dateEnd
                                +'<button class="deleteButton" data-id="'+data.id+'">x</button>'
                                +'</p></div></li>';

                                $('.crossComments ul').prepend(html);

                                $('#comment-text').val('');

                                var count = $('#commentsCount').text().replace(/#/gi, '');
                                count = parseInt(count) + 1;
                                $('#commentsCount').text('#'+count);
                            }
                        }
                    });
                }
				return false;
			});

            var months = [
                '', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь',
            ];

            function strip_tags( str ){ // Strip HTML and PHP tags from a string            
                return str.replace(/<\/?[^>]+>/gi, '');
            }

            $(document).on('click', '.deleteButton', function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    async: false,
                    url: '/ru/user/delete-comment',
                    type: 'get',
                    data: {
                        id: id,
                    },
                    dataType: 'JSON',
                    success: function(data)
                    {
                        console.log(data);
                        if(data)
                        {                            
                            $('#commentNumber'+id).remove();

                            var count = $('#commentsCount').text().replace(/#/gi, '');
                            count = parseInt(count) - 1;
                            $('#commentsCount').text('#'+count);
                        }
                    }
                });
            });
		<?php } ?>

	});

	
</script>