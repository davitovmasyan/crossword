<?php

use yii\helpers\Url;

$this->title="Crossword.am | Quiz";

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#quiz</h1>
    <div class="boxQuiz">
        <p style="color: #f7931e; ">good luck...</p>
    </div>
    <div class="shareBox">
        <div class="addthis_sharing_toolbox"></div>
    </div>
    <div class="boxQuizList">
        <?php if(!empty($quizes)) { ?>
        <ul class="quizList">
            <?php $index = 1; ?>
            <?php foreach($quizes as $quiz) { ?>
            <?php if($index == 1) { ?>
            <li class="quizRow">
                <ul>
            <?php } ?>
                    <li class="quizItem">
                        <a href="<?php echo Url::base(true); ?>/en/quiz/play?id=<?php echo $quiz['id']; ?>">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/<?php echo $quiz['image']; ?>">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/<?php echo $quiz['image']; ?>');"></div>
                        </a>
                        <h3 title="" class="quizTitle" data-url="<?php echo Url::base(true); ?>/en/quiz/play?id=<?php echo $quiz['id']; ?>">#<?php echo $quiz['category_name']; ?><h3>
                        <div class="clear"></div>
                        <h3 data-url="<?php echo Url::base(true); ?>/en/quiz/play?id=<?php echo $quiz['id']; ?>" class="quizPlayButtonLink quizPlayButton">play</h3>
                    </li>
            <?php if($index == 6) { ?>
                </ul>
            </li>
            <?php 
            $index = 1; } else {
                $index++;
            }
            ?>
            <?php } ?>
            <?php if($index != 1) { ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <h4 class="noInfSt">no categories</h4>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
    <img src="<?php echo $fbImage; ?>" hidden>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // $('.quizList').magnificPopup({
        //     delegate: 'a',
        //     type: 'image',
        //     closeOnContentClick: false,
        //     closeBtnInside: false,
        //     gallery: {
        //         enabled: true
        //     },
        //     zoom: {
        //         enabled: true,
        //         duration: 300, // don't foget to change the duration also in CSS
        //         opener: function (element) {
        //             return element.find('img');
        //         }
        //     }
        // });

        $('.quizTitle').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.quizPlayButtonLink').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
    });
</script>