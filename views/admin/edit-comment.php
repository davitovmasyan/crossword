<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\AdminLoginForm;

?>

        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit Comment</h3>
            <hr>
            <a href="/admin/comments" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'edit-comment-form',                                 
            ]); ?>
            <?php
            
            $model->id = $comment->id;
            $model->game_type = $comment->game_type;
            $model->game_id = $comment->game_id;
            $model->user_id = $comment->user_id;
            $model->comment = $comment->comment;
            $model->deleted = $comment->deleted;
            
            ?>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'comment')->textArea(['placeholder' => 'Comment', 'class' => 'form-control', 'rows' => 3]); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'user_id')->textInput(['placeholder' => 'User ID', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'game_id')->textInput(['placeholder' => 'Game ID', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                  
                    <?php echo $form->field($model, 'game_type')->dropDownList([
                        'crossword' => 'Crossword',
                        'sudoku' => 'Sudoku',
                        'quiz' => 'Quiz',
                    ]); ?>
                </div>        
                <div class="form-group">                  
                    <?php echo $form->field($model, 'deleted')->dropDownList([
                        'yes' => 'Yes',
                        'no' => 'No',
                    ]); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        
    });
    
</script>