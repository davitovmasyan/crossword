<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Categories;
use yii\helpers\ArrayHelper;
?>

        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit word</h3>
            <hr>
            <a href="/admin/words" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'edit-word-form',                                 
            ]); ?>
            <?php
            
            $model->id = $word->id;
            $model->question = $word->question;
            $model->answers = $word->answers;
            $model->answer = $word->answer;
            $model->category = $word->category;
            $model->point = $word->point;
            
            ?>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
                    <?php echo $form->field($model, 'question')->textInput(['placeholder' => 'Question', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'answers')->textArea(['placeholder' => 'Answers', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'answer')->textArea(['placeholder' => 'Answer', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <?php $categories = new Categories(); ?>
                    <?php echo $form->field($model, 'category')->dropDownList(
                        ArrayHelper::map($categories->find()->all(), 'id', 'category_name')
                    ); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'point')->textArea(['placeholder' => 'Point', 'class' => 'form-control']); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        
    });
    
</script>