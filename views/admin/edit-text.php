<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit Text</h3>
            <hr>
            <a href="/admin/all-discussions" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'edit-discussion-form',                                 
            ]); ?>
            <?php
            
            $model->id = $discussion->id;                            
            $model->text = $discussion->text;
            $model->deleted = $discussion->deleted;
            
            ?>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'text')->textArea(['placeholder' => 'text', 'class' => 'form-control']); ?>
                </div>       
                <div class="form-group">                  
                    <?php echo $form->field($model, 'deleted')->dropDownList([
                        'yes' => 'Yes',
                        'no' => 'No',
                    ]); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        
    });
    
</script>