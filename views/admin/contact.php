<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>

<div class="container">
    <div class="adminCont">
        <h3>Contacts</h3> Total:<?php echo $count; ?> contacts.                
        <hr>
        <div class="table-responsive">
            <?php if(!empty($contacts)) { ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">id</th>
                        <th style="width: 20%;">First Last Names</th>
                        <th style="width: 10%;">Email</th>
                        <th style="width: 20%;">Message</th>
                        <th style="width: 10%;">Created</th>
                        <th style="width: 10%;">Reply</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 10%;">Status</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($contacts as $contact) { ?>
                    <tr>
                        <td><?php echo $contact['id']; ?></td>
                        <td><?php echo $contact['first_last_name']; ?></td>
                        <td><?php echo $contact['email']; ?></td>
                        <td><?php echo $contact['message']; ?></td>
                        <td><?php echo $contact['created']; ?></td>
                        <td class="text-center"><a href="/admin/reply?email=<?php echo $contact['email'];?>" class="btn btn-success">Reply</a></td>
                        <td class="text-center"><a href="/admin/delete-contact?id=<?php echo $contact['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <td class="text-center">
                            <?php if($contact['public'] == 0) { ?>
                            <a href="/admin/publicate-contact?id=<?php echo $contact['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-warning">Publicate</a>
                            <?php } else { ?>
                            <a href="/admin/disable-contact?id=<?php echo $contact['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-info">Disable</a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>
