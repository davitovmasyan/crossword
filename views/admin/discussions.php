<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<div class="container">
    <div class="adminCont">
        <h3>Discussions</h3>
        <a href="/admin/add-discussion" class="btn btn-primary">Add discussion</a>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($discussions)) { ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 3%;">ID</th>
                        <th style="width: 7%;">Name</th>
                        <th style="width: 13%;">Created</th>
                        <th style="width: 10%;">View</th>
                        <th style="width: 10%;">Delete</th>
                        <th style="width: 10%;">Edit</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($discussions as $discussion) { ?>
                    <tr>
                        <td><?php echo $discussion['id']; ?></td>
                        <td><?php echo $discussion['name']; ?></td>
                        <td><?php echo $discussion['created']; ?></td>
                        <td class="text-center"><a href="/admin/discussion?id=<?php echo $discussion['id'];?>">View</a></td>
                        <td class="text-center">
                            <a href="/admin/delete-discussion?id=<?php echo $discussion['id'];?>">Delete</a>
                        </td>
                        <td class="text-center">
                            <a href="/admin/edit-discussion?id=<?php echo $discussion['id'];?>">Edit</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>            
        </div>
    </div>
</div>
