<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="container">
    <div class="adminCont">
        <h3>Files</h3> Total:<?php echo count($files); ?>
        <hr>
        <div class="table-responsive">
            <div class="form-group">
            <?php $form = ActiveForm::begin([
                'id' => 'upload-file',
                'method' => 'post',
                'action' => '/admin/upload-image',
                'options' => [
                    'enctype' => 'multipart/form-data'
                ],
            ]); ?>
            <div class="form-group">
                <?php echo Html::fileInput('images[]', '', ['multiple' => 'multiple']); ?>
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Upload</button>
            </div>
            <?php ActiveForm::end(); ?>
            <?php if(!empty($files)) { $index = 0; ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 25%;">Path</th>
                        <th style="width: 25%;">Image</th>
                        <th style="width: 20%;">Tag</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($files as $file) { $index++; $file=explode('/', $file); $file = end($file); ?>
                    <tr>
                        <td><?php echo $file; ?></td>
                        <td><img style="width:200px;" src="/images/uploads/<?php echo $file; ?>"></td>
                        <td><pre><code>&lt;img href="/images/uploads/<?php echo $file; ?>" src="/images/uploads/<?php echo $file; ?>"></code></pre></td>
                        <td><a href="/admin/delete-image?id=<?php echo $file;?>" class="btn btn-danger">Delete</a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>
</div>
