<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="container">
    <div class="adminCont">
        <a href="/admin/words" class="btn btn-primary">Words</a>
        <a href="/admin/add-word" class="btn btn-primary">Add Word</a>
        <hr>
        <h3>Saved Words</h3> Total:<?php echo $count; ?>.
        <hr>
        <div class="table-responsive">
            <?php if(!empty($saved)) { ?>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">id</th>
                        <th style="width: 20%;">Word ID</th>
                        <th style="width: 20%;">Username</th>
                        <th style="width: 20%;">Created</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($saved as $savedone) { ?>
                    <tr>
                        <td><?php echo $savedone['id']; ?></td>
                        <td><?php echo $savedone['word_id']; ?></td>
                        <td><?php echo $savedone['username']; ?></td>
                        <td><?php echo $savedone['created']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>