<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Categories;

?>

        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Add word</h3>
            <hr>
            <a href="/admin/words" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'add-word-form',                                 
            ]); ?>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'question')->textInput(['placeholder' => 'Question', 'class' => 'form-control']); ?>
                </div>
                <span class="helpers"  data-text="|">pipe(|)</span><span class="helpers" data-text="-">line(-)</span><span class="helpers" data-text="ոչ մեկը">ոչ մեկը</span>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'answers')->textArea(['placeholder' => 'Answers', 'class' => 'form-control', 'rows' => 3]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'answer')->textInput(['placeholder' => 'Answer', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'point')->textInput(['placeholder' => 'Point', 'class' => 'form-control', 'value' => 50]); ?>
                </div>
                <div class="form-group">
                    <?php $categories = new Categories();?>
                    <?php echo $form->field($model, 'category')->dropDownList(
                        ArrayHelper::map($categories->find()->all(), 'id', 'category_name')
                    ); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        $('.helpers').on('click', function(){
            $('#addwordform-answers').val($('#addwordform-answers').val()+' '+$(this).attr('data-text'));
        });
    });
    
</script>