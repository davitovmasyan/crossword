<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\AddDiscussionForm;

?>

        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Add discussion</h3>
            <hr>
            <a href="/admin/discussions" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'add-discussion-form',                                 
            ]); ?>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'name')->textInput(['placeholder' => 'Name', 'class' => 'form-control']); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>