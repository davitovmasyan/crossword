<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>

<div class="container">
    <div class="adminCont">
        <h3>Users Total - <?php echo $count; ?></h3>
        <hr>
        <?php 
        echo Html::dropDownList('order', $order,
                ['users.modified' => 'Փոփոխված', 'users.created' => 'Ստեղծված', 'users.username' => 'Մուտքանուն', 'users.email' => 'Էլ հասցե', 'users.points' => 'Միավորներ'],
                ['id' => 'select-order', 'class' => 'form-control select']
            ); ?>
        <div class="table-responsive">
            <?php if(!empty($users)) { ?>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                             ?>     
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 3%;">ID</th>
                        <th style="width: 7%;">Username</th>
                        <th style="width: 13%;">Email</th>
                        <th style="width: 10%;">First name</th>
                        <th style="width: 13%;">Sex</th>
                        <th style="width: 13%;">Address</th>
                        <th style="width: 10%;">Points</th>
                        <th style="width: 10%;">Pluses</th>
                        <th style="width: 10%;">Age</th>
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 10%;">Status</th>
                        <th style="width: 10%;">Created</th>
                        <th style="width: 10%;">Modified</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($users as $user) { ?>
                    <tr>
                        <td><?php echo $user['id']; ?></td>
                        <td><?php echo $user['username']; ?></td>
                        <td><?php echo $user['email']; ?></td>
                        <td><?php echo $user['first_name']; ?></td>
                        <td><?php echo $user['sex']; ?></td>
                        <td><?php echo $user['address']; ?></td>
                        <td><?php echo $user['points']; ?></td>
                        <td><?php echo $user['pluses']; ?></td>
                        <td><?php echo $user['age']; ?></td>
                        <td class="text-center"><a href="/admin/edit-user?id=<?php echo $user['id'];?>" class="btn btn-primary">Edit</a></td>
                        <td class="text-center"><a href="/admin/delete-user?id=<?php echo $user['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <td class="text-center">
                            <?php if($user['active']) { ?>
                            <a href="/admin/disable-user?id=<?php echo $user['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Disable</a>
                            <?php } else { ?>
                            <a href="/admin/activate-user?id=<?php echo $user['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-warning">Activate</a>
                            <?php } ?>
                        </td>
                        <td><?php echo $user['created']; ?></td>
                        <td><?php echo $user['modified']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>            
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var baseUrl = '<?php echo Url::base(true);?>';
        $('#select-order').on('change', function(){
            window.location = baseUrl+'/admin/users?order='+$(this).val();
        });
    });
</script>