<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>

<div class="container">
    <div class="adminCont">
        <h3>Crosswords</h3> Total:<?php echo $count; ?> crosswords.
        <hr>
        <a href="/admin/add-crossword" class="btn btn-primary">Add crossword</a>
        <a href="/admin/saved-crosswords" class="btn btn-primary">Saved Crosswords</a>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($crosswords)) { ?>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">id</th>
                        <th style="width: 13%;">Public Number</th>
                        <th style="width: 10%;">Category</th>
                        <th style="width: 10%;">Language</th>
                        <th style="width: 10%;">Name</th>
                        <th style="width: 6%;">Description</th>
                        <th style="width: 13%;">Created</th>                        
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 5%;">Status</th>
                        <th style="width: 5%;">Fetch</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($crosswords as $crossword) { ?>
                    <tr>
                        <td><?php echo $crossword['id']; ?></td>
                        <td><?php echo $crossword['public_number']; ?></td>                        
                        <td><?php echo $crossword['category']; ?></td>
                        <td>
                            <?php if($crossword['lang'] == 'arm') { ?>
                            <img src="<?php echo Url::base(true); ?>/images/flags/32/Armenia.png">
                            <?php } else if($crossword['lang'] == 'rus') { ?>
                            <img src="<?php echo Url::base(true); ?>/images/flags/32/Russian Federation.png">
                            <?php } else if($crossword['lang'] == 'eng') { ?>
                            <img src="<?php echo Url::base(true); ?>/images/flags/32/England.png">
                            <?php } ?>
                        </td>
                        <td><?php echo $crossword['crossword_name']; ?></td>
                        <td><?php echo $crossword['description']; ?></td>
                        <td><?php echo $crossword['created']; ?></td>                        
                        <td class="text-center"><a href="/admin/edit-crossword?id=<?php echo $crossword['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-primary">Edit</a></td>
                        <td class="text-center"><a href="/admin/delete-crossword?id=<?php echo $crossword['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <td class="text-center">
                            <?php if($crossword['active'] == 'yes') { ?>
                            <a href="/admin/disable-crossword?id=<?php echo $crossword['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-warning">Disable</a>
                            <?php } else { ?>
                            <a href="/admin/activate-crossword?id=<?php echo $crossword['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-info">Activate</a>
                            <?php } ?>
                        </td>
                        <td class="text-center"><button class="btn btn-primary scrapBtn" data-url="<?php echo Url::base(true).'/cross-words/play?id='.$crossword['id'];?>">Fetch</button></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>
