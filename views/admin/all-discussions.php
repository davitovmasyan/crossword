<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<div class="container">
    <div class="adminCont">
        <h3>Discussions - Total <?php echo $count;?></h3>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($discussions)) { ?>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                             ?> 
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 3%;">ID</th>
                        <th style="width: 7%;">Username</th>
                        <th style="width: 20%;">Text</th>
                        <th style="width: 10%;">Disc Name</th>
                        <th style="width: 13%;">Created</th>
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 5%;">Block</th>
                        <th style="width: 5%;">View</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($discussions as $disc) { ?>
                    <tr>
                        <td><?php echo $disc['id']; ?></td>
                        <td><?php echo $disc['username']; echo $disc['active'] ? '-active' : '-blocked'; ?></td>
                        <td><?php echo $disc['text']; ?></td>
                        <td><?php echo $disc['discname']; ?></td>
                        <td><?php echo $disc['created']; ?></td>                        
                        <td class="text-center"><a href="/admin/edit-text?id=<?php echo $disc['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-primary">Edit</a></td>
                        <?php if($disc['deleted'] == "yes") { ?>
                        <td>Deleted</td>
                        <?php } else { ?>
                        <td class="text-center"><a href="/admin/delete-text?id=<?php echo $disc['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <?php } ?>
                        <td class="text-center">
                            <?php if($disc['active'] == 1) { ?>
                            <a href="/admin/disable-user?id=<?php echo $disc['user_id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Block</a>
                            <?php } else { ?>
                            Blocked
                            <?php } ?>
                        </td>
                        <td class="text-center">                            
                            <a href="/site/discussions?id=<?php echo $disc['discussion_id'];?>" target="_blank" class="btn btn-warning">View</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>            
        </div>
    </div>
</div>
