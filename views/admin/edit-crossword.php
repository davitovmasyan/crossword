<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$alphabets = [
    'arm' => [
        '0' => 'ա',
        '1' => 'բ',
        '2' => 'գ',
        '3' => 'դ',
        '4' => 'ե',
        '5' => 'զ',
        '6' => 'է',
        '7' => 'ը',
        '8' => 'թ',
        '9' => 'ժ',
        '10' => 'ի',
        '11' => 'լ',
        '12' => 'խ',
        '13' => 'ծ',
        '14' => 'կ',
        '15' => 'հ',
        '16' => 'ձ',
        '17' => 'ղ',
        '18' => 'ճ',
        '19' => 'մ',
        '20' => 'յ',
        '21' => 'ն',
        '22' => 'շ',
        '23' => 'ո',
        '24' => 'չ',
        '25' => 'պ',
        '26' => 'ջ',
        '27' => 'ռ',
        '28' => 'ս',
        '29' => 'վ',
        '30' => 'տ',
        '31' => 'ր',
        '32' => 'ց',
        '33' => 'ու',
        '34' => 'փ',
        '35' => 'ք',
        '36' => 'և',
        '37' => 'օ',
        '38' => 'ֆ'
    ],
    'rus' => [
        '0' => 'а',
        '1' => 'б',
        '2' => 'в',
        '3' => 'г',
        '4' => 'д',
        '5' => 'е',
        '6' => 'ж',
        '7' => 'з',
        '8' => 'и',
        '9' => 'й',
        '10' => 'к',
        '11' => 'л',
        '12' => 'м',
        '13' => 'н',
        '14' => 'о',
        '15' => 'п',
        '16' => 'р',
        '17' => 'с',
        '18' => 'т',
        '19' => 'у',
        '20' => 'ф',
        '21' => 'х',
        '22' => 'ц',
        '23' => 'ч',
        '24' => 'ш',
        '25' => 'щ',
        '26' => 'ъ',
        '27' => 'ы',
        '28' => 'ь',
        '29' => 'э',
        '30' => 'ю',
        '31' => 'я'
    ],
    'eng' => [
        '0' => 'a',
        '1' => 'b',
        '2' => 'c',
        '3' => 'd',
        '4' => 'e',
        '5' => 'f',
        '6' => 'g',
        '7' => 'h',
        '8' => 'i',
        '9' => 'j',
        '10' => 'k',
        '11' => 'l',
        '12' => 'm',
        '13' => 'n',
        '14' => 'o',
        '15' => 'p',
        '16' => 'q',
        '17' => 'r',
        '18' => 's',
        '19' => 't',
        '20' => 'u',
        '21' => 'v',
        '22' => 'w',
        '23' => 'x',
        '24' => 'y',
        '25' => 'z',
    ],
];

?>

<style>
table {
    margin: 0 auto;
    border:2px solid black;
}
td {
    text-align: center;
    border-radius: 2px;
    color:#fff;
    border-spacing: 2px!important;
    border-collapse: collapse!important;
    outline:0; height: 50px; width: 50px; border:2px solid; margin: 0; padding: 0; font-weight: bold;
}
tr {margin: 0; padding: 0;}
td.empty {
    border-color: #222;
    color:#222; 
    font-size: 140%; 
    cursor:pointer; 
    background-color: #fff; 
    width: 40px;
    height: 40px;
    transition:all 0.2s;
    -webkit-transition:all 0.2s;
    -ms-transition:all 0.2s;
    -moz-transition:all 0.2s;
    -o-transition:all 0.2s;
}
td.empty:hover {background-color: #222;}
td.block {border-color: #fff; background-color: #222; background-position: center;}
/*td.block {border:none;}*/
td.letter {border-color: #fff;}
td.true {border-color: #fff; color:#fff; font-size: 141%; background-color: #008500; line-height: 2em; line-height: 2em;}
td.question {background-color: #EA6826; color:#fff; font-size: 110%; border:2px solid #fff; cursor: pointer;}
td.selected {border-color: #fff; background-color: #EA6826; color:#fff; font-size: 110%; line-height: 2em;}

</style>        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit crossword</h3>
            <hr>
                <a href="/admin/crosswords" class="btn btn-primary">Back</a>
                <a href="#cmakerAdd" class="btn btn-primary mgnPopupBtn" id="build">Build</a>
            <hr>
            <?php 

            $model->id = $crossword->id;
            $model->structure = $crossword->structure;
            $model->questions = $crossword->questions;
            $model->category = $crossword->category;
            $model->lang = $crossword->lang;
            $model->description = $crossword->description;
            $model->type = $crossword->type;
            $model->public_number = $crossword->public_number;
            $model->crossword_name = $crossword->crossword_name;

            ?>
            <?php $form = ActiveForm::begin([
                'id' => 'add-crossword-form',
            ]); ?>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'id')->textInput(['class' => 'form-control']); ?>
                    <?php echo $form->field($model, 'questions')->textArea(['placeholder' => 'Questions', 'class' => 'form-control', 'rows' => 5]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'structure')->textArea(['placeholder' => 'Structure', 'class' => 'form-control', 'rows' => 5]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'public_number')->textInput(['placeholder' => 'Public Number', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'crossword_name')->textInput(['placeholder' => 'Crossword Name', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'description')->textArea(['placeholder' => 'Description', 'class' => 'form-control', 'rows' => 3]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'type')->dropDownList([
                        'easy' => 'Easy',
                        'medium' => 'Medium',
                        'hard' => 'Hard',
                        'difficult' => 'Difficult',
                    ]); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->field($model, 'category')->textInput(['placeholder' => 'Category', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                  
                    <?php echo $form->field($model, 'lang')->dropDownList([
                        'arm' => 'Armenian',
                        'rus' => 'Russian',
                        'eng' => 'English',
                    ]); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!-- login popup -->
<div id="cmakerAdd" class="mfp-hide mgnPopup cmakerAdd">
    <h5 class="popupTitle">Cmaker</h5>
    <div class="customScroll">
        <table> 
            <?php 
            for($i=0; $i<=50; $i++)
            {
                echo '<tr>';
                for($j=0; $j<=25; $j++)
                {
                    echo '<td id="',$i,'-',$j,'" class="block" tabindex=1 data-x="',$j,'" data-y="',$i,'"></td>';
                }   
                echo '</tr>';
            }
            ?>
        </table>
    </div>
    <div class="clearFull">
        <div class="form-group">
            <label for="cmaker-questions">Questions</label>
            <?php echo HTML::textArea('cmaker-questions', $model->questions, ['class' => 'form-control', 'id' => 'cmaker-questions', 'rows' => 4]); ?>

            <label for="cmaker-structrue">Structure</label>
            <?php echo HTML::textArea('cmaker-structure', $model->structure, ['class' => 'form-control', 'id' => 'cmaker-structure', 'rows' => 4]); ?>            

            <label for="firstx">First X</label>
            <?php echo HTML::textInput('firstx', '', ['class' => 'form-control', 'id' => 'firstx']); ?>
            <label for="lastx">Last X</label>
            <?php echo HTML::textInput('lastx', '', ['class' => 'form-control', 'id' => 'lastx']); ?>            
            <label for="firsty">First Y</label>
            <?php echo HTML::textInput('firsty', '', ['class' => 'form-control', 'id' => 'firsty']); ?>
            <label for="lasty">Last Y</label>
            <?php echo HTML::textInput('lasty', '', ['class' => 'form-control', 'id' => 'lasty']); ?>
        </div>
        <div class="form-group">                  
            <a href="#" id="generate" class="btn btn-success">Generate</a>
            <a href="#" id="reset" class="btn btn-danger">Reset</a>
            <a href="#" id="close" class="btn btn-warning">Close</a>
            <a href="#" id="fill" class="btn btn-primary">Fill</a>
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function(){
        $(window).bind("beforeunload", function() {                         
                return 'Are you sure?'; 
        });

        <?php
            $i = 0;
            $structure = explode('|', $model->structure);
            foreach ($structure as $row) {
                $i++;
                $j = 0;
                $columns = explode(',', $row);
                foreach ($columns as $column) {
                    $j++;
                    if($column[0] == '?') { ?>
                        $('td#'+<?php echo $i; ?>+'-'+<?php echo $j; ?>).text('<?php echo $column[2] == "b" ? "ri" : "dw";?>').addClass('question');
                    <?php } else if(is_numeric($column)) { ?>
                        $('td#'+<?php echo $i; ?>+'-'+<?php echo $j; ?>).text('<?php echo $alphabets["arm"][$column];?>').addClass('true');
                    <?php }
                }
            }
        ?>


        $('td').on('click', function(){
            $('td.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('#fill').on('click', function(){
            $('#editcrosswordform-questions').val($('#cmaker-questions').val());
            $('#editcrosswordform-structure').val($('#cmaker-structure').val());
            $.magnificPopup.close();
        });

        $('#close').on('click', function(){
            $.magnificPopup.close();
        });

        $('#reset').on('click', function(){
            $('td').each(function(){
                $(this).text('').attr('class', 'block');                
            });

            questionNumber = 1;
            questText = '';
            $('#firstx').val('');
            $('#firsty').val(''); 
            $('#lastx').val('');
            $('#lasty').val('');
            $('#cmaker-structure').val('');
            $('#cmaker-questions').val('');
        });

        $('td').bind('keypress', function(e){
            var $this = $(this);
            var code = (e.keyCode ? e.keyCode : e.which);
            var key = String.fromCharCode(code);
            if(key == '1') {
                $('#firstx').val($(this).attr('data-x'));
                $('#firsty').val($(this).attr('data-y'));
            }
            else if(key == '0') {
                $('#lastx').val($(this).attr('data-x'));
                $('#lasty').val($(this).attr('data-y'));
            }
            else if(key == '-') {
                $this.text('');
                $this.attr('class', 'block');   
            }
            else if(key == '?') {
                $this.text('dw');
                $this.attr('class', 'question');    
            }
            else if(key == '/') {
                $this.text('ri');
                $this.attr('class', 'question');    
            }
            else {
                $this.text(key);
                $this.attr('class', 'true');    
            }
            
        });

        var questionNumber = 1;
        var questText = '';
        function loopMaker(firstj,lastj,i,txt) {
            var ntxt = '';
            var dir = '';
            for(var p = firstj; p<=lastj; p++) {
                var item = $('td#'+i+'-'+p).text();
                    if(item == '') {
                        item = '-';
                    }
                    if(item == 'ri' || item == 'dw'){
                        dir = '';
                        $('td#'+i+'-'+p).text(questionNumber);
                        questText += questionNumber;
                        questText += '|   $';
                        dir = item == 'ri' ? 'r' : 'b';
                        item = '?'+questionNumber++;
                        item += dir;
                    }
                    else 
                    {
                        var langValue = $('#editcrosswordform-lang').val();
                        if(langValue == 'arm') {
                            var alphabetIndex = armAlphabet.indexOf(item);
                        }
                        else if(langValue == 'rus') {
                            var alphabetIndex = rusAlphabet.indexOf(item);
                        }
                        else if(langValue == 'eng') {
                            var alphabetIndex = engAlphabet.indexOf(item);
                        }
                        if(alphabetIndex > -1)
                        {
                            item = alphabetIndex;
                        }
                    }
                    if(p==firstj & p!=10) {
                        ntxt += item;   
                    }
                    else {
                        ntxt += ',' + item;
                    }                    
            }
            return ntxt;
        }

        $('#generate').on('click', function(){
            var ni = parseInt($('#lasty').val());
            var nj = parseInt($('#lastx').val());
            var fi = parseInt($('#firsty').val());
            var fj = parseInt($('#firstx').val());
            var txt = '';
            // var i = fi;
            if(ni>=10) {
                for(var i = fi; i<=9; i++) {
                    if(nj>=10) {
                        var tj = 9;
                        txt += loopMaker(fj,tj,i,txt);   
                        txt += loopMaker(10,nj,i,txt);   
                        txt += '|';
                    }
                    if(nj<10) {
                        txt += loopMaker(fj,nj,i,txt);   
                        txt += '|';
                    }           
                }
                for(var i = 10; i<=ni; i++) {
                    if(nj>=10) {
                        var tj = 9;
                        txt += loopMaker(fj,tj,i,txt);   
                        txt += loopMaker(10,nj,i,txt);   
                        txt += '|';
                    }
                    if(nj<10) {
                        txt += loopMaker(fj,nj,i,txt);   
                        txt += '|';
                    }           
                }
            }
            else {
                for(var i = fi; i<=ni; i++) {
                    if(nj>=10) {
                        var tj = 9;
                        txt += loopMaker(fj,tj,i,txt);   
                        txt += loopMaker(10,nj,i,txt);   
                        txt += '|';
                    }
                    if(nj<10) {
                        txt += loopMaker(fj,nj,i,txt);   
                        txt += '|';
                    }           
                }
            }
            txt = txt.substring(0, txt.length - 1);
            questText = questText.substring(0, questText.length - 1);
            $('#cmaker-structure').val('').val(txt);
            if($('#cmaker-questions').val() != '')
            {
                $('#cmaker-questions').val($('#cmaker-questions').val() +'----------'+ questText);
            }
            else {
                $('#cmaker-questions').val(questText);   
            }
            questionNumber = 1;
        });

     
        var armAlphabet = [
            'ա',
            'բ',
            'գ',
            'դ',
            'ե',
            'զ',
            'է',
            'ը',
            'թ',
            'ժ',
            'ի',
            'լ',
            'խ',
            'ծ',
            'կ',
            'հ',
            'ձ',
            'ղ',
            'ճ',
            'մ',
            'յ',
            'ն',
            'շ',
            'ո',
            'չ',
            'պ',
            'ջ',
            'ռ',
            'ս',
            'վ',
            'տ',
            'ր',
            'ց',
            'ւ',
            'փ',
            'ք',
            'և',
            'օ',
            'ֆ'
        ];

        var rusAlphabet = [
            'а',
            'б',
            'в',
            'г',
            'д',
            'е',
            'ж',
            'з',
            'и',
            'й',
            'к',
            'л',
            'м',
            'н',
            'о',
            'п',
            'р',
            'с',
            'т',
            'у',
            'ф',
            'х',
            'ц',
            'ч',
            'ш',
            'щ',
            'ъ',
            'ы',
            'ь',
            'э',
            'ю',
            'я'
        ];

        var engAlphabet = [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z'
        ];

    });
    
</script>