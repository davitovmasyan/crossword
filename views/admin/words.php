<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Categories;
?>

<div class="container">
    <div class="adminCont">
        <h3>Words</h3> Total:<?php echo $count; ?> words.
        <hr>
        <a href="/admin/add-word" class="btn btn-primary">Add word</a>
        <a href="/admin/saved-words" class="btn btn-primary">Saved words</a>
        <hr>
        <?php 
            $categories = new Categories(); 
            echo Html::dropDownList('category', $category,
                    ArrayHelper::map($categories->find()->all(), 'id', 'category_name'),
                    ['id' => 'select-category', 'class' => 'form-control select']
                ); ?>
        <div class="table-responsive">
            <?php if(!empty($words)) { ?>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">id</th>
                        <th style="width: 20%;">Question</th>
                        <th style="width: 10%;">Answers</th>
                        <th style="width: 10%;">Answer</th>
                        <th style="width: 10%;">Category</th>
                        <th style="width: 6%;">Point</th>
                        <th style="width: 13%;">Created</th>                        
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 10%;">Status</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($words as $word) { ?>
                    <tr>
                        <td><?php echo $word['id']; ?></td>
                        <td><?php echo $word['question']; ?></td>
                        <td><?php echo $word['answers']; ?></td>
                        <td><?php echo $word['answer']; ?></td>
                        <td><?php echo $word['category_name']; ?></td>
                        <td><?php echo $word['point']; ?></td>
                        <td><?php echo $word['created']; ?></td>                        
                        <td class="text-center"><a href="/admin/edit-word?id=<?php echo $word['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-primary">Edit</a></td>
                        <td class="text-center"><a href="/admin/delete-word?id=<?php echo $word['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <td class="text-center">
                            <?php if($word['active'] == 'yes') { ?>
                            <a href="/admin/disable-word?id=<?php echo $word['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-warning">Disable</a>
                            <?php } else { ?>
                            <a href="/admin/activate-word?id=<?php echo $word['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-info">Activate</a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var baseUrl = '<?php echo Url::base(true);?>';
        $('#select-category').on('change', function(){
            window.location = baseUrl+'/admin/words?category='+$(this).val();
        });
    });
</script>