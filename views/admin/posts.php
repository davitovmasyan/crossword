<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>

<div class="container">
    <div class="adminCont">
        <h3>Posts</h3> Total:<?php echo $count; ?> posts.
        <hr>
        <a href="/admin/add-post" class="btn btn-primary">Add post</a>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($posts)) { ?>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                        ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 13%;">Image</th>
                        <th style="width: 10%;">Title</th>
                        <th style="width: 15%;">Description</th>
                        <th style="width: 6%;">Keywords</th>
                        <th style="width: 5%;">Language</th>
                        <th style="width: 13%;">Created</th>                        
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 5%;">Status</th>
                        <th style="width: 5%;">Fetch</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($posts as $post) { ?>
                    <tr>
                        <td><?php echo $post['id']; ?></td>
                        <td><?php echo $post['image']; ?></td>                        
                        <td><?php echo $post['title']; ?></td>
                        <td><?php echo $post['description']; ?></td>
                        <td><?php echo $post['keywords']; ?></td>
                        <td><?php echo $post['lang']; ?></td>
                        <td><?php echo $post['created']; ?></td>                        
                        <td class="text-center"><a href="/admin/edit-post?id=<?php echo $post['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-primary">Edit</a></td>
                        <td class="text-center"><a href="/admin/delete-post?id=<?php echo $post['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <td class="text-center">
                            <?php if($post['active'] == 'yes') { ?>
                            <a href="/admin/disable-post?id=<?php echo $post['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-warning">Disable</a>
                            <?php } else { ?>
                            <a href="/admin/activate-post?id=<?php echo $post['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-info">Activate</a>
                            <?php } ?>
                        </td>
                        <?php $prefix = $post['lang'] == 'hy' ? '' : '/'.$post['lang'];?>
                        <td class="text-center"><button class="btn btn-primary scrapBtn" data-url="<?php echo Url::base(true).$prefix.'/site/post?id='.$post['id'];?>">Fetch</button></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>
