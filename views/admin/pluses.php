<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="container">
    <div class="adminCont">
        <h3>Pluses</h3> Total:<?php echo $count; ?>.
        <hr>
        <div class="table-responsive">
            <?php if(!empty($pluses)) { ?>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 30%;">From user</th>
                        <th style="width: 30%;">To user</th>
                        <th style="width: 20%;">Created</th>
                        <th style="width: 5%;">Delete</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($pluses as $pluse) { ?>
                    <tr>
                        <td><?php echo $pluse['id']; ?></td>
                        <td><?php echo $pluse['fromusername']; ?></td>
                        <td><?php echo $pluse['tousername']; ?></td>
                        <td><?php echo $pluse['created']; ?></td>
                        <td class="text-center"><a href="/admin/delete-pluse?id=<?php echo $pluse['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>