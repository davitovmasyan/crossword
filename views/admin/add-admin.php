<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\AddAdminForm;

?>

        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Add admin</h3>
            <hr>
            <a href="/admin/admins" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'add-admin-form',                                 
            ]); ?>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'first_name')->textInput(['placeholder' => 'First Name', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'last_name')->textInput(['placeholder' => 'Last Name', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'username')->textInput(['placeholder' => 'Username', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                  
                    <?php echo $form->field($model, 'password')->textInput(['placeholder' => 'Password', 'class' => 'form-control']); ?>
                </div>               
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        
    });
    
</script>