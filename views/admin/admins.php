<?php

?>

<div class="container">
    <div class="adminCont">
        <h3>Admins</h3>
        <hr>
        <a href="/admin/add-admin" class="btn btn-primary">Add Admin</a>
        <hr>
        <div class="table-responsive">
            
            <?php if(!empty($admins)) { ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 10%;">Username</th>                        
                        <th style="width: 10%;">First name</th>
                        <th style="width: 10%;">Last name</th>
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>                        
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($admins as $admin) { ?>
                    <tr>
                        <td><?php echo $admin['username']; ?></td>                        
                        <td><?php echo $admin['first_name']; ?></td>
                        <td><?php echo $admin['last_name']; ?></td>                                                
                        <td class="text-center"><a href="/admin/edit-admin?id=<?php echo $admin['id'];?>" class="btn btn-primary">Edit</a></td>
                        <td class="text-center"><a href="/admin/delete-admin?id=<?php echo $admin['id'];?>" class="btn btn-danger">Delete</a></td>                        
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>
</div>
