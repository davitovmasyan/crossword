<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$index = 0;

?>

<div class="container">
    <div class="adminCont">
        <h3>Users from Excel file</h3>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                    	<th style="width: 5%;">ID</th>
                        <th style="width: 10%;">Username</th>                        
                        <th style="width: 10%;">First name</th>
                        <th style="width: 10%;">Last name</th>                                                
                        <th style="width: 13%;">Email</th>                        
                        <th style="width: 10%;">Points</th>
                        <th style="width: 10%;">Password</th>
                        <th style="width: 10%;">Age</th>
                        <th style="width: 13%;">Sex</th>
                        <th style="width: 15%;">Save</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($users as $user) { ?>
                    <tr data-id="<?php echo $index; ?>">
                        <td><?php echo $user[0]; ?></td>
                        <td class="username"><?php echo $user[1]; ?></td>
                        <td class="first_name"><?php echo $user[2]; ?></td>
                        <td class="last_name"><?php echo $user[3]; ?></td>
                        <td class="email"><?php echo $user[4]; ?></td>                        
                        <td class="points"><?php echo $user[5]; ?></td>
                        <td class="password"><?php echo $user[6]; ?></td>
                        <td class="age"><?php echo $user[7]; ?></td>
                        <td class="sex"><?php echo $user[8]; ?></td>
                        <td class="text-center"><button data-number="<?php echo $index; ?>" class="btn btn-primary saveButton">Save</button></td>
                    </tr>
                    <?php $index++; ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php // save user hidden form
$form = ActiveForm::begin([
                'id' => 'save-user-form',
                'action' => '/admin/save-user',
                'fieldConfig' => [
                    'template' => '{input}'
                    ],
                ]); ?>
	<?php echo Html::hiddenInput('username', '', ['id' => 'username']); ?>
	<?php echo Html::hiddenInput('first_name', '', ['id' => 'first_name']); ?>
	<?php echo Html::hiddenInput('last_name', '', ['id' => 'last_name']); ?>
	<?php echo Html::hiddenInput('email', '', ['id' => 'email']); ?>
	<?php echo Html::hiddenInput('password', '', ['id' => 'password']); ?>
	<?php echo Html::hiddenInput('points', '', ['id' => 'points']); ?>
	<?php echo Html::hiddenInput('age', '', ['id' => 'age']); ?>
	<?php echo Html::hiddenInput('sex', '', ['id' => 'sex']); ?>
<?php ActiveForm::end(); ?> 
<script type="text/javascript">
$(document).ready(function()
{
	$('.saveButton').on('click', function()
	{
		var id = $(this).attr('data-number');
        var $self = $(this);

		$('#username').val('').val($('tr[data-id="'+id+'"] td.username').text());
		$('#first_name').val('').val($('tr[data-id="'+id+'"] td.first_name').text());
		$('#last_name').val('').val($('tr[data-id="'+id+'"] td.last_name').text());
		$('#email').val('').val($('tr[data-id="'+id+'"] td.email').text());
		$('#password').val('').val($('tr[data-id="'+id+'"] td.password').text());
		$('#points').val('').val($('tr[data-id="'+id+'"] td.points').text());
		$('#age').val('').val($('tr[data-id="'+id+'"] td.age').text());
		$('#sex').val('').val($('tr[data-id="'+id+'"] td.sex').text());

		$.ajax({
            async: false,
			url: '/admin/save-user',
			type: 'POST',
			data: $('#save-user-form').serialize(),
            dataType: 'JSON',
			success: function(data)
			{
                console.log(data);
				if(data.data == true)
				{
					$('tr').each(function(){
                        if($(this).attr('data-id') == id) {
                            $(this).remove();
                        }
                    });
				}
			}
		});
	});
});

</script>