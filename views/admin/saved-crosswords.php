<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="container">
    <div class="adminCont">
        <a href="/admin/crosswords" class="btn btn-primary">Crosswords</a>
        <a href="/admin/add-crossword" class="btn btn-primary">Add Crossword</a>
        <hr>
        <h3>Saved Crosswords</h3> Total:<?php echo $count; ?>.
        <hr>
        <div class="table-responsive">
            <?php if(!empty($saved)) { ?>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">id</th>
                        <th style="width: 5%;">CID</th>
                        <th style="width: 10%;">Crossword</th>
                        <th style="width: 10%;">Active</th>
                        <th style="width: 5%;">Number</th>
                        <th style="width: 10%;">Username</th>
                        <th style="width: 13%;">Status</th>                        
                        <th style="width: 10%;">Modified</th>
                        <th style="width: 10%;">Created</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($saved as $savedone) { ?>
                    <tr>
                        <td><?php echo $savedone['id']; ?></td>
                        <td><?php echo $savedone['cid']; ?></td>
                        <td><?php echo $savedone['crossword_name']; ?></td>
                        <td><?php echo $savedone['cactive']; ?></td>
                        <td><?php echo $savedone['public_number']; ?></td>
                        <td><?php echo $savedone['username']; ?></td>
                        <td><?php echo $savedone['status']; ?></td>
                        <td><?php echo $savedone['modified']; ?></td>
                        <td><?php echo $savedone['created']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>