<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<div class="container">
    <div class="adminCont">
        <h3>Comments - Total <?php echo $count;?></h3>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($comments)) { ?>
            <?php 
            $game_type_link = [
                'crossword' => '/cross-words/play?id=',
                'word' => '/quiz/play?id=',
                'post' => '/site/post?id=',
                'sudoku' => '/sudoku/play?id=',
                'city' => '/city/play?id=',
            ];
            echo LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 3%;">ID</th>
                        <th style="width: 7%;">Username</th>
                        <th style="width: 13%;">Comment</th>
                        <th style="width: 10%;">Game ID</th>
                        <th style="width: 10%;">Game Type</th>
                        <th style="width: 13%;">Created</th>
                        <th style="width: 5%;">Edit</th>
                        <th style="width: 5%;">Delete</th>
                        <th style="width: 5%;">Block</th>
                        <th style="width: 5%;">View</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($comments as $comment) { ?>
                    <tr>
                        <td><?php echo $comment['id']; ?></td>
                        <td><?php echo $comment['username']; echo $comment['active'] ? '-active' : '-blocked'; ?></td>
                        <td><?php echo $comment['comment']; ?></td>
                        <td><?php echo $comment['game_id']; ?></td>
                        <td><?php echo $comment['game_type']; ?></td>
                        <td><?php echo $comment['created']; ?></td>
                        <td class="text-center"><a href="/admin/edit-comment?id=<?php echo $comment['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-primary">Edit</a></td>
                        <?php if($comment['deleted'] == "yes") { ?>
                        <td>Deleted</td>
                        <?php } else { ?>
                        <td class="text-center"><a href="/admin/delete-comment?id=<?php echo $comment['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a></td>
                        <?php } ?>
                        <td class="text-center">
                            <?php if($comment['active'] == 1) { ?>
                            <a href="/admin/disable-user?id=<?php echo $comment['user_id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Block</a>
                            <?php } else { ?>
                            Blocked
                            <?php } ?>
                        </td>
                        <td class="text-center">
                            <?php 
                                foreach ($game_type_link as $key => $value) {
                                    if($comment['game_type'] == $key) {
                                        $link = $value;
                                    }
                                }
                            ?>
                            <a href="<?php echo $link,$comment['game_id'];?>" target="_blank" class="btn btn-warning">View</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>            
        </div>
    </div>
</div>
