<div class="container">
    <div class="adminCont">
        <h3>Admins</h3>
        <hr>
        <div style="width:100%; text-align:center;">
            <h3>Hi mister <?php echo ucfirst(Yii::$app->user->identity->attributes['last_name']);?>!</h3>
            <hr>
            <h3>Welcome to admin panel!</h3>
            <hr>
            <h3>Here are last events list!</h3>
            <hr>
            <ul style="list-style:none;">
                <li>
                    Pluses Total : <?php echo $pluses; ?>
                </li>
                <li>
                    Saved Words Total : <?php echo $saved_words; ?>
                </li>
                <li>
                    Saved Crosswords Total : <?php echo $saved_crosswords; ?>
                </li>
                <li>
                    Last Comment : <?php echo $comment['comment'],' - ',$comment['created'];?>
                </li>
                <li>
                    Last Discussion : <?php echo $discussion['text'],' - ',$discussion['created'];?>
                </li>
                <li>
                    Last User : <?php echo $user['username'],' - ',$user['created'];?>
                </li>
            </ul>
        </div>
    </div>
</div>