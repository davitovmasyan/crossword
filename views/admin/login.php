<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
 
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\LoginForm */
 
$this->title = 'Login';
?>
<style type="text/css">
.g-recaptcha div {
    margin: 20px auto;
    margin-bottom: 20px;
}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container">
                <div class="adminCont">
                    <div class="user-default-login">
                        <h1><?php Html::encode($this->title) ?></h1>

                        <p>Please fill out the following fields to login:</p>

                        <div class="row">
                            <div class="col-lg-5">
                                <?php $form = ActiveForm::begin([
                                        'id' => 'login-form',
                                        'method' => 'post',
                                        'action' => '/admin/login']); ?>
                                <?php echo $form->field($model, 'username'); ?>
                                <?php echo $form->field($model, 'password')->passwordInput(); ?>
                                <div class="g-recaptcha" data-sitekey="6LcZzg8TAAAAAA7FvgA2v9Qv4LKi8WAUB-UueYq3"></div>
                                <div class="form-group">
                                    <?php echo Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>