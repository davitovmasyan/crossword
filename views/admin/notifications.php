<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="container">
    <div class="adminCont">
        <h3>Notifications</h3> Total:<?php echo $count; ?>.
        <hr>
        <div class="table-responsive">
            <?php if(!empty($notifications)) { ?>
            <?php echo LinkPager::widget([
                'pagination' => $pages,
            ]);?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 30%;">Notifications</th>
                        <th style="width: 10%;">To user</th>
                        <th style="width: 10%;">Url</th>
                        <th style="width: 10%;">From User</th>
                        <th style="width: 20%;">Created</th>
                        <th style="width: 5%;">Delete</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($notifications as $note) { ?>
                    <tr>
                        <td><?php echo $note['id']; ?></td>
                        <td><?php echo $note['notification']; ?></td>
                        <td><?php echo $note['username']; ?></td>
                        <td><?php echo $note['url']; ?></td>
                        <td><?php echo $note['from_user_id']; ?></td>
                        <td><?php echo $note['created']; ?></td>
                        <td class="text-center"><a href="/admin/delete-notif?id=<?php echo $note['id'];?>&url=<?php echo Url::current(); ?>" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>