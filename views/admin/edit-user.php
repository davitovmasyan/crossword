<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\EditUserForm;
?>

<?php 
        $editUser = new EditUserForm(); 
        $editUser->id = $user->id;
        $editUser->first_name = $user->first_name;
        $editUser->last_name = $user->last_name;
        $editUser->username = $user->username;
        $editUser->email = $user->email;
        $editUser->points = $user->points;
        $editUser->current_points = $user->current_points;
        $editUser->sex = $user->sex;
        $editUser->address = $user->address;
        $editUser->state = $user->state;
        $editUser->city = $user->city;
        $editUser->zip_code = $user->zip_code;
        $editUser->country = $user->country;
        $editUser->lat_lng = $user->lat_lng;
        $editUser->age = $user->age;        
        ?>
<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit User</h3>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'edit-user-form', 
                'action' => '/admin/edit-user?id=' . $user->id,
                'fieldConfig' => [
                    'template' => '{input}'
                    ],
            ]); ?>
                <div class="form-group">
                    <label>First name</label>
                    <?php echo $form->field($editUser, 'id')->hiddenInput(); ?>
                    <?php echo $form->field($editUser, 'first_name')->textInput(['placeholder' => 'First Name', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Last name</label>
                    <?php echo $form->field($editUser, 'last_name')->textInput(['placeholder' => 'Last Name', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <?php echo $form->field($editUser, 'username')->textInput(['placeholder' => 'Username', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <?php echo $form->field($editUser, 'email')->textInput(); ?>
                </div>
                <div class="form-group">
                    <label>Age</label>
                    <?php echo $form->field($editUser, 'age')->textInput(); ?>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <?php echo $form->field($editUser, 'address')->textInput(); ?>
                    <?php echo $form->field($editUser, 'city', ['inputOptions' => ['id' => 'city']])->hiddenInput()->label(false); ?>
                    <?php echo $form->field($editUser, 'country', ['inputOptions' => ['id' => 'country']])->hiddenInput()->label(false); ?>
                    <?php echo $form->field($editUser, 'street', ['inputOptions' => ['id' => 'street']])->hiddenInput()->label(false); ?>
                    <?php echo $form->field($editUser, 'state', ['inputOptions' => ['id' => 'state']])->hiddenInput()->label(false); ?>
                    <?php echo $form->field($editUser, 'zip_code', ['inputOptions' => ['id' => 'zip_code']])->hiddenInput()->label(false); ?>
                    <?php echo $form->field($editUser, 'lat_lng', ['inputOptions' => ['id' => 'lat_lng']])->hiddenInput()->label(false); ?>
                </div>
                <div class="form-group">
                    <label>Points</label>
                    <?php echo $form->field($editUser, 'points')->textInput(); ?>
                </div>
                <div class="form-group">
                    <label>Current points</label>
                    <?php echo $form->field($editUser, 'current_points')->textInput(); ?>
                </div>
                <div class="form-group">
                    <label>Sex</label>
                    <?php echo $form->field($editUser, 'sex')->dropDownList(
                        ['boy' => 'boy', 'girl' => 'Girl']
                    ); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
var autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};


function initialize() {
    
    

   autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('edituserform-address')),
                {types: ['geocode'],
        });
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
        });
 
}

google.maps.event.addDomListener(window, 'load', initialize);


function getLatLngFromAddress(address) {
           var geocoder = new google.maps.Geocoder();
           geocoder.geocode({'address': address}, function(results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                   $('#lat_lng').val(results[0].geometry.location.lat()+','+results[0].geometry.location.lng())                   
               } else {
                   console.log("Geocode was not successful for the following reason: " + status);
               }
           });
       }

       function fillInAddress() {
           
            $("#city").val('');
            $("#street").val('');
            $("#country").val('');
            $("#zip_code").val('');
            $("#state").val('');
            $("#street").val('');
           // Get the place details from the autocomplete object.
           var place = autocomplete.getPlace();
           for (var component in componentForm) {
               if (document.getElementById(component)) {
                   document.getElementById(component).value = '';
                   document.getElementById(component).disabled = false;
               }

           }

           // Get each component of the address from the place details
           // and fill the corresponding field on the form.
           var address = '';
           var city="";
           for (var i = 0; i < place.address_components.length; i++) {
               var addressType = place.address_components[i].types[0];

               if (componentForm[addressType]) {
                   var val = place.address_components[i][componentForm[addressType]];
                   if (addressType === "locality") {
                    $("#city").val(val);
                   }                   
                   if (addressType === "street_number") {
                    $("#street").val(val);
                   }
                   if (addressType === "administrative_area_level_1") {
                    $("#state").val(val);
                   }                    
                   
                    if (addressType === "country") {
                    $("#country").val(val);
                   }
                    if (addressType === "postal_code") {
                    $("#zip_code").val(val);
                   }                             
                   address = address + ' ' + val;
                  
                   if (document.getElementById(addressType)) {
                       document.getElementById(addressType).value = val;
                   }
               }
           }
           getLatLngFromAddress(address);
       }
</script>