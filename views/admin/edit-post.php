<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit Post</h3>
            <hr>
            <a href="/admin/blog" class="btn btn-primary">Back</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'edit-post-form',                                 
            ]); ?>
            <?php
            
            $model->id = $post->id;
            $model->title = $post->title;
            $model->description = $post->description;
            $model->keywords = $post->keywords;
            $model->post = $post->post;
            $model->image = $post->image;
            $model->lang = $post->lang; 
            
            ?>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
                    <?php echo $form->field($model, 'title')->textInput(['placeholder' => 'Title', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'description')->textArea(['placeholder' => 'Description', 'class' => 'form-control', 'rows' => 3]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'image')->textInput(['placeholder' => 'Image', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'keywords')->textInput(['placeholder' => 'Keywords', 'class' => 'form-control']);?>
                </div>
                <div class="form-group">                  
                    <?php echo $form->field($model, 'lang')->dropDownList([
                        'hy' => 'Armenian',
                        'ru' => 'Russian',
                        'en' => 'English',
                    ]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'post')->textArea(['placeholder' => 'post', 'class' => 'form-control', 'rows' => 8]); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Update">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        
    });
    
</script>