<?php
use yii\helpers\Url;
?>

<div class="container">
    <div class="adminCont">
        <h3>Discussion</h3>
        <a href="/admin/discussions" class="btn btn-primary">Back</a>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($discussion)) { ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 3%;">ID</th>
                        <th style="width: 7%;">Discussion ID</th>
                        <th style="width: 13%;">Username</th>
                        <th style="width: 10%;">Discussion Text</th>
                        <th style="width: 10%;">Created</th>
                        <th style="width: 10%;">Delete</th>
                        <th style="width: 10%;">Block</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($discussion as $d) { ?>
                    <tr>
                        <td><?php echo $d['id']; ?></td>
                        <td><?php echo $d['discussion_id']; ?></td>
                        <td><?php echo $d['username']; echo $d['active'] ? '-active' : '-blocked'; ?></td>
                        <td><?php echo $d['text']; ?></td>
                        <td><?php echo $d['created']; ?></td>
                        <td class="text-center"><a href="/admin/delete-discussion-text?id=<?php echo $d['id'];?>&url=<?php echo Url::current();?>">Delete</a></td>
                        <td class="text-center">
                            <a href="/admin/disable-user?id=<?php echo $d['uid'];?>&url=<?php echo Url::current();?>">Block</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>            
        </div>
    </div>
</div>

