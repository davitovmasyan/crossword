<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<style>
table {
    margin: 0 auto;
    border:2px solid black;
}
td {
    text-align: center;
    border-radius: 2px;
    color:#fff;
    border-spacing: 2px!important;
    border-collapse: collapse!important;
    outline:0; height: 50px; width: 50px; border:2px solid; margin: 0; padding: 0; font-weight: bold;
}
tr {margin: 0; padding: 0;}
td.empty {
    border-color: #222;
    color:#222; 
    font-size: 140%; 
    cursor:pointer; 
    background-color: #fff; 
    width: 40px;
    height: 40px;
    transition:all 0.2s;
    -webkit-transition:all 0.2s;
    -ms-transition:all 0.2s;
    -moz-transition:all 0.2s;
    -o-transition:all 0.2s;
}
td.empty:hover {background-color: #222;}
td.block {border-color: #fff; background-color: #222; background-position: center;}
/*td.block {border:none;}*/
td.letter {border-color: #fff;}
td.true {border-color: #fff; color:#fff; font-size: 141%; background-color: #008500; line-height: 2em; line-height: 2em;}
td.question {background-color: #EA6826; color:#fff; vertical-align: middle; font-size: 110%; border:2px solid #fff; cursor: pointer;}
td.selected {border-color: #fff; background-color: #EA6826; color:#fff; font-size: 110%; line-height: 2em;}

</style>        

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Add crossword</h3>
            <hr>
                <a href="/admin/crosswords" class="btn btn-primary">Back</a>
                <a href="#cmakerAdd" class="btn btn-primary mgnPopupBtn">C-Maker</a>
                <a href="/admin/crosswords" target="_blank" class="btn btn-primary">Crosswords</a>
            <hr>
            <?php $form = ActiveForm::begin([
                'id' => 'add-crossword-form',
                'options' => ['enctype'=>'multipart/form-data'],
            ]); ?>
                <div class="form-group">                    
                    <?php echo $form->field($model, 'questions')->textArea(['placeholder' => 'Questions', 'class' => 'form-control', 'rows' => 5]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'structure')->textArea(['placeholder' => 'Structure', 'class' => 'form-control', 'rows' => 5]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'public_number')->textInput(['placeholder' => 'Public Number', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'crossword_name')->textInput(['placeholder' => 'Crossword Name', 'class' => 'form-control']); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'description')->textArea(['placeholder' => 'Description', 'class' => 'form-control', 'rows' => 3]); ?>
                </div>
                <div class="form-group">                                        
                    <?php echo $form->field($model, 'type')->dropDownList([
                        'easy' => 'Easy',
                        'medium' => 'Medium',
                        'hard' => 'Hard',
                        'difficult' => 'Difficult',
                    ]); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->field($model, 'category')->textInput(['placeholder' => 'Category', 'class' => 'form-control', 'value' => 'other']); ?>
                </div>
                <div class="form-group">                  
                    <?php echo $form->field($model, 'lang')->dropDownList([
                        'arm' => 'Armenian',
                        'rus' => 'Russian',
                        'eng' => 'English',
                    ]); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->field($model, 'image')->fileInput(); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!-- login popup -->
<div id="cmakerAdd" class="mfp-hide mgnPopup cmakerAdd">
    <h5 class="popupTitle">Cmaker</h5>
    <div class="customScroll">
        <table> 
            <?php 
            for($i=0; $i<=50; $i++)
            {
                echo '<tr>';
                for($j=0; $j<=25; $j++)
                {
                    echo '<td id="',$i,'-',$j,'" class="block" tabindex=1 data-x="',$j,'" data-y="',$i,'"></td>';
                }   
                echo '</tr>';
            }
            ?>
        </table>
    </div>
    <div class="clearFull">
        <div class="form-group">
            <label for="cmaker-questions">Questions</label>
            <?php echo HTML::textArea('cmaker-questions', '', ['class' => 'form-control', 'id' => 'cmaker-questions', 'rows' => 4]); ?>

            <label for="cmaker-structrue">Structure</label>
            <?php echo HTML::textArea('cmaker-structure', '', ['class' => 'form-control', 'id' => 'cmaker-structure', 'rows' => 4]); ?>            

            <label for="firstx">First X</label>
            <?php echo HTML::textInput('firstx', '', ['class' => 'form-control', 'id' => 'firstx']); ?>
            <label for="lastx">Last X</label>
            <?php echo HTML::textInput('lastx', '', ['class' => 'form-control', 'id' => 'lastx']); ?>            
            <label for="firsty">First Y</label>
            <?php echo HTML::textInput('firsty', '', ['class' => 'form-control', 'id' => 'firsty']); ?>
            <label for="lasty">Last Y</label>
            <?php echo HTML::textInput('lasty', '', ['class' => 'form-control', 'id' => 'lasty']); ?>
        </div>
        <div class="form-group">                  
            <a href="#" id="generate" class="btn btn-success">Generate</a>
            <a href="#" id="reset" class="btn btn-danger">Reset</a>
            <a href="#" id="close" class="btn btn-warning">Close</a>
            <a href="#" id="fill" class="btn btn-primary">Fill</a>
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function(){
        $(window).bind("beforeunload", function() {
                return 'Are you sure?'; 
        });
        
        $('td').on('click', function(){
            $('td.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('#fill').on('click', function(){
            $('#addcrosswordform-questions').val($('#cmaker-questions').val());
            $('#addcrosswordform-structure').val($('#cmaker-structure').val());
            $.magnificPopup.close();
        });

        $('#close').on('click', function(){
            $.magnificPopup.close();
        });

        $('#reset').on('click', function(){
            $('td').each(function(){
                $(this).text('').attr('class', 'block');                
            });

            questionNumber = 1;
            questText = '';
            $('#firstx').val('');
            $('#firsty').val(''); 
            $('#lastx').val('');
            $('#lasty').val('');
            $('#cmaker-structure').val('');
            $('#cmaker-questions').val('');
        });

        $('td').bind('keypress', function(e){
            var $this = $(this);
            var code = (e.keyCode ? e.keyCode : e.which);
            var key = String.fromCharCode(code);
            if(key == '1') {
                $('#firstx').val($(this).attr('data-x'));
                $('#firsty').val($(this).attr('data-y'));
            }
            else if(key == '0') {
                $('#lastx').val($(this).attr('data-x'));
                $('#lasty').val($(this).attr('data-y'));
            }
            else if(key == '-') {
                $this.text('');
                $this.attr('class', 'block');   
            }
            else if(key == '?') {
                $this.text('dw');
                $this.attr('class', 'question');    
            }
            else if(key == '/') {
                $this.text('ri');
                $this.attr('class', 'question');    
            }
            else {
                $this.text(key);
                $this.attr('class', 'true');    
            }
            
        });

        var questionNumber = 1;
        var questText = '';
        function loopMaker(firstj,lastj,i,txt) {
            var ntxt = '';
            var dir = '';
            for(var p = firstj; p<=lastj; p++) {
                var item = $('td#'+i+'-'+p).text();
                    if(item == '') {
                        item = '-';
                    }
                    if(item == 'ri' || item == 'dw'){
                        dir = '';
                        $('td#'+i+'-'+p).text(questionNumber);
                        questText += questionNumber;
                        questText += '|   $';
                        dir = item == 'ri' ? 'r' : 'b';
                        $('td#'+i+'-'+p).attr('data-dir', dir);
                        item = '?'+questionNumber++;
                        item += dir;
                    }
                    else if(Number.isInteger(item)) {
                        dir = '';
                        $('td#'+i+'-'+p).text(questionNumber);
                        questText += questionNumber;
                        questText += '|   $';
                        dir = $('td#'+i+'-'+p).attr('dir')
                        item = '?'+questionNumber++;
                        item += dir;
                    }
                    else 
                    {
                        var langValue = $('#addcrosswordform-lang').val();
                        if(langValue == 'arm') {
                            var alphabetIndex = armAlphabet.indexOf(item);
                        }
                        else if(langValue == 'rus') {
                            var alphabetIndex = rusAlphabet.indexOf(item);
                        }
                        else if(langValue == 'eng') {
                            var alphabetIndex = engAlphabet.indexOf(item);
                        }
                        if(alphabetIndex > -1)
                        {
                            item = alphabetIndex;
                        }
                    }
                    if(p==firstj & p!=10) {
                        ntxt += item;   
                    }
                    else {
                        ntxt += ',' + item;
                    }                    
            }
            return ntxt;
        }

        $('#generate').on('click', function(){
            var ni = parseInt($('#lasty').val());
            var nj = parseInt($('#lastx').val());
            var fi = parseInt($('#firsty').val());
            var fj = parseInt($('#firstx').val());
            var txt = '';
            // var i = fi;
            if(ni>=10) {
                for(var i = fi; i<=9; i++) {
                    if(nj>=10) {
                        var tj = 9;
                        txt += loopMaker(fj,tj,i,txt);   
                        txt += loopMaker(10,nj,i,txt);   
                        txt += '|';
                    }
                    if(nj<10) {
                        txt += loopMaker(fj,nj,i,txt);   
                        txt += '|';
                    }           
                }
                for(var i = 10; i<=ni; i++) {
                    if(nj>=10) {
                        var tj = 9;
                        txt += loopMaker(fj,tj,i,txt);   
                        txt += loopMaker(10,nj,i,txt);   
                        txt += '|';
                    }
                    if(nj<10) {
                        txt += loopMaker(fj,nj,i,txt);   
                        txt += '|';
                    }           
                }
            }
            else {
                for(var i = fi; i<=ni; i++) {
                    if(nj>=10) {
                        var tj = 9;
                        txt += loopMaker(fj,tj,i,txt);   
                        txt += loopMaker(10,nj,i,txt);   
                        txt += '|';
                    }
                    if(nj<10) {
                        txt += loopMaker(fj,nj,i,txt);   
                        txt += '|';
                    }           
                }
            }
            txt = txt.substring(0, txt.length - 1);
            questText = questText.substring(0, questText.length - 1);
            $('#cmaker-structure').val('').val(txt);
            if($('#cmaker-questions').val() != '')
            {
                $('#cmaker-questions').val($('#cmaker-questions').val() +'----------'+ questText);
            }
            else {
                $('#cmaker-questions').val(questText);   
            }
            questionNumber = 1;
        });

        var armAlphabet = [
            'ա',
            'բ',
            'գ',
            'դ',
            'ե',
            'զ',
            'է',
            'ը',
            'թ',
            'ժ',
            'ի',
            'լ',
            'խ',
            'ծ',
            'կ',
            'հ',
            'ձ',
            'ղ',
            'ճ',
            'մ',
            'յ',
            'ն',
            'շ',
            'ո',
            'չ',
            'պ',
            'ջ',
            'ռ',
            'ս',
            'վ',
            'տ',
            'ր',
            'ց',
            'ւ',
            'փ',
            'ք',
            'և',
            'օ',
            'ֆ'
        ];

        var rusAlphabet = [
            'а',
            'б',
            'в',
            'г',
            'д',
            'е',
            'ж',
            'з',
            'и',
            'й',
            'к',
            'л',
            'м',
            'н',
            'о',
            'п',
            'р',
            'с',
            'т',
            'у',
            'ф',
            'х',
            'ц',
            'ч',
            'ш',
            'щ',
            'ъ',
            'ы',
            'ь',
            'э',
            'ю',
            'я'
        ];

        var engAlphabet = [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z'
        ];

    });
    
</script>