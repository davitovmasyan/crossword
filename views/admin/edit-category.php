<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\EditWordForm;

?>

<div class="container">
    <div class="adminCont">
        <div class="col-md-5 padLeft0 editUserBox margauto">
            <h3 class="text-center">Edit word</h3>
            <hr>
            <a href="/admin/words" class="btn btn-primary">Back</a>
            <hr>
            <?php 
            $model->id = $category->id;
            $model->category_name = $category->category_name;
            $model->image = $category->image;
            $model->keywords = $category->keywords;
            $form = ActiveForm::begin([
                'id' => 'edit-category',
                'options' => ['enctype'=>'multipart/form-data'],
            ]);?>
                <div class="form-group">
                    <?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
                    <?php echo $form->field($model, 'category_name')->textInput(); ?>
                    <?php echo $form->field($model, 'keywords')->textInput(); ?>
                </div>
                <div class="form-group">                  
                    <?php echo $form->field($model, 'lang')->dropDownList([
                        'hy' => 'Armenian',
                        'ru' => 'Russian',
                        'en' => 'English',
                    ]); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->field($model, 'image')->textInput(); ?>
                </div>
                <input type="submit" class="btn btn-primary" value="Update">
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>