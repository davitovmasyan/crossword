<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="container">
    <div class="adminCont">
        <h3>Categories</h3> Total:<?php echo $count; ?> categories.
        <?php $form = ActiveForm::begin([
            'id' => 'add-category',
            'options' => ['enctype'=>'multipart/form-data'],
        ]);?>
            <div class="form-group">
                    <?php echo $form->field($model, 'category_name')->textInput(); ?>
            </div>
            <div class="form-group">                  
                <?php echo $form->field($model, 'lang')->dropDownList([
                    'hy' => 'Armenian',
                    'ru' => 'Russian',
                    'en' => 'English',
                ]); ?>
            </div>
            <div class="form-group">
                    <?php echo $form->field($model, 'image')->fileInput(); ?>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit">
        <?php ActiveForm::end(); ?>
        <hr>
        <div class="table-responsive">
            <?php if(!empty($categories)) { ?>
            <table class="table table-bordered usersTbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">Id</th>
                        <th style="width: 13%;">Name</th>
                        <th style="width: 10%;">Image</th>
                        <th style="width: 10%;">Keywords</th>
                        <th style="width: 10%;">Created</th>
                        <th style="width: 10%;">Status</th>
                        <th style="width: 10%;">Edit</th>
                        <th style="width: 10%;">Delete</th>
                        <th style="width: 5%;">Fetch</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($categories as $cat) { ?>
                    <tr>
                        <td><?php echo $cat['id']; ?></td>
                        <td><?php echo $cat['category_name']; ?></td>
                        <td><img style="width: 200px; heigth: 200px;" src="/images/uploads/<?php echo $cat['image']; ?>"></td>
                        <td><?php echo $cat['keywords']; ?></td>
                        <td><?php echo $cat['created']; ?></td>
                        <td>
                           <?php if($cat['active'] == "yes") { ?>
                           <a href="/admin/disable-category?id=<?php echo $cat['id'];?>" class="btn btn-danger">Disable</a>
                           <?php } else { ?>
                           <a href="/admin/activate-category?id=<?php echo $cat['id'];?>" class="btn btn-info">Activate</a>
                           <?php } ?>
                        </td>
                        <td><a href="/admin/edit-category?id=<?php echo $cat['id'];?>" class="btn btn-success">Edit</a></td>
                        <td><a href="/admin/delete-category?id=<?php echo $cat['id'];?>" class="btn btn-danger">Delete</a></td>
                        <?php $prefix = $cat['lang'] == 'hy' ? '' : '/'.$cat['lang']; ?>
                        <td class="text-center"><button class="btn btn-primary scrapBtn" data-url="<?php echo Url::base(true).$prefix.'/quiz/play?id='.$cat['id'];?>">Fetch</button></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php 
            
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
                            } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    
});
</script>