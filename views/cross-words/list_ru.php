<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title="Crossword.am | Кроссворды";

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#кроссворды</h1>
    <div class="box">
        <ul class="filterBox">
            <li>
                сложность : <?php echo Html::dropDownList('type', $selectedType ? $selectedType : 'any', [
                    'hard' => 'трудные',
                    'difficult' => 'очень трудные',
                    'medium' => 'средние',
                    'easy' => 'легкие',
                    'any' => 'все',
                ], ['id' => 'selectType']); ?>
            </li>
            <li>
                язык : <?php echo Html::dropDownList('lang', $selectedLang ? $selectedLang : 'any', [
                    'arm' => 'армянский',
                    'rus' => 'русский',
                    'eng' => 'английский',
                    'any' => 'все',
                ], ['id' => 'selectLang']); ?>
            </li>
            <li>
                <button class="searchBtn">поиск</button>
            </li>
        </ul>
    </div>
    <div class="shareBox">
        <div class="addthis_sharing_toolbox"></div>
    </div>
    <div class="boxCrosswordList">
        <?php if(!empty($crosswords)) { ?>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <ul class="crosswordsList">
            <?php $index = 1; ?>
            <?php foreach($crosswords as $crossword) { ?>
            <?php if($index == 1) { ?>
            <li class="crosswordRow">
                <ul>
            <?php } ?>
                    <li class="crosswordItem">
                        <a href="<?php echo Url::base(true); ?>/ru/cross-words/play?id=<?php echo $crossword['id']; ?>">
                            <img src="<?php echo Url::base(true); ?>/images/crosswords/<?php echo $crossword['image']; ?>">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/crosswords/<?php echo $crossword['image']; ?>');"></div>
                        </a>
                        <h3 title="<?php echo $crossword['public_number']; ?> <?php echo $crossword['crossword_name']; ?>" class="crosswordTitle" data-url="<?php echo Url::base(true); ?>/ru/cross-words/play?id=<?php echo $crossword['id']; ?>">#<?php echo $crossword['public_number']; ?> <?php echo $crossword['crossword_name']; ?><h3>
                        <div class="crosswordType" title="сложность">
                            <?php if($crossword['type'] == 'easy') { ?>
                            <span class="crosswordTypeSpan green"></span>
                            <span class="crosswordTypeSpan green"></span>
                            <span class="crosswordTypeSpan"></span>
                            <span class="crosswordTypeSpan"></span>
                            <span class="crosswordTypeSpan"></span>
                            <?php } else if($crossword['type'] == 'medium') { ?>
                            <span class="crosswordTypeSpan blue"></span>
                            <span class="crosswordTypeSpan blue"></span>
                            <span class="crosswordTypeSpan blue"></span>
                            <span class="crosswordTypeSpan"></span>
                            <span class="crosswordTypeSpan"></span>
                            <?php } else if($crossword['type'] == 'hard') { ?>
                            <span class="crosswordTypeSpan orange"></span>
                            <span class="crosswordTypeSpan orange"></span>
                            <span class="crosswordTypeSpan orange"></span>
                            <span class="crosswordTypeSpan orange"></span>
                            <span class="crosswordTypeSpan"></span>
                            <?php } else { ?>
                            <span class="crosswordTypeSpan red"></span>
                            <span class="crosswordTypeSpan red"></span>
                            <span class="crosswordTypeSpan red"></span>
                            <span class="crosswordTypeSpan red"></span>
                            <span class="crosswordTypeSpan red"></span>
                            <?php } ?>
                            <div class="crosswordLang" title="язык">
                                <?php if($crossword['lang'] == 'arm') { ?>
                                <img src="<?php echo Url::base(true); ?>/images/flags/32/Armenia.png">
                                <?php } else if($crossword['lang'] == 'rus') { ?>
                                <img src="<?php echo Url::base(true); ?>/images/flags/32/Russian Federation.png">
                                <?php } else if($crossword['lang'] == 'eng') { ?>
                                <img src="<?php echo Url::base(true); ?>/images/flags/32/England.png">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <?php if(isset($crossword['saved_id']) && $crossword['sstatus'] == 'part') { ?> 
                        <h3 data-url="<?php echo Url::base(true); ?>/ru/cross-words/play?id=<?php echo $crossword['id']; ?>" class="playButtonLink continueButton">продолжить</h3>
                        <?php } else if(isset($crossword['saved_id']) && $crossword['sstatus'] == 'all') { ?> 
                        <h3 data-url="<?php echo Url::base(true); ?>/ru/cross-words/play?id=<?php echo $crossword['id']; ?>" class="playButtonLink viewButton">посмотреть</h3>
                        <?php } else { ?>
                        <h3 data-url="<?php echo Url::base(true); ?>/ru/cross-words/play?id=<?php echo $crossword['id']; ?>" class="playButtonLink playButton">решить</h3>
                        <?php } ?>
                    </li>
            <?php if($index == 4) { ?>
                </ul>
            </li>
            <?php 
            $index = 1; } else {
                $index++;
            }
            ?>
            <?php } ?>
            <?php if($index != 1) { ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <?php } else { ?>
        <h4 class="noInfSt">кроссвордов нет</h4>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainCity" data-url="/ru/city/list">
            <h1>#города</h1>
            <p>город - город</p>
        </div>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
    <img src="<?php echo $fbImage; ?>" hidden>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var baseUrl = '<?php echo Url::base(true);?>'+'/ru';

        $('.crosswordTitle').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.playButtonLink').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.searchBtn').on('click', function(){
            baseUrl = baseUrl+'/cross-words/list';
            var conditions = '';
            if($('#selectLang').val() != 'any')
            {
                conditions = '?lang='+$('#selectLang').val();
            }
            if($('#selectType').val() != 'any')
            {
                if(conditions != '')
                {
                    conditions = conditions+'&type='+$('#selectType').val();
                }
                else {
                    conditions = '?type='+$('#selectType').val();
                }
            }
        
            window.location = baseUrl+conditions;
    
        });
    });
</script>