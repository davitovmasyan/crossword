<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Comments;

$this->title="Crossword.am | ".$crossword['crossword_name'];

$alphabets = [
    'arm' => [
        '0' => 'ա',
        '1' => 'բ',
        '2' => 'գ',
        '3' => 'դ',
        '4' => 'ե',
        '5' => 'զ',
        '6' => 'է',
        '7' => 'ը',
        '8' => 'թ',
        '9' => 'ժ',
        '10' => 'ի',
        '11' => 'լ',
        '12' => 'խ',
        '13' => 'ծ',
        '14' => 'կ',
        '15' => 'հ',
        '16' => 'ձ',
        '17' => 'ղ',
        '18' => 'ճ',
        '19' => 'մ',
        '20' => 'յ',
        '21' => 'ն',
        '22' => 'շ',
        '23' => 'ո',
        '24' => 'չ',
        '25' => 'պ',
        '26' => 'ջ',
        '27' => 'ռ',
        '28' => 'ս',
        '29' => 'վ',
        '30' => 'տ',
        '31' => 'ր',
        '32' => 'ց',
        '33' => 'ու',
        '34' => 'փ',
        '35' => 'ք',
        '36' => 'և',
        '37' => 'օ',
        '38' => 'ֆ'
    ],
    'rus' => [
        '0' => 'а',
        '1' => 'б',
        '2' => 'в',
        '3' => 'г',
        '4' => 'д',
        '5' => 'е',
        '6' => 'ж',
        '7' => 'з',
        '8' => 'и',
        '9' => 'й',
        '10' => 'к',
        '11' => 'л',
        '12' => 'м',
        '13' => 'н',
        '14' => 'о',
        '15' => 'п',
        '16' => 'р',
        '17' => 'с',
        '18' => 'т',
        '19' => 'у',
        '20' => 'ф',
        '21' => 'х',
        '22' => 'ц',
        '23' => 'ч',
        '24' => 'ш',
        '25' => 'щ',
        '26' => 'ъ',
        '27' => 'ы',
        '28' => 'ь',
        '29' => 'э',
        '30' => 'ю',
        '31' => 'я'
    ],
    'eng' => [
        '0' => 'a',
        '1' => 'b',
        '2' => 'c',
        '3' => 'd',
        '4' => 'e',
        '5' => 'f',
        '6' => 'g',
        '7' => 'h',
        '8' => 'i',
        '9' => 'j',
        '10' => 'k',
        '11' => 'l',
        '12' => 'm',
        '13' => 'n',
        '14' => 'o',
        '15' => 'p',
        '16' => 'q',
        '17' => 'r',
        '18' => 's',
        '19' => 't',
        '20' => 'u',
        '21' => 'v',
        '22' => 'w',
        '23' => 'x',
        '24' => 'y',
        '25' => 'z',
    ],
];

$months = [
	'01' => 'january',
	'02' => 'february',
	'03' => 'march',
	'04' => 'april',
	'05' => 'may',
	'06' => 'june',
	'07' => 'jule',
	'08' => 'august',
	'09' => 'september',
	'10' => 'october',
	'11' => 'november',
	'12' => 'december',	
];

$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"    
];

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle"><span style="font-family: TopModern;">#<?php echo $crossword['public_number'];?></span> crossword <a href="/en/cross-words/list" class="backBtn">#crosswords</a></h1>
    <div class="box">
    	<ul class="crosswordInfo">
    		<li>
    			<h1 class="crosswordName" title="title"><?php echo $crossword['crossword_name']; ?></h1>
    		</li>
    		<li>
    			<a href="/en/cross-words/list?type=<?php echo $crossword['type'];?>" title="difficulty">
    				<?php if($crossword['type'] == 'easy') { ?>
                    <span class="crosswordTypeSpan green"></span>
                    <span class="crosswordTypeSpan green"></span>
                    <span class="crosswordTypeSpan"></span>
                    <span class="crosswordTypeSpan"></span>
                    <span class="crosswordTypeSpan"></span>
                    <?php } else if($crossword['type'] == 'medium') { ?>
                    <span class="crosswordTypeSpan blue"></span>
                    <span class="crosswordTypeSpan blue"></span>
                    <span class="crosswordTypeSpan blue"></span>
                    <span class="crosswordTypeSpan"></span>
                    <span class="crosswordTypeSpan"></span>
                    <?php } else if($crossword['type'] == 'hard') { ?>
                    <span class="crosswordTypeSpan orange"></span>
                    <span class="crosswordTypeSpan orange"></span>
                    <span class="crosswordTypeSpan orange"></span>
                    <span class="crosswordTypeSpan orange"></span>
                    <span class="crosswordTypeSpan"></span>
                    <?php } else { ?>
                    <span class="crosswordTypeSpan red"></span>
                    <span class="crosswordTypeSpan red"></span>
                    <span class="crosswordTypeSpan red"></span>
                    <span class="crosswordTypeSpan red"></span>
                    <span class="crosswordTypeSpan red"></span>
                    <?php } ?>
    			</a>
    		</li>
    		<li>
    			<a href="/en/cross-words/list?lang=<?php echo $crossword['lang'];?>" title="language">
	    			<?php if($crossword['lang'] == 'arm') { ?>
	                <img src="<?php echo Url::base(true); ?>/images/flags/32/Armenia.png">
	                <?php } else if($crossword['lang'] == 'rus') { ?>
	                <img src="<?php echo Url::base(true); ?>/images/flags/32/Russian Federation.png">
	                <?php } else if($crossword['lang'] == 'eng') { ?>
	                <img src="<?php echo Url::base(true); ?>/images/flags/32/England.png">
	                <?php } ?>
                </a>
    		</li>
            <li>
            </li>
    		<li>
    		    <a title="comments" href="#" class="scrollToComments">> comments</a>	
    		</li>
    		<li>
    			<span title="publish date" class="crosswordDate"><?php echo substr($crossword['created'], 0, 4),' ',$months[substr($crossword['created'], 5,2)],' ',substr($crossword['created'], 8,2);?></span>
    		</li>
    	</ul>
    </div>
    <div class="boxButtons">
    	<ul class="crosswordPlayButton">
    		<li>
    			<div class="addthis_sharing_toolbox"></div>
    		</li>	
    		<li>
    			<button class="hand" data-setup="on" title="second keyboard"></button>
    		</li>
    		<li>
                <?php if($saved_status != 'all') { ?>
    			<button id="save" class="standartBtn orangeBtn" title="save">save</button>
                <?php } ?>
    		</li>
    		<li>
    			<button id="random" class="standartBtn blackBtn" title="random" data-url="<?php echo Url::base(true); ?>/en/cross-words/random">random</button>
    		</li>
    		<li>
                <?php if($saved_status != 'all') { ?>
    			<span id="limit" class="limit" title="limit"></span>
                <?php } ?>
    		</li>
    	</ul>
    </div>
    <div class="boxCrossword">
        <div class='questions'></div>
        <table>
            <?php 
            $i = 0; 
            $answers = array();
            foreach($structure as $row) {
                $i++;
                $j = 0;
                $columns = explode(',', $row);?>
                <tr>
                <?php foreach ($columns as $column) {
                    $flag = false;
                    $j++;
                    if($column[0] == '?') { ?>
                        <?php if(isset($column[3])) { ?>
                        <td tabindex=1 class='question <?php echo $column[3] == 'b' ? 'bobble-bottom' : 'bobble-right'; ?>' title='' id="<?php echo $i . '-' . $j; ?>" data-q='<?php echo $column[1].$column[2]; ?>' data-i='<?php echo $i; ?>' data-j='<?php echo $j;?>' data-letter=""><?php echo $column[1].$column[2]; ?></td>
                        <?php } else { ?>
                        <td tabindex=1 class='question <?php echo $column[2] == 'b' ? 'bobble-bottom' : 'bobble-right'; ?>' title='' id="<?php echo $i . '-' . $j; ?>" data-q='<?php echo $column[1]; ?>' data-i='<?php echo $i; ?>' data-j='<?php echo $j;?>' data-letter=""><?php echo $column[1]; ?></td>
                        <?php } ?>
                    <?php } else if($column == '-'){ ?>
                        <td tabindex=1 class="block" title="" id="<?php echo $i . '-' . $j; ?>" data-i='<?php echo $i; ?>' data-j='<?php echo $j;?>' data-letter=""></td>
                    <?php } else if(is_numeric($column) && $saved_id && $saved_structure) { 
                        foreach ($saved_structure as $saved) {
                            if($saved == $i.'-'.$j.'_'.$column) { $flag = true; ?>
                            <td tabindex=1 class="true" title="" id="<?php echo $i . '-' . $j; ?>" data-i='<?php echo $i; ?>' data-j='<?php echo $j;?>' data-letter="<?php echo $column; ?>"><?php echo $alphabets[$crossword['lang']][$column]; ?></td>
                            <?php } } ?>
                    <?php } if(is_numeric($column) && !$flag){ 
                        $key = $i . '-' . $j;
                        $answers[$key] = $column; ?>
                        <td tabindex=1 class="empty" title="" id="<?php echo $i . '-' . $j; ?>" data-i='<?php echo $i; ?>' data-j='<?php echo $j;?>' data-letter=""></td>
                    <?php }
                }?>
                </tr>
            <?php }
            ?>
        </table>
        <div class="questionsList">
            <?php foreach ($questions as $questionData) {
            $question = explode('|', $questionData); ?>
            <span class="questionsListButton" data-q="<?php echo $question[0]; ?>"><?php echo $question[0]; ?>. <?php echo trim($question[1]); ?></span>
            <?php } ?>
        </div>
        <div class='keyboard'>
            <div class='letters'>
                <div class='min-keyboard buzz'>_</div>
                <div class='close-keyboard buzz'>x</div><br>
            <?php
                $keyboard = '';
                if($crossword['lang'] == 'arm') {
                    $kLetters = explode(',','0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38');
                    foreach ($kLetters as $letter) {                            
                        $keyboard .= '<span class=letter title=' . $alphabets['arm'][$letter] . ' data-letter="'.$letter.'">' . $alphabets['arm'][$letter] . '</span>';
                    }
                    $keyboard .= '<span class=letter title=delete>-</span>'; 
                }
                elseif($crossword['lang'] == 'rus') {
                    $kLetters = explode(',','0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31');
                    foreach ($kLetters as $letter) {
                        $keyboard .= '<span class=letter title=' . $alphabets['rus'][$letter] . ' data-letter="'.$letter.'">' . $alphabets['rus'][$letter] . '</span>';
                    }
                    $keyboard .= '<span class=letter title=delete>-</span>'; 
                }
                elseif($crossword['lang'] == 'eng') {
                    $kLetters = explode(',','0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25');
                    foreach ($kLetters as $letter) {
                        $keyboard .= '<span class=letter title=' . $alphabets['eng'][$letter] . ' data-letter="'.$letter.'">' . $alphabets['eng'][$letter] . '</span>';
                    }
                    $keyboard .= '<span class=letter title=delete>-</span>'; 
                }
                echo $keyboard;
            ?>
            </div>
        </div>
    </div>
    <div class="boxComments">
    	<h4 class="commentsCount">comments <span id="commentsCount">#<?php echo count($crossword['comments']); ?></span></h4>
		<div class="crossComments customScroll">
			<ul>
				<?php if(!empty($crossword['comments'])) { ?>
					<?php foreach($crossword['comments'] as $comment) { ?>
                        <?php 
                            foreach($smileys as $smiley) { 
                                $comment['comment'] = str_replace('('.$smiley.')', '<img src="'.Url::base(true).'/images/smileys/smiley-'.$smiley.'.gif">', $comment['comment']);
                            } 
                        ?>
						<li class="commentRow" id="commentNumber<?php echo $comment['id'];?>">
		                    <div class="commentContainer">
		                            <p class="commentText"><?php echo $comment['comment'];?></p>
		                            <p class="commentDate"><span class="commenter" data-url="<?php echo Url::base(true);?>/en/user/page-view?id=<?php echo $comment['uid'];?>"><?php echo $comment['username'];?></span> <?php echo substr($comment['created'], 10, 6),', ',substr($comment['created'], 8, 2),' ',$months[substr($comment['created'], 5, 2)],' ',substr($comment['created'], 0, 4);?>
		                            	<?php if(!Yii::$app->User->isGuest && $comment['user_id'] == Yii::$app->User->id) { ?>
		                            	<button class="deleteButton" data-id="<?php echo $comment['id'];?>">x</button>
		                            	<?php } ?>
		                            </p>
		                    </div>
		                </li>
	                <?php } ?>
                <?php } ?>
			</ul>
		</div>				
		<?php if(!Yii::$app->User->isGuest) { ?>
		<div>
			<?php 
                $comment = new Comments(); 
                $comment->game_id = $crossword['id'];
                $comment->game_type = 'crossword';
            ?>
        	<?php $form = ActiveForm::begin([
            'id' => 'add-comment-form',
            'action' => '/en/user/comment',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
            ]);?>
	            <div class="formRow">
	            	<?php echo $form->field($comment, 'game_id')->hiddenInput(); ?>
	            	<?php echo $form->field($comment, 'game_type')->hiddenInput(); ?>
	                <?php echo $form->field($comment, 'comment')->textArea(['id' => 'comment-text', 'rows' => 3]); ?>
                    <p id="commentErrorMessages" class="help-block-error"></p>
	            </div> 
	            <div class="formRow">
	                <?php echo Html::submitButton('add', ['id' => 'add-comment-button',  'class' => 'standartBtn orangeBtn', 'name' => 'add-comment-button', 'style' => 'cursor:pointer;']) ?>
                    <a href="#smileys" class="borderBtn orangeBorderBtn mgnPopupBtn">#smileys</a>
	            </div>
            <?php ActiveForm::end();?>
        </div>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<div id="nolimitpop" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#sorry</h5>
    </div>
    <div class="popupCont">
        <p style="color: #222; font-size:20px; padding: 10px;">limit is over:</p>
        <a href="/en/cross-words/list" class="standartBtn orangeBtn">crosswords</a>
        <a href="#" class="closePop standartBtn orangeBtn">close</a>
        <a href="#nolimitpop" id="nolimitbtn" class="mgnPopupBtn" hidden></a>
    </div>
</div>
<div id="finishpop" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#congratulations</h5>
    </div>
    <div class="popupCont">
        <p style="color: #222; font-size:20px; padding: 10px;">congratulations you are a winner:</p>
        <a href="/en/cross-words/list" class="standartBtn orangeBtn">crosswords</a>
        <a href="#" class="closePop standartBtn orangeBtn">close</a>
        <a href="#finishpop" id="finishbtn" class="mgnPopupBtn" hidden></a>
    </div>
</div>
<?php if($saved_status != 'all') { ?>
<?php
$form = ActiveForm::begin([
            'id' => 'save-crossword-form',
            'action' => '/cross-words/save',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
            ]);
echo Html::hiddenInput('id', $crossword['id'], ['id' => 'save-crossword-id']);
echo Html::hiddenInput('type', $saved_id ? 'partial' : 'new', ['id' => 'save-crossword-save-type']);
echo Html::hiddenInput('structure', '', ['id' => 'save-crossword-structure']);
ActiveForm::end(); ?>
<?php } ?>
<script type="text/javascript">
    var questions = [];
    var c_letters = [];
    var inputlang = '<?php echo $crossword["lang"];?>';
	$(document).ready(function(){
        $(document).on('click', '.questionsListButton', function(){
            var q = $(this).attr('data-q');
            $('td.question').each(function(i,v){
                if($(this).attr('data-q') == q) {
                    $(this).trigger('click');
                    $('html, body').animate({
                        scrollTop: $(this).offset().top-150
                    }, 500);
                }
            });
        });


        <?php foreach ($questions as $questionData) {
        $question = explode('|', $questionData); ?> 
        questions['<?php echo $question[0]; ?>'] = '<?php echo trim($question[1]); ?>';
        <?php } 
        foreach($answers as $key => $answer) { ?>
                var c_letter = '<?php echo $key,'-',$answer; ?>';
                c_letters.push(c_letter);           
        <?php } ?>

        $('#random').on('click', function(){
            window.location = $(this).attr('data-url');
        });

        <?php if(!Yii::$app->user->isGuest) { ?>

        

        <?php } ?>

        $('.scrollToComments').on('click', function()
        {
             $('html, body').animate({
                scrollTop: $(".boxComments").offset().top
            }, 500);

            return false;
        });

        $(document).on('click', '.commenter', function(){
            window.location = $(this).attr('data-url');
        });


		<?php if(!Yii::$app->User->isGuest) { ?>
            var user_id = <?php echo Yii::$app->user->id; ?>;

            var smileys = [
                "baby",
                "black_eye",
                "blink",
                "blush",
                "boredom",
                "clapping",
                "cray",
                "eye",
                "fool",
                "friends",
                "goblin",
                "good",
                "hi",
                "kiss",
                "lazy",
                "lol",
                "lol2",
                "love",
                "mda",
                "not_i",
                "ok",
                "pioneer",
                "rolleyes",
                "scare",
                "scenic",
                "sclerosis",
                "secret",
                "shok",
                "shout",
                "sorry",
                "sos",
                "stop",
                "sing",
                "tease",
                "telephone",
                "this",
                "vayreni",
                "victory",
                "wink",
                "yahoo",
                "zagar"    
            ];


			$('#add-comment-button').on('click', function(){
                $('#commentErrorMessages').text('');
                var commentText = $('#comment-text').val();
                if(commentText == '')
                {
                    $('#commentErrorMessages').text('Write a comment');
                    return false;
                }
                else if(commentText.length < 2 || commentText.length > 200)
                {
                    $('#commentErrorMessages').text('Comment must be between 2 and 200 symbols');
                    return false;
                }
                else {
                    $.ajax({
                        async: false,
                        url: '/en/user/comment',
                        type: 'POST',
                        data: $('#add-comment-form').serialize(),
                        dataType: 'JSON',
                        success: function(data)
                        {
                            if(data)
                            {
                                commentText = strip_tags(commentText);
                                var index = 0;
                                for(index; index<40; index++)
                                {
                                    commentText = str_replace('('+smileys[index]+')', '<img src="<?php echo Url::base(true);?>/images/smileys/smiley-'+smileys[index]+'.gif">', commentText);
                                }
                                var html = '<li class="commentRow" id="commentNumber'+data.id+'"><div class="commentContainer">'
                                +'<p class="commentText">'+commentText+'</p><p class="commentDate">'+'<span class="commenter" data-url="<?php echo Url::base(true);?>/en/user/page-view?id='+user_id+'">'+data.username+'</span>'
                                +data.date+months[parseInt(data.month)]+data.dateEnd
                                +'<button class="deleteButton" data-id="'+data.id+'">x</button>'
                                +'</p></div></li>';

                                $('.crossComments ul').prepend(html);

                                $('#comment-text').val('');

                                var count = $('#commentsCount').text().replace(/#/gi, '');
                                count = parseInt(count) + 1;
                                $('#commentsCount').text('#'+count);
                            }
                        }
                    });
                }
				return false;
			});

            var months = [
                '', 'january', 'february', 'march', 'april', 'may', 'june', 'jule', 'august', 'september', 'october', 'november', 'december',
            ];

            function strip_tags( str ){ // Strip HTML and PHP tags from a string            
                return str.replace(/<\/?[^>]+>/gi, '');
            }

            $(document).on('click', '.deleteButton', function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    async: false,
                    url: '/en/user/delete-comment',
                    type: 'get',
                    data: {
                        id: id,
                    },
                    dataType: 'JSON',
                    success: function(data)
                    {
                        console.log(data);
                        if(data)
                        {                            
                            $('#commentNumber'+id).remove();

                            var count = $('#commentsCount').text().replace(/#/gi, '');
                            count = parseInt(count) - 1;
                            $('#commentsCount').text('#'+count);
                        }
                    }
                });
            });
		<?php } ?>
	});
</script>