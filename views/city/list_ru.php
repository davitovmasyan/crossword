 <?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title="Crossword.am | Города";

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#города</h1>    
    <div class="shareBox">
        <div class="addthis_sharing_toolbox"></div>
    </div>
    <div class="boxCrosswordList">
        <ul class="crosswordsList">
            <li class="crosswordRow">
                <ul>
                    <li class="crosswordItem">
                        <a href="<?php echo Url::base(true); ?>/ru/city/am">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/3e844c6aa26e466d19771989be66dee5.jpg">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/3e844c6aa26e466d19771989be66dee5.jpg');"></div>
                        </a>
                        <h3 title="Армения" class="crosswordTitle" data-url="<?php echo Url::base(true); ?>/ru/city/am">#Армения<h3>
                        <h3 data-url="<?php echo Url::base(true); ?>/ru/city/am" class="playButtonLink viewButton">играть</h3>
                    </li>
                    <li class="crosswordItem">
                        <a href="<?php echo Url::base(true); ?>/ru/city/us">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/79331e7c3973510fb5895b0b4f901839.jpg">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/79331e7c3973510fb5895b0b4f901839.jpg');"></div>
                        </a>
                        <h3 title="США" class="crosswordTitle" data-url="<?php echo Url::base(true); ?>/ru/city/us">#США<h3>
                        <h3 data-url="<?php echo Url::base(true); ?>/ru/city/us" class="playButtonLink viewButton">играть</h3>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var baseUrl = '<?php echo Url::base(true);?>';

        $('.crosswordTitle').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.playButtonLink').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
        
    });
</script>