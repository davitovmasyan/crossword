<?php

use yii\helpers\Url;
use app\models\Comments;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title="Crossword.am | Հայաստանի Հանրապետություն";
$months = [
    '01' => 'հունվար',
    '02' => 'փետրվար',
    '03' => 'մարտ',
    '04' => 'ապրիլ',
    '05' => 'մայիս',
    '06' => 'հունիս',
    '07' => 'հուլիս',
    '08' => 'օգոստոս',
    '09' => 'սեպտեմբեր',
    '10' => 'հոկտեմբեր',
    '11' => 'նոյեմբեր',
    '12' => 'դեկտեմբեր',    
];

$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"    
];

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<link rel="stylesheet" type="text/css" href="/css/city.css">
<script type="text/javascript" src="/js/city.js"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle"><span style="font-family: TopModern;">#հայաստանի հանրապետություն <a href="/city/list" class="backBtn">#քաղաքներ</a></h1>
    <div class="shareBox">
        <div class="addthis_sharing_toolbox"></div>
    </div>
    <div class="cityBox">
        <div class="answerBox">
            <input class="firstLetter" type="text" value="<?php echo $cities['last_char'];?>" disabled>
            <input class="answer" type="text" placeholder="(abc...)">
            <input class="doSearch" type="text">
            <input type="text" class="resetButton" data-url="/city/reset?name=am">
        </div>
    	<div class="citiesList customScroll">
        </div>
    </div>
    <div class="boxComments">
    	<h4 class="commentsCount">մեկնաբանություններ <span id="commentsCount">#<?php echo count($game['comments']); ?></span></h4>
		<div class="crossComments customScroll">
			<ul>
				<?php if(!empty($game['comments'])) { ?>
					<?php foreach($game['comments'] as $comment) { ?>
                        <?php 
                            foreach($smileys as $smiley) { 
                                $comment['comment'] = str_replace('('.$smiley.')', '<img src="'.Url::base(true).'/images/smileys/smiley-'.$smiley.'.gif">', $comment['comment']);
                            } 
                        ?>
						<li class="commentRow" id="commentNumber<?php echo $comment['id'];?>">
		                    <div class="commentContainer">
		                            <p class="commentText"><?php echo $comment['comment'];?></p>
		                            <p class="commentDate"><span class="commenter" data-url="<?php echo Url::base(true);?>/user/page-view?id=<?php echo $comment['uid'];?>"><?php echo $comment['username'];?></span> <?php echo substr($comment['created'], 10, 6),', ',substr($comment['created'], 8, 2),' ',$months[substr($comment['created'], 5, 2)],' ',substr($comment['created'], 0, 4);?>
		                            	<?php if(!Yii::$app->User->isGuest && $comment['user_id'] == Yii::$app->User->id) { ?>
		                            	<button class="deleteButton" data-id="<?php echo $comment['id'];?>">x</button>
		                            	<?php } ?>
		                            </p>
		                    </div>
		                </li>
	                <?php } ?>
                <?php } ?>
			</ul>
		</div>				
		<?php if(!Yii::$app->User->isGuest) { ?>
		<div>
			<?php 
                $comment = new Comments(); 
                $comment->game_id = 1;
                $comment->game_type = 'city';
            ?>
        	<?php $form = ActiveForm::begin([
            'id' => 'add-comment-form',
            'action' => '/user/comment',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
            ]);?>
	            <div class="formRow">
	            	<?php echo $form->field($comment, 'game_id')->hiddenInput(); ?>
	            	<?php echo $form->field($comment, 'game_type')->hiddenInput(); ?>
	                <?php echo $form->field($comment, 'comment')->textArea(['id' => 'comment-text', 'rows' => 3]); ?>
                    <p id="commentErrorMessages" class="help-block-error"></p>
	            </div> 
	            <div class="formRow">
	                <?php echo Html::submitButton('ավելացնել', ['id' => 'add-comment-button',  'class' => 'standartBtn orangeBtn', 'name' => 'add-comment-button', 'style' => 'cursor:pointer;']) ?>
                    <a href="#smileys" class="borderBtn orangeBorderBtn mgnPopupBtn">#սմայլիկներ</a>
	            </div>
            <?php ActiveForm::end();?>
        </div>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function(){

    <?php foreach ($cities['cities'] as $city) { ?>
        $('.citiesList').append('<span class="singleCity"><?php echo $city['name']; ?></span>');
    <?php } ?>

    $('.doSearch').on('click', function(){
        $('.answer').focus();
    });

    $('.resetButton').on('click', function(){
        $('.answer').focus();
        window.location = $(this).attr('data-url');
    });

    $('.answer').on('keyup', function(e){        
        var patt = new RegExp("^[a-zA-Z]+$", "i");
        var res = patt.test($(this).val());
        if(!res) {
            $(this).val('');
        }
    });

    $('.answer').on('keypress', function(e){
        $('.answer').css('color', 'black');
	    var code = (e.keyCode ? e.keyCode : e.which);
		var key = String.fromCharCode(code);
		if ($.inArray(code, [46, 8, 9, 20, 27, 13, 110, 190, 116, 144, 17, 35, 36, 37, 38, 39, 40, 16]) === -1) {
            if($(this).val().length == 16) {
                return false;   
            }
			var patt = new RegExp("^[a-zA-Z]+$", "i");
			var res = patt.test(key);
			if(res) {
		  		$(this).val($(this).val()+key.toLowerCase());
		  		return false;
			} else {
		  		return false;
			}
		} else if(code == 13) {
            $('.doSearch').trigger('click');
		}
    });

    $('html, body').animate({
                scrollTop: $(".answer").offset().top-150,
            }, 1500);

    $('.answer').focus();

    $('.doSearch').on('click', function(){
        if($('.answer').val() != '') {
            enter($('.answer').val());
        }
    });

    $(document).on('click', '.commenter', function(){
        window.location = $(this).attr('data-url');
    });

    
    <?php if(!Yii::$app->User->isGuest) { ?>
            var user_id = <?php echo Yii::$app->user->id; ?>;

            var smileys = [
                "baby",
                "black_eye",
                "blink",
                "blush",
                "boredom",
                "clapping",
                "cray",
                "eye",
                "fool",
                "friends",
                "goblin",
                "good",
                "hi",
                "kiss",
                "lazy",
                "lol",
                "lol2",
                "love",
                "mda",
                "not_i",
                "ok",
                "pioneer",
                "rolleyes",
                "scare",
                "scenic",
                "sclerosis",
                "secret",
                "shok",
                "shout",
                "sorry",
                "sos",
                "stop",
                "sing",
                "tease",
                "telephone",
                "this",
                "vayreni",
                "victory",
                "wink",
                "yahoo",
                "zagar"    
            ];


            $('#add-comment-button').on('click', function(){
                $('#commentErrorMessages').text('');
                var commentText = $('#comment-text').val();
                if(commentText == '')
                {
                    $('#commentErrorMessages').text('Ավելացրեք մեկնաբանություն');
                    return false;
                }
                else if(commentText.length < 2 || commentText.length > 200)
                {
                    $('#commentErrorMessages').text('Մեկնաբանություն պետք է լինի 2-ից 200 տառ');
                    return false;
                }
                else {
                    $.ajax({
                        async: false,
                        url: '/user/comment',
                        type: 'POST',
                        data: $('#add-comment-form').serialize(),
                        dataType: 'JSON',
                        success: function(data)
                        {
                            if(data)
                            {
                                commentText = strip_tags(commentText);
                                var index = 0;
                                for(index; index<40; index++)
                                {
                                    commentText = str_replace('('+smileys[index]+')', '<img src="<?php echo Url::base(true);?>/images/smileys/smiley-'+smileys[index]+'.gif">', commentText);
                                }
                                var html = '<li class="commentRow" id="commentNumber'+data.id+'"><div class="commentContainer">'
                                +'<p class="commentText">'+commentText+'</p><p class="commentDate">'+'<span class="commenter" data-url="<?php echo Url::base(true);?>/user/page-view?id='+user_id+'">'+data.username+'</span>'
                                +data.date+months[parseInt(data.month)]+data.dateEnd
                                +'<button class="deleteButton" data-id="'+data.id+'">x</button>'
                                +'</p></div></li>';

                                $('.crossComments ul').prepend(html);

                                $('#comment-text').val('');

                                var count = $('#commentsCount').text().replace(/#/gi, '');
                                count = parseInt(count) + 1;
                                $('#commentsCount').text('#'+count);
                            }
                        }
                    });
                }
                return false;
            });

            var months = [
                '', 'հունվար', 'փետրվար', 'մարտ', 'ապրիլ', 'մայիս', 'հունիս', 'հուլիս', 'օգոստոս', 'սեպտեմբեր', 'հոկտեմբեր', 'նոյեմբեր', 'դեկտեմբեր',
            ];

            function strip_tags( str ){ // Strip HTML and PHP tags from a string            
                return str.replace(/<\/?[^>]+>/gi, '');
            }

            $(document).on('click', '.deleteButton', function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    async: false,
                    url: '/user/delete-comment',
                    type: 'get',
                    data: {
                        id: id,
                    },
                    dataType: 'JSON',
                    success: function(data)
                    {
                        console.log(data);
                        if(data)
                        {                            
                            $('#commentNumber'+id).remove();

                            var count = $('#commentsCount').text().replace(/#/gi, '');
                            count = parseInt(count) - 1;
                            $('#commentsCount').text('#'+count);
                        }
                    }
                });
            });
        <?php } ?>

});

</script>