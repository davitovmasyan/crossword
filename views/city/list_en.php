 <?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title="Crossword.am | Cities";

?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#cities</h1>    
    <div class="shareBox">
        <div class="addthis_sharing_toolbox"></div>
    </div>
    <div class="boxCrosswordList">
        <ul class="crosswordsList">
            <li class="crosswordRow">
                <ul>
                    <li class="crosswordItem">
                        <a href="<?php echo Url::base(true); ?>/en/city/am">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/3e844c6aa26e466d19771989be66dee5.jpg">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/3e844c6aa26e466d19771989be66dee5.jpg');"></div>
                        </a>
                        <h3 title="Armenia" class="crosswordTitle" data-url="<?php echo Url::base(true); ?>/en/city/am">#Republic of Armenia<h3>
                        <h3 data-url="<?php echo Url::base(true); ?>/en/city/am" class="playButtonLink viewButton">play</h3>
                    </li>
                    <li class="crosswordItem">
                        <a href="<?php echo Url::base(true); ?>/en/city/us">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/79331e7c3973510fb5895b0b4f901839.jpg">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/79331e7c3973510fb5895b0b4f901839.jpg');"></div>
                        </a>
                        <h3 title="USA" class="crosswordTitle" data-url="<?php echo Url::base(true); ?>/en/city/us">#USA<h3>
                        <h3 data-url="<?php echo Url::base(true); ?>/en/city/us" class="playButtonLink viewButton">play</h3>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var baseUrl = '<?php echo Url::base(true);?>';

        $('.crosswordTitle').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });

        $('.playButtonLink').on('click', function()
        {
            window.location = $(this).attr('data-url');
        });
        
    });
</script>