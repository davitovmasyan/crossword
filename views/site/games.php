<?php
use yii\helpers\Url;
$this->title="Crossword.am | Խաղեր";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#խաղեր</h1>
    <div class="box">                
        <ul class="gamesList">
            <li>
                <ul class="gameList">
                    <li>
                        <img class="clickAble" data-url="<?php echo Url::base(true); ?>/cross-words/list" src="/images/crosswordgame.jpg">
                    </li>
                    <li>
                        <h1 class="gamesHeading crossHead">#խաչբառ</h1>
                        <p>ձեզ բոլորիդ ծանոթ խաչբառերը արդեն նաև այլ լեզուներով և ավելի հարմարավետ էջում։ ինչպես նաև 4 աստիճանի բարդություն և այլ հետաքրքիր հարմարավետություններ։</p>
                        <button class="standartBtn crossBtn" data-url="<?php echo Url::base(true); ?>/cross-words/list">#խաչբառեր</button><button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/cross-words/random">#պատահական</button>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="gameList">
                    <li>
                        <img class="clickAble" data-url="<?php echo Url::base(true); ?>/sudoku/list" src="/images/sudokugame.jpg">
                    </li>
                    <li>
                        <h1 class="gamesHeading sudoHead">#սուդոկու</h1>
                        <p>հայտնի սուդոկու խաղը շուտով նաև մեզ մոտ տարբեր բարդության աստիճաններով և հարմարավետ ինտերֆեյսով։
                        </p>
                        <button class="standartBtn sudoBtn" data-url="<?php echo Url::base(true); ?>/sudoku/list">#սուդոկուներ</button><button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/sudoku/random">#պատահական</button>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="gameList">
                    <li>
                        <img src="/images/quizgame.jpg" class="clickAble" data-url="<?php echo Url::base(true); ?>/quiz/list">
                    </li>
                    <li>
                        <h1 class="gamesHeading quizHead">#վիկտորինա</h1>
                        <p>նոր և հետաքրքիր խաղ, որտեղ խաղի ժամանակ տրվում են տարբեր բարդության և թեմաներով հարցեր, որոնց պետք է պատասխանել ընտրելով առաջարկվող տարբերակներից մեկը։</p>
                        <button class="standartBtn quizBtn" data-url="<?php echo Url::base(true); ?>/quiz/list">#վիկտորինաներ</button><button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/quiz/random">#պատահական</button>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainCity" data-url="/city/list">
            <h1>#քաղաքներ</h1>
            <p>քաղաք - քաղաք</p>
        </div>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/site/leaderboard">
            <h1>առաջատարներ</h1>
            <p>կուտակեք միավորներ և լրացրեք առաջատարների շարքերը</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.quizBtn, .sudoBtn, .crossBtn, .randomBtn').on('click', function(){
            window.location = $(this).attr('data-url');
        });

        $('.clickAble').on('click', function(){
            window.location = $(this).attr('data-url');
        }).hover(function(){
            $(this).css('cursor', 'pointer');
        });
    });
</script>