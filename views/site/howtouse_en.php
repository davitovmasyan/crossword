<?php 
$this->title="Crossword.am | Help";
?>


<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#help</h1>
	<div class="box">
		<ul>
		<li>
			
			<h1 class="howToHeading">#crossword</h1>

			<p class="howToText">
				բոլոր խաչբառերում կան 2 տեսակ դաշտեր. սպիտակ գույնի դաշտերում
						պետք է տեղադրել տառերը, նարնջագույն գույնի դաշտերը հարցի դաշտերն են
						: հարցերի ուղղությունները հորիզոնական են և ուղղահայաց
						համապատասխանաբար ձախից աջ և վերևից ներքև: այդ ուղղությունները սլաքներով նշվում են հարցի
						դաշտերի վրա սեղմելով:
						երբ դաշտը կանաչում է, մուտքագրված տառը ճիշտ է հակառակ դեպքում դաշտը մնում է սպիտակ:
						դաշտում սխալ մուտքագրված տառը կարող եք ջնջել կամ փոխել ուղղակի այդ նույն դաշտում նոր տառ
						մուտքագրելով: տառերի մուտքագրման քանակը սահմանափակ է այն կախված է խաչբառի դատարկ դաշտերի
						քանակից և խաչբառի բարդության աստիճանից:
			</p>

		</li>
		<li>
			

			<h1 class="howToHeading">#quiz</h1>

			<p class="howToText">
				նոր և հետաքրքիր խաղ, որտեղ խաղի ժամանակ տրվում են տարբեր բարդության և թեմաներով հարցեր, որոնց պետք է պատասխանել ընտրելով առաջարկվող տարբերակներից մեկը։
դուք կարող եք ընտրել ձեզ հետաքրքրող թեմաներով հարցեր կամ պարզապես պատահական հարցեր։
հարցին միայն առաջին անգամ ճիշտ պատասխանելու դեպքում է ձեզ տրվում միավոր։
			</p>
		</li>

	</ul>
	</div>
	<?php echo $this->renderFile($this->findViewFile('contactButtons_en')); ?>   
	<div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	});
</script>