<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
$this->title="Crossword.am | Связь";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#связаться с нами</h1>
    <div class="box">
      <div class="contactCover">

      </div>
      <div class="contactInfo">
        <ul>
          <li class="contactItem email" title="почта">
            info@crossword.am
          </li>
          <li class="contactItem location" title="адрес">
            Yerevan, Armenia        
          </li>
          <li class="contactItem mobile" title="телефон">
            +374(77)590-123
          </li>
        </ul>
      </div>
      <div class="contactForm">
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
              <div class="formRow">
                  <?php echo $form->field($model, 'name')->label('Имя'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'email')->label('Почта'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'message')->textArea(['rows' => 3])->label('Текст'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                      'template' => '<div class="captchaImage">{image}</div><div class="captchainput">{input}</div>',
                  ])->label('Код защиты'); ?>
              </div>
              <div class="submitSect">
                  <?php echo Html::submitButton('Отправить', ['class' => 'borderBtn orangeBorderBtn', 'name' => 'contact-button']); ?>
              </div>
      </div>
      <div class="clear">
      </div>
   </div>
   <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){       
       $('#contactform-verifycode-image ').click(function() {
         src = $('#contactform-verifycode-image ').attr('src');
          // check for existing ? and remove if found
         queryPos = src.indexOf('?');
         if(queryPos != -1) {
            src = src.substring(0, queryPos);
         }    
         $('#contactform-verifycode-image').attr('src', src + '?' + Math.random());
         return false;
      });
    });
</script>