<?php

$this->title="Crossword.am | Discussions";

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#discussions</h1>
    <div class="box">
    	<?php if(!empty($discussions)) { ?>
    	<ul>
    		<?php foreach($discussions as $d) { ?>
    		<li class="discussions">
    			<a href="/en/site/discussions?id=<?php echo $d['id'];?>"><?php echo $d['name']; ?> - (<?php echo $d['count']; ?>)</a>
    		</li>
    		<?php } ?>
    	</ul>
    	<?php } else { ?>
    	<h4 class="noInfSt">
    		no discussions
    	</h4>
    	<?php } ?>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_en')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>