<?php

use app\models\Comments;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Crossword.am | '.$post['title'];
$months = [
    '01' => 'january',
    '02' => 'february',
    '03' => 'march',
    '04' => 'april',
    '05' => 'may',
    '06' => 'june',
    '07' => 'jule',
    '08' => 'august',
    '09' => 'september',
    '10' => 'october',
    '11' => 'november',
    '12' => 'december', 
];

$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"    
];

?>
<link rel="stylesheet" type="text/css" href="/css/post.css">
<script type="text/javascript" src="/js/post.js"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55570d887146a497" async="async"></script>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#blog <a href="/en/site/blog" class="backBtn">#articles</a></h1>
    <div class="shareBox">
        <div class="addthis_sharing_toolbox"></div>
    </div>
    <div class="boxPost">
        <h1 class="postTitle"><?php echo $post['title'];?></h1>
        <span class="scrollToComments">#comments</span>
        <span class="postDate"><?php echo substr($post['created'], 8, 2),' ',$months[substr($post['created'], 5, 2)],' ',substr($post['created'], 0, 4);?></span>
        <div class="postMainImg"><img src="/images/uploads/<?php echo $post['image'];?>" title=""></div>
        <div class="mainContent">
            <?php echo $post['post'];?>
        </div>
        <div class="keywords">
            <?php 
                $keywords = explode(' ', $post['keywords']);
                foreach ($keywords as $key) {
                    echo ' <span class="keyword" data-tag="',$key,'">#',$key,'</span>';
                }
            ?>
        </div>
        <div class="bottomShareBox">
            <div class="addthis_sharing_toolbox"></div>
        </div>
	</div>
    <?php if($suggestions) { ?>
    <div class="suggestions">
        <ul>
            <?php foreach($suggestions as $sug) { ?>
                <li class="suggestion" data-url="/en/site/post?id=<?php echo $sug['id'];?>">
                    <h1 class="title"><?php echo $sug['title'];?></h1>
                    <img src="/images/uploads/<?php echo $sug['image'];?>">
                    <span class="suggestionDate"><?php echo substr($sug['created'], 8, 2),' ',$months[substr($sug['created'], 5, 2)],' ',substr($sug['created'], 0, 4);?></span>
                </li>
            <?php } ?>
        </ul>
    </div>
    <?php } ?>
	<div class="boxComments">
    	<h4 class="commentsCount">comments <span id="commentsCount">#<?php echo count($comments); ?></span></h4>
		<div class="postComments customScroll">
			<ul>
				<?php if(!empty($comments)) { ?>
					<?php foreach($comments as $comment) { ?>
                        <?php 
                            foreach($smileys as $smiley) { 
                                $comment['comment'] = str_replace('('.$smiley.')', '<img src="'.Url::base(true).'/images/smileys/smiley-'.$smiley.'.gif">', $comment['comment']);
                            } 
                        ?>
						<li class="commentRow" id="commentNumber<?php echo $comment['id'];?>">
		                    <div class="commentContainer">
		                            <p class="commentText"><?php echo $comment['comment'];?></p>
		                            <p class="commentDate"><span class="commenter" data-url="<?php echo Url::base(true);?>/en/user/page-view?id=<?php echo $comment['uid'];?>"><?php echo $comment['username'];?></span> <?php echo substr($comment['created'], 10, 6),', ',substr($comment['created'], 8, 2),' ',$months[substr($comment['created'], 5, 2)],' ',substr($comment['created'], 0, 4);?>
		                            	<?php if(!Yii::$app->User->isGuest && $comment['user_id'] == Yii::$app->User->id) { ?>
		                            	<button class="deleteButton" data-id="<?php echo $comment['id'];?>">x</button>
		                            	<?php } ?>
		                            </p>
		                    </div>
		                </li>
	                <?php } ?>
                <?php } ?>
			</ul>
		</div>				
		<?php if(!Yii::$app->User->isGuest) { ?>
		<div>
			<?php 
                $comment = new Comments(); 
                $comment->game_id = $post['id'];
                $comment->game_type = 'post';
            ?>
        	<?php $form = ActiveForm::begin([
            'id' => 'add-comment-form',
            'action' => '/en/user/comment',
                    'fieldConfig' => [
                        'template' => '{input}'
                        ],
            ]);?>
	            <div class="formRow">
	            	<?php echo $form->field($comment, 'game_id')->hiddenInput(); ?>
	            	<?php echo $form->field($comment, 'game_type')->hiddenInput(); ?>
	                <?php echo $form->field($comment, 'comment')->textArea(['id' => 'comment-text', 'rows' => 3]); ?>
                    <p id="commentErrorMessages" class="help-block-error"></p>
	            </div> 
	            <div class="formRow">
	                <?php echo Html::submitButton('add', ['id' => 'add-comment-button',  'class' => 'standartBtn orangeBtn', 'name' => 'add-comment-button', 'style' => 'cursor:pointer;']) ?>
                    <a href="#smileys" class="borderBtn orangeBorderBtn mgnPopupBtn">#smileys</a>
	            </div>
            <?php ActiveForm::end();?>
        </div>
        <?php } ?>
    </div>    
</div>
<script type="text/javascript">
$(document).ready(function(){
        $('.suggestion').on('click', function(){
            window.location = $(this).attr('data-url');
        });
        $('.mainContent').magnificPopup({
            delegate: 'img',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element;
                }
            }
        });

		$('.scrollToComments').on('click', function()
        {
             $('html, body').animate({
                scrollTop: $(".boxComments").offset().top
            }, 500);

            return false;
        });

        $(document).on('click', '.commenter', function(){
            window.location = $(this).attr('data-url');
        });


		<?php if(!Yii::$app->User->isGuest) { ?>
            var user_id = <?php echo Yii::$app->user->id; ?>;

            var smileys = [
                "baby",
                "black_eye",
                "blink",
                "blush",
                "boredom",
                "clapping",
                "cray",
                "eye",
                "fool",
                "friends",
                "goblin",
                "good",
                "hi",
                "kiss",
                "lazy",
                "lol",
                "lol2",
                "love",
                "mda",
                "not_i",
                "ok",
                "pioneer",
                "rolleyes",
                "scare",
                "scenic",
                "sclerosis",
                "secret",
                "shok",
                "shout",
                "sorry",
                "sos",
                "stop",
                "sing",
                "tease",
                "telephone",
                "this",
                "vayreni",
                "victory",
                "wink",
                "yahoo",
                "zagar"    
            ];


			$('#add-comment-button').on('click', function(){
                $('#commentErrorMessages').text('');
                var commentText = $('#comment-text').val();
                if(commentText == '')
                {
                    $('#commentErrorMessages').text('Write a comment');
                    return false;
                }
                else if(commentText.length < 2 || commentText.length > 200)
                {
                    $('#commentErrorMessages').text('Comment must be between 2 and 200 symbols');
                    return false;
                }
                else {
                    $.ajax({
                        async: false,
                        url: '/en/user/comment',
                        type: 'POST',
                        data: $('#add-comment-form').serialize(),
                        dataType: 'JSON',
                        success: function(data)
                        {
                            if(data)
                            {
                                commentText = strip_tags(commentText);
                                var index = 0;
                                for(index; index<40; index++)
                                {
                                    commentText = str_replace('('+smileys[index]+')', '<img src="<?php echo Url::base(true);?>/images/smileys/smiley-'+smileys[index]+'.gif">', commentText);
                                }
                                var html = '<li class="commentRow" id="commentNumber'+data.id+'"><div class="commentContainer">'
                                +'<p class="commentText">'+commentText+'</p><p class="commentDate">'+'<span class="commenter" data-url="<?php echo Url::base(true);?>/en/user/page-view?id='+user_id+'">'+data.username+'</span>'
                                +data.date+months[parseInt(data.month)]+data.dateEnd
                                +'<button class="deleteButton" data-id="'+data.id+'">x</button>'
                                +'</p></div></li>';

                                $('.postComments ul').prepend(html);

                                $('#comment-text').val('');

                                var count = $('#commentsCount').text().replace(/#/gi, '');
                                count = parseInt(count) + 1;
                                $('#commentsCount').text('#'+count);
                            }
                        }
                    });
                }
				return false;
			});

            var months = [
                '', 'january', 'february', 'march', 'april', 'may', 'june', 'jule', 'august', 'september', 'october', 'november', 'december',
            ];

            function strip_tags( str ){ // Strip HTML and PHP tags from a string            
                return str.replace(/<\/?[^>]+>/gi, '');
            }

            $(document).on('click', '.deleteButton', function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    async: false,
                    url: '/en/user/delete-comment',
                    type: 'get',
                    data: {
                        id: id,
                    },
                    dataType: 'JSON',
                    success: function(data)
                    {
                        console.log(data);
                        if(data)
                        {                            
                            $('#commentNumber'+id).remove();

                            var count = $('#commentsCount').text().replace(/#/gi, '');
                            count = parseInt(count) - 1;
                            $('#commentsCount').text('#'+count);
                        }
                    }
                });
            });
		<?php } ?>
	});
</script>