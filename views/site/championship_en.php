<?php

$this->title = 'Crossword.am | Championship';

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#championship</h1>
    <div class="box">
        <h1 class="commingSoon">#comming soon</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_en')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
    <div class="box">
        <div class="mainGamesBox" data-url="/en/site/games">
            <h1>games</h1>
            <p>our games</p>
        </div>
    </div>
</div>