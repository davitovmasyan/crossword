<?php

use yii\widgets\LinkPager;
$this->title="Crossword.am | Այլ";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#այլ խաղեր</h1>
    <div class="box">
        <h1 class="commingSoon">#շուտով</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>