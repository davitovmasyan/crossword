<?php

$this->title = 'Crossword.am | Соревнования';

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#соревнования</h1>
    <div class="box">
        <h1 class="commingSoon">#скоро</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_ru')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
    <div class="box">
        <div class="mainGamesBox" data-url="/ru/site/games">
            <h1>игры</h1>
            <p>наши игры</p>
        </div>
    </div>
</div>