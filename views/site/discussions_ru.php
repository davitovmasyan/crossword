<?php

$this->title="Crossword.am | Обсуждения";

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#обсуждения</h1>
    <div class="box">
    	<?php if(!empty($discussions)) { ?>
    	<ul>
    		<?php foreach($discussions as $d) { ?>
    		<li class="discussions">
    			<a href="/ru/site/discussions?id=<?php echo $d['id'];?>"><?php echo $d['name']; ?> - (<?php echo $d['count']; ?>)</a>
    		</li>
    		<?php } ?>
    	</ul>
    	<?php } else { ?>
    	<h4 class="noInfSt">
    		нет обсуждений
    	</h4>
    	<?php } ?>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_ru')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>