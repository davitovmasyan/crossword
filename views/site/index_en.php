<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title="Crossword.am | Home";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#home</h1>    
    <div class="box">
        <div class="mainCity" data-url="/en/city/list">
            <h1>#cities</h1>
            <p>city - city</p>
        </div>
    </div>
    <div class="box">
        <div class="mainQuiz" data-url="<?php echo Url::base(true); ?>/en/quiz/list">
            <h1>#quiz</h1>
            <p>quiz game</p>
        </div>
        <ul class="mainGames">
            <li class="mainCross" data-url="<?php echo Url::base(true); ?>/en/cross-words/list">
                <h1>#crossword</h1>
                <p>many languages</p>
            </li>
            <li class="mainSudoku" data-url="<?php echo Url::base(true); ?>/en/sudoku/list">
                <h1>#sudoku</h1>
                <p>comming soon</p>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/en/site/blog">
            <h1>blog</h1>
            <p>awsome news</p>
        </div>
    </div>
</div>    

<?php if(isset($reset_password)) { ?>
<a id="openReset" href="#resetPopup" class="mgnPopupBtn" style="display:none;"></a>
<script type="text/javascript">
    $(document).ready(function(){
        $('#openReset').trigger('click');
    });
</script>
<div id="resetPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#reset password</h5>
    </div>
    <div class="popupCont">
        <?php $form = ActiveForm::begin([
            'id' => 'password-reset-form',
            'method' => 'post']); ?>
        <div class="formRow">
            <label>#new password</label>
            <?php echo $form->field($reset_password, 'password')->label(false); ?>
        </div>
        <div class="formRow">
            <label>#confirm password</label>
            <?php echo $form->field($reset_password, 'password_repeat')->label(false); ?>
        </div>
        <div class="formRow">
            <input type="submit" value="accept" class="borderBtn orangeBorderBtn">
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php } ?>


<script type="text/javascript">
        $(document).ready(function(){
            $('.mainQuiz, .mainCross, .mainSudoku, .mainFb, .mainCity, #myCanvas').on('click', function(){
                if($(this).attr('data-target') == 'new')
                {
                    window.open($(this).attr('data-url'),'_blank');
                    return false;
                }
                window.location = $(this).attr('data-url');
            });
        });
</script>