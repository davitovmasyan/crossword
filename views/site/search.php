<?php

use yii\helpers\Url;

$this->title="Crossword.am | Որոնում";

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#որոնում</h1>
	<div class="boxQuizList">
        <?php if(!empty($results)) { ?>
        <ul class="quizList">
            <?php $index = 1; ?>
            <?php foreach($results as $row) { ?>
            <?php if($index == 1) { ?>
            <li class="quizRow">
                <ul>
            <?php } ?>
            <li class="quizItem">
            <?php if($row['type'] == 'crossword') { ?>
                    
                        <a href="<?php echo Url::base(true); ?>/cross-words/play?id=<?php echo $row['id']; ?>" target="_blank">
                            <img src="<?php echo Url::base(true); ?>/images/crosswords/<?php echo $row['image']; ?>">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/crosswords/<?php echo $row['image']; ?>');"></div>
                        </a>
                        <h3 title="" class="quizTitle" data-url="<?php echo Url::base(true); ?>/cross-words/play?id=<?php echo $row['id']; ?>"><?php echo $row['title']; ?><h3>
                        <div class="clear"></div>
                        <h3 data-url="<?php echo Url::base(true); ?>/cross-words/play?id=<?php echo $row['id']; ?>" class="quizPlayButtonLink quizPlayButton">Բացել</h3>
                    
                    <?php }  else if($row['type'] == 'quiz') { ?>
                        <a href="<?php echo Url::base(true); ?>/quiz/play?id=<?php echo $row['id']; ?>" target="_blank">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/<?php echo $row['image']; ?>">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/<?php echo $row['image']; ?>');"></div>
                        </a>
                        <h3 title="" class="quizTitle" data-url="<?php echo Url::base(true); ?>/quiz/play?id=<?php echo $row['id']; ?>"><?php echo $row['title']; ?><h3>
                        <div class="clear"></div>
                        <h3 data-url="<?php echo Url::base(true); ?>/quiz/play?id=<?php echo $row['id']; ?>" class="quizPlayButtonLink quizPlayButton">Բացել</h3>
                    <?php } else { ?>
                        <a href="<?php echo Url::base(true); ?>/site/post?id=<?php echo $row['id']; ?>" target="_blank">
                            <img src="<?php echo Url::base(true); ?>/images/uploads/<?php echo $row['image']; ?>">
                            <div class="imageDiv" style="background-image: url('<?php echo Url::base(true); ?>/images/uploads/<?php echo $row['image']; ?>');"></div>
                        </a>
                        <h3 title="" class="quizTitle" data-url="<?php echo Url::base(true); ?>/site/post?id=<?php echo $row['id']; ?>"><?php echo $row['title']; ?><h3>
                        <div class="clear"></div>
                        <h3 data-url="<?php echo Url::base(true); ?>/site/post?id=<?php echo $row['id']; ?>" class="quizPlayButtonLink quizPlayButton">Բացել</h3>
                    <?php } ?>

                    </li>
            <?php if($index == 6) { ?>
                </ul>
            </li>
            <?php 
            $index = 1; } else {
                $index++;
            }
            ?>
            <?php } ?>
            <?php if($index != 1) { ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <h4 class="noInfSt">արդյունքներ չկան</h4>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainCity" data-url="/city/list">
            <h1>#քաղաքներ</h1>
            <p>քաղաք - քաղաք</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am" data-target="new">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function(){
        $('.quizTitle').on('click', function()
        {
            window.open($(this).attr('data-url'));
        });

        $('.quizPlayButtonLink').on('click', function()
        {
            window.open($(this).attr('data-url'));
        });
    });
</script>