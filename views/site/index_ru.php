<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title="Crossword.am | Главная";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#главная</h1>    
    <div class="box">
        <div class="mainCity" data-url="/ru/city/list">
            <h1>#города</h1>
            <p>город - город</p>
        </div>
    </div>
    <div class="box">
        <div class="mainQuiz" data-url="<?php echo Url::base(true); ?>/ru/quiz/list">
            <h1>#викторина</h1>
            <p>викторина</p>
        </div>
        <ul class="mainGames">
            <li class="mainCross" data-url="<?php echo Url::base(true); ?>/ru/cross-words/list">
                <h1>#кроссворды</h1>
                <p>самые интересные кроссворды</p>
            </li>
            <li class="mainSudoku" data-url="<?php echo Url::base(true); ?>/ru/sudoku/list">
                <h1>#судоку</h1>
                <p>скоро</p>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/ru/site/blog">
            <h1>блог</h1>
            <p>интересные публикации</p>
        </div>
    </div>
</div>    

<?php if(isset($reset_password)) { ?>
<a id="openReset" href="#resetPopup" class="mgnPopupBtn" style="display:none;"></a>
<script type="text/javascript">
    $(document).ready(function(){
        $('#openReset').trigger('click');
    });
</script>
<div id="resetPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#изменение пароля</h5>
    </div>
    <div class="popupCont">
        <?php $form = ActiveForm::begin([
            'id' => 'password-reset-form',
            'method' => 'post']); ?>
        <div class="formRow">
            <label>#новый пароль</label>
            <?php echo $form->field($reset_password, 'password')->label(false); ?>
        </div>
        <div class="formRow">
            <label>#пов. пароль</label>
            <?php echo $form->field($reset_password, 'password_repeat')->label(false); ?>
        </div>
        <div class="formRow">
            <input type="submit" value="изменить" class="borderBtn orangeBorderBtn">
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php } ?>


<script type="text/javascript">
        $(document).ready(function(){
            $('.mainQuiz, .mainCross, .mainSudoku, .mainFb, .mainCity, #myCanvas').on('click', function(){
                if($(this).attr('data-target') == 'new')
                {
                    window.open($(this).attr('data-url'),'_blank');
                    return false;
                }
                window.location = $(this).attr('data-url');
            });
        });
</script>