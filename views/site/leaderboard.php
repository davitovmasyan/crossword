<?php
use yii\helpers\Url;
$this->title="Crossword.am | Առաջատարներ";
$map_leaders = [];

?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="/js/markerclusterer.js"></script>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#առաջատարներ</h1>
	<div class="box">
		<?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->attributes['points'] > 0) { ?>
			<button class="toMeButton" title="դեպի ինձ">#ես</button>
		<?php } ?>
		<div class="listNames">
			<ul>
				<li style="width: 6%;">#տեղ</li>
				<li style="width: 6%;">#լօգօ</li>
				<li style="width: 16%;">#մուտքանուն</li>
				<li style="width: 15%;">#միավորներ</li>
				<li style="width: 16%;">#գումարածներ</li>
				<li style="width: 16%;">#երկիր</li>
			</ul>
		</div>
		<ul class="leaderboardList leaderboardCustomScroll">				
			<?php $i=1; foreach ($leaders as $leader) { ?>
			<?php if($leader['lat_lng']) { $map_leaders[] = ['username' => $leader['username'], 'lat_lng' => $leader['lat_lng'], 'id' => $leader['id']]; } ?>
			<li title="դիտել" data-url="<?php echo Url::base(true); ?>/user/page-view?id=<?php echo $leader['id']; ?>" class="singleLeader <?php echo $leader['id'] == Yii::$app->User->id ? 'singleLeaderMe' : ''; ?>">
				<ul>
					<li style="width: 6%;">#<?php echo $i++; ?></li>
					<li style="width: 6%;"><img src="/images/users/<?php echo $leader['username'];?>.png"></li>
					<li style="width: 16%;">#<?php echo $leader['username'];?></li>
					<li style="width: 15%;"><?php echo $leader['points'];?></li>
					<li style="width: 16%;">+<?php echo $leader['pluses'];?></li>
					<li style="width: 16%;"><?php echo $leader['country'] ? $leader['country'] : 'նշված չէ';?></li>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</div>
	<?php if(!empty($map_leaders)) { ?>
	<div class="box" title="առաջատերների քարտեզ">
		<div id="map-canvas" style="width: 100%; height: 400px;">
		</div>
	</div>
	<?php } ?>
	<div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>
<script type="text/javascript">
	<?php if(!empty($map_leaders)) { ?>
	function initialize() {
	  	var data = [
	      	<?php foreach ($map_leaders as $leader) {
		        	$marker_lat_lng = explode(',', $leader['lat_lng']);
		        	$title = $leader['username']; 
		        	$userid = $leader['id']; ?>
		        	{
	        		"userid": "<?php echo $userid;?>",
		          	"user": "<?php echo $title;?>",
		          	"lat": <?php echo $marker_lat_lng['0'];?>,
		          	"lng": <?php echo $marker_lat_lng['1'];?>,
		        	},               	 
		       	 
	      	<?php }?>  
	  	];

   	 

    	var markers = [];
    	var latlngbounds = new google.maps.LatLngBounds();
    	var countOfData = data.length
    	for (var i = 0; i < countOfData; i++) {
	      	var latLng = new google.maps.LatLng(data[i].lat,
	          	data[i].lng);
	      	var marker = new google.maps.Marker({
	        	position: latLng,
	        	title: data[i].user,
	      	});
	      	markers.push(marker);
	      	latlngbounds.extend(marker.position);
	      	var content = "<div class='infoWindow'>"+
                "<a href='/user/page-view?id="+data[i].userid+"' class='userName'>"+data[i].user+"</a>"+
                "</div>";

	        var infowindow = new google.maps.InfoWindow()
	          	google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
	                return function() {
	                   infowindow.setContent(content);
	                   infowindow.open(map,marker);
	                };
	            })(marker,content,infowindow));
		}

		 
   	 

		var map = new google.maps.Map(document.getElementById('map-canvas'), {
	  	zoom: 3,             	 
	  	// scrollwheel:false,
	  	mapTypeId: google.maps.MapTypeId.ROADMAP,
		});

		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);
		var markerCluster = new MarkerClusterer(map, markers);   	 
  	}
  	google.maps.event.addDomListener(window, 'load', initialize);
  	<?php } ?>

	$(document).ready(function(){
		$(".leaderboardCustomScroll").mCustomScrollbar({
	        scrollbarPosition: 'outside',
	        scrollInertia: 1500,
	        autoHideScrollbar: true
	    });

	    function scrollToMe()
	    {
	    	$(".leaderboardCustomScroll").mCustomScrollbar("scrollTo",".singleLeaderMe",{
			    scrollEasing:"easeOut"
			});
	    }
	    
	    scrollToMe();

	    $('.toMeButton').on('click', function(){
	    	scrollToMe();
	    });

	    $('.singleLeader').on('click', function(){
	    	window.location = $(this).attr('data-url');
	    });
	});
</script>