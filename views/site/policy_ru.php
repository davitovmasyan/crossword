<?php 

$this->title="Crossword.am | Правила";

?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#правила</h1>
	<div class="box">
		<ul>
		<li>
			
			<h1 class="privacyHeading">#регистрация</h1>

			<p class="privacyText">
				խորհուրդ է տրվում օգտագործել իրական տվյալներ գրանցման ժամանակ հետագայում խնդիրներից խուսափելու նպատակով։
			</p>

		</li>
		<li>
			

			<h1 class="privacyHeading">#пользователи</h1>

			<p class="privacyText">
				օգտատերերի մասին ինֆորմացիան մասամբ բաց է բոլոր այցելուների համար (<span class="orangeText">մուտքանուն, միավորներ, կոորդինատներ և գումարածներ</span>)
			</p>
		</li>

		<li>
			

			<h1 class="privacyHeading">#куки</h1>

			<p class="privacyText">
				մենք ինչպես այլ կայքերի մեծամասնությունը օգտագործում ենք քուքիները կայքի աշխատանքի արագ կազմակերպման և ձեր հարմարավետության ապահովման նպատակներով։
			</p>
		</li>

		<li>
			

			<h1 class="privacyHeading">#проблемы</h1>

			<p class="privacyText">
				չի թույլատրվում մեկնաբանություններում չարություն անել և օգտագործել այլ մարդկանց տվյալներ հակառակ դեպքում ձեր էջը կարգելափակվի։
			</p>
		</li>
	</ul>
	</div>
	<?php echo $this->renderFile($this->findViewFile('contactButtons_ru')); ?>   
	<div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>