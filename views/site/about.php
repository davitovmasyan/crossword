<?php
use yii\widgets\LinkPager;
$this->title="Crossword.am | Մեր մասին";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#նախագծի մասին</h1>
    <script type="text/javascript" src="http://s3.amazonaws.com/codecademy-content/courses/hour-of-code/js/alphabet.js"></script>
    <canvas id="myCanvas" style="display: block; margin:0 auto; cursor:pointer;" data-url="https://www.facebook.com/crossword.am" data-target="new"></canvas>
    <script type="text/javascript" src="http://s3.amazonaws.com/codecademy-content/courses/hour-of-code/js/bubbles.js"></script>
    <script type="text/javascript">
      var myName = "Crossword.am";

      var red = [0, 100, 63];
      var orange = [40, 100, 60];
      var green = [75, 100, 40];
      var blue = [196, 77, 55];
      var purple = [280, 50, 60];
      var letterColors = [red, orange, green, blue, purple];
      
      drawName(myName, letterColors);
      
      if(10 < 3)
      {
          bubbleShape = 'square';
      }
      else
      {
          bubbleShape = 'circle';
      }
      
      bounceBubbles();
    </script>
    <div class="box">
        <ul>
            <li>
                
                <h1 class="aboutHeading">#խաչբառի մասին</h1>

                <p class="aboutText">Խաչբառ (անգլ.՝ <span class="bold orangeText">Crossword</span>, բառացի՝ «խաչվող բառեր»), ամենատարածված բառախաղն աշխարհում։ Անհրաժեշտ է գտնել առաջադրվող հարցի պատասխանը և տեղադրել խաչբառի ցանցի համապատասխան համարով վանդակներում։ Լրացված բառերը հատվում են միմյանց, ինչն օգնում է խաչբառը լուծելուն։ Գոյություն ունեն բազմաթիվ թերթեր, հանդեսներ, որոնք մասնագիտացած են խաչբառեր հրապարակելու գործում։ Խաչբառերի տեսակներից է <span class="bold orangeText">Հայկական խաչբառը</span>:
                </p>

            </li>
            <li>
                

                <h1 class="aboutHeading">#հայկական խաչբառ</h1>

                <p class="aboutText">Հայկական խաչբառերում, ի տարբերություն խաչբառերի մնացած բոլոր տեսակների, հարցադրման համար կարելի է օգտագործել տվյալ լեզվում հայտնի բոլոր բառերը, անկախ նրանից, թե դրանք գոյական են, բայ, ձայնարկություն, թե մի այլ խոսքի մաս, բառի ուղիղ ձև են, թե հոլովված կամ խոնարհված բառաձևեր։ Այսինքն՝ այս խաչբառում լեզվի բոլոր բառերն ու բառաձևերը իրավահավասար են։ Պատասխանների ծածկագրման համար այստեղ ընդունելի է ցանկացած հնարք՝ բացատրություն, հոմանիշ, հականիշ, նկարելուկ, բառքամոցի, հանելուկ, նկար, տրամաբանական խնդիր, մեջբերում, բառախաղ, օրինաչափություն, գաղտնագիր և այլն։ Շատերս ենք սիրում լուծել խաչբառեր, և ժամանակն է, երբ կարող ենք անել դա <span class="bold orangeText">օնլայն: </span>
                </p>
            </li>

            <li>
                

                <h1 class="aboutHeading">#օնլայն խաչբառ</h1>

                <p class="aboutText">
                    Խաչբառի օնլայն տարբերակը հնարավորություն է տալիս այցելուներին ցանկացած պահի, լինի դա երթուղայինում, աշխատավայրում, թե պարզապես տանը նստած, զվարճանալ կամ ստուգել գիտելիքները:
                    Օնլայն խաչբառի առավելությունները. <span class="orangeText bold">հասանելիություն հետաքրքիր, ինտելեկտուալ և չկրկնվող հարցեր անընդհատ թարմացվող կոնտենտ</span> առանձնացված <span class="orangeText bold">չորս</span> աստիճանի բարդության խաչբառը պահպանելու հնարավորություն Գրանցվելով օնլայն խաչբառում՝ կուտակում եք միավորներ, որոնք հետագայում հնարավորություն են տալիս մասնակցել մրցույթի և ստանալ մրցանակներ: Օնլայն խաչբառը բաժանված է թեմաների և յուրաքանչյուր հետաքրքրությունների տեր այցելուն կարող է լրացնել իր նախընտրած խաչբառը: Առանձնացված թեմաները. <span class="orangeText bold">#առօրյա, #պատմական, #աշխարհագրական, #գիտական, #այլ</span>:
                </p>
            </li>
        </ul>
    </div>
    <div style="width:630px; margin: 0 auto; text-align:center; padding-bottom:50px;">
        <script type="text/javascript" charset="utf-8" src="http://static.polldaddy.com/p/9172583.js"></script>
        <noscript><a href="http://polldaddy.com/poll/9172583/">Ինչպես կգնահատեք կայքը՞</a></noscript>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
    <div class="box">
        <div class="mainGamesBox" data-url="/site/games">
            <h1>խաղեր</h1>
            <p>մեր խաղերը</p>
        </div>
    </div>
</div>