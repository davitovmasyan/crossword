<?php

use yii\bootstrap\ActiveForm;
use app\models\ResetPasswordForm;
?>
<div class="mainCont">
    <div class="containerReset">
        <div class="pageTitleCont">
            <h3 class="pageTitle">Գաղտնաբառի փոփոխություն</h3>
        </div>
<?php if(isset($_GET['token']))  { ?>
    <?php $resetPassword = new ResetPasswordForm($_GET['token']); ?>    
            <?php $form = ActiveForm::begin([
                'id' => 'password-reset-form',
                'action' => '/site/reset-password?token=' . $_GET['token'],
                'fieldConfig' => [
                        'template' => '{input}<p class="help-block"></p>'
                        ]]);?>
            <div class="formRow">
                <label for="passwordresetform_password">Նոր գաղտնաբառ</label>
                <?php echo $form->field($resetPassword, 'password')->passwordInput()->label(false);?>
            </div>
            <div class="formRow">
                <label for="passwordresetform_password_repeat">Նոր գաղտնաբառի կրկ.</label>
                <?php echo $form->field($resetPassword, 'password_repeat')->passwordInput()->label(false);?>
            </div>
            <div class="submitSect">
                <input type="submit" class="btn basicBtn blockBtn" value="Հաստատել">
            </div>
            <?php ActiveForm::end(); ?>    
    <?php } ?>
    </div>
</div>