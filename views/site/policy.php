<?php 

$this->title="Crossword.am | Կանոններ";

?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#կանոններ</h1>
	<div class="box">
		<ul>
		<li>
			
			<h1 class="privacyHeading">#գրանցման մասին</h1>

			<p class="privacyText">
				խորհուրդ է տրվում օգտագործել իրական տվյալներ գրանցման ժամանակ հետագայում խնդիրներից խուսափելու նպատակով։
			</p>

		</li>
		<li>
			

			<h1 class="privacyHeading">#օգտատերերի մասին</h1>

			<p class="privacyText">
				օգտատերերի մասին ինֆորմացիան մասամբ բաց է բոլոր այցելուների համար (<span class="orangeText">մուտքանուն, միավորներ, կոորդինատներ և գումարածներ</span>)
			</p>
		</li>

		<li>
			

			<h1 class="privacyHeading">#քուքի</h1>

			<p class="privacyText">
				մենք ինչպես այլ կայքերի մեծամասնությունը օգտագործում ենք քուքիները կայքի աշխատանքի արագ կազմակերպման և ձեր հարմարավետության ապահովման նպատակներով։
			</p>
		</li>

		<li>
			

			<h1 class="privacyHeading">#կանոնների խախտում</h1>

			<p class="privacyText">
				չի թույլատրվում մեկնաբանություններում չարություն անել և օգտագործել այլ մարդկանց տվյալներ հակառակ դեպքում ձեր էջը կարգելափակվի։
			</p>
		</li>
	</ul>
	</div>
	<?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
	<div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>