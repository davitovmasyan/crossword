<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\DiscussionTexts;

$this->title = 'Crossword.am | Обсуждение';

$months = [
    '01' => 'январь',
    '02' => 'февраль',
    '03' => 'март',
    '04' => 'апрель',
    '05' => 'май',
    '06' => 'июнь',
    '07' => 'июль',
    '08' => 'август',
    '09' => 'сентябрь',
    '10' => 'октябрь',
    '11' => 'ноябрь',
    '12' => 'декабрь',  
];

$smileys = [
    "baby",
    "black_eye",
    "blink",
    "blush",
    "boredom",
    "clapping",
    "cray",
    "eye",
    "fool",
    "friends",
    "goblin",
    "good",
    "hi",
    "kiss",
    "lazy",
    "lol",
    "lol2",
    "love",
    "mda",
    "not_i",
    "ok",
    "pioneer",
    "rolleyes",
    "scare",
    "scenic",
    "sclerosis",
    "secret",
    "shok",
    "shout",
    "sorry",
    "sos",
    "stop",
    "sing",
    "tease",
    "telephone",
    "this",
    "vayreni",
    "victory",
    "wink",
    "yahoo",
    "zagar"    
];
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#<?php echo $discussionName;?></h1>
	<div class="boxDiscussion">
		<h4 class="discussionCount">обсуждения <span id="discussionsCount">#<?php echo count($discussion); ?></span></h4>
		<div class="crossDiscussion customScroll">
			<ul>
				<?php if(!empty($discussion)) { ?>
					<?php foreach($discussion as $d) { ?>
	                    <?php 
	                        foreach($smileys as $smiley) { 
	                            $d['text'] = str_replace('('.$smiley.')', '<img src="'.Url::base(true).'/images/smileys/smiley-'.$smiley.'.gif">', $d['text']);
	                        } 
	                    ?>
						<li class="discussionRow" id="discussionNumber<?php echo $d['id'];?>">
		                    <div class="discussionContainer">
		                            <p class="discussionText"><?php echo $d['text'];?></p>
		                            <p class="discussionDate"><span class="writer" data-url="<?php echo Url::base(true);?>/ru/user/page-view?id=<?php echo $d['user_id'];?>"><?php echo $d['username'];?></span> <?php echo substr($d['created'], 10, 6),', ',substr($d['created'], 8, 2),' ',$months[substr($d['created'], 5, 2)],' ',substr($d['created'], 0, 4);?>
		                            	<?php if(!Yii::$app->User->isGuest && $d['user_id'] == Yii::$app->User->id) { ?>
		                            	<button class="deleteButton" data-id="<?php echo $d['id'];?>">x</button>
		                            	<?php } ?>
		                            </p>
		                    </div>
		                </li>
	                <?php } ?>
	            <?php } ?>
			</ul>
		</div>				
		<?php if(!Yii::$app->User->isGuest) { ?>
		<div>
			<?php 
	            $discussion = new DiscussionTexts(); 
	            $discussion->discussion_id = $id;
	        ?>
	    	<?php $form = ActiveForm::begin([
	        'id' => 'add-discussion-form',
	        'action' => '/ru/site/add-discussion',
	                'fieldConfig' => [
	                    'template' => '{input}'
	                    ],
	        ]);?>
	            <div class="formRow">
	            	<?php echo $form->field($discussion, 'discussion_id')->hiddenInput(); ?>
	                <?php echo $form->field($discussion, 'text')->textArea(['id' => 'discussion-text', 'rows' => 3]); ?>
	                <p id="discussionErrorMessages" class="help-block-error"></p>
	            </div> 
	            <div class="formRow">
	                <?php echo Html::submitButton('добавить', ['id' => 'add-discussion-button',  'class' => 'standartBtn orangeBtn', 'name' => 'add-discussion-button', 'style' => 'cursor:pointer;']) ?>
	                <a href="#smileys" class="borderBtn orangeBorderBtn mgnPopupBtn">#смайлики</a>
	            </div>
	        <?php ActiveForm::end();?>
	    </div>
	    <?php } ?>
	</div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script type="text/javascript">

	$(document).ready(function(){
            $(document).on('click', '.writer', function(){
                window.location = $(this).attr('data-url');
            });

			<?php if(!Yii::$app->User->isGuest) { ?>
            var user_id = <?php echo Yii::$app->user->id; ?>;

            var smileys = [
                "baby",
                "black_eye",
                "blink",
                "blush",
                "boredom",
                "clapping",
                "cray",
                "eye",
                "fool",
                "friends",
                "goblin",
                "good",
                "hi",
                "kiss",
                "lazy",
                "lol",
                "lol2",
                "love",
                "mda",
                "not_i",
                "ok",
                "pioneer",
                "rolleyes",
                "scare",
                "scenic",
                "sclerosis",
                "secret",
                "shok",
                "shout",
                "sorry",
                "sos",
                "stop",
                "sing",
                "tease",
                "telephone",
                "this",
                "vayreni",
                "victory",
                "wink",
                "yahoo",
                "zagar"    
            ];

            $('#add-discussion-button').on('click', function(){
                $('#discussionErrorMessages').text('');
                var discussionText = $('#discussion-text').val();
                if(discussionText == '')
                {
                    $('#discussionErrorMessages').text('Напишите коментарий');
                    return false;
                }
                else if(discussionText.length < 2 || discussionText.length > 500)
                {
                    $('#discussionErrorMessages').text('Коментарий должен быть с 2-ух до 500 букв');
                    return false;
                }
                else {
                    $.ajax({
                        async: false,
                        url: '<?php echo Url::base(true);?>/ru/site/add-discussion',
                        type: 'POST',
                        data: $('#add-discussion-form').serialize(),
                        dataType: 'JSON',
                        success: function(data)
                        {
                            if(data)
                            {
                                discussionText = strip_tags(discussionText);
                                var index = 0;
                                for(index; index<40; index++)
                                {
                                    discussionText = str_replace('('+smileys[index]+')', '<img src="<?php echo Url::base(true);?>/images/smileys/smiley-'+smileys[index]+'.gif">', discussionText);
                                }
                                var html = '<li class="discussionRow" id="discussionNumber'+data.id+'"><div class="discussionContainer">'
                                +'<p class="discussionText">'+discussionText+'</p><p class="discussionDate">'+'<span class="writer" data-url="<?php echo Url::base(true);?>/ru/user/page-view?id='+user_id+'">'+data.username+'</span>'
                                +data.date+months[parseInt(data.month)]+data.dateEnd
                                +'<button class="deleteButton" data-id="'+data.id+'">x</button>'
                                +'</p></div></li>';

                                $('.crossDiscussion ul').prepend(html);

                                $('#discussion-text').val('');

                                var count = $('#discussionsCount').text().replace(/#/gi, '');
                                count = parseInt(count) + 1;
                                $('#discussionsCount').text('#'+count);
                            }
                        }
                    });
                }
				return false;
			});
             var months = [
                '', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь',
            ];

            function strip_tags( str ){ // Strip HTML and PHP tags from a string            
                return str.replace(/<\/?[^>]+>/gi, '');
            }

            $(document).on('click', '.deleteButton', function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    async: false,
                    url: '/ru/site/delete-discussion',
                    type: 'get',
                    data: {
                        id: id,
                    },
                    dataType: 'JSON',
                    success: function(data)
                    {
                        console.log(data);
                        if(data)
                        {                            
                            $('#discussionNumber'+id).remove();

                            var count = $('#discussionsCount').text().replace(/#/gi, '');
                            count = parseInt(count) - 1;
                            $('#discussionsCount').text('#'+count);
                        }
                    }
                });
            });
        <?php } ?>
    });
</script>