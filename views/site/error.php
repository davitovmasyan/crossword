<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
if(strpos(Url::current(), '?language=en')) {
$this->title = '404 error | page not found';
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#404 error</h1>
    <div class="box">
        <h1 class="error404Title">#404 error</h1>
        <p class="error404Message">
            page does not exists or you do not have permission to access this page։
        </p>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_en')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<?php } else if(strpos(Url::current(), '?language=ru')) {
$this->title = '404 ошибка | страница не существует';
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#404 ошибка</h1>
    <div class="box">
        <h1 class="error404Title">#404 ошибка</h1>
        <p class="error404Message">
            страница которую вы ищите не существует։
        </p>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons_ru')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<?php } else {

$this->title = '404 սխալ | էջը գոյություն չունի';
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#404 սխալ</h1>
    <div class="box">
        <h1 class="error404Title">#404 սխալ</h1>
        <p class="error404Message">
            էջը, որը դուք փնտրում եք գոյություն չունի, կամ դուք չունենք թույլտվություւն դիմելու այս էջին։
        </p>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/site/leaderboard">
            <h1>առաջատարներ</h1>
            <p>կուտակեք միավորներ և լրացրեք առաջատարների շարքերը</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>
<?php } ?>
<div class="error404Popup"></div>
<script type="text/javascript">    

    $(document).ready(function(){
        $('.error404Popup').show();

        $('.error404Popup').on('mousemove', function( event ){
                var x = event.pageX-1500;
                var y = event.pageY-1500;
                $('.error404Popup').css('background-position', x+'px '+y+'px');
        });

        $('.error404Popup').on('click', function()
        {
            $(this).fadeOut('200');
        });
    });
</script>