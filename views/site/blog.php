<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$months = [
    '01' => 'հունվար',
    '02' => 'փետրվար',
    '03' => 'մարտ',
    '04' => 'ապրիլ',
    '05' => 'մայիս',
    '06' => 'հունիս',
    '07' => 'հուլիս',
    '08' => 'օգոստոս',
    '09' => 'սեպտեմբեր',
    '10' => 'հոկտեմբեր',
    '11' => 'նոյեմբեր',
    '12' => 'դեկտեմբեր',    
];


$this->title="Crossword.am | Բլոգ";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#բլոգ</h1>
    <div class="box">
        <?php if(!empty($posts)) { ?>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <ul class="blogList">
            <?php foreach ($posts as $post) { ?>
            <li class="postLink" data-link="<?php echo Url::base(true); ?>/site/post?id=<?php echo $post['id'];?>">
                <div class="blogImageDiv" style="background-image: url('/images/uploads/<?php echo $post['image'];?>');"></div>
                <div>
                    <h1><?php echo $post['title'];?></h1>
                    <p><?php echo $post['description'];?></p>
                    <span><?php echo substr($post['created'], 0, 4),' ',$months[substr($post['created'], 5,2)],' ',substr($post['created'], 8,2);?></span>
                </div>
            </li>
            <?php } ?>
        </ul>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainCity" data-url="/city/list">
            <h1>#քաղաքներ</h1>
            <p>քաղաք - քաղաք</p>
        </div>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/site/leaderboard">
            <h1>առաջատարներ</h1>
            <p>կուտակեք միավորներ և լրացրեք առաջատարների շարքերը</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainGamesBox" data-url="/site/games">
            <h1>խաղեր</h1>
            <p>մեր խաղերը</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.postLink').on('click', function(){
            window.location = $(this).attr('data-link');
        });
    });
</script>