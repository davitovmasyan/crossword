<?php

$this->title="Crossword.am | Քննարկումներ";

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#քննարկումներ</h1>
    <div class="box">
    	<?php if(!empty($discussions)) { ?>
    	<ul>
    		<?php foreach($discussions as $d) { ?>
    		<li class="discussions">
    			<a href="/site/discussions?id=<?php echo $d['id'];?>"><?php echo $d['name']; ?> - (<?php echo $d['count']; ?>)</a>
    		</li>
    		<?php } ?>
    	</ul>
    	<?php } else { ?>
    	<h4 class="noInfSt">
    		քննարկումներ չկան
    	</h4>
    	<?php } ?>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainGamesBox" data-url="/site/games">
            <h1>խաղեր</h1>
            <p>մեր խաղերը</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>