<?php

$this->title = 'Crossword.am | Մրցույթներ';

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#մրցույթներ</h1>
    <div class="box">
        <h1 class="commingSoon">#շուտով</h1>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
    <div class="box">
        <div class="mainLeaders" data-url="/site/leaderboard">
            <h1>առաջատարներ</h1>
            <p>կուտակեք միավորներ և լրացրեք առաջատարների շարքերը</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainGamesBox" data-url="/site/games">
            <h1>խաղեր</h1>
            <p>մեր խաղերը</p>
        </div>
    </div>
</div>