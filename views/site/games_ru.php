<?php
use yii\helpers\Url;
$this->title="Crossword.am | Игры";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#игры</h1>
    <div class="box">                
        <ul class="gamesList">
            <li>
                <ul class="gameList">
                    <li>
                        <img class="clickAble" data-url="<?php echo Url::base(true); ?>/ru/cross-words/list" src="/images/crosswordgame.jpg">
                    </li>
                    <li>
                        <h1 class="gamesHeading crossHead">#кроссворд</h1>
                        <p>кроссворы разных типов и категорий</p>
                        <button class="standartBtn crossBtn" data-url="<?php echo Url::base(true); ?>/ru/cross-words/list">#кроссворды</button>
                        <button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/ru/cross-words/random">#случайный</button>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="gameList">
                    <li>
                        <img class="clickAble" data-url="<?php echo Url::base(true); ?>/ru/sudoku/list" src="/images/sudokugame.jpg">
                    </li>
                    <li>
                        <h1 class="gamesHeading sudoHead">#судоку</h1>
                        <p>судоку игра
                        </p>
                        <button class="standartBtn sudoBtn" data-url="<?php echo Url::base(true); ?>/ru/sudoku/list">#список</button>
                        <button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/ru/sudoku/random">#случайный</button>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="gameList">
                    <li>
                        <img src="/images/quizgame.jpg" class="clickAble" data-url="<?php echo Url::base(true); ?>/ru/quiz/list">
                    </li>
                    <li>
                        <h1 class="gamesHeading quizHead">#викторина</h1>
                        <p>викторина на разных категориях</p>
                        <button class="standartBtn quizBtn" data-url="<?php echo Url::base(true); ?>/ru/quiz/list">#список</button>
                        <button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/ru/quiz/random">#случайный</button>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.quizBtn, .sudoBtn, .crossBtn, .randomBtn').on('click', function(){
            window.location = $(this).attr('data-url');
        });

        $('.clickAble').on('click', function(){
            window.location = $(this).attr('data-url');
        }).hover(function(){
            $(this).css('cursor', 'pointer');
        });
    });
</script>