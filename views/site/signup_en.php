<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title="Crossword.am | Signup";
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> 
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#signup</h1>
    <div class="box">            
            <?php $form = ActiveForm::begin([
            'id' => 'signupform', 
            'action' => '/en/site/signup',
            'fieldConfig' => [
                'template' => '{label}{input}<p class="help-block"></p>'
                ],
            ]); ?>                    
            <div class="formRow twoCol">
                <?php echo $form->field($signup, 'username')->textInput()->label('username'); ?>
                <?php echo $form->field($signup, 'email')->textInput()->label('email'); ?>
            </div>
            <div class="formRow twoCol">
                <?php echo $form->field($signup, 'password')->passwordInput()->label('password'); ?>
                <?php echo $form->field($signup, 'password_repeat')->passwordInput()->label('confrim password'); ?>
            </div>
            <div class="formRow twoCol">
                <?php echo $form->field($signup, 'first_name')->textInput()->label('first name'); ?>
                <?php echo $form->field($signup, 'last_name')->textInput()->label('last name'); ?>
            </div>
            <div class="formRow twoCol">
                <?php echo $form->field($signup, 'address')->textInput()->label('address'); ?>
                <?php echo $form->field($signup, 'city', ['inputOptions' => ['id' => 'city']])->hiddenInput()->label(false); ?>
                <?php echo $form->field($signup, 'country', ['inputOptions' => ['id' => 'country']])->hiddenInput()->label(false); ?>
                <?php echo $form->field($signup, 'street', ['inputOptions' => ['id' => 'street']])->hiddenInput()->label(false); ?>
                <?php echo $form->field($signup, 'state', ['inputOptions' => ['id' => 'state']])->hiddenInput()->label(false); ?>
                <?php echo $form->field($signup, 'zip_code', ['inputOptions' => ['id' => 'zip_code']])->hiddenInput()->label(false); ?>
                <?php echo $form->field($signup, 'lat_lng', ['inputOptions' => ['id' => 'lat_lng']])->hiddenInput()->label(false); ?>
            </div>
            <div class="formRow twoCol">
                    <?php $ages = array(); ?>
                    <?php 
                    for($i=7; $i<=90; $i++) { 
                        $ages[$i] = $i;
                    } ?>
                    <?php
                    echo $form->field($signup, 'age')->dropDownList(
                            $ages, ['prompt'=>'#age'])->label(false);
                    echo $form->field($signup, 'sex')->radioList(array('boy'=>'#male','girl'=>'#female'))->label('gender'); 
                    ?>
            </div>
            <div class="clear"></div>
            <?php echo $form->field($signup, 'facebook_id')->hiddenInput()->label(false); ?>
            <div class="submitSect">
                <?php echo Html::submitButton('Signup', ['class' => 'borderBtn orangeBorderBtn', 'name' => 'signup-button']); ?>
            </div>
            <div class="alreadyMember">
              #already a member? <a href="#loginPopup" class="standartBtn orangeBtn mgnPopupBtn">Login</a>
            </div>
            <div class="clear"></div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
<script>

$(document).ready(function(){
  $('.aboutSignUp').on('click', function()
  {
    $('.aboutSignUpText').slideToggle('500', function(){

    });
  });
});

var autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};


function initialize() {
    
    

   autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('signupform-address')),
                {types: ['geocode'],
        });
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
        });
 
}

google.maps.event.addDomListener(window, 'load', initialize);


function getLatLngFromAddress(address) {
           var geocoder = new google.maps.Geocoder();
           geocoder.geocode({'address': address}, function(results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                   $('#lat_lng').val(results[0].geometry.location.lat()+','+results[0].geometry.location.lng())                   
               } else {
                   console.log("Geocode was not successful for the following reason: " + status);
               }
           });
       }

       function fillInAddress() {
           
            $("#city").val('');
            $("#street").val('');
            $("#country").val('');
            $("#zip_code").val('');
            $("#state").val('');
            $("#street").val('');
           // Get the place details from the autocomplete object.
           var place = autocomplete.getPlace();
           for (var component in componentForm) {
               if (document.getElementById(component)) {
                   document.getElementById(component).value = '';
                   document.getElementById(component).disabled = false;
               }

           }

           // Get each component of the address from the place details
           // and fill the corresponding field on the form.
           var address = '';
           var city="";
           for (var i = 0; i < place.address_components.length; i++) {
               var addressType = place.address_components[i].types[0];

               if (componentForm[addressType]) {
                   var val = place.address_components[i][componentForm[addressType]];
                   if (addressType === "locality") {
                    $("#city").val(val);
                   }                   
                   if (addressType === "street_number") {
                    $("#street").val(val);
                   }
                   if (addressType === "administrative_area_level_1") {
                    $("#state").val(val);
                   }                    
                   
                    if (addressType === "country") {
                    $("#country").val(val);
                   }
                    if (addressType === "postal_code") {
                    $("#zip_code").val(val);
                   }                             
                   address = address + ' ' + val;
                  
                   if (document.getElementById(addressType)) {
                       document.getElementById(addressType).value = val;
                   }
               }
           }
           getLatLngFromAddress(address);
       }
</script>