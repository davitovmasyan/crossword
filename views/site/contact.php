<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
$this->title="Crossword.am | Կապ";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#կապ մեզ հետ</h1>
    <div class="box">
      <div class="contactCover">

      </div>
      <div class="contactInfo">
        <ul>
          <li class="contactItem email" title="էլ հասցե">
            info@crossword.am
          </li>
          <li class="contactItem location" title="բնակավայր">
            Yerevan, Armenia        
          </li>
          <li class="contactItem mobile" title="հեռախոս">
            +374(77)590-123
          </li>
        </ul>
      </div>
      <div class="contactForm">
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
              <div class="formRow">
                  <?php echo $form->field($model, 'name'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'email'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'message')->textArea(['rows' => 3]); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                      'template' => '<div class="captchaImage">{image}</div><div class="captchainput">{input}</div>',
                  ]); ?>
              </div>
              <div class="submitSect">
                  <?php echo Html::submitButton('Ուղարկել', ['class' => 'borderBtn orangeBorderBtn', 'name' => 'contact-button']); ?>
              </div>
      </div>
      <div class="clear">
      </div>
   </div>
   <div class="box">
        <div class="mainCity" data-url="/city/list">
            <h1>#քաղաքներ</h1>
            <p>քաղաք - քաղաք</p>
        </div>
    </div>
   <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){       
       $('#contactform-verifycode-image ').click(function() {
         src = $('#contactform-verifycode-image ').attr('src');
          // check for existing ? and remove if found
         queryPos = src.indexOf('?');
         if(queryPos != -1) {
            src = src.substring(0, queryPos);
         }    
         $('#contactform-verifycode-image').attr('src', src + '?' + Math.random());
         return false;
      });
    });
</script>