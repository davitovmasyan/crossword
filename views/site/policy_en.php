<?php 

$this->title="Crossword.am | Privacy Policy";

?>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#privacy policy</h1>
	<div class="box">
		<ul>
		<li>
			
			<h1 class="privacyHeading">#sign up</h1>

			<p class="privacyText">
				խորհուրդ է տրվում օգտագործել իրական տվյալներ գրանցման ժամանակ հետագայում խնդիրներից խուսափելու նպատակով։
			</p>

		</li>
		<li>
			

			<h1 class="privacyHeading">#users</h1>

			<p class="privacyText">
				օգտատերերի մասին ինֆորմացիան մասամբ բաց է բոլոր այցելուների համար (<span class="orangeText">մուտքանուն, միավորներ, կոորդինատներ և գումարածներ</span>)
			</p>
		</li>

		<li>
			

			<h1 class="privacyHeading">#cookie</h1>

			<p class="privacyText">
				մենք ինչպես այլ կայքերի մեծամասնությունը օգտագործում ենք քուքիները կայքի աշխատանքի արագ կազմակերպման և ձեր հարմարավետության ապահովման նպատակներով։
			</p>
		</li>

		<li>
			

			<h1 class="privacyHeading">#problems</h1>

			<p class="privacyText">
				չի թույլատրվում մեկնաբանություններում չարություն անել և օգտագործել այլ մարդկանց տվյալներ հակառակ դեպքում ձեր էջը կարգելափակվի։
			</p>
		</li>
	</ul>
	</div>
	<?php echo $this->renderFile($this->findViewFile('contactButtons_en')); ?>   
	<div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>