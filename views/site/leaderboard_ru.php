<?php
use yii\helpers\Url;
$this->title="Crossword.am | Лидеры";
$map_leaders = [];

?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="/js/markerclusterer.js"></script>
<div class="container">
	<h1 class="mainPageTitle blackPageTitle">#лидеры</h1>
	<div class="box">
		<?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->attributes['points'] > 0) { ?>
			<button class="toMeButton" title="где я">#я</button>
		<?php } ?>
		<div class="listNames">
			<ul>
				<li style="width: 10%;">#место</li>
				<li style="width: 10%;">#лого</li>
				<li style="width: 20%;">#никнейм</li>
				<li style="width: 15%;">#очки</li>
				<li style="width: 16%;">#плюсы</li>
				<li style="width: 16%;">#страна</li>
			</ul>
		</div>
		<ul class="leaderboardList leaderboardCustomScroll">				
			<?php $i=1; foreach ($leaders as $leader) { ?>
			<?php if($leader['lat_lng']) { $map_leaders[] = ['username' => $leader['username'], 'lat_lng' => $leader['lat_lng'], 'id' => $leader['id']]; } ?>
			<li title="смотреть" data-url="<?php echo Url::base(true); ?>/ru/user/page-view?id=<?php echo $leader['id']; ?>" class="singleLeader <?php echo $leader['id'] == Yii::$app->User->id ? 'singleLeaderMe' : ''; ?>">
				<ul>
					<li style="width: 6%;">#<?php echo $i++; ?></li>
					<li style="width: 6%;"><img src="/images/users/<?php echo $leader['username'];?>.png"></li>
					<li style="width: 16%;">#<?php echo $leader['username'];?></li>
					<li style="width: 15%;"><?php echo $leader['points'];?></li>
					<li style="width: 16%;">+<?php echo $leader['pluses'];?></li>
					<li style="width: 16%;"><?php echo $leader['country'] ? $leader['country'] : 'не указано';?></li>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</div>
	<?php if(!empty($map_leaders)) { ?>
	<div class="box" title="карта лидеров">
		<div id="map-canvas" style="width: 100%; height: 400px;">
		</div>
	</div>
	<?php } ?>
	<div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script type="text/javascript">
	<?php if(!empty($map_leaders)) { ?>
	function initialize() {
	  	var data = [
	      	<?php foreach ($map_leaders as $leader) {
		        	$marker_lat_lng = explode(',', $leader['lat_lng']);
		        	$title = $leader['username']; 
		        	$userid = $leader['id']; ?>
		        	{
	        		"userid": "<?php echo $userid;?>",
		          	"user": "<?php echo $title;?>",
		          	"lat": <?php echo $marker_lat_lng['0'];?>,
		          	"lng": <?php echo $marker_lat_lng['1'];?>,
		        	},               	 
		       	 
	      	<?php }?>  
	  	];

   	 

    	var markers = [];
    	var latlngbounds = new google.maps.LatLngBounds();
    	var countOfData = data.length
    	for (var i = 0; i < countOfData; i++) {
	      	var latLng = new google.maps.LatLng(data[i].lat,
	          	data[i].lng);
	      	var marker = new google.maps.Marker({
	        	position: latLng,
	        	title: data[i].user,
	      	});
	      	markers.push(marker);
	      	latlngbounds.extend(marker.position);
	      	var content = "<div class='infoWindow'>"+
                "<a href='/ru/user/page-view?id="+data[i].userid+"' class='userName'>"+data[i].user+"</a>"+
                "</div>";

	        var infowindow = new google.maps.InfoWindow()
	          	google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
	                return function() {
	                   infowindow.setContent(content);
	                   infowindow.open(map,marker);
	                };
	            })(marker,content,infowindow));
		}

		 
   	 

		var map = new google.maps.Map(document.getElementById('map-canvas'), {
	  	zoom: 3,             	 
	  	// scrollwheel:false,
	  	mapTypeId: google.maps.MapTypeId.ROADMAP,
		});

		map.setCenter(latlngbounds.getCenter());
		map.fitBounds(latlngbounds);
		var markerCluster = new MarkerClusterer(map, markers);   	 
  	}
  	google.maps.event.addDomListener(window, 'load', initialize);
  	<?php } ?>

	$(document).ready(function(){
		$(".leaderboardCustomScroll").mCustomScrollbar({
	        scrollbarPosition: 'outside',
	        scrollInertia: 1500,
	        autoHideScrollbar: true
	    });

	    function scrollToMe()
	    {
	    	$(".leaderboardCustomScroll").mCustomScrollbar("scrollTo",".singleLeaderMe",{
			    scrollEasing:"easeOut"
			});
	    }
	    
	    scrollToMe();

	    $('.toMeButton').on('click', function(){
	    	scrollToMe();
	    });

	    $('.singleLeader').on('click', function(){
	    	window.location = $(this).attr('data-url');
	    });
	});
</script>