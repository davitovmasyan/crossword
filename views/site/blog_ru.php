<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$months = [
    '01' => 'январь',
    '02' => 'февраль',
    '03' => 'март',
    '04' => 'апрель',
    '05' => 'май',
    '06' => 'июнь',
    '07' => 'июль',
    '08' => 'август',
    '09' => 'сентябрь',
    '10' => 'октябрь',
    '11' => 'ноябрь',
    '12' => 'декабрь',  
];


$this->title="Crossword.am | Блог";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#блог</h1>
    <div class="box">
        <?php if(!empty($posts)) { ?>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <ul class="blogList">
            <?php foreach ($posts as $post) { ?>
            <li class="postLink" data-link="<?php echo Url::base(true); ?>/ru/site/post?id=<?php echo $post['id'];?>">
                <div class="blogImageDiv" style="background-image: url('/images/uploads/<?php echo $post['image'];?>');"></div>
                <div>
                    <h1><?php echo $post['title'];?></h1>
                    <p><?php echo $post['description'];?></p>
                    <span><?php echo substr($post['created'], 0, 4),' ',$months[substr($post['created'], 5,2)],' ',substr($post['created'], 8,2);?></span>
                </div>
            </li>
            <?php } ?>
        </ul>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <?php } else { ?>
        <h4 class="noInfSt">публикаций нет</h4>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/ru/site/leaderboard">
            <h1>лидеры</h1>
            <p>получайте очки и поднимайтесь в топе</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/ru/site/discussions">
            <h1>обсуждения</h1>
            <p>участвуйте в обсуждениях</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>мы в фейсбуке</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.postLink').on('click', function(){
            window.location = $(this).attr('data-link');
        });
    });
</script>