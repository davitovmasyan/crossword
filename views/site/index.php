<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title="Crossword.am | Գլխավոր";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#գլխավոր</h1>    
    <div class="box">
        <div class="mainCity" data-url="<?php echo $this->params['language_prefix'];?>/city/list">
            <h1>#քաղաքներ</h1>
            <p>քաղաք - քաղաք</p>
        </div>
    </div>
    <div class="box">
        <div class="mainQuiz" data-url="<?php echo Url::base(true); ?>/quiz/list">
            <h1>#վիկտորինա</h1>
            <p>նոր հետաքրքիր խաղ</p>
        </div>
        <ul class="mainGames">
            <li class="mainCross" data-url="<?php echo Url::base(true); ?>/cross-words/list">
                <h1>#խաչբառ</h1>
                <p>այժմ նաև այլ լեզուներով</p>
            </li>
            <li class="mainSudoku" data-url="<?php echo Url::base(true); ?>/sudoku/list">
                <h1>#սուդոկու</h1>
                <p>սուդոկուն շուտով մեզ մոտ</p>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am" data-target="new">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>    

<?php if(isset($reset_password)) { ?>
<a id="openReset" href="#resetPopup" class="mgnPopupBtn" style="display:none;"></a>
<script type="text/javascript">
    $(document).ready(function(){
        $('#openReset').trigger('click');
    });
</script>
<div id="resetPopup" class="mfp-hide mgnPopup mgnSmallPopup">
    <div class="popupTitleWrap">
        <h5 class="popupTitle">#գաղտնաբառի փոփոխություն</h5>
    </div>
    <div class="popupCont">
        <?php $form = ActiveForm::begin([
            'id' => 'password-reset-form',
            'method' => 'post']); ?>
        <div class="formRow">
            <label>#նոր գաղտնաբառ</label>
            <?php echo $form->field($reset_password, 'password')->label(false); ?>
        </div>
        <div class="formRow">
            <label>#կրկ. գաղտնաբառ</label>
            <?php echo $form->field($reset_password, 'password_repeat')->label(false); ?>
        </div>
        <div class="formRow">
            <input type="submit" value="հաստատել" class="borderBtn orangeBorderBtn">
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php } ?>


<script type="text/javascript">
        $(document).ready(function(){
            $('.mainQuiz, .mainCross, .mainSudoku, .mainFb, .mainCity, #myCanvas').on('click', function(){
                if($(this).attr('data-target') == 'new')
                {
                    window.open($(this).attr('data-url'),'_blank');
                    return false;
                }
                window.location = $(this).attr('data-url');
            });
        });
</script>