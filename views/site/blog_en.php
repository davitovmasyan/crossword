<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$months = [
    '01' => 'january',
    '02' => 'february',
    '03' => 'march',
    '04' => 'april',
    '05' => 'may',
    '06' => 'june',
    '07' => 'jule',
    '08' => 'august',
    '09' => 'september',
    '10' => 'october',
    '11' => 'november',
    '12' => 'december', 
];


$this->title="Crossword.am | Blog";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#blog</h1>
    <div class="box">
        <?php if(!empty($posts)) { ?>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <ul class="blogList">
            <?php foreach ($posts as $post) { ?>
            <li class="postLink" data-link="<?php echo Url::base(true); ?>/en/site/post?id=<?php echo $post['id'];?>">
                <div class="blogImageDiv" style="background-image: url('/images/uploads/<?php echo $post['image'];?>');"></div>
                <div>
                    <h1><?php echo $post['title'];?></h1>
                    <p><?php echo $post['description'];?></p>
                    <span><?php echo substr($post['created'], 0, 4),' ',$months[substr($post['created'], 5,2)],' ',substr($post['created'], 8,2);?></span>
                </div>
            </li>
            <?php } ?>
        </ul>
        <?php // display pagination
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        <?php } else { ?>
        <h4 class="noInfSt">no posts</h4>
        <?php } ?>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.postLink').on('click', function(){
            window.location = $(this).attr('data-link');
        });
    });
</script>