<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
$this->title="Crossword.am | Contact";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#contact us</h1>
    <div class="box">
      <div class="contactCover">

      </div>
      <div class="contactInfo">
        <ul>
          <li class="contactItem email" title="email">
            info@crossword.am
          </li>
          <li class="contactItem location" title="address">
            Yerevan, Armenia        
          </li>
          <li class="contactItem mobile" title="phone">
            +374(77)590-123
          </li>
        </ul>
      </div>
      <div class="contactForm">
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
              <div class="formRow">
                  <?php echo $form->field($model, 'name')->label('Name'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'email')->label('Email'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'message')->textArea(['rows' => 3])->label('Message'); ?>
              </div>
              <div class="formRow">
                  <?php echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                      'template' => '<div class="captchaImage">{image}</div><div class="captchainput">{input}</div>',
                  ])->label('Verification code'); ?>
              </div>
              <div class="submitSect">
                  <?php echo Html::submitButton('Send', ['class' => 'borderBtn orangeBorderBtn', 'name' => 'contact-button']); ?>
              </div>
      </div>
      <div class="clear">
      </div>
   </div>
   <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){       
       $('#contactform-verifycode-image ').click(function() {
         src = $('#contactform-verifycode-image ').attr('src');
          // check for existing ? and remove if found
         queryPos = src.indexOf('?');
         if(queryPos != -1) {
            src = src.substring(0, queryPos);
         }    
         $('#contactform-verifycode-image').attr('src', src + '?' + Math.random());
         return false;
      });
    });
</script>