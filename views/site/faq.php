<?php

$this->title="Crossword.am | ՀՏՀ";

?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#հաճախակի տրվող հարցեր</h1>
    <div class="box">
    <ul>
            <li>
                
                <h1 class="faqHeading">#ինչպես մուտք գործել</h1>

                <p class="faqText">կայքի վերևի աջ անկյունում դուք կտեսնեք <?php echo Yii::$app->user->isGuest ? '<a href="#loginPopup" class="mgnPopupBtn orangeText">մուտք</a>' : '<span class="orangeText">մուտք</span>'; ?> կոճակը,
                 սեղմելով այն դուք կբացեք մուտքի պատուհանը, որի առաջին մուտքանուն դաշտում կարող եք մուտքագրել 
                    ձեր մուտքանունը կամ էլ հասցեն, իսկ մյուս գաղտնաբառ դաշտում ձեր գաղտնաբառը։ մուտքագրելուց հետո ձեզ
                     մնում է միայն սեղմեք մուտք կոճակը և վերջ։</p>

            </li>
            <li>
                

                <h1 class="faqHeading">#ինպես գրանցվել</h1>

                <p class="faqText">

                        կայքի վերևի աջ անկյունում դուք կտեսնեք գրանցում կոճակը, որը սեղմելով կտեղափոխվեք 
                        <?php echo Yii::$app->user->isGuest ? '<a href="/site/signup/" class="orangeText">գրանցման էջ</a>' : '<span class="orangeText">գրանցման էջ</span>'; ?>
                        այնուհետեև լրացնելով համապատասխան (մուտքանուն, էլ հասցե, գաղտնաբառ՝ պարտադիր, և անուն, ազգանուն, 
                        սեռ, հասցե, տարիք՝ ոչ պարտադիր) դաշտերը, պետք է սեղմեք գրանցվել կոճակը, որից հետո ավտոմատ մուտք կգործեք ձեր էջ։

                </p>
            </li>

            <li>
                

                <h1 class="faqHeading">#ինչի համար է գրանցումը</h1>

                <p class="faqText">

գրանցումը նախատեսված է այն օգտատերերի համար, որոնք պարբերաբար այցելուներ են։ տարբերությունն այն է, որ գրանցված օգտատերերը կարող են պահպանել իրենց խաղերի արդյունքը, կուտակել միավորներ և լինել առաջատարների ցուցակում։ իսկ խաղալ կարող են թե գրանցված և թե չգրանցած օգտատերերը։


                    
                </p>
            </li>

            <li>
                

                <h1 class="faqHeading">#ինչպես պահպանել խաչբառերը</h1>

                <p class="faqText">

ցանկացած խաչբառի էջում դուք կտեսնեք պահպանել կոճակը, որը աշխատում է ինչպես ձեր սեղմումով այնպես էլ ինքնուրույն (որպեսզի չհիշելու դեպքում արդյունը չկորչի)։ ձեր պահպանված խաչբառերը կարող եք տեսնել իմ խաչբառեր բաժնում։
                                    
                </p>
            </li>
            <li>
                

                <h1 class="faqHeading">#ինչպես վերականգնել գաղտնաբառը</h1>

                <p class="faqText">


<?php echo Yii::$app->user->isGuest ? 'գաղտնաբառը վերականգնելու համար կարող եք սեղմել <a href="#forgotPopup" class="mgnPopupBtn orangeText">այտեղ</a>, կամ' :''; ?>մուտքի պատուհանից կարող եք սեղմել վերականգնել գաղտնաբառը կոճակը։ այնուհետև մուտքագրելով ձեր էլ հասցեն, որով գրանցված է հաշիվը ձեր էլ հասցեին կստանաք գաղտնաբառի վերականգման հղում։ հենց այդ հղման միջոցով կարող եք վերականգնել գաղտնաբառը։

                    </p>
            </li>
            <li>
                

                <h1 class="faqHeading">#միավորների կուտակման մասին</h1>

                <p class="faqText">

խաչբառի միավորները փոխանցվում են ձեզ խաչբառը լրիվությամբ լուծելուց անմիջապես հետ։ վիկտորինայի միավորները փոխանցվում են ձեզ ամեն ճիշտ պատասխանից հետո։

                    </p>
            </li>
            <li>
                

                <h1 class="faqHeading">#ինչ է նշանակում ձեր էջը ակտիվ չէ</h1>

                <p class="faqText">

այս ծանուցումը նշանակում է, որ ձեր էջը արգելափակված է <a href="/site/privacy-policy" class="orangeText">կանոնների</a> խախտման պատճառով։

                    </p>
            </li>
            <li>
                

                <h1 class="faqHeading">#չեմ ստանում նամակ իմ նշած էլ հասցեին</h1>

                <p class="faqText">

նամակները կարող են հայտնվել սպամում կամ հնարավոր է, որ դուք չստանաք նամակներ ձեր էլ հասցեի տեսակի պատճառով։ էլ հասցեի հետ կապված խնդիր ունենալու դեպքում կարող եք <a href="#reportAboutProblem" class="orangeText mgnPopupBtn">տեղեկացնել</a> մեզ այդ մասին։

                    </p>
            </li>
        </ul>
    </div>
    <?php echo $this->renderFile($this->findViewFile('contactButtons')); ?>   
    <div class="box">
        <div class="mainCity" data-url="/city/list">
            <h1>#քաղաքներ</h1>
            <p>քաղաք - քաղաք</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>միացեք մեզ ֆեյսբուքում</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/site/discussions">
            <h1>քննարկումներ</h1>
            <p>մասնակցեք մեր քննարկումներին</p>
        </div>
    </div>
    <div class="box">
        <div class="mainBlog" data-url="/site/blog">
            <h1>բլոգ</h1>
            <p>հետաքրքիր տեղեկատվություն</p>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.faqHeading').on('click', function(){
        var item = $(this).next();
        item.slideToggle('800', function(){

        });
        item.parent().siblings().find('.faqText').slideUp(800);
    });
});
</script>