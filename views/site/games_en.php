<?php
use yii\helpers\Url;
$this->title="Crossword.am | Games";
?>
<div class="container">
    <h1 class="mainPageTitle blackPageTitle">#games</h1>
    <div class="box">                
        <ul class="gamesList">
            <li>
                <ul class="gameList">
                    <li>
                        <img class="clickAble" data-url="<?php echo Url::base(true); ?>/en/cross-words/list" src="/images/crosswordgame.jpg">
                    </li>
                    <li>
                        <h1 class="gamesHeading crossHead">#crossword</h1>
                        <p>online crosswords in different categories.</p>
                        <button class="standartBtn crossBtn" data-url="<?php echo Url::base(true); ?>/en/cross-words/list">#crosswords</button><button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/en/cross-words/random">#random</button>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="gameList">
                    <li>
                        <img class="clickAble" data-url="<?php echo Url::base(true); ?>/en/sudoku/list" src="/images/sudokugame.jpg">
                    </li>
                    <li>
                        <h1 class="gamesHeading sudoHead">#sudoku</h1>
                        <p>sudoku game.
                        </p>
                        <button class="standartBtn sudoBtn" data-url="<?php echo Url::base(true); ?>/en/sudoku/list">#սուդոկուներ</button><button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/en/sudoku/random">#random</button>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="gameList">
                    <li>
                        <img src="/images/quizgame.jpg" class="clickAble" data-url="<?php echo Url::base(true); ?>/en/quiz/list">
                    </li>
                    <li>
                        <h1 class="gamesHeading quizHead">#quiz</h1>
                        <p>quiz game in different categories.</p>
                        <button class="standartBtn quizBtn" data-url="<?php echo Url::base(true); ?>/en/quiz/list">#quizes</button><button class="standartBtn randomBtn" data-url="<?php echo Url::base(true); ?>/en/quiz/random">#random</button>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="box">
        <div class="mainLeaders" data-url="/en/site/leaderboard">
            <h1>leaders</h1>
            <p>get points and be on top</p>
        </div>
    </div>
    <div class="box">
        <div class="mainDisc" data-url="/en/site/discussions">
            <h1>discussions</h1>
            <p>participation in our discussions</p>
        </div>
    </div>
    <div class="box">
        <div class="mainFb" data-url="https://www.facebook.com/crossword.am">
            <h1>facebook</h1>
            <p>find us on facebook</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.quizBtn, .sudoBtn, .crossBtn, .randomBtn').on('click', function(){
            window.location = $(this).attr('data-url');
        });

        $('.clickAble').on('click', function(){
            window.location = $(this).attr('data-url');
        }).hover(function(){
            $(this).css('cursor', 'pointer');
        });
    });
</script>