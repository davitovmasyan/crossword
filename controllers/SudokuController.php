<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;
use app\models\Users;
use yii\data\Pagination;

 
class SudokuController extends AppController
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],                    
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['list', 'random'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],                    
                ],
                'denyCallback'  => function ($rule, $action) {
                     return $this->redirect($this->prefix_language.'/site/errorPage');
                },
            ]
        ];
    }

    public $socialData = [];
    

    public function actionRandom()
    {
        return $this->redirect($this->prefix_language.'/sudoku/list');
    }

    public function actionList()
    {
        $this->socialData = [
        'description' => '', 
        'keywords' => '', 
        'image' => '/images/sudoku-list-main-image.jpg'];
        return $this->render('list'.$this->language_view);
    }

}