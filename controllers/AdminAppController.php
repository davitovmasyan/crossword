<?php
namespace app\controllers;

use yii\web\Controller;
use yii\web\Application;
use yii\helpers\Url;
use Yii;

class AdminAppController extends Controller {
    
    public function alert($type, $message)
    {
       $alertTypes = [
           'error',
           'danger',
           'success',
           'info',
           'warning'
       ];
       if(in_array($type, $alertTypes)) {
           Yii::$app->getSession()->setFlash($type, $message);
       }
    }
    
    public function beforeAction($action)
    {
        date_default_timezone_set('Asia/Yerevan');
        return parent::beforeAction($action);
    }
}