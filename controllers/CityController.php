<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use app\models\Users;
use app\models\Notifications;
use yii\helpers\Url;
use yii\db\Query;
use Yii;

 
class CityController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],                    
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['list', 'am', 'us', 'get-city', 'reset'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],                    
                ],
                'denyCallback'  => function ($rule, $action) {
                     return $this->redirect($this->prefix_language.'/site/errorPage');
                },
            ]
        ];
    }

    public $socialData = [];

    public function actionList()
    {
        $this->socialData = [
        'description' => $this->getSocialData('001'),
        'keywords' => 'crossword.am online-crossword, armenian crossword, ցիտիես, armenian քաղաքներ, հայկական goroda, vgoroda, gorodki, խաչբառ, օնլայն, հայկական, crossword.am գլխավոր էջ, հայկական խաչբառեր, օնլայն խաղեր', 
        'image' => '/images/city-list-main-image.jpg'];
        return $this->render('list'.$this->language_view);
    }

    public function actionPlay($id) {
        $cities = [
            '1' => 'am',
            '2' => 'us',
        ];
        if(isset($cities[$id])) {
            return $this->redirect($this->prefix_language.'/city/'.$cities[$id]);
        } else {
            return $this->redirect($this->prefix_language.'/city/list');
        }
    }

    public function getCities()
    {
        return Yii::$app->session['cities'];
    }

    public function saveCity($data)
    {
        Yii::$app->session['cities'] = $data;
    }

    public function actionAm()
    {
        $array = [];
        $array = $this->getCities();
        if(!$array || empty($array) || $array['country'] != 'am') {
            $query = new Query();
            $city = $query->select('am_cities.*')    
                            ->from('am_cities')
                            ->orderBy('rand()')
                            ->one();

            $array['country'] = 'am';
            $array['ids'][] = $city['id'];
            $array['cities'][] = ['name' => $city['name'], 'lat_lng' => $city['lat_lng']];
            $rest = substr($city['name'], -2);
            if($rest == 'gh' || $rest == 'ch' || $rest == 'sh') {
                $array['last_char'] = $rest;
            } else {
                $array['last_char'] = substr($city['name'], -1);
            }
            $this->saveCity($array);
        }
        $query = new Query();
        $game['comments'] = $query->select('comments.*, users.username, users.id AS uid')
                                ->from('comments')
                                ->leftJoin('users', 'users.id = comments.user_id AND users.active = 1')
                                ->where('comments.game_id = :id AND comments.game_type = "city" AND comments.deleted = "no"', [':id' => 1])
                                ->orderBy('comments.created DESC')
                                ->having('users.username IS NOT NULL')
                                ->all();

        $this->socialData = [
        'description' => $this->getSocialData('002'),
        'keywords' => 'crossword.am online-crossword, armenian crossword, ցիտիես, armenian քաղաքներ, հայկական goroda, vgoroda, gorodki, խաչբառ, օնլայն, հայկական, crossword.am գլխավոր էջ, հայկական խաչբառեր, օնլայն խաղեր', 
        'image' => '/images/uploads/3e844c6aa26e466d19771989be66dee5.jpg'];

        return $this->render('am'.$this->language_view, ['game' => $game, 'cities' => $array]);
    }

    public function actionUs()
    {
        $array = [];
        $array = $this->getCities();
        if(!$array || empty($array) || $array['country'] != 'us') {
            $query = new Query();
            $city = $query->select('us_cities.*')    
                            ->from('us_cities')
                            ->orderBy('rand()')
                            ->one();

            $array['country'] = 'us';
            $array['ids'][] = $city['id'];
            $array['cities'][] = ['name' => $city['name'], 'lat_lng' => $city['lat_lng']];
            
            $array['last_char'] = substr($city['name'], -1);            
            $this->saveCity($array);
        }
        $query = new Query();
        $game['comments'] = $query->select('comments.*, users.username, users.id AS uid')
                                ->from('comments')
                                ->leftJoin('users', 'users.id = comments.user_id AND users.active = 1')
                                ->where('comments.game_id = :id AND comments.game_type = "city" AND comments.deleted = "no"', [':id' => 2])
                                ->orderBy('comments.created DESC')
                                ->having('users.username IS NOT NULL')
                                ->all();
        $this->socialData = [
        'description' => $this->getSocialData('003'), 
        'keywords' => 'crossword.am online-crossword, armenian crossword, ցիտիես, armenian քաղաքներ, հայկական goroda, vgoroda, gorodki, խաչբառ, օնլայն, հայկական, crossword.am գլխավոր էջ, հայկական խաչբառեր, օնլայն խաղեր', 
        'image' => '/images/uploads/79331e7c3973510fb5895b0b4f901839.jpg'];
        return $this->render('us'.$this->language_view, ['game' => $game, 'cities' => $array]);
    }

    public function actionReset()
    {
        $country = Yii::$app->request->get('name', '');
        Yii::$app->session['cities'] = [];
        if($country) {
            return $this->redirect($this->prefix_language.'/city/'.$country);
        } else {
            return $this->redirect($this->prefix_language.'/city/am');
        }
    }

    public function actionGetCity()
    {
        $city = Yii::$app->request->get('city', '');
        $array = $this->getCities();
        if($array && !empty($array) && $city) {
            $query = new Query();

            $data = $query->select($array['country'].'_cities.*')
                        ->from($array['country'].'_cities')
                        ->where('id NOT IN ('.implode(',', $array['ids']).') AND '
                            .'name = "'.$array['last_char'].$city.'"')
                        ->orderBy('rand()')
                        ->one();

            if(!empty($data)) {
                
                $array['ids'][] = $data['id'];
                $array['cities'][] = ['name' => $data['name'], 'lat_lng' => $data['lat_lng']];
                $rest = substr($data['name'], -2);
                if(($rest == 'gh' || $rest == 'ch' || $rest == 'sh') && $array['country'] == 'am') {
                    $array['last_char'] = $rest;
                } else {
                    $array['last_char'] = substr($data['name'], -1);
                }
                $this->saveCity($array);
                $lat = null;
                $lng = null;
                if($data['lat_lng']) {
                    $lat_lng = explode(',', $data['lat_lng']);
                    if(isset($lat_lng[0])) {
                        $lat = $lat_lng[0];
                    }
                    if(isset($lat_lng[1])) {
                        $lng = $lat_lng[1];   
                    }
                }
                $json = [];
                $json['user'] = ['cityname' => $data['name'], 'lat' => $lat, 'lng' => $lng];

                $query = new Query();

                $comp = $query->select($array['country'].'_cities.*')
                            ->from($array['country'].'_cities')
                            ->where('id NOT IN ('.implode(',', $array['ids']).') AND '
                                .'name LIKE "'.$array['last_char'].'%"')
                            ->orderBy('rand()')
                            ->one();
                if(!empty($comp)) {
                
                    $array['ids'][] = $comp['id'];
                    $array['cities'][] = ['name' => $comp['name'], 'lat_lng' => $comp['lat_lng']];
                    $rest = substr($comp['name'], -2);
                    if(($rest == 'gh' || $rest == 'ch' || $rest == 'sh') && $array['country'] == 'am') {
                        $array['last_char'] = $rest;
                    } else {
                        $array['last_char'] = substr($comp['name'], -1);
                    }
                    $this->saveCity($array);
                    $lat = null;
                    $lng = null;
                    if($comp['lat_lng']) {
                        $lat_lng = explode(',', $comp['lat_lng']);
                        if(isset($lat_lng[0])) {
                            $lat = $lat_lng[0];
                        }
                        if(isset($lat_lng[1])) {
                            $lng = $lat_lng[1];   
                        }
                    }
                    $json['comp'] = ['compname' => $comp['name'], 'complat' => $lat, 'complng' => $lng];
                    $json['firstLetter'] = $array['last_char'];
                    echo json_encode($json);
                } else {
                    echo json_encode($json);
                }
            } else {
                echo json_encode(['message' => 'Invalid city.']);
            }
        }
        else {
            echo json_encode(['message' => 'Invalid data.']);
        }
        die;
    }
}