<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\data\Pagination;
use app\models\Users;
use app\models\EditUserForm;
use app\models\Admins;
use app\models\AdminLoginForm;
use app\models\AddAdminForm;
use app\models\EditAdminForm;
use app\models\Words;
use app\models\AddWordForm;
use app\models\EditWordForm;
use app\models\Contact;
use app\models\Crosswords;
use app\models\AddCrosswordForm;
use app\models\EditCrosswordForm;
use app\models\Pluses;
use app\models\Comments;
use app\models\EditCommentForm;
use app\components\XLSXReader;
use app\models\Discussion;
use app\models\DiscussionTexts;
use app\models\AddDiscussionForm;
use app\models\EditDiscussionForm;
use app\models\Categories;
use app\models\AddCategoryForm;
use app\models\EditCategoryForm;
use app\models\EditTextForm;
use app\models\SavedWords;
use app\models\SavedCrosswords;
use app\models\Posts;
use app\models\AddPostForm;
use app\models\EditPostForm;
use app\models\Notifications;

use Yii;

class AdminController extends AdminAppController 
{    
    public $layout = '/admin';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                                    'championship',
                                    'logout',
                                    'login',
                                    'index', 
                                    'blog',
                                    'activate-post',
                                    'disable-post',
                                    'delete-post',
                                    'add-post',
                                    'edit-post',
                                    'contact',
                                    'publicate-contact',
                                    'disable-contact',
                                    'delete-contact',
                                    'users',
                                    'disable-user',
                                    'admins',
                                    'edit-admin',
                                    'delete-admin',
                                    'add-admin',
                                    'delete-user',
                                    'edit-user',
                                    'activate-user',
                                    'words',
                                    'saved-words',
                                    'saved-crosswords',
                                    'all-discussions',
                                    'pluses',
                                    'crosswords',
                                    'categories',
                                    'activate-category',
                                    'disable-category',
                                    'edit-category',
                                    'delete-category',
                                    'add-word',
                                    'edit-word',
                                    'delete-word',
                                    'activate-word',
                                    'disable-word',
                                    'add-crossword',
                                    'edit-crossword',
                                    'delete-crossword',
                                    'activate-crossword',
                                    'disable-crossword',
                                    'import-users',
                                    'save-user',
                                    'comments',
                                    'delete-comment',
                                    'edit-comment',
                                    'discussions',
                                    'discussion',
                                    'add-discussion',
                                    'edit-discussion',
                                    'delete-discussion',
                                    'delete-discussion-text',
                                    'delete-text',
                                    'edit-text',
                                    'images',
                                    'upload-image',
                                    'delete-image',
                                    'blog',
                                    'notifications',
                                    'delete-notif',
                                    'delete-pluse',
                                    'blank-action',
                                    ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?']
                    ],   
                ],
            ]
        ];
    }            

    public function actionBlankAction()
    {
        echo 'Blank action used for doing somethings';

        die;
    }
    
    public function actionIndex() 
    {        
        $last_comment = new Comments();
        $last_comment = $last_comment->find()->orderBy('created DESC')->asArray()->one();

        $last_discussion = new DiscussionTexts();
        $last_discussion = $last_discussion->find()->orderBy('created DESC')->asArray()->one();

        $last_user = new Users();
        $last_user = $last_user->find()->orderBy('modified DESC')->asArray()->one();

        $pluses = Yii::$app->db->createCommand('SELECT COUNT(pluses.id) FROM pluses')
             ->queryScalar();

        $words = Yii::$app->db->createCommand('SELECT COUNT(saved_words.id) FROM saved_words')
             ->queryScalar();

        $crosswords = Yii::$app->db->createCommand('SELECT COUNT(saved_crosswords.id) FROM saved_crosswords')
             ->queryScalar();
        
        return $this->render('index', [
            'comment' => $last_comment, 
            'discussion' => $last_discussion, 
            'user' => $last_user,
            'pluses' => $pluses,
            'saved_words' => $words,
            'saved_crosswords' => $crosswords,
            ]);
    }

    public function actionPluses()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;        

        $pluses = new Pluses();
        $result = $pluses->getAll($limit, $offset);

        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        

        return $this->render('pluses', ['pluses' => $result['pluses'], 'pages' => $pages, 'count' => $result['count']]);
    }

    public function actionDeletePluse($id, $url)
    {
        if($id)
        {
            Yii::$app->db->createCommand()
                ->delete('pluses', 'id = :id',[':id' => $id])
                ->execute();
            $this->alert('success', 'Pluse deleted.');
        }
        return $this->redirect($url);
    }

    public function actionNotifications()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;        

        $notifications = new Notifications();
        $result = $notifications->getAll($limit, $offset);

        $pages = new Pagination();
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        

        return $this->render('notifications', ['notifications' => $result['notifications'], 'pages' => $pages, 'count' => $result['count']]);
    }

    public function actionDeleteNotif($id, $url)
    {
        if($id)
        {
            Yii::$app->db->createCommand()
                    ->delete('notifications', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Notification deleted.');
        }
        return $this->redirect($url);
    }

    public function actionSavedWords()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;        

        $saved = new SavedWords();
        $result = $saved->getAll($limit, $offset);

        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        

        return $this->render('saved-words', ['saved' => $result['saved'], 'pages' => $pages, 'count' => $result['count']]);
    }
    
    public function actionSavedCrosswords()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;        

        $saved = new SavedCrosswords();
        $result = $saved->getAll($limit, $offset);

        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        

        return $this->render('saved-crosswords', ['saved' => $result['saved'], 'pages' => $pages, 'count' => $result['count']]);
    }

    public function actionDiscussions()
    {
        $discussions = new Discussion();
        $discussions = $discussions->getAll();

        return $this->render('discussions', ['discussions' => $discussions]);
    }

    public function actionAllDiscussions()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;        

        $discussions = new DiscussionTexts();
        $result = $discussions->getAllTexts($limit, $offset);

        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        

        return $this->render('all-discussions', ['discussions' => $result['discussions'], 'pages' => $pages, 'count' => $result['count']]);
    }

    public function actionCategories()
    {
        $model = new AddCategoryForm();
        $editModel = new EditCategoryForm();

        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->add())
        {
            $this->alert('success', 'Category added successfully.');
            return $this->redirect('/admin/categories');
        }
        
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        
        $conditions = '';

        $result = new Categories();
        $result = $result->getAllCategories($conditions, $limit, $offset);
        
        $categories = $result['categories'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
                
        return $this->render('categories', ['categories' => $categories,  'count' => $result['count'], 'pages' => $pages, 'model' => $model]);
    }

    public function actionEditCategory($id)
    {
        $editModel = new EditCategoryForm();

        if($editModel->load(Yii::$app->request->post()) && $editModel->edit())
        {
            $this->alert('success', 'Category edited successfully.');
            return $this->redirect('/admin/categories');   
        }

        $category = new Categories();
        $category = $category->find()->where(['id' => $id])->one();

        return $this->render('edit-category', ['model' => $editModel, 'category' => $category]);
    }

    public function actionActivateCategory($id)
    {
        if($id)
        {
            Yii::$app->db->createCommand('UPDATE categories SET active = "yes" WHERE id = '.$id)
                         ->execute();
            $this->alert('success', 'Category activated.');
        }
        return $this->redirect('/admin/categories');
    }

    public function actionDisableCategory($id)
    {
        if($id)
        {
            Yii::$app->db->createCommand('UPDATE categories SET active = "no" WHERE id = '.$id)
                         ->execute();
            $this->alert('success', 'Category disabled.');
        }
        return $this->redirect('/admin/categories');
    }

    public function actionDeleteCategory($id)
    {
        if($id)
        {
            Yii::$app->db->createCommand()
                    ->delete('categories', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Category deleted.');
        }
        return $this->redirect('/admin/categories');
    }
    public function actionDiscussion($id)
    {
        if($id)
        {
            $discussion = new Discussion();
            $discussion = $discussion->getDiscussion($id);

            return $this->render('discussion', ['discussion' => $discussion]);
        }
        else {
            return $this->redirect('/admin/discussions');
        }
    }

    public function actionAddDiscussion()
    {
        $model = new AddDiscussionForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate() && $model->add()){
                $this->alert('success', 'Discussion added successfully.');
                return $this->redirect('/admin/discussions');
            }
            else {
                var_dump($model->getErrors());die;
            }
        }
        return $this->render('add-discussion', ['model' => $model]);     
    }

    public function actionEditDiscussion($id)
    {
        $model = new EditDiscussionForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate() && $model->edit()){
                $this->alert('success', 'Discussion edited successfully.');
                return $this->redirect('/admin/discussions');
            }
            else {
                var_dump($model->getErrors());die;
            }
        }
        if($id)
        {
            $discussion = new Discussion();
            $discussion = $discussion->findOne(['id' => $id]);
            return $this->render('edit-discussion', ['model' => $model, 'discussion' => $discussion]);     
        }
    }

    public function actionDeleteDiscussionText($id, $url)
    {
        if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('discussion_texts', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Discussion Text deleted successfully.');
        }
        return $this->redirect($url);
    }

    public function actionDeleteDiscussion($id)
    {
        if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('discussion', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Discussion deleted successfully.');
        }
        return $this->redirect('/admin/discussions');
    }
    
    public function actionLogin() {

        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/admin/index');
        }
        

        $model = new AdminLoginForm();
        if ($model->load(Yii::$app->request->post())){
            $captcha = Yii::$app->request->post('g-recaptcha-response', '');

            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcZzg8TAAAAAGcYr6I6RtoNxldFMHXf7gUXE55u&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
            $response = json_decode($response);
            if($response->success == false && stripos($_SERVER['SERVER_NAME'], '.am')) {
                $this->alert('success', 'Invalid captcha.');
                return $this->redirect('/admin/index');
            }
            if($model->login()) {
                return $this->redirect('/admin/index');
            }
        }        

        return $this->render('login', ['model' => $model]);
    }


    public function actionCrosswords()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        
        $conditions = '';

        $result = new Crosswords();
        $result = $result->getAllCrosswords($conditions, $limit, $offset);
        
        $crosswords = $result['crosswords'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
        
        return $this->render('crosswords', ['crosswords' => $crosswords, 'count' => $result['count'], 'pages' => $pages]);
    }

    public function actionAddCrossword()
    {
        $model = new AddCrosswordForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate() && $model->add()){
                $this->alert('success', 'Crossword added successfully.');
                return $this->redirect('/admin/crosswords');
            }
            else {
                var_dump($model->getErrors());die;
            }
        }
        return $this->render('add-crossword', ['model' => $model]);     
    }

    public function actionDeleteCrossword($id, $url)
    {        
        if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('crosswords', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Crossword deleted successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionEditCrossword($id, $url) {
                
        $model = new EditCrosswordForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'Crossword updated successfully.');
            }
            else {
                var_dump($model->getErrors()); die;
            }
        }    
        
        if ($id) {
            $crossword = new Crosswords();
            $crossword = $crossword->find()->where(['id' => $id])->one();
            return $this->render('edit-crossword',['model' => $model, 'crossword' => $crossword]);
        }   
        else {
            return $this->redirect($url);
        }                
    }
    
    public function actionActivateCrossword($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE crosswords SET active = "yes" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Crossword activated successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionDisableCrossword($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE crosswords SET active = "no" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Crossword disabled successfully.');
        }
        
        return $this->redirect($url);
    }

    
    public function actionUsers() {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        $order = Yii::$app->request->get('order', 'users.modified');
        $conditions = '';

        $users = array();               
        
        $users = new Users();
        $users = $users->getAllUsers($conditions, $limit, $offset, $order);
        
        $pages = new Pagination();                   
        $pages->totalCount = $users['count'];
        $pages->setPageSize('20'); 
        
        return $this->render('users', ['users' => $users['users'], 'pages' => $pages, 'order' => $order, 'count' => $users['count']]);
    }
    
    public function actionWords() {
        
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        
        $category = Yii::$app->request->get('category', '');

        $conditions = '';

        if($category)
        {
            $conditions = " AND words.category_id = ".$category;
        }
        $words = array();
        
        $result = new Words();
        $result = $result->getAllWords($conditions, $limit, $offset);
        
        $words = $result['words'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
        
        return $this->render('words', ['words' => $words, 'count' => $result['count'], 'pages' => $pages, 'category' => $category]);
    }
    
    public function actionContact() {
        
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
                
        $contacts = array();
        
        $result = new Contact();
        $result = $result->getAllContacts($limit, $offset);
        
        $contacts = $result['contacts'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
        
        return $this->render('contact', ['contacts' => $contacts, 'count' => $result['count'], 'pages' => $pages]);
    }
    
    public function actionDeleteContact($id, $url)
    {        
         if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('contact', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Contact deleted successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionPublicateContact($id, $url)
    {        
        if($id) {
            Yii::$app->db->createCommand('UPDATE contact SET public = 1 '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Contact message published successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionDisableContact($id, $url)
    {        
        if($id) {
            Yii::$app->db->createCommand('UPDATE contact SET public = 0 '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Contact message disabled successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionAddWord() 
    {        
        $model = new AddWordForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate() && $model->add()){
                $this->alert('success', 'Word added successfully.');
                return $this->redirect('/admin/words');
            }
        }
        return $this->render('add-word', ['model' => $model]);        
    }
    
    public function actionDeleteWord($id, $url)
    {        
        if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('words', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Word deleted successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionEditWord($id, $url) {
                
        $model = new EditWordForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'Word updated successfully.');
            }
        }    
        
        if ($id) {
            $word = new Words();
            $word = $word->find()->where(['id' => $id])->one();            
            return $this->render('edit-word',['model' => $model, 'word' => $word]);
        }   
        else {
            return $this->redirect($url);
        }                
    }
    
    public function actionActivateWord($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE words SET active = "yes" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Word activated successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionDisableWord($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE words SET active = "no" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Word disabled successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionEditUser($id) {
                
        $model = new EditUserForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'User has been successfully saved.');
            }            
        }    
        
        if ($id) {
            $user = new Users();
            $user = $user->find()->where(['id' => $id])->one();            
        }   
        
        return $this->render('edit-user',['model' => $model, 'user' => $user]);
    }
    
    public function actionActivateUser($id) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE users SET active = true '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Success.');
        }
        
        return $this->redirect('/admin/users');
    }
    
    public function actionDeleteUser($id, $url) {

        if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('users', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'User deleted successfully.');
            return $this->redirect($url);
        }   
    }

    public function actionDisableUser($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE users SET active = "0" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'User disabled successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionAdmins()
    {
        $admins = array();
        
        $admins = new Admins();
        $admins = $admins->getAll();
        
        return $this->render('admins', ['admins' => $admins]);
    }
    
    public function actionAddAdmin() 
    {        
        $model = new AddAdminForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate() && $model->add()){
                $this->alert('success', 'Admin added successfully.');
                return $this->redirect('/admin/admins');
            }
        }
        return $this->render('add-admin', ['model' => $model]);        
    }
    
    public function actionDeleteAdmin($id)
    {        
         if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('admins', 'id = :id AND username <> :username',[':id' => $id, ':username' => 'davittovmasyan'])
                    ->execute();
            $this->alert('success', 'Admin deleted successfully.');
            return $this->redirect('/admin/admins');
        }
        
        return $this->redirect('/admin/admins');
    }
    
    public function actionEditAdmin($id) {
                
        $model = new EditAdminForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'Admin updated successfully.');
            }            
        }    
        
        if ($id) {
            $admin = new Admins();
            $admin = $admin->find()->where(['id' => $id])->one();            
            return $this->render('edit-admin',['model' => $model, 'admin' => $admin]);
        }   
        else {
            return $this->redirect('/admin/admins');
        }
        
        
    }

    public function actionImportUsers()
    {
        die;

        $path = __DIR__;
        $path = str_replace('controllers', '', $path);
        $path = $path . 'dev/users.xlsx';
        $xlsx = new XLSXReader($path);

        $sheetNames = $xlsx->getSheetNames();
        foreach ($sheetNames as $sheetName) {
            $sheet = $xlsx->getSheet($sheetName);
            $users = $sheet->getData();
        }

        unset($users[0]);

        return $this->render('import-users',['users' => $users]);
    }

    public function actionSaveUser()
    {
        if(Yii::$app->request->post())
        {
            $randomCoords = [
                '40.228779,44.4573611',
                '40.2374277,44.453572',
                '40.245679,44.4448623',
                '40.2590925,44.4537144',
                '40.2785442,44.4651606',
                '40.2950191,44.4772594',
                '40.2998442,44.4536704',
                '40.3710165,44.4486236',
                '41.0005906,44.4700772',
                '40.9308058,44.3951986',
                '40.8699843,44.321196',
                '40.9475034,44.1688446',
                '40.9842674,44.1392442',
                '41.0255741,44.1275985',
                '41.0523986,44.1677575',
                '41.085746, 43.988877',
                '40.0804893,44.5223821',
                '40.0942091,44.5116128',
                '40.1093889,44.4910872',
                '40.1141797,44.5137942',
                '40.127288,44.5379182',
                '40.1436578,44.5335636',
                '40.1632195,44.5183257',
                '40.1738271,44.5091649',
                '40.1909048,44.4891805',
            ];
            $user = new Users();
            $user->username = strtolower(Yii::$app->request->post('username'));
            $user->email = strtolower(Yii::$app->request->post('email'));
            $user->first_name = Yii::$app->request->post('first_name', null);
            $user->last_name = Yii::$app->request->post('last_name', null);
            if($user->first_name)
            {
                $user->first_name = strtolower($user->first_name);
            }
            if($user->last_name)
            {
                $user->last_name = strtolower($user->last_name);
            }
            $user->points = Yii::$app->request->post('points', 0);
            $user->current_points = Yii::$app->request->post('points', 0);
            $user->age = Yii::$app->request->post('age', '18');
            $user->sex = Yii::$app->request->post('sex', 'boy');
            $user->setPassword(Yii::$app->request->post('password'));
            $user->address = null;
            $user->city = null;
            $user->state = null;
            $user->street = null;
            $user->zip_code = null;
            $user->pluses = 1;
            $user->lat_lng = $randomCoords[rand(0,23)];
            $user->image = Yii::$app->ImageMaker->generate($user->username);
            $user->active = 1;
            $user->facebook_id = null;
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();

            if ($user->save()) {
                echo json_encode(array('data' => true));
                $pluse = new Pluses();
                $pluse->to_user_id = $user->id;
                $pluse->from_user_id = 1;
                $pluse->save();
                if(!strpos($user->email, '@crossword.am'))
                {
                    try {
                        $name = ($user->first_name && $user->last_name) ? ucfirst($user->first_name) . ' ' . ucfirst($user->last_name) : ucfirst($user->username);
                        Yii::$app->mailer->compose('invite', ['name' => $name, 'username' => $user->username, 'password' => Yii::$app->request->post('password'), 'points' => $user->points])
                            ->setFrom([Yii::$app->params['noReplyEmail'] => 'Crossword.am - Ծանուցումներ'])
                            ->setTo($user->email)
                            ->setSubject('Կայքի թարմացում.')
                            ->send();
                    }
                    catch(Exception $e) {
                        
                    }
                }
            } 
            else {
                echo json_encode(array('data' => false));
                var_dump($user->getErrors()); die;
            }
        }
        die;
    }


    public function actionComments()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        
        $conditions = '';

        $result = new Comments();
        $result = $result->getAllComments($conditions, $limit, $offset);
        
        $comments = $result['comments'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
        
        return $this->render('comments', ['comments' => $comments, 'count' => $result['count'], 'pages' => $pages]);
    }

    public function actionDeleteComment($id, $url)
    {        
        if ($id) {
            Yii::$app->db->createCommand()
                    ->update('comments', ['deleted' => "yes"], 'id = '.$id)->execute();
            $this->alert('success', 'Comment deleted successfully.');
        }
        
        return $this->redirect($url);
    }

    public function actionDeleteText($id, $url)
    {        
        if ($id) {
            Yii::$app->db->createCommand()
                    ->update('discussion_texts', ['deleted' => "yes"], 'id = '.$id)->execute();
            $this->alert('success', 'Discussion deleted successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionEditComment($id, $url) {
                
        $model = new EditCommentForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'Comment updated successfully.');
                return $this->redirect($url);
            }
            else {
                var_dump($model->getErrors()); die;
            }
        }    
        
        if ($id) {
            $comment = new Comments();
            $comment = $comment->find()->where(['id' => $id])->one();
            return $this->render('edit-comment',['model' => $model, 'comment' => $comment]);
        }   
        else {
            return $this->redirect($url);
        }                
    }

    public function actionEditText($id, $url) {
                
        $model = new EditTextForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'Discussion updated successfully.');
                return $this->redirect($url);
            }
            else {
                var_dump($model->getErrors()); die;
            }
        }    
        
        if ($id) {
            $discussion = new DiscussionTexts();
            $discussion = $discussion->find()->where(['id' => $id])->one();
            return $this->render('edit-text',['model' => $model, 'discussion' => $discussion]);
        }   
        else {
            return $this->redirect($url);
        }                
    }

    public function actionImages()
    {
        $path = str_replace('controllers', 'web/images/uploads/', __DIR__);
        $files = glob($path.'*.{jpg,jpeg,png,gif}', GLOB_BRACE);
        array_multisort(array_map('filemtime', $files), SORT_NUMERIC, SORT_DESC, $files);

        return $this->render('images', ['files' => $files]);
    }

    public function actionUploadImage()
    {
        $path = str_replace('controllers', 'web/images/uploads/', __DIR__);
        if(Yii::$app->request->post())
        {
            $files = $_FILES['images'];
            for($i = 0; $i<count($files['name']); $i++) {
                $new_name = md5(date("Y-mm-dd H:i:s").microtime().$i);
                $file_ext = explode('.',$files['name'][$i]);
                $file_ext = end($file_ext);
                $file_ext = strtolower($file_ext);
                move_uploaded_file($files['tmp_name'][$i], $path.$new_name.'.'.$file_ext);
                $this->alert('success', 'Image uploaded successfully.');
            }
        }
        return $this->redirect('/admin/images');
    }

    public function actionDeleteImage($id)
    {
        $path = str_replace('controllers', 'web/images/uploads/', __DIR__);
        $image = $path.$id;
        unlink($image);
        $this->alert('success', 'Image deleted successfully.');
        return $this->redirect('/admin/images');
    }
    
    public function actionLogout()
    {        
        Yii::$app->user->logout();        
        return $this->redirect('/admin/login');
    }

    public function actionChampionship()
    {
        Yii::$app->db->createCommand()
                        ->delete('points')
                        ->execute();
        $users = new Users();
        $users = $users->find()->asArray()->all();
        $points = [];
        $now = date("Y-m-d H:i:s");
        foreach ($users as $user) {
            $points[] = [
                $user['id'], $user['points'], $now, $now
            ];
        }
        Yii::$app->db->createCommand()
                    ->batchInsert('points', ['user_id', 'points', 'created', 'modified'], $points)
                    ->execute();

        $this->alert('success', 'Championship started.');
        return $this->redirect('/admin/index');
    }

    public function actionBlog()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;

        $conditions = '';

        $posts = array();
        
        $result = new Posts();
        $result = $result->getAllPosts($limit, $offset, $conditions);
        
        $posts = $result['posts'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
        
        return $this->render('posts', ['posts' => $posts, 'count' => $result['count'], 'pages' => $pages]);
    }

    public function actionAddPost() 
    {        
        $model = new AddPostForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate() && $model->add()){
                $this->alert('success', 'Post added successfully.');
                return $this->redirect('/admin/blog');
            }
        }
        return $this->render('add-post', ['model' => $model]);        
    }
    
    public function actionDeletePost($id, $url)
    {        
        if ($id) {
            Yii::$app->db->createCommand()
                    ->delete('posts', 'id = :id',[':id' => $id])
                    ->execute();
            $this->alert('success', 'Post deleted successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionEditPost($id, $url) {
                
        $model = new EditPostForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->edit()) {
                $this->alert('success', 'Post updated successfully.');
            }
        }    
        
        if ($id) {
            $post = new Posts();
            $post = $post->find()->where(['id' => $id])->one();
            return $this->render('edit-post',['model' => $model, 'post' => $post]);
        }   
        else {
            return $this->redirect($url);
        }                
    }
    
    public function actionActivatePost($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE posts SET active = "yes" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Post activated successfully.');
        }
        
        return $this->redirect($url);
    }
    
    public function actionDisablePost($id, $url) {
                
        if($id) {
            Yii::$app->db->createCommand('UPDATE posts SET active = "no" '
                    . 'WHERE id = '.$id)->execute();
            $this->alert('success', 'Post disabled successfully.');
        }
        
        return $this->redirect($url);
    }
}