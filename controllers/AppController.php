<?php
namespace app\controllers;

use yii\web\Controller;
use yii\web\Application;
use yii\helpers\Url;
use Yii;

class AppController extends Controller {
    
    public $language = '';
    public $prefix_language = '';
    public $language_prefix = '';
    public $language_view = '';
    
    public function init()
    {
        parent::init();
        
        $languages = Yii::$app->params['languages'];
        Yii::$app->language = 'hy';
        // Set the application language if provided by GET, session or cookie
        if(isset($_GET['language']) && $_GET['language'] == 'hy') {
            Yii::$app->language = 'hy';
            Yii::$app->session->set('language', 'hy');
            $cookie = new \yii\web\Cookie([
                'name' => 'language',
                'value' => 'hy',
            ]);
            $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
            Yii::$app->response->cookies->add($cookie);
        } else if (isset($_GET['language']) && isset($languages[$_GET['language']])) {
            Yii::$app->language = $_GET['language'];
            Yii::$app->session->set('language', $_GET['language']);
            $cookie = new \yii\web\Cookie([
                'name' => 'language',
                'value' => $_GET['language'],
            ]);
            $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
            Yii::$app->response->cookies->add($cookie);
        } else if(isset($_GET['language']) == false && Url::to() != '/') {
            Yii::$app->language = 'hy';
            Yii::$app->session->set('language', 'hy');
            $cookie = new \yii\web\Cookie([
                'name' => 'language',
                'value' => 'hy',
            ]);
            $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
            Yii::$app->response->cookies->add($cookie);
        } else if (Yii::$app->session->has('language') && Url::to() == '/') {
            Yii::$app->language = Yii::$app->session->get('language');
        } else if (isset(Yii::$app->request->cookies['language']) && Url::to() == '/') {
            Yii::$app->language = Yii::$app->request->cookies['language']->value;
        } else {
            Yii::$app->language = 'hy';
            Yii::$app->session->set('language', 'hy');
            $cookie = new \yii\web\Cookie([
                'name' => 'language',
                'value' => 'hy',
            ]);
            $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
            Yii::$app->response->cookies->add($cookie);
        }
        
        $this->language = (Yii::$app->language && isset($languages[Yii::$app->language]) ) ? Yii::$app->language : '';
        $this->prefix_language = $this->language ? '/'.$this->language : '';
        $this->language_prefix = $this->language ? $this->language.'/' : '';
        $this->language_view = $this->language ? '_'.$this->language : '';
    }

    
    
    public function alert($type, $message)
    {
       $alertTypes = [
           'error',
           'danger',
           'success',
           'info',
           'warning'
       ];
       if(in_array($type, $alertTypes)) {
           Yii::$app->getSession()->setFlash($type, $message);
       }
    }
    
    public function beforeAction($action)
    {
        date_default_timezone_set('Asia/Yerevan');
        Yii::$app->view->params['language'] = $this->language;
        Yii::$app->view->params['prefix_language'] = $this->prefix_language;
        Yii::$app->view->params['language_prefix'] = $this->language_prefix;
        $this->layout = 'main'.$this->language_view;
        return parent::beforeAction($action);
    }

    public function getAlertMessage($code)
    {
        $messages = [
            '001' => [
                'hy' => 'Ցավոք դեռ նոր խաչբառեր չկան...',
                'en' => 'Sorry, new crosswords comming soon...',
                'ru' => 'Извините, новых кроссвордов пока нет...',
            ],
            '002' => [
                'hy' => 'ձեր փնտրած խաչբառը գոյություն չունի կամ ակտիվ չէ',
                'en' => 'crossword not exists',
                'ru' => 'такого кроссворда у нас нет',
            ],
            '003' => [
                'hy' => 'շնորհակալություն կապ հաստատելու համար, անհրաժեշտության դեպքում մենք կկապնվենք ձեզ հետ Էլ հասցեի միջոցով...',
                'en' => 'thank for message, we will contact with you by email...',
                'ru' => 'спасибо за внимание, мы свяжемся с вами по электронной почте...',
            ],
            '004' => [
                'hy' => 'քննարկումը, որը դուք փնտրում եք գոյություն չունի կամ ակտիվ չէ...',
                'en' => 'discussion is not exists...',
                'ru' => 'такого обсуждения у нас нет...',
            ],
            '005' => [
                'hy' => 'դուք հաջողությամբ գրանցվեցիք...',
                'en' => 'you are registered successfully...',
                'ru' => 'вы успешно зарегестрировались...',
            ],
            '006' => [
                'hy' => 'մուտք գործեք ձեր էլ հասցե, որպեզի կարողանաք վերականգնել գաղտնաբառը...',
                'en' => 'check your email to reset your password...',
                'ru' => 'провертье почту, чтобы восстановить пароль...',
            ],
            '007' => [
                'hy' => ' այս էլ հասցեն գոյություն չունի մեր տվյալներում....',
                'en' => ' this email is not exists...',
                'ru' => ' такого адреса у нас нет...',
            ],
            '008' => [
                'hy' => 'ձեր հղման ժամկետը սպառված է, նորից փորձեք կատարել հարցում...',
                'en' => 'reset link expired, try again...',
                'ru' => 'время ссылки истекло, попробуйте снова...',
            ],
            '009' => [
                'hy' => 'գաղտնաբառը փոփոխված է...',
                'en' => 'password changed...',
                'ru' => 'пароль изменен...',
            ],
            '010' => [
                'hy' => 'Ուշադիր լրացրեք բոլոր դաշտերը.',
                'en' => 'All field are required.',
                'ru' => 'Заполняйте все поля.',
            ],
            '011' => [
                'hy' => 'ձեր փնտրած հոդվածը գոյություն չունի կամ ակտիվ չէ',
                'en' => 'the page you are looking is not exists or inactive',
                'ru' => 'страница которую вы ищите не существует или не активна',
            ],
            '012' => [
                'hy' => 'ձեր փոփոխությունները պահպանված են...',
                'en' => 'your changes updated...',
                'ru' => 'ваши изменения сохранены...',
            ],
        ];

        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        return isset($messages[$code][$lang]) ? $messages[$code][$lang] : '';
    }

    public function getSocialData($code)
    {
        $messages = [
            '001' => [
                'hy' => 'Հայտնի քաղաքներ խաղը նաև օնլայն, առանձնացված պետություններով:',
                'en' => 'City-City game online - Crossword.am.',
                'ru' => 'Вгорода онлайн.',
            ],
            '002' => [
                'hy' => 'ՀՀ քաղաքներ',
                'en' => 'RA cities',
                'ru' => 'РА города',
            ],
            '003' => [
                'hy' => 'ԱՄՆ քաղաքներ',
                'en' => 'USA cities',
                'ru' => 'США города',
            ],
            '004' => [
                'hy' => 'Օնլայն խաչբառերի տարբեր լեզուներով, թեմաներով և բարդություններով:',
                'en' => 'Online crosswords in different languages and types.',
                'ru' => 'Онлайн кроссворды на разных языках.',
            ],
            '005' => [
                'hy' => 'Վիկտորինա օնլայն հետաքրքիր խաղը ձեզ է սպասում իր յուրահատուկ թեմաներով:',
                'en' => 'Online quiz game in different categories.',
                'ru' => 'Онлайн викторина на разных категориях.',
            ],
            '006' => [
                'hy' => 'Հետաքրքիր տեղեկություններ, հանելուկներ, նկարներ, տրամաբանական խնդիրներ:',
                'en' => 'Awesome new, posts, pictures, test and more.',
                'ru' => 'Интересные задачи, головаломки, картинки и публикации.',
            ],
            '007' => [
                'hy' => 'Օնլայն խաչբառերի և այլ հետաքրքիր խաղերի պորտալ: Սիրով սպասում ենք ձեզ:',
                'en' => 'Online crosswords and other awesome games portal. We love you.',
                'ru' => 'Портал различных игр - кроссворды, викторины и другое.',
            ],
        ];

        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        return isset($messages[$code][$lang]) ? $messages[$code][$lang] : '';
    }
}