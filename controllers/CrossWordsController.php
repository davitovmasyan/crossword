<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use app\models\Users;
use app\models\Crosswords;
use app\models\SavedCrosswords;
use app\models\Notifications;
use yii\helpers\Url;
use Yii;

 
class CrossWordsController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],                    
                    [
                        'actions' => ['all-solvers'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['list', 'play', 'save', 'random'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],                    
                ],
                'denyCallback'  => function ($rule, $action) {
                     return $this->redirect($this->prefix_language.'/site/errorPage');
                },
            ]
        ];
    }

    public $socialData = [];

    public function actionRandom()
    {
        if(Yii::$app->user->isGuest)
        {
            $crossword = new Crosswords();
            $crossword = $crossword->find(['active' => 'yes'])->orderBy('RAND()')->one();
            return $this->redirect($this->prefix_language.'/cross-words/play?id='.$crossword->id);
        } else {
            $crossword = new Crosswords();
            $crossword = $crossword->findRandom(Yii::$app->user->id);
            if(!empty($crossword))
            {
                return $this->redirect($this->prefix_language.'/cross-words/play?id='.$crossword['id']);
            }
            else {
                $this->alert('success', $this->getAlertMessage('001'));
                return $this->redirect($this->prefix_language.'/cross-words/list');
            }
        }
    }
    
    public function actionList()
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        
        $conditions = '';
        $crosswords = array();

        $selectedLang = Yii::$app->request->get('lang', '');
        if($selectedLang && $selectedLang != 'any')
        {
            $conditions .= ' AND crosswords.lang = "'.$selectedLang.'"';
        }
        $selectedType = Yii::$app->request->get('type', '');
        if($selectedType && $selectedType != 'any')
        {
            $conditions .= ' AND crosswords.type = "'.$selectedType.'"';   
        }
        
        $result = new Crosswords();
        $result = $result->getList($conditions, $limit, $offset);
        
        $crosswords = $result['crosswords'];
            
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
        $this->socialData = [
        'description' => $this->getSocialData('004'), 
        'keywords' => 'crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական, crossword.am գլխավոր էջ, հայկական խաչբառեր, օնլայն խաղեր', 
        'image' => '/images/cross-list-main-image.jpg'];
        return $this->render('list'.$this->language_view, [
            'crosswords' => $crosswords, 
            'pages' => $pages,
            'selectedType' => $selectedType,
            'selectedLang' => $selectedLang,
            'fbImage' => '/images/cross-list-main-image.jpg',
            ]);
    }

    public function actionPlay($id = 1)
    {        
        if($user_id = Yii::$app->user->id)
        {
            $crossword = new Crosswords();
            $crossword = $crossword->getCrossword($id, $user_id);
        }
        else {
            $crossword = new Crosswords();
            $crossword = $crossword->getCrossword($id);
        }

        if(!$crossword) {
            $this->alert('success', $this->getAlertMessage('002'));
            return $this->redirect($this->prefix_language.'/cross-words/list');
        }

        $saved_id = null;
        $saved_structure = '';
        $saved_status = '';
        if(isset($crossword['saved_id']) && isset($crossword['saved_structure']) && $crossword['saved_structure'] && $crossword['saved_id'])
        {
            $saved_structure = explode('|', $crossword['saved_structure']);
            
            $saved_id = $crossword['saved_id'];

            $saved_status = $crossword['sstatus'];
        }

        $structure = explode('|', $crossword['structure']);
        $questions = explode('$', $crossword['questions']);
        $this->socialData = ['description' => implode(', ', $questions), 'keywords' => implode(', ', $questions), 'image' => '/images/crosswords/'.$crossword['image']];
        return $this->render('play'.$this->language_view, [
            'crossword' => $crossword, 
            'structure' => $structure, 
            'questions' => $questions, 
            'saved_id' => $saved_id, 
            'saved_structure' => $saved_structure,
            'saved_status' => $saved_status,
        ]);
    }

    public function actionSave()
    {
        if(Yii::$app->request->post())
        {
            if(Yii::$app->user->isGuest)
            {
                $crossword = Yii::$app->request->post('id', '');
                $type = Yii::$app->request->post('type', '');
                $structure = Yii::$app->request->post('structure', '');                
                if($structure && $type && $crossword)
                {
                    $saved = [];
                    $saved[$crossword] = $structure;                    
                    Yii::$app->session->set('saved', ['structure' => $saved, 'type' => $type]);
                    echo json_encode(['data' => true]);
                }
            }
            else {
                $crossword = Yii::$app->request->post('id', '');
                $type = Yii::$app->request->post('type', '');
                $structure = Yii::$app->request->post('structure', '');
                $user_id = Yii::$app->user->id;
                $savedCheck = new SavedCrosswords();
                $savedCheck = $savedCheck->find()->where(['crossword_id' => $crossword, 'user_id' => $user_id])->asArray()->one();
                if(!empty($savedCheck) && $savedCheck['status'] != 'all') {
                    $saved = new SavedCrosswords();
                    $saved = $saved->find()->where(['crossword_id' => $crossword, 'user_id' => $user_id])->one();
                    if($type == 'partial' || $type == 'new') {
                        $saved->structure = $structure;
                        if($saved->validate() && $saved->save())
                        {
                            echo json_encode(['data' => true]);
                        }
                        else {
                            var_dump($saved->getErrors());
                        }
                    }
                    else if($type == 'all') {
                        $crosswordModel = new Crosswords();
                        $crosswordModel = $crosswordModel->find()->where(['id' => $crossword])->one();

                        $main_structure = explode('|', $crosswordModel->structure);

                        $saved_flag = false;
                        $saved->structure = $structure;
                        $saved->status = 'all';

                        $structure = explode('|', $structure);

                        $i = 0;
                        $index = 0;
                        $compared = 0;
                        foreach ($main_structure as $ms) {
                            $i++;
                            $j = 0;
                            $columns = explode(',', $ms);
                            foreach ($columns as $column) {
                                $j++;
                                if(is_numeric($column))
                                {
                                    $index++;
                                    foreach ($structure as $row) {
                                        if($row == $i.'-'.$j.'_'.$column)
                                        {
                                            $compared++;
                                        }
                                    }
                                }
                            }                            
                        }
                        if($index && $index == $compared)
                        {
                            $user = new Users();
                            $user = $user->find()->where(['id' => $user_id])->one();

                            $pk = 10;
                            switch($crosswordModel->type)
                            {
                                case 'easy' : $pk = 10; break;
                                case 'medium' : $pk = 20; break;
                                case 'hard' : $pk = 30; break;
                                case 'difficult' : $pk = 50; break;
                            }
                            $points = $index*$pk;
                            $user->points = $user->points + $points;
                            $user->current_points = $user->current_points + $points;

                            if($saved->update() && $user->update()) {
                                $n = new Notifications();
                                $n->to_user_id = $user->id;
                                $n->status = 'unreaded';
                                $n->url = Url::base(true).'/cross-words/play?id='.$saved->crossword_id;
                                $n->from_user_id = null;
                                $n->notification = 'Դուք հավաքեցիք '.$points.' միավոր համար '.$crosswordModel->public_number.' խաչբառից';
                                $n->save();
                                echo json_encode(['data' => true]);   
                            }
                            else {
                                echo json_encode(['data' => false]);   
                            }
                        }
                    }
                } else {
                    $saved = new SavedCrosswords();
                    $saved->user_id = $user_id;
                    $saved->crossword_id = $crossword;
                    $saved->structure = $structure;
                    if($type == 'partial' || $type == 'new') {
                        $saved->status = 'part';
                        if($saved->validate() && $saved->save())
                        {
                            echo json_encode(['data' => true]);   
                        }
                        else {
                            echo json_encode(['data' => false]);   
                        }
                    } else if($type == 'all') {
                        $saved->status = 'all';
                        $crosswordModel = new Crosswords();
                        $crosswordModel = $crosswordModel->find()->where(['id' => $crossword])->one();

                        $main_structure = explode('|', $crosswordModel->structure);

                        $saved_flag = false;
                        $saved->structure = $structure;
                        $saved->status = 'all';

                        $structure = explode('|', $structure);

                        $i = 0;
                        $index = 0;
                        $compared = 0;
                        foreach ($main_structure as $ms) {
                            $i++;
                            $j = 0;
                            $columns = explode(',', $ms);
                            foreach ($columns as $column) {
                                $j++;
                                if(is_numeric($column))
                                {
                                    $index++;
                                    foreach ($structure as $row) {
                                        if($row == $i.'-'.$j.'_'.$column)
                                        {
                                            $compared++;
                                        }
                                    }
                                }
                            }                            
                        }
                        if($index && $index == $compared)
                        {
                            $user = new Users();
                            $user = $user->find()->where(['id' => $user_id])->one();

                            $pk = 10;
                            switch($crosswordModel->type)
                            {
                                case 'easy' : $pk = 10; break;
                                case 'medium' : $pk = 20; break;
                                case 'hard' : $pk = 30; break;
                                case 'difficult' : $pk = 50; break;
                            }
                            $points = $index*$pk;
                            $user->points = $user->points + $points;
                            $user->current_points = $user->current_points + $points;

                            if($saved->save() && $user->update()) {
                                echo json_encode(['data' => true]);   
                            }
                            else {
                                echo json_encode(['data' => false]);   
                            }
                        }
                    }
                }
            }
        }
        die();
    }   

    public function actionAllSolvers($id)
    {
        if($id) {
            $saved = new SavedCrosswords();
            $result = $saved->getSolvers($id, Yii::$app->user->id);
            if($result) {
                echo json_encode(['count' => $result['count'], 'users' => $result['saved']]);
            } else {
                echo json_encode(['saved' => [], 'count' => 0]);
            }
        }
        die;
    }
}