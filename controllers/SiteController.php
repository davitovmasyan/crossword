<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\ConfirmEmailForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignUpForm;
use app\models\Users;
use app\models\Crosswords;
use app\models\Categories;
use app\models\Contact;
use app\models\DiscussionTexts;
use app\models\Posts;
use yii\helpers\Url;
use app\models\Notifications;
use Yii;

 
class SiteController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),        
                'rules' => [
                    [
                        'actions' => ['error', 'index', 'login', 'signup', 
                            'confirm-email', 'request-password-reset', 'reset-password', 'search',
                            'error', 'contact-us', 'captcha', 'about', 'faq', 'privacy-policy', 'games', 'leaderboard',
                            'how-to-use', 'other', 'blog', 'problem', 'discussions', 'post', 'championship'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],                    
                    [
                        'actions' => ['error', 'index', 'logout', 'error', 'contact-us', 'search',
                            'captcha', 'about', 'faq', 'privacy-policy', 'games', 'leaderboard', 
                            'how-to-use', 'other', 'blog', 'problem', 'discussions', 
                            'add-discussion', 'delete-discussion', 'post', 'championship'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback'  => function ($rule, $action) {
                     return $this->redirect($this->prefix_language.'/site/errorPage');
                },
            ]
        ];
    }

    public $socialData = [];
 
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor' => 0xf7931e,                
//                'fixedVerifyCode' => $this->generateCaptcha(),
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception, 'language' => $this->language_view]);
        }
    }
    
    public function actionGames()
    {
        return $this->render('games'.$this->language_view);
    }

    public function actionOther()
    {
        return $this->render('other'.$this->language_view);
    }

    public function actionBlog()
    {
        $limit = Yii::$app->request->get('per-page',10);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
        
        $conditions = '';

        $posts = new Posts();

        $result = $posts->getPosts($limit, $offset, $conditions);

        $posts = $result['posts'];

        $pages = new Pagination();
        $pages->totalCount = $result['count'];
        $pages->setPageSize('10');

        $this->socialData = [
        'description' => $this->getSocialData('006'),
        'keywords' => 'crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական, բլոգ, blog, տեղեկատվություն, հետաքրքիր', 
        'image' => '/images/blog-image.jpg'];

        return $this->render('blog'.$this->language_view, ['posts' => $posts, 'pages' => $pages]);
    }

    public function actionLeaderboard()
    {
        $leaders = new Users();
        $leaders = $leaders->getLeaders();

        return $this->render('leaderboard'.$this->language_view, ['leaders' => $leaders]);
    }

    public function actionHowToUse()
    {
        return $this->render('howtouse'.$this->language_view);
    }    
    
    public function actionAbout() 
    {
        $limit = Yii::$app->request->get('per-page',20);
        $offset = (Yii::$app->request->get('page', 1)-1)*$limit;
                
        $contacts = array();
        
        $result = new Contact();
        $result = $result->getPublicContacts($limit, $offset);
        
        $contacts = $result['contacts'];
        
        $pages = new Pagination();                   
        $pages->totalCount = $result['count'];
        $pages->setPageSize('20');        
                
        return $this->render('about'.$this->language_view, ['contacts' => $contacts, 'pages' => $pages]);
    }
    
    public function actionFaq() 
    {
        return $this->render('faq'.$this->language_view);
    }
 
    public function actionPrivacyPolicy() 
    {
        return $this->render('policy'.$this->language_view);
    }

    public function actionContactUs()
    {
        $model = new ContactForm();
        if (!Yii::$app->user->isGuest) {
            $model->email = Yii::$app->user->identity->attributes['email'];
            $model->name = Yii::$app->user->identity->attributes['first_name'] . ' ' . Yii::$app->user->identity->attributes['last_name'];
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->contact()) {
            $this->alert('success', $this->getAlertMessage('003'));
            return $this->refresh();
        }
                
        return $this->render('contact'.$this->language_view, [
            'model' => $model,
        ]);
    }

    public function actionChampionship()
    {
        return $this->render('championship'.$this->language_view);
    }

    public function actionSearch($id)
    {
        $results = [];
        $key = strip_tags($id);
        if($key) {
            $keys = explode(' ', $key);
            $crossword = '';
            $post = '';
            $quiz = '';
            if(!empty($keys)) {
                foreach ($keys as $key) {
                    if($crossword == '') {
                        $crossword = '(crosswords.crossword_name LIKE "%'.$key.'%" OR crosswords.category LIKE "%'.$key.'%"';
                    } else {
                        $crossword = $crossword.' OR crosswords.crossword_name LIKE "%'.$key.'%" OR crosswords.category LIKE "%'.$key.'%"';
                    }
                    if($post == '') {
                        $post = '(posts.title LIKE "%'.$key.'%" OR posts.keywords LIKE "%'.$key.'%"';
                    } else {
                        $post = $post.' OR posts.title LIKE "%'.$key.'%" OR posts.keywords LIKE "%'.$key.'%"';
                    }
                    if($quiz == '') {
                        $quiz = '(categories.category_name LIKE "%'.$key.'%" OR categories.keywords LIKE "%'.$key.'%"';
                    } else {
                        $quiz = $quiz.' OR categories.category_name LIKE "%'.$key.'%" OR categories.keywords LIKE "%'.$key.'%"';
                    }
                }
                $post = $post.')';
                $crossword = $crossword.')';
                $quiz = $quiz.')';
                $posts = new Posts();
                $posts = $posts->search($post);
                $crosswords = new Crosswords();
                $crosswords = $crosswords->search($crossword);
                $categories = new Categories();
                $categories = $categories->search($quiz);
                if(!empty($posts)) {
                    foreach ($posts as $post) {
                        $results[] = [
                            'type' => 'post',
                            'id' => $post['id'],
                            'title' => $post['title'],
                            'image' => $post['image'],
                        ];
                    }
                }
                if(!empty($crosswords)) {
                    foreach ($crosswords as $crossword) {
                        $results[] = [
                            'type' => 'crossword',
                            'id' => $crossword['id'],
                            'title' => $crossword['crossword_name'],
                            'image' => $crossword['image'],
                        ];
                    }   
                }
                if(!empty($categories)) {
                    foreach ($categories as $category) {
                        $results[] = [
                            'type' => 'quiz',
                            'id' => $category['id'],
                            'title' => $category['category_name'],
                            'image' => $category['image'],
                        ];
                    }   
                }
            } else {
                return $this->redirect($this->prefix_language.'/site/index');    
            }
        } else {
            return $this->redirect($this->prefix_language.'/site/index');
        }
        return $this->render('search'.$this->language_view, ['results' => $results]);
    }
    
    public function actionIndex() 
    {
        $this->socialData = [
        'description' => $this->getSocialData('007'),
        'keywords' => 'crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական, crossword.am գլխավոր էջ, հայկական խաչբառեր, օնլայն խաղեր',
        'image' => '/images/main-image.jpg'];
        return $this->render('index'.$this->language_view);
    }
 
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect($this->prefix_language.'/site/index');
        }
 
        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(isset(Yii::$app->session['saved'])) {

            }
            $url = Yii::$app->request->post('redirect', $this->prefix_language.'/user/account');
            $url = $url == $this->prefix_language.'/site/index' || $url == $this->prefix_language.'/site/signup' ? $this->prefix_language.'/user/account' : $url;  
            return $this->redirect($url);
        }
        else {
            return $this->redirect($this->prefix_language.'/site/index');
        }
        
    }
 
    public function actionLogout()
    {
        Yii::$app->user->logout();
 
        return $this->redirect($this->prefix_language.'/site/index');
    }


    public function actionDiscussions($id = null)
    {
        if($id)
        {
            $discussion = new DiscussionTexts();
            $discussion = $discussion->getTexts($id);
            $discussionName = Yii::$app->db->createCommand('SELECT name FROM discussion WHERE id = '.$id)
                                        ->queryOne();

            if(!empty($discussionName))
            {
                return $this->render('discussion'.$this->language_view, ['discussion' => $discussion, 'id' => $id, 'discussionName' => $discussionName['name']]);
            }
            else {
                $this->alert('success', $this->getAlertMessage('004'));
                return $this->redirect($this->prefix_language.'/site/discussions');
            }
        }
        else {
            $discussions = new DiscussionTexts();
            $discussions = $discussions->getDiscussions();
            return $this->render('discussions'.$this->language_view, ['discussions' => $discussions]);
        }
    }

    public function actionAddDiscussion()
    {
        $user_id = Yii::$app->user->id;

        if (Yii::$app->request->post()) {
            $months = [
                '01' => 'հունվար',
                '02' => 'փետրվար',
                '03' => 'մարտ',
                '04' => 'ապրիլ',
                '05' => 'մայիս',
                '06' => 'հունիս',
                '07' => 'հուլիս',
                '08' => 'օգոստոս',
                '09' => 'սեպտեմբեր',
                '10' => 'հոկտեմբեր',
                '11' => 'նոյեմբեր',
                '12' => 'դեկտեմբեր',    
            ];

            $discussionData = Yii::$app->request->post('DiscussionTexts', []);
            if($discussionData['discussion_id'] && $discussionData['text'])
            {
                $discussion = new DiscussionTexts();
                $discussion->user_id = $user_id;
                $discussion->text = strip_tags($discussionData['text']);
                $discussion->discussion_id = $discussionData['discussion_id'];
                $discussion->deleted = 'no';
                if($discussion->save())
                {
                    $date = substr($discussion->created, 10, 6).', '.substr($discussion->created, 8, 2).' ';
                    $dateEnd = ' '.substr($discussion->created, 0, 4);
                    $month = substr($discussion->created, 5, 2);
                    echo json_encode(array('id' => $discussion->id, 'date' => $date, 'dateEnd' => $dateEnd, 'month' => $month, 'username' => Yii::$app->user->identity->attributes['username']));
                    $model = new DiscussionTexts();
                    $otherDiscussions = $model->getLastFive($discussion->user_id, $discussion->discussion_id);
                    if(!empty($otherDiscussions)) {
                        foreach ($otherDiscussions as $commenter) {
                            $n = new Notifications();
                            $n->to_user_id = $commenter['user_id'];
                            $n->status = 'unreaded';
                            $n->url = Url::base(true).'/site/discussions?id='.$commenter['discussion_id'];
                            $n->from_user_id = null;
                            $n->notification = 'Նոր քննարկում ձեր քնարկումից հետո';
                            $n->save();
                        }
                    }
                }
                else {
                    var_dump($discussion->getErrors());
                }
            }
            else {
                echo json_encode(array('data' => false));
            }
        }

        die();
    }

    public function actionDeleteDiscussion($id)
    {
        $user_id = Yii::$app->user->id;

        $discussion = DiscussionTexts::find(['id' => $id])->one();

        if($discussion->user_id == $user_id)
        {
            Yii::$app->db->createCommand('UPDATE discussion_texts SET deleted = "yes" WHERE id = :id AND user_id = :user_id AND deleted = "no"',
                ['id' => $id, 'user_id' => $user_id])->execute();
            echo json_encode(array('data' => true));
        }
        else {
            echo json_encode(array('data' => false));   
        }

        die();
    }
 
    public function actionSignup()
    {
        $model = new SignUpForm();        
        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {                
                $this->alert('success', $this->getAlertMessage('005'));
                $model = new LoginForm();
                $model->username = $user['login'];
                $model->password = $user['password'];
                if($model->login()) {
                    if(isset(Yii::$app->session['saved'])) {
                
                    }
                    return $this->redirect($this->prefix_language.'/user/account');
                }
                return $this->redirect($this->prefix_language.'/site/index');
            }            
        }
        return $this->render('signup'.$this->language_view, ['signup' => $model]);
    }
 
    public function actionConfirmEmail($token)
    {
        try {
            $model = new ConfirmEmailForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
 
        if ($model->confirmEmail()) {
            $this->alert('success', 'ձեր էլ փոստը հաստատված է...');
        } else {
            $this->alert('error', 'Error.');
        }
 
        return $this->redirect($this->prefix_language.'/site/index');
    }
 
    public function actionRequestPasswordReset()
    {        
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $this->alert('success', $this->getAlertMessage('006'));
            } else {
                $this->alert('error', $model->email.$this->getAlertMessage('007'));
            }
        } else {
            var_dump($model->getErrors()); die;
        }       
        return $this->redirect($this->prefix_language.'index');        
    }
    

 
    public function actionResetPassword($token = null)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            $this->alert('error', $this->getAlertMessage('008'));
            return $this->redirect($this->prefix_language.'/site/index');
        }        
 
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $this->alert('success', $this->getAlertMessage('009'));
 
            return $this->redirect($this->prefix_language.'/site/index');
        }
 
        return $this->render('index', [
            'reset_password' => $model,
        ]);
    }
    

    public function actionProblem()
    {
        if(Yii::$app->request->post())
        {
            $email = Yii::$app->request->post('email');
            $subject = Yii::$app->request->post('subject');
            $message = Yii::$app->request->post('message');
            $captcha = Yii::$app->request->post('g-recaptcha-response', '');
            $url = Yii::$app->request->post('redirect', $this->prefix_language.'/site/index');
            
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcZzg8TAAAAAGcYr6I6RtoNxldFMHXf7gUXE55u&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
            $response = json_decode($response);
            if($response->success == false) {
                $this->alert('success', $this->getAlertMessage('010'));
                return $this->redirect($url);    
            }

            $body = '(subject:'.$subject.')(email:'.$email.')(message:'.strip_tags($message).')';

            Yii::$app->mailer->compose()
                ->setTo('davittomasso@gmail.com')
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setSubject('Problem')
                ->setTextBody($body)
                ->send();
            
            $this->alert('success', $this->getAlertMessage('003'));
            return $this->redirect($url);
        }

        return $this->redirect($this->prefix_language.'/site/index');
    }

    public function actionPost($id = 1)
    {
        $post = new Posts();
        $result = $post->getPost($id);
        $post = $result['post'];
        $comments = $result['comments'];
        if(!$post) {
            $this->alert('success', $this->getAlertMessage('011'));
            return $this->redirect($this->prefix_language.'/site/blog');
        }
        $conditions = '';
        $keywords = '';
        if($post['keywords']) {
            $keywords = explode(' ', $post['keywords']);
        }

        if(!empty($keywords)) {
            foreach ($keywords as $value) {
                if($conditions == '') {
                    $conditions .= " AND (posts.keywords LIKE '%".$value."%'";
                } else {
                    $conditions .= " OR posts.keywords LIKE '%".$value."%'";
                }
            }
            $conditions = $conditions.')';
        }
        $suggestions = new Posts();
        $suggestions = $suggestions->getSuggestions($post['id'], $conditions);
        
        $this->socialData = [
        'description' => $post['description'], 
        'keywords' => $post['keywords'], 
        'image' => '/images/uploads/'.$post['image']];
        return $this->render('post'.$this->language_view, ['post' => $post, 'comments' => $comments, 'socialData' => $this->socialData, 'suggestions' => $suggestions]);
    }
    
}