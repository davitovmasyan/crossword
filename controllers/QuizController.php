<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use app\models\Users;
use app\models\Words;
use app\models\Categories;
use yii\db\Query;
use app\models\Notifications;
use yii\helpers\Url;
use Yii;

 
class QuizController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],                    
                    [
                        'actions' => ['save'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['list', 'play', 'random'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],                    
                ],
                'denyCallback'  => function ($rule, $action) {
                     return $this->redirect($this->prefix_language.'/site/errorPage');
                },
            ]
        ];
    }
    
    public $socialData = [];

    public function actionRandom() {
        $category = new Categories();
        $category = $category->findRandom();
        
        return $this->redirect($this->prefix_language.'/quiz/play?id='.$category['id']);
    }

    public function actionList()
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $quizes = new Categories();
        $quizes = $quizes->find()->where(['active' => 'yes', 'lang' => $lang])->orderBy(['created' => SORT_DESC])->all();
        $this->socialData = [
        'description' => $this->getSocialData('005'), 
        'keywords' => 'crossword.am online-crossword, armenian crossword, victorina, armenian victorina, հայկական վիկտորինա, խաչբառ, օնլայն, հայկական, crossword.am գլխավոր էջ, հայկական խաչբառեր, օնլայն խաղեր', 
        'image' => '/images/quiz-list-main-image.jpg'];
        return $this->render('list'.$this->language_view, ['quizes' => $quizes, 'fbImage' => '/images/cross-list-main-image.jpg']);
    }    

    public function actionPlay($id)
    {
        $result = new Words();
        $result = $result->getSevenWords($id);
        $comments = $result['comments'];
        unset($result['comments']);
        $quizes = $result;
        $description = ' ';
        foreach($quizes as $quiz) {
            $description .= trim($quiz['question']).', ';
        }
        $this->socialData = ['description' => $description, 'keywords' => $description, 'image' => '/images/uploads/'.$quizes[0]['image']];
        return $this->render('play'.$this->language_view, ['quizes' => $quizes, 'comments' => $comments]);
    }    

    public function actionSave()
    {
        if(Yii::$app->request->post())
        {
            $data = Yii::$app->request->post('answers');
            $data = trim($data, '|');
            $data = explode('|', $data);
            $ids = [];
            $answers = [];
            foreach ($data as $row) {
                $quiz = explode('?_?', $row);
                if(isset($quiz[0]) && isset($quiz[1]))
                {
                    $ids[] = $quiz[0];
                    $answers[$quiz[0]] = $quiz[1];
                }
            }
            $query = new Query();
            $user_id = Yii::$app->user->id;
            if(!empty($ids)) {
                $quizes = $query->select('words.*, saved_words.word_id')
                            ->from('words')
                            ->leftJoin('saved_words', 'saved_words.word_id = words.id AND saved_words.user_id = '.$user_id)
                            ->where('words.id IN('.implode(',',$ids).')')
                            ->limit(7)
                            ->all();
                if(!empty($quizes)) {
                    $game_id = $quizes[0]['category_id'];
                    $points = 0;
                    $save_this_ids = [];
                    foreach($quizes as $quiz)
                    {
                        if(isset($answers[$quiz['id']]) && $answers[$quiz['id']] && $answers[$quiz['id']] == $quiz['answer'])
                        {
                            if($quiz['word_id']) {
                                $points += 10;
                            } else {
                                $points += $quiz['point'];
                                $save_this_ids[] = $quiz['id'];
                            }
                        }
                    }
                    if(!empty($save_this_ids))
                    {
                        $insert = [];
                        $now = date('Y-m-d H:i:s');
                        foreach ($save_this_ids as $id) {
                            $insert[] = [
                                $id, $user_id, 'success', $now, $now
                            ];
                        }
                        Yii::$app->db->createCommand()
                            ->batchInsert('saved_words', ['word_id', 'user_id', 'status', 'created', 'modified'], 
                            
                                $insert
                            )
                            ->execute();
                    }
                    if($points > 0)
                    {
                        $user = new Users();
                        $user = $user->find()->where(['id' => $user_id])->one();
                        $user->points = $user->points + $points;
                        $user->current_points = $user->current_points + $points;
                        $user->update();
                        $n = new Notifications();
                        $n->to_user_id = $user->id;
                        $n->status = 'unreaded';
                        $n->url = Url::base(true).'/quiz/play?id='.$game_id;
                        $n->from_user_id = null;
                        $n->notification = 'Դուք հավաքեցիք '.$points.' միավոր Վիկտորինա խաղից';
                        $n->save();
                    }
                }
            }

        }
        die();
    }
}