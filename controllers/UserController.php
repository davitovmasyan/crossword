<?php
namespace app\controllers;
 
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Users;
use app\models\EditProfileForm;
use app\models\Comments;
use app\models\Pluses;
use app\models\SavedCrosswords;
use app\models\Notifications;
use app\models\SavedWords;
use app\models\Words;
use yii\db\Query;
use yii\helpers\Url;
use Yii;

 
class UserController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [                    
                    [
                        'actions' => ['account',                                                                     
                                    'unique-email',                                    
                                    'unique-username',
                                    'market',
                                    'page',
                                    'games',
                                    'notifications',
                                    'crosswords',
                                    'words',
                                    'comment',
                                    'delete-comment',
                                    'page-view',
                                    'pluse'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['unique-email',                                    
                                    'unique-username',
                                    'page-view'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionAccount()
    {  
        $user_id = Yii::$app->user->id;
        
        $model = new EditProfileForm();
        
        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            if ($user = $model->edit()) {
                $this->alert('success', $this->getAlertMessage('012'));
                return $this->redirect($this->prefix_language.'/user/account');
            }   
        }      
        
        $user = Users::findOne(['id' => $user_id]);
        
        return $this->render('account'.$this->language_view, ['user' => $user]);
    }  

    public function actionPage()
    {
        $user_id = Yii::$app->user->id;

        $user = new Users();
        $user = $user->findOne(['id' => $user_id]);

        $data = [];
        $data['pluses'] = [];
        $data['medals'] = [];

        if($user['pluses'] > 0) {
            $data = new Pluses();
            $data = $data->getAllPluses($user_id);
        }

        return $this->render('page'.$this->language_view, ['user' => $user, 'data' => $data]); 
    }

    public function actionPageView($id = null)
    {
        $user_id = null;
        if(!Yii::$app->user->isGuest) {
            $user_id = Yii::$app->user->id;
        }

        if($id) {
            $user = new Users();
            $user = $user->findOne(['id' => $id]);
            $data = [];
            $data['pluses'] = [];
            $data['medals'] = [];
            $data['plused'] = false;

            if($user['pluses'] > 0) {
                $result = new Pluses();
                $result = $result->getAllPluses($user['id'], $user_id);
                $data['pluses'] = $result['pluses'];
                $data['plused'] = $result['plused'];
            }
            return $this->render('page-view'.$this->language_view, ['user' => $user, 'data' => $data]); 
        }
        else {
            return $this->redirect($this->prefix_language.'/site/leaderboard');
        }                
    }

    public function actionMarket()
    {                  
        return $this->render('market'.$this->language_view);
    }       
    
    public function actionUniqueEmail() 
    {
        $email = $_GET['email'];
        $condition = '';
        if(isset($_GET['id'])) {
            $condition = 'id <> '. $_GET['id'] . ' AND ';
        }
        else if(!Yii::$app->user->isGuest)
        {
            $condition = 'id <> '. Yii::$app->user->id . ' AND ';
        }
        
        $user = new Users();
        $user = $user->find('id')->asArray()->where($condition . 'email = :email', [':email' => $email])->one();        
        if($user) {
            echo json_encode(array('data' => false));
        }
        else {
            echo json_encode(array('data' => true));
        }
        
    }
    
    public function actionUniqueUsername() 
    {
        $username = $_GET['username'];
        $condition = '';
        if(isset($_GET['id'])) {
            $condition = 'id <> '. $_GET['id'] . ' AND ';
        }
        else if(!Yii::$app->user->isGuest)
        {
            $condition = 'id <> '. Yii::$app->user->id . ' AND ';
        }
        
        $user = new Users();
        $user = $user->find('id')->asArray()->where($condition . 'username = :username', [':username' => $username])->one();        
        if($user) {
            echo json_encode(array('data' => false));
        }
        else {
            echo json_encode(array('data' => true));
        }
        
    }  

    public function actionComment()
    {
        $user_id = Yii::$app->user->id;

        if (Yii::$app->request->post()) {
            $months = [
                '01' => 'հունվար',
                '02' => 'փետրվար',
                '03' => 'մարտ',
                '04' => 'ապրիլ',
                '05' => 'մայիս',
                '06' => 'հունիս',
                '07' => 'հուլիս',
                '08' => 'օգոստոս',
                '09' => 'սեպտեմբեր',
                '10' => 'հոկտեմբեր',
                '11' => 'նոյեմբեր',
                '12' => 'դեկտեմբեր',    
            ];

            $commentData = Yii::$app->request->post('Comments', []);
            if($commentData['game_id'] && $commentData['game_type'] && $commentData['comment'])
            {
                $comment = new Comments();
                $comment->user_id = $user_id;
                $comment->comment = strip_tags($commentData['comment']);
                $comment->game_type = $commentData['game_type'];
                $comment->game_id = $commentData['game_id'];
                $comment->deleted = 'no';
                if($comment->save())
                {
                    $date = substr($comment->created, 10, 6).', '.substr($comment->created, 8, 2).' ';
                    $dateEnd = ' '.substr($comment->created, 0, 4);
                    $month = substr($comment->created, 5, 2);
                    echo json_encode(array('id' => $comment->id, 'date' => $date, 'dateEnd' => $dateEnd, 'month' => $month, 'username' => Yii::$app->user->identity->attributes['username']));
                    $model = new Comments();
                    $otherCommenters = $model->getLastFive($comment->user_id, $comment->game_id, $comment->game_type);
                    if(!empty($otherCommenters)) {
                        foreach ($otherCommenters as $commenter) {
                            $n = new Notifications();
                            $n->to_user_id = $commenter['user_id'];
                            $n->status = 'unreaded';                            
                            switch($commenter['game_type'])
                            {
                                case 'crossword' : $action = '/cross-words/play?id='; break;
                                case 'word' : $action = '/quiz/play?id='; break;
                                case 'sudoku' : $action = '/sudoku/play?id='; break;
                                case 'post' : $action = '/site/post?id='; break;
                                case 'city' : $action = '/city/play?id='; break;
                            }
                            $n->url = Url::base(true).$action.$commenter['game_id'];
                            $n->from_user_id = null;
                            $n->notification = 'Նոր մեկնաբանություն ձեր մեկնաբանությունից հետո';
                            $n->save();
                        }
                    }
                }
                else {
                    var_dump($comment->getErrors());
                }
            }
            else {
                echo json_encode(array('data' => false));
            }
        }

        die();
    }

    public function actionCrosswords()
    {
        $user_id = Yii::$app->user->id;

        $crosswords = new SavedCrosswords();
        $crosswords = $crosswords->getMyCrosswords($user_id);

        return $this->render('crosswords'.$this->language_view, ['crosswords' => $crosswords]);
    }

    public function actionPluse()
    {
        $user_id = Yii::$app->user->id;

        if (Yii::$app->request->post()) {
            $months = [
                '01' => 'հունվար',
                '02' => 'փետրվար',
                '03' => 'մարտ',
                '04' => 'ապրիլ',
                '05' => 'մայիս',
                '06' => 'հունիս',
                '07' => 'հուլիս',
                '08' => 'օգոստոս',
                '09' => 'սեպտեմբեր',
                '10' => 'հոկտեմբեր',
                '11' => 'նոյեմբեր',
                '12' => 'դեկտեմբեր',    
            ];

            $pluseData = Yii::$app->request->post('Pluses', []);
            if($pluseData['from_user_id'] && $pluseData['to_user_id'] && $pluseData['from_user_id'] == $user_id)
            {
                $pluse = new Pluses();
                $pluse->from_user_id = $pluseData['from_user_id'];
                $pluse->to_user_id = $pluseData['to_user_id'];

                $to_user = new Users();
                $to_user = $to_user->findOne(['id' => $pluseData['to_user_id']]);
                $to_user->pluses++;

                if($pluse->save() && $to_user->update())
                {
                    $n = new Notifications();
                    $n->to_user_id = $to_user->id;
                    $n->status = 'unreaded';
                    $n->url = Url::base(true).'/user/page-view?id='.$user_id;
                    $n->from_user_id = $user_id;
                    $n->notification = 'Ձեզ փոխանցեց մեկ գումարած '.Yii::$app->user->identity->attributes['username'].' օգտատերը';
                    $n->save();
                    $date = substr($pluse->created, 8, 2).' ';
                    $dateEnd = ' '.substr($pluse->created, 0, 4);
                    $month = substr($pluse->created, 5, 2);
                    echo json_encode(array('date' => $date, 'dateEnd' => $dateEnd, 'month' => $month, 'username' => Yii::$app->user->identity->attributes['username']));
                }
                else {
                    var_dump($pluse->getErrors());
                }
            }
            else {
                echo json_encode(array('data' => false));
            }
        }

        die();
    }

    public function actionDeleteComment($id)
    {
        $user_id = Yii::$app->user->id;
        

        $comment = new Comments();
        $comment = $comment->find(['id' => $id])->one();        

        if($comment->user_id == $user_id)
        {
            Yii::$app->db->createCommand('UPDATE comments SET deleted = "yes" WHERE id = :id AND user_id = :user_id AND deleted = "no"',
                ['id' => $id, 'user_id' => $user_id])->execute();
            echo json_encode(array('data' => true));
        }
        else {
            echo json_encode(array('data' => false));   
        }

        die();
    }

    public function actionWords()
    {
        $user_id = Yii::$app->user->id;
        $savedWords = new SavedWords();
        $myWords = $savedWords->getMyWords($user_id);
        $words = new Words();
        $allWords = $words->getAllWordsGroupByCategory();
        return $this->render('words'.$this->language_view, ['allWords' => $allWords, 'myWords' => $myWords]);
    }

    public function actionGames()
    {
        return $this->render('games'.$this->language_view);
    }


    public function actionNotifications()
    {
        $user_id = Yii::$app->user->id;
        $notifications = new Notifications();
        $notifications = $notifications->getMyNotifications($user_id);
        Yii::$app->db->createCommand('UPDATE notifications SET status = "readed" WHERE to_user_id = '.$user_id)->execute();
        return $this->render('notifications'.$this->language_view, ['notifications' => $notifications]);
    }
     
}