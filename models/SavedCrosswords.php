<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
/**
 * This is the model class for table "saved_crosswords".
 *
 * @property string $id
 * @property string $crossword_id
 * @property string $user_id
 * @property string $structure
 * @property string $status
 * @property string $created
 * @property string $modified
 *
 * @property Crosswords $crossword
 * @property Users $user
 */
class SavedCrosswords extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'saved_crosswords';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crossword_id', 'user_id', 'structure', 'status'], 'required'],
            [['crossword_id', 'user_id'], 'integer'],
            [['status'], 'string'],
            [['created', 'modified'], 'safe'],
            [['structure'], 'string', 'max' => 800]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'crossword_id' => 'Crossword ID',
            'user_id' => 'User ID',
            'structure' => 'Structure',
            'status' => 'Status',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrossword()
    {
        return $this->hasOne(Crosswords::className(), ['id' => 'crossword_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyCrosswords($user_id)
    {
        $query = new Query();

        $result = [];

        $result = $query->select('saved_crosswords.*, crosswords.id AS cid,'
            .' crosswords.active AS cactive, crosswords.public_number, '
            .'crosswords.image, crosswords.crossword_name')
                        ->from('saved_crosswords')
                        ->leftJoin('crosswords', 'crosswords.id = saved_crosswords.crossword_id')
                        ->where('saved_crosswords.user_id = :user_id', [':user_id' => $user_id])
                        ->orderBy('saved_crosswords.status ASC')
                        ->having('cactive = "yes"')
                        ->all();

        return $result;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAll($limit, $offset)
    {
        $query = new Query();

        $result = [];

        $result['saved'] = $query->select('saved_crosswords.*, crosswords.id AS cid,'
            .' crosswords.active AS cactive, crosswords.public_number, '
            .'crosswords.image, crosswords.crossword_name, users.username, users.id as uid')
                        ->from('saved_crosswords')
                        ->leftJoin('crosswords', 'crosswords.id = saved_crosswords.crossword_id')
                        ->leftJoin('users', 'users.id = saved_crosswords.user_id')
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy('saved_crosswords.modified DESC')
                        ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(saved_crosswords.id) FROM saved_crosswords')
             ->queryScalar();

        return $result;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolvers($id)
    {
        $query = new Query();

        $result = [];

        $result['saved'] = $query->select('saved_crosswords.*, users.username, users.id as uid')
                        ->from('saved_crosswords')
                        ->leftJoin('users', 'users.id = saved_crosswords.user_id')
                        ->where('saved_crosswords.status = "all" AND saved_crosswords.crossword_id = :id', [':id' => $id])
                        ->orderBy('saved_crosswords.modified DESC')
                        ->all();

        if(empty($result['saved'])) {
            return false;
        }

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(saved_crosswords.id) FROM saved_crosswords WHERE status = "all" AND crossword_id = '.$id)
             ->queryScalar();

        return $result;
    }
}
