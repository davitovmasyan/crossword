<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Words;
 
/**
 * Signup form
 */
class EditCommentForm extends Model
{
    public $id;       
    public $user_id;        
    public $game_id;        
    public $game_type;     
    public $comment;     
    public $deleted;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id', 'user_id', 'game_id', 'deleted', 'game_type', 'comment'], 'required'],
            
            [['id', 'user_id', 'game_id'], 'integer'],

            [['deleted', 'game_type', 'comment'], 'string'],

                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => 'ID',
            'game_id' => 'Game ID',
            'game_type' => 'Game Type',
            'user_id' => 'User ID',
            'deleted' => 'Deleted',
            'comment' => 'Comment',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {        
        if ($this->validate()) {
            $comment = new Comments();
            $comment = $comment->find()->where('id = :id', [':id' => $this->id])->one();
            $comment->game_id = $this->game_id;
            $comment->game_type = $this->game_type;
            $comment->user_id = $this->user_id;        
            $comment->deleted = $this->deleted;
            $comment->comment = $this->comment;

            if ($comment->update()) {
                return $comment;
            }
            else {
                var_dump($comment->getErrors()); die;
            }
        }    
        return false;
    }

}