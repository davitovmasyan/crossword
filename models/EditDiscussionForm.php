<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Discussion;

class EditDiscussionForm extends Model {
    public $id;
	public $name;
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],                  
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {
    	$discussion = new Discussion();
        $discussion = $discussion->findOne(['id' => $this->id]);
    	$discussion->name = $this->name;
    	if($discussion->update())        
    	{
    		return $discussion;
    	}
    	return false;
    }
}