<?php
 
namespace app\models;

use yii\base\Model;
use app\models\Admins;
use Yii;
 
/**
 * LoginForm is the model behind the login form.
 */
class AdminLoginForm extends Model
{
    public $username;
    public $password;
 
    private $_admin = false;
 
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],            
            ['password', 'string', 'min' => 6],
            ['password', 'validatePassword'],
        ];
    }
 
    /**
     * Validates the username and password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {                
                $this->addError('error', 'Invalid username of password.');
                Yii::$app->getSession()->setFlash('error', 'Invalid username of password.');
            }
        }
    }
 
    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {            
            return Yii::$app->user->login($this->getUser(), 3600*3600);
        } else {            
            return false;
        }
    }
 
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_admin === false) {
            $this->_admin = Admins::findByUsername($this->username);
        }
 
        return $this->_admin;
    }
}