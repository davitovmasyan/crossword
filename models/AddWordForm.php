<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Words;
 
/**
 * Signup form
 */
class AddWordForm extends Model
{
    public $answer;        
    public $answers;        
    public $question;     
    public $point;    
    public $category;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['question', 'required'],
            ['question', 'filter', 'filter' => 'trim'],            
            ['question', 'string', 'min' => 2, 'max' => 255],
            
            ['answers', 'required'],
            ['answers', 'filter', 'filter' => 'trim'],
            ['answers', 'string', 'min' => 2, 'max' => 500],
            
            ['answer', 'required'],
            ['answer', 'filter', 'filter' => 'trim'],            
            ['answer', 'string', 'min' => 1, 'max' => 255],
            
            ['category', 'required'],
            ['category', 'integer'],

            ['point', 'required'],
            ['point', 'integer'],
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'answer' => 'Answer',
            'question' => 'Question',            
            'answers' => 'Answers',
            'category' => 'Category',
            'point' => 'Point',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function add()
    {        
        if ($this->validate()) {
            $word = new Words();
            $word ->answers = $this->answers;
            $word ->answer = $this->answer;
            $word ->question = $this->question;
            $word ->active = 'no';
            $word->category_id = $this->category;
            $word->point = $this->point ? $this->point : 50;

            if ($word->save()) {
                return $word;
            }
            else {
                var_dump($word->getErrors()); die;
            }
        }    
        return false;
    }

}