<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;

/**
 * This is the model class for table "crosswords".
 *
 * @property string $id
 * @property string $public_number
 * @property string $lang
 * @property string $crossword_name
 * @property string $description
 * @property string $type
 * @property string $structure
 * @property string $questions
 * @property string $category
 * @property integer $active
 * @property string $image
 * @property string $created
 * @property string $modified
 */
class Crosswords extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crosswords';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['public_number'], 'integer'],
            [['lang', 'type'], 'required'],
            [['lang', 'type', 'structure', 'questions', 'category'], 'string'],
            [['created', 'modified'], 'safe'],
            [['crossword_name', 'description', 'image', 'active'], 'string', 'max' => 255],
            [['public_number'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Crossword ID',
            'public_number' => 'Public Number',
            'lang' => 'Lang',
            'crossword_name' => 'Crossword Name',
            'description' => 'Description',
            'type' => 'Type',
            'structure' => 'Structure',
            'questions' => 'Questions',
            'category' => 'Category',
            'active' => 'Active',
            'image' => 'Image',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    
    public function getList($conditions, $limit, $offset)
    {
        $crosswords = array();
        $count = 0;
        $query = new Query();
        
        if(!Yii::$app->user->isGuest) {
            $crosswords = $query->select('crosswords.*, saved_crosswords.id AS saved_id, saved_crosswords.status AS sstatus')
                        ->from('crosswords')
                        ->leftJoin('saved_crosswords', 'saved_crosswords.user_id = :user_id AND saved_crosswords.crossword_id = crosswords.id', [':user_id' => Yii::$app->user->id])
                        ->where('crosswords.active = "yes"'.$conditions)
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('crosswords.created DESC')
                        ->all();
        }
        else {
            $crosswords = $query->select('crosswords.*')
                        ->from('crosswords')                                                
                        ->where('crosswords.active = "yes"'.$conditions)
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('crosswords.created DESC')
                        ->all();
        }
        
        $count = Yii::$app->db->createCommand('SELECT COUNT(id) FROM crosswords'
                . ' WHERE active = "yes"'.$conditions)
             ->queryScalar();
         
        
        return ['crosswords' => $crosswords, 'count' => $count];
    }


    public function getCrossword($id, $user_id = null)
    {
        $crossword = array();
        $query = new Query();

        if(!$user_id)
        {
            $crossword = $query->select('crosswords.*')
                               ->from('crosswords')
                               ->where('crosswords.id = :id AND crosswords.active="yes"', [':id' => $id])
                               ->one();

        } else {
            $crossword = $query->select('crosswords.*, saved_crosswords.structure AS saved_structure, saved_crosswords.id AS saved_id, saved_crosswords.status AS sstatus')
                               ->from('crosswords')
                               ->leftJoin('saved_crosswords', 'saved_crosswords.crossword_id = crosswords.id AND saved_crosswords.user_id = :user_id', [':user_id' => $user_id])
                               ->where('crosswords.id = :id AND crosswords.active="yes"', [':id' => $id])
                               ->one();
        }

        if(!empty($crossword)) {
            $query = new Query();
            $crossword['comments'] = $query->select('comments.*, users.username, users.id AS uid')
                                    ->from('comments')
                                    ->leftJoin('users', 'users.id = comments.user_id AND users.active = 1')
                                    ->where('comments.game_id = :id AND comments.game_type = "crossword" AND comments.deleted = "no"', [':id' => $id])
                                    ->orderBy('comments.created DESC')
                                    ->having('users.username IS NOT NULL')
                                    ->all();
        }

        return $crossword;
    }


    public function getAllCrosswords($conditions, $limit, $offset)
    {
        $crosswords = array();
        $count = 0;
        $query = new Query();

        $crosswords = $query->select('crosswords.*')
                        ->from('crosswords')                        
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('crosswords.created DESC')
                        ->all();

        $count = Yii::$app->db->createCommand('SELECT COUNT(crosswords.id) FROM crosswords')
             ->queryScalar();
        
        return ['crosswords' => $crosswords, 'count' => $count];
    }

    public function findRandom($user_id)
    {
        $result = [];
        $query = new Query();

        $result = $query->select('crosswords.id, saved_crosswords.status')
                        ->from('crosswords')
                        ->leftJoin('saved_crosswords', 'saved_crosswords.crossword_id = crosswords.id AND saved_crosswords.user_id = '.$user_id)
                        ->where('crosswords.active = "yes"')
                        ->orderBy('RAND()')
                        ->having('saved_crosswords.status <> "all"')
                        ->one();

        return $result;
    }

    public function search($conditions) 
    {
        $result = array();
        $query = new Query();

        $result = $query->select('crosswords.id, crosswords.crossword_name, crosswords.image')
                        ->from('crosswords')
                        ->where('crosswords.active = "yes" AND '.$conditions)
                        ->limit(25)
                        ->orderBy('RAND()')
                        ->all();

        return $result;
    }
}
