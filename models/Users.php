<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\components\DateTimeStampBehavior;
use yii\db\Query;
/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $username
 * @property string $image
 * @property string $address
 * @property string $points
 * @property string $current_points
 * @property string $pluses
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $street
 * @property string $zip_code
 * @property string $email_confirm_token
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $lat_lng
 * @property string $facebook_id
 * @property string $sex
 * @property string $first_name
 * @property string $last_name
 * @property string $age
 * @property integer $active
 * @property string $created
 * @property string $modified
 *
 * @property UserImages[] $userImages
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'username'], 'required'],
            [['facebook_id', 'age', 'active', 'points', 'current_points', 'pluses'], 'integer'],
            [['sex'], 'string'],
            [['created', 'modified'], 'safe'],
            [['email', 'password', 'username', 'image', 'address', 'email_confirm_token', 'password_reset_token'], 'string', 'max' => 255],
            [['country', 'state', 'city', 'street', 'zip_code', 'auth_key', 'lat_lng', 'first_name', 'last_name'], 'string', 'max' => 50],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['facebook_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'username' => 'Username',
            'image' => 'Image',
            'address' => 'Address',
            'country' => 'Country',
            'points' => 'Points',
            'current_points' => 'Current Points',
            'pluses' => 'Pluses',
            'state' => 'State',
            'city' => 'City',
            'street' => 'Street',
            'zip_code' => 'Zip Code',
            'email_confirm_token' => 'Email Confirm Token',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'lat_lng' => 'Lat Lng',
            'facebook_id' => 'Facebook ID',
            'sex' => 'Sex',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'age' => 'Age',
            'active' => 'Active',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserImages()
    {
        return $this->hasMany(UserImages::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllUsers($conditions, $limit, $offset, $order)
    {
        $result = [];
        $query = new Query();

        $result['users'] = $query->select('users.*')
                        ->from('users')
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy($order.' DESC')
                        ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(users.id) FROM users')
             ->queryScalar();

        return $result;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    
    /**
    * @inheritdoc
    */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'active' => true]);
    }
 
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }
 
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
 
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
 
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }
 
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {        
        return Yii::$app->security->validatePassword($password, $this->password);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
 
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
 
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {                
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'active' => true,
        ]);
    }
 
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['users.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }
 
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
 
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token, 'active' => false]);
    }
 
    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }
 
    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }


    public function getLeaders()
    {
        $query = new Query();
        $result = [];

        $result = $query->select('users.*')
                        ->from('users')
                        ->where('users.active = 1 AND users.points > 0')
                        ->orderBy('users.points DESC')
                        ->all();

        return $result;
    }
}
