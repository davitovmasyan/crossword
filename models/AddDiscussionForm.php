<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Discussion;

class AddDiscussionForm extends Model {
	public $name;
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],                  
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'name' => 'Name',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function add()
    {
    	$discussion = new Discussion();
    	$discussion->name = $this->name;
    	if($discussion->save())        
    	{
    		return $discussion;
    	}
    	return false;
    }
}