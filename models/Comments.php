<?php

namespace app\models;

use app\components\DateTimeStampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property string $id
 * @property string $user_id
 * @property string $game_id
 * @property string $game_type
 * @property string $comment
 * @property string $deleted
 * @property string $created
 * @property string $modified
 *
 * @property Users $user
 */
class Comments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'game_id', 'game_type', 'comment', 'deleted'], 'required'],
            [['user_id', 'game_id'], 'integer'],
            [['game_type', 'comment', 'deleted'], 'string'],
            [['comment'], 'string', 'max' => 200],
            [['created', 'modified'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'game_id' => 'Game ID',
            'game_type' => 'Game Type',
            'comment' => 'Comment Text',
            'deleted' => 'Deleted',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function getAllComments($conditions, $limit, $offset)
    {
        $comments = array();
        $count = 0;
        $query = new Query();

        $comments = $query->select('comments.*, users.username, users.active')
                        ->from('comments')
                        ->leftJoin('users', 'users.id = comments.user_id')
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('comments.created DESC')
                        ->all();

        $count = Yii::$app->db->createCommand('SELECT COUNT(comments.id) FROM comments')
             ->queryScalar();
        
        return ['comments' => $comments, 'count' => $count];
    }
    public function getLastFive($user_id, $game_id, $game_type)
    {
        $commenters = array();
        $query = new Query();

        $commenters = $query->select('comments.*')
                        ->from('comments')
                        ->where('comments.user_id <> :user_id AND comments.game_id = :game_id AND comments.game_type = :game_type AND comments.deleted = "no"',
                            [
                            ':user_id' => $user_id,
                            ':game_id' => $game_id,
                            ':game_type' => $game_type,
                            ])
                        ->limit(5)
                        ->orderBy('comments.created DESC')
                        ->all();

        
        return $commenters;
    }
}
 