<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Crosswords;
use yii\web\UploadedFile;
 
/**
 * Signup form
 */
class AddCrosswordForm extends Model
{
    public $public_number;
    public $lang;
    public $crossword_name;
    public $description;
    public $type;    
    public $structure;
    public $questions;
    public $category;
    public $image;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questions', 'structure', 'public_number', 'description', 'type', 'category', 'lang'], 'required'],
            [['questions', 'structure'], 'string'],            
            
            ['public_number', 'integer'],
//            [['image'], 'image', 'types' => 'png,jpg,jpeg'],
            [['crossword_name', 'lang', 'description'], 'string', 'max' => 255],                  
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'public_number' => 'Public Number',
            'lang' => 'Lang',
            'crossword_name' => 'Crossword Name',
            'description' => 'Description',
            'type' => 'Type',
            'structure' => 'Structure',
            'questions' => 'Questions',
            'category' => 'Category',
            'image' => 'Image',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function add()
    {        
        if ($this->validate()) {
            $path = $path = __DIR__;
            $path = str_replace('models', '', $path);
            $path .= 'web/images/crosswords/';

            $crossword = new Crosswords();
            $crossword ->type = $this->type ? $this->type : 'medium';
            $crossword ->category = $this->category ? $this->category : 'other';
            $crossword->lang = $this->lang ? $this->lang : 'arm';
            $crossword ->questions = $this->questions;
            $crossword ->structure = $this->structure;
            $crossword ->description = $this->description;
            $crossword ->crossword_name = $this->crossword_name ? $this->crossword_name : null;
            $crossword ->public_number = $this->public_number;
            
            $file = UploadedFile::getInstance($this, 'image');            
            $image = md5(date("Y-m-d H:i:s"));
            if(strpos($file->name, '.jpg')) {
                $image .= '.jpg';
            } else if(strpos($file->name, '.jpeg')) {
                $image .= '.jpeg';
            } else if(strpos($file->name, '.png')) {
                $image .= '.png';
            }            
            $uploaded = $file->saveAs( $path . $image );
            $crossword ->image = $image;
            $crossword ->active = 'no';

            if ($crossword->save()) {
                return $crossword;
            }
            else {
                var_dump($crossword->getErrors()); die;
            }
        }    
        return false;
    }

}