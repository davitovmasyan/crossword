<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Users;
 
/**
 * Signup form
 */
class EditUserForm extends Model
{
    public $id;       
    public $username;
    public $email;
    public $age;
    public $sex;
    public $points;
    public $current_points;
    public $address;
    public $state;
    public $city;
    public $country;
    public $zip_code;
    public $street;
    public $lat_lng;
    public $first_name;        
    public $last_name;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id', 'email', 'username', 'sex', 'age'], 'required'],
                        
            [['points', 'current_points', 'age'], 'integer'],
            [['email', 'username', 'first_name', 'last_name', 'sex', 'city', 'address', 'country', 'street', 'zip_code', 'state', 'lat_lng'], 'string'],
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => 'Id',
            'username' => 'Username',
            'age' => 'Age', 
            'email' => 'Email',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'address' => 'Address',
            'points' => 'Points',
            'current_points' => 'Current Points',
            'sex' => 'Sex',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'zip_code' => 'Zip Code',
            'street' => 'Street',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {    
        $user = new Users();
        $user = $user->find()->where('id = :id', ['id' => $this->id])->one();
        $user->username = $this->username;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->email = $this->email;
        $user->address = $this->address ? $this->address : null;
        $user->city = $this->city ? $this->city : null;
        $user->country = $this->country ? $this->country : null;
        $user->state = $this->state ? $this->state : null;
        $user->zip_code = $this->zip_code ? $this->zip_code : null;
        $user->street = $this->street ? $this->street : null;
        $user->lat_lng = $this->lat_lng ? $this->lat_lng : null;
        $user->sex = $this->sex;
        $user->age = $this->age; 
        $user->points = $this->points;
        $user->current_points = $this->current_points;

        if ($user->update()) {
            return $user;
        }
        else {
            var_dump($user->getErrors()); die;
        }
        return false;
    }

}