<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Categories;
use yii\web\UploadedFile;
 
/**
 * Signup form
 */
class AddCategoryForm extends Model
{    
    public $category_name;
    public $keywords;
    public $lang;
    public $image;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name', 'lang'], 'required'],
            ['keywords', 'string', 'max' => 255]
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_name' => 'Category',
            'image' => 'Image',
            'keywords' => 'Keywords',
            'lang' => 'Language',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function add()
    {
            $path = $path = __DIR__;
            $path = str_replace('models', '', $path);
            $path .= 'web/images/uploads/';

            $category = new Categories();
            $category ->category_name = $this->category_name;            
            
            $file = UploadedFile::getInstance($this, 'image');            
            $image = md5(date("Y-m-d H:i:s"));
            if(strpos($file->name, '.jpg')) {
                $image .= '.jpg';
            } else if(strpos($file->name, '.jpeg')) {
                $image .= '.jpeg';
            } else if(strpos($file->name, '.png')) {
                $image .= '.png';
            }            
            $uploaded = $file->saveAs( $path . $image );
            $category->image = $image;
            $category->lang = $this->lang;
            $category->keywords = 'victorina quiz վիկտորինա քուիզ տեստ';
            $category->active = 'no';

            if ($category->save()) {
                return $category;
            }
            else {
                var_dump($category->getErrors()); die;
            }
        return false;    
    }
}