<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "pluses".
 *
 * @property string $id
 * @property string $from_user_id
 * @property string $to_user_id
 * @property string $created
 * @property string $modified
 *
 * @property Users $fromUser
 * @property Users $toUser
 */
class Pluses extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pluses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id'], 'required'],
            [['from_user_id', 'to_user_id'], 'integer'],
            [['created', 'modified'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user_id' => 'From User ID',
            'to_user_id' => 'To User ID',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'to_user_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /*
    *function get user all pluses
    */
    public function getAllPluses($user_id, $logged_user_id = null)
    {
        $pluses = [];
        $plused = false;
        $query = new Query();

        $pluses = $query->select('pluses.id, pluses.created, users.username')
                        ->from('pluses')
                        ->leftJoin('users', 'users.id = pluses.from_user_id')
                        ->where('pluses.to_user_id = :user_id', ['user_id' => $user_id])
                        ->orderBy('pluses.created DESC')
                        ->all();

        if(!empty($pluses) && $logged_user_id != null && $user_id != $logged_user_id) {
            $query = new Query();
            $plused = $query->select('pluses.id')
                          ->from('pluses')
                          ->where('pluses.from_user_id = :from_user AND pluses.to_user_id = :to_user', ['from_user' => $logged_user_id, 'to_user' => $user_id])
                          ->one();
        }

        return ['pluses' => $pluses, 'plused' => $plused];
    }
    /**
     * @for admin
     * @return \yii\db\ActiveQuery
     */
    public function getAll($limit, $offset)
    {
        $query = new Query();

        $result = [];

        $result['pluses'] = $query->select('pluses.*, from.username as fromusername, to.username as tousername')
                        ->from('pluses')
                        ->leftJoin('users as from', 'from.id = pluses.from_user_id')
                        ->leftJoin('users as to', 'to.id = pluses.to_user_id')
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy('pluses.created DESC')
                        ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(pluses.id) FROM pluses')
             ->queryScalar();

        return $result;
    }
}
