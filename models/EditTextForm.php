<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
 
/**
 * Signup form
 */
class EditTextForm extends Model
{
    public $id;       
    public $text;  
    public $deleted;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id', 'deleted', 'text'], 'required'],                

            [['deleted', 'text'], 'string'],

                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => 'ID',
            'text' => 'Text',
            'deleted' => 'Deleted',            
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {        
        if ($this->validate()) {
            $discussion = new DiscussionTexts();
            $discussion = $discussion->find()->where('id = :id', [':id' => $this->id])->one();
            $discussion->text = $this->text;
            $discussion->deleted = $this->deleted;

            if ($discussion->update()) {
                return $discussion;
            }
            else {
                var_dump($discussion->getErrors()); die;
            }
        }    
        return false;
    }

}