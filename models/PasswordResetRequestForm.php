<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
 
/**
 * PasswordReset form
 */

class PasswordResetRequestForm extends Model {
	 public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],            
        ];
    }
    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = new Users();
        $user = $user->findOne([            
            'email' => $this->email,
        ]);
        if ($user) {
            if (!Users::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'Crossword.am - Ծանուցումներ'])
                    ->setTo($this->email)
                    ->setSubject('Վերականգնել գաղտնաբառը.')
                    ->send();
            }
        }
        return false;
    }
}