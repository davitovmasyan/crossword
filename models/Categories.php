<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
use yii\db\Query;
/**
 * This is the model class for table "categories".
 *
 * @property string $id
 * @property string $category_name
 * @property string $active
 * @property string $image
 * @property string $keywords
 * @property string $lang
 * @property string $created
 * @property string $modified
 *
 * @property Words[] $words
 */
class Categories extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name', 'active', 'image', 'lang'], 'required'],
            [['active', 'lang'], 'string'],
            [['created', 'modified'], 'safe'],
            [['category_name', 'image', 'keywords'], 'string', 'max' => 255],
            [['category_name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'active' => 'Active',
            'image' => 'Image',
            'keywords' => 'Keywords',
            'lang' => 'Language',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWords()
    {
        return $this->hasMany(Words::className(), ['category_id' => 'id']);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllCategories($conditions, $limit, $offset)
    {
        $categories = array();
        $count = 0;
        $query = new Query();

        $categories = $query->select('categories.*')
                        ->from('categories')                        
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('categories.active DESC')
                        ->all();

        $count = Yii::$app->db->createCommand('SELECT COUNT(categories.id) FROM categories')
             ->queryScalar();
        
        return ['categories' => $categories, 'count' => $count];
    }
    /**
    * @return \yii\db\ActiveQuery
    */
    public function findRandom()
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $result = [];
        $query = new Query();

        $result = $query->select('categories.id')
                        ->from('categories')
                        ->where('categories.active = "yes" AND categories.lang = "'.$lang.'"')
                        ->orderBy('RAND()')
                        ->one();

        return $result;
    }

     public function search($conditions) 
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $result = array();
        $query = new Query();

        $result = $query->select('categories.id, categories.category_name, categories.image')
                        ->from('categories')
                        ->where('categories.active = "yes" AND categories.lang = "'.$lang.'" AND '.$conditions)
                        ->limit(25)
                        ->orderBy('RAND()')
                        ->all();

        return $result;
    }
}
