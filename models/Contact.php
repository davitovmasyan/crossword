<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;

/**
 * This is the model class for table "contact".
 *
 * @property string $id
 * @property string $email
 * @property string $first_last_name
 * @property string $message
 * @property integer $public
 * @property string $created
 * @property string $modified
 */
class Contact extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'first_last_name', 'message'], 'required'],
            [['public'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['email', 'first_last_name'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_last_name' => 'First Last Name',
            'message' => 'Message',
            'public' => 'Public',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    
    public function getPublicContacts($limit, $offset)
    {
        $contacts = array();
        $count = 0;
        $query = new Query();
                        
        $contacts = $query->select('contact.*')
                        ->from('contact')                                                
                        ->where('contact.public = 1')
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('contact.created DESC')
                        ->all();        
        
        $count = Yii::$app->db->createCommand('SELECT COUNT(contact.id) FROM contact'
                . ' WHERE contact.public = 1')
             ->queryScalar();
        
        return ['contacts' => $contacts, 'count' => $count];
    }
    
    public function getAllContacts($limit, $offset)
    {
        $contacts = array();
        $count = 0;
        $query = new Query();
                        
        $contacts = $query->select('contact.*')
                        ->from('contact')                        
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('contact.created DESC')
                        ->all();        
        
        $count = Yii::$app->db->createCommand('SELECT COUNT(contact.id) FROM contact')
             ->queryScalar();
        
        return ['contacts' => $contacts, 'count' => $count];
    }
}
