<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "notifications".
 *
 * @property string $id
 * @property string $notification
 * @property string $from_user_id
 * @property string $to_user_id
 * @property string $status
 * @property string $url
 * @property string $created
 * @property string $modified
 *
 * @property Users $fromUser
 * @property Users $toUser
 */
class Notifications extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification', 'to_user_id', 'status'], 'required'],
            [['from_user_id', 'to_user_id'], 'integer'],
            [['status'], 'string'],
            [['created', 'modified'], 'safe'],
            [['notification', 'url'], 'string', 'max' => 255],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['to_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification' => 'Notification',
            'from_user_id' => 'From User ID',
            'to_user_id' => 'To User ID',
            'status' => 'Status',
            'url' => 'Url',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'to_user_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyNotifications($user_id)
    {
        $result = [];
        $query = new Query();

        $result = $query->select('notifications.*, users.username')
                        ->from('notifications')
                        ->leftJoin('users', 'users.id = notifications.from_user_id')
                        ->where('notifications.to_user_id = :user_id', [':user_id' => $user_id])
                        ->orderBy('notifications.created DESC')
                        ->all();

        return $result;
    }
    /**
     * @for admin
     * @return \yii\db\ActiveQuery
     */
    public function getAll($limit, $offset)
    {
        $query = new Query();

        $result = [];

        $result['notifications'] = $query->select('notifications.*, users.username')
                        ->from('notifications')
                        ->leftJoin('users', 'users.id = notifications.to_user_id')
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy('notifications.created DESC')
                        ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(notifications.id) FROM notifications')
             ->queryScalar();

        return $result;
    }
}
