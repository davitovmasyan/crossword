<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Contact;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;    
    public $message;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'message'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Անուն / Ազգանուն',
            'message' => 'Հաղորդագրություն',
            'email' => 'Էլ հասցե',
            'verifyCode' => 'Անվտանգության կոդ',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact()
    {
        if ($this->validate()) {
            $contact = new Contact();
            $contact->first_last_name = $this->name;
            $contact->email = $this->email;
            $contact->message = $this->message;
            $contact->public = 0;
            if($contact->save()) {
                Yii::$app->mailer->compose()
                  ->setTo('davittomasso@gmail.com')
                ->setFrom([$this->email => $this->name])
                ->setSubject('Contact Us')
                ->setTextBody($this->message)
                ->send();
                return true;
            }
            else {
               var_dump($contact->getErrors()); die; 
            }                        
        } else {
            return false;
        }
    }
}
