<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Admins;
 
/**
 * Signup form
 */
class AddAdminForm extends Model
{
    public $username;        
    public $password;     
    public $first_name;
    public $last_name;    
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-d]+$#i'],
            ['username', 'unique', 'targetClass' => Admins::className(), 'message' => 'This username is already exists.'],
            ['username', 'string', 'min' => 2, 'max' => 50],
             
 
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
 
            
            ['first_name', 'string', 'max' => 255],                         
            ['last_name', 'string', 'max' => 255], 
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'password' => 'Password',            
            'username' => 'Username',            
            'first_name' => 'First name',
            'last_name' => 'Last name',            
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function add()
    {        
        if ($this->validate()) {
            $admin = new Admins();                        
            $admin ->username = $this->username;                        
            $admin ->first_name = $this->first_name ? $this->first_name : null;
            $admin ->last_name = $this->last_name ? $this->last_name : null;            
            $admin->password = $admin ->setPassword($this->password);            
            $admin ->generateAuthKey();                    

            if ($admin->save()) {
                return $admin;
            }
        }    
        return false;
    }

}