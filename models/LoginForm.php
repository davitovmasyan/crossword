<?php
 
namespace app\models;

use yii\base\Model;
use app\models\Users;
use Yii;
 
/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;   
    public $rememberMe = true;
 
    private $_user = false;
 
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'string', 'min' => 6],
            ['password', 'validatePassword'],
        ];
    }
 
    /**
     * Validates the username and password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $messages = [
            'a' => [
                'hy' => 'սխալ մուտքի տվյալներ...',
                'en' => 'invalid username or password...',
                'ru' => 'неправильние данные...',
            ],
            'b' => [
                'hy' => 'ձեր էջը ակտիվ չէ...',
                'en' => 'your account is inacvitve...',
                'ru' => 'ваша страница не активна...',
            ],
        ];
        if (!$this->hasErrors()) {
            $user = $this->getUser();             
            if (!$user || !$user->validatePassword($this->password)) {                
                $this->addError('error', $messages['a'][$lang]);
                Yii::$app->getSession()->setFlash('error', $messages['a'][$lang]);
            } elseif ($user && $user->active == false) {
                $this->addError('error', $messages['b'][$lang]);
                Yii::$app->getSession()->setFlash('error', $messages['b'][$lang]);
            }
        }
    }
 
    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {            
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            return false;
        }
    }
 
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            if(strpos($this->username, '@')) {
                $this->_user = Users::findByEmail($this->username);
            }
            else {
                $this->_user = Users::findByUsername($this->username);
            }            
        }
 
        return $this->_user;
    }
}