<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Admins;
use yii\db\Query;
 
/**
 * Signup form
 */
class EditAdminForm extends Model
{
    public $id;
    public $username;
    public $password;     
    public $first_name;
    public $last_name;    
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            ['id', 'required'],
            
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            ['username', 'uniqueUsername'],
            ['username', 'string', 'min' => 2, 'max' => 50],
                         
            ['password', 'string', 'min' => 6],
 
            
            ['first_name', 'string', 'max' => 255],                         
            ['last_name', 'string', 'max' => 255], 
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'password' => 'Password',            
            'username' => 'Username',            
            'first_name' => 'First name',
            'last_name' => 'Last name',            
        ];
    }
    /**
     * uniquie username.
     *
     * @return add error if username not unique
     */
    public function uniqueUsername($attribute, $params) {
        $admin_id = $this->id;
        $user = array();
        $query = new Query();
        $admin = $query->select('id')
                ->from('admins')
                ->where('id <> :admin_id AND username = :username',[':admin_id' => $admin_id, ':username' => $this->$attribute])
                ->all();
        if(!empty($admin)) {
            $this->addError($attribute, 'This username has already been taken.');
        }        
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {        
        if ($this->validate()) {
            $admin = new Admins();
            $admin = $admin->find()->where('id = :id', [':id' => $this->id])->one();
            $admin ->username = $this->username;                        
            $admin ->first_name = $this->first_name ? $this->first_name : null;
            $admin ->last_name = $this->last_name ? $this->last_name : null;
            if($this->password) {
                $admin->password = $admin ->setPassword($this->password);            
            }                        

            if ($admin->update()) {
                return $admin;
            }
        }    
        return false;
    }

}