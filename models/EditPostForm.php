<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Posts;
 
/**
 * Signup form
 */
class EditPostForm extends Model
{
    public $id;
    public $title;
    public $image;  
    public $description;
    public $keywords;
    public $lang;
    public $post;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['id', 'integer'],
            [['post', 'lang'], 'string'],
            [['image', 'title'], 'string', 'max' => 255],
            [['description', 'keywords'], 'string', 'max' => 500]
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Desctiption',
            'image' => 'Image',
            'keywords' => 'Keywords',
            'lang' => 'Language',
            'post' => 'Post',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {        
        if ($this->validate()) {
            $post = new Posts();
            $post = $post->find()->where(['id' => $this->id])->one();
            $post->title = $this->title;
            $post->keywords = $this->keywords;
            $post->description = $this->description;
            $post->post = $this->post;
            $post->lang = $this->lang;
            $post->image = $this->image;

            if ($post->update()) {
                return $post;
            }
            else {
                var_dump($post->getErrors()); die;
            }
        }    
        return false;
    }

}

