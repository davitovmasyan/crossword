<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
use yii\db\Query;
/**
 * This is the model class for table "posts".
 *
 * @property string $id
 * @property string $image
 * @property string $title
 * @property string $post
 * @property string $description
 * @property string $active
 * @property string $keywords
 * @property string $lang
 * @property string $created
 * @property string $modified
 */
class Posts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'title', 'post', 'description', 'active', 'keywords', 'lang'], 'required'],
            [['post', 'active', 'lang'], 'string'],
            [['created', 'modified'], 'safe'],
            [['image', 'title'], 'string', 'max' => 255],
            [['description', 'keywords'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'title' => 'Title',
            'post' => 'Post',
            'description' => 'Description',
            'active' => 'Active',
            'keywords' => 'Keywords',
            'lang' => 'Language',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }
    /**
     * @return posts for blog page
     */
    public function getPosts($limit, $offset, $conditions)
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $result = [];
        $query = new Query();

        $result['posts'] = $query->select('posts.*')
                                 ->from('posts')   
                                 ->where('posts.active = "yes" AND posts.lang = "'.$lang.'"')
                                 ->limit($limit)
                                 ->offset($offset)
                                 ->orderBy('posts.created DESC')
                                 ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(posts.id) FROM posts WHERE posts.active = "yes" AND posts.lang = "'.$lang.'"')
                                        ->queryScalar();

        return $result;
    }
    /**
     * @return posts for admin blog page
     */
    public function getAllPosts($limit, $offset, $conditions)
    {
        $result = [];
        $query = new Query();

        $result['posts'] = $query->select('posts.*')
                                 ->from('posts')
                                 ->limit($limit)
                                 ->offset($offset)
                                 ->orderBy('posts.created DESC')
                                 ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(posts.id) FROM posts')
                                        ->queryScalar();

        return $result;
    }
    /**
     * @return single post for reading
     */
    public function getPost($id)
    {
        $result = [];
        $result['post'] = [];
        $result['comments'] = [];

        $query = new Query();

        $result['post'] = $query->select('posts.*')
                        ->from('posts')
                        ->where('posts.active = "yes" AND posts.id = '.$id)
                        ->one();

        if(!empty($result)) {
            $query = new Query();
            $result['comments'] = $query->select('comments.*, users.username, users.id AS uid')
                                    ->from('comments')
                                    ->leftJoin('users', 'users.id = comments.user_id AND users.active = 1')
                                    ->where('comments.game_id = :id AND comments.game_type = "post" AND comments.deleted = "no"', [':id' => $id])
                                    ->orderBy('comments.created DESC')
                                    ->having('users.username IS NOT NULL')
                                    ->all();
        }

        return $result;
    }
    /**
     * @return suggestions for post by keywords
     */
    public function getSuggestions($id, $conditions)
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $result = [];
        $query = new Query();

        $result = $query->select('posts.id, posts.title, posts.image, posts.created')
                        ->from('posts')
                        ->where('posts.active = "yes" AND posts.id <> '.$id.' AND posts.lang = "'.$lang.'" '.$conditions)
                        ->limit(4)
                        ->orderBy('rand()')
                        ->all();

        return $result;
    }

    public function search($conditions) 
    {
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $result = array();
        $query = new Query();

        $result = $query->select('posts.id, posts.title, posts.image')
                        ->from('posts')
                        ->where('posts.active = "yes" AND posts.lang = "'.$lang.'" AND '.$conditions)
                        ->limit(25)
                        ->orderBy('RAND()')
                        ->all();

        return $result;
    }
}
