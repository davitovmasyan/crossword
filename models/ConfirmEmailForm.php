<?php

namespace app\models;
 
use yii\base\InvalidParamException;
use yii\base\Model;
use app\models\Users;
use Yii;
 
class ConfirmEmailForm extends Model
{
    /**
     * @var User
     */
    private $_user;
 
    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array $config
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Missing Token.');
        }
        $this->_user = Users::findByEmailConfirmToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Incorrect Token.');
        }
        parent::__construct($config);
    }
 
    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function confirmEmail()
    {
        $user = $this->_user;
        $user->active = true;
        $user->removeEmailConfirmToken();
 
        return $user->save();
    }
}