<?php

namespace app\models;

use app\components\DateTimeStampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use Yii;

/**
 * This is the model class for table "saved_words".
 *
 * @property string $id
 * @property string $word_id
 * @property string $user_id
 * @property string $status
 * @property string $created
 * @property string $modified
 *
 * @property Users $user
 * @property Words $word
 */
class SavedWords extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'saved_words';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['word_id', 'user_id', 'status'], 'required'],
            [['word_id', 'user_id'], 'integer'],
            [['status'], 'string'],
            [['created', 'modified'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word_id' => 'Word ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'word_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAll($limit, $offset)
    {
        $query = new Query();

        $result = [];

        $result['saved'] = $query->select('saved_words.*, users.username')
                        ->from('saved_words')
                        ->leftJoin('users', 'users.id = saved_words.user_id')
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy('saved_words.created DESC')
                        ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(saved_words.id) FROM saved_words')
             ->queryScalar();

        return $result;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyWords($user_id)
    {        
        $result = [];
        $query = new Query();

        $result = $query->select('COUNT(words.id) AS count, words.category_id, categories.category_name, categories.image')
                        ->from('saved_words')
                        ->leftJoin('words', 'words.id = saved_words.word_id')
                        ->leftJoin('categories', 'categories.id = words.category_id')
                        ->where('saved_words.status = "success" AND saved_words.user_id = :user_id', [':user_id' => $user_id])
                        ->groupBy('words.category_id')
                        ->all();


        return $result;
    }
}
