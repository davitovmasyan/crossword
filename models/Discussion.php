<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
/**
 * This is the model class for table "discussion".
 *
 * @property string $id
 * @property string $name
 * @property string $created
 * @property string $modified
 *
 * @property DiscussionTexts[] $discussionTexts
 */
class Discussion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discussion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created', 'modified'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscussionTexts()
    {
        return $this->hasMany(DiscussionTexts::className(), ['discussion_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAll()
    {
        $result = [];
        $query = new Query();

        $result = $query->select('discussion.*')
                        ->from('discussion')
                        ->all();

        return $result;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscussion($id)
    {
        $result = [];
        $query = new Query();

        $result = $query->select('discussion_texts.*, users.username, users.id AS uid, users.active')
                        ->from('discussion_texts')
                        ->leftJoin('users', 'users.id = discussion_texts.user_id')
                        ->where('discussion_texts.discussion_id = :id', [':id' => $id])
                        ->orderBy('discussion_texts.created DESC')
                        ->all();

        return $result;
    }
}
