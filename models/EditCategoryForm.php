<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Categories;
 
/**
 * Signup form
 */
class EditCategoryForm extends Model
{   
    public $id; 
    public $category_name;
    public $image;
    public $keywords;
    public $lang;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name', 'id', 'lang'], 'required'],
            ['image', 'required'],
            ['keywords', 'string', 'max' => 255],
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_name' => 'Category',
            'keywords' => 'Keywords',
            'image' => 'Image',
            'lang' => 'Language', 
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {
            $category = new Categories();
            $category = $category->find()->where('id = :id', [':id' => $this->id])->one();

            $category->category_name = $this->category_name;
            $category->keywords = $this->keywords ? $this->keywords : null;
            $category->image = $this->image;
            $category->lang = $this->lang;

            if ($category->update()) {
                return $category;
            }
            else {
                var_dump($category->getErrors());die;
            }
        return false;    
    }
}