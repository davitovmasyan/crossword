<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;
use yii\db\Query;
/**
 * This is the model class for table "words".
 *
 * @property string $id
 * @property string $category_id
 * @property string $question
 * @property string $answers
 * @property string $answer
 * @property string $active
 * @property string $lang
 * @property string $point
 * @property string $created
 * @property string $modified
 *
 * @property SavedWords[] $savedWords
 * @property Categories $category
 */
class Words extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'words';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'question', 'answers', 'answer', 'active', 'point'], 'required'],
            [['category_id', 'point'], 'integer'],
            [['active'], 'string'],
            [['created', 'modified'], 'safe'],
            [['question', 'answer'], 'string', 'max' => 255],
            [['answers'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'question' => 'Question',
            'answers' => 'Answers',
            'answer' => 'Answer',
            'active' => 'Active',            
            'point' => 'Point',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSavedWords()
    {
        return $this->hasMany(SavedWords::className(), ['word_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllWords($conditions, $limit, $offset)
    {
        $words = array();
        $count = 0;
        $query = new Query();

        $words = $query->select('words.*, categories.category_name')
                        ->from('words')
                        ->leftJoin('categories', 'categories.id = words.category_id')
                        ->where('words.id IS NOT NULL'.$conditions)
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('words.created DESC')
                        ->all();

        $count = Yii::$app->db->createCommand('SELECT COUNT(words.id) FROM words WHERE id IS NOT NULL'.$conditions)
             ->queryScalar();
        
        return ['words' => $words, 'count' => $count];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSevenWords($id, $user_id = null)
    {
        $result = [];
        $query = new Query();

        $result = $query->select('words.*, categories.category_name, categories.image')
                        ->from('words')
                        ->leftJoin('categories', 'categories.id = words.category_id')
                        ->where('words.active = "yes" AND words.category_id = '.$id)
                        ->limit(7)
                        ->orderBy('RAND()')
                        ->all();

        if(!empty($result)) {
            $query = new Query();
            $result['comments'] = $query->select('comments.*, users.username, users.id AS uid')
                                    ->from('comments')
                                    ->leftJoin('users', 'users.id = comments.user_id AND users.active = 1')
                                    ->where('comments.game_id = :id AND comments.game_type = "word" AND comments.deleted = "no"', [':id' => $id])
                                    ->orderBy('comments.created DESC')
                                    ->having('users.username IS NOT NULL')
                                    ->all();
        }

        return $result;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllWordsGroupByCategory()
    {        
        $lang = Yii::$app->language ? Yii::$app->language : 'hy';
        $result = [];
        $query = new Query();

        $result = $query->select('COUNT(words.id) AS count, words.category_id, categories.category_name, categories.image, categories.active')
                        ->from('words')
                        ->leftJoin('categories', 'categories.id = words.category_id AND categories.active = "yes" AND categories.lang = "'.$lang.'"')
                        ->where('words.active = "yes"')
                        ->groupBy('category_id')
                        ->all();

        return $result;
    }
}
