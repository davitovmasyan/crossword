<?php
namespace app\models;
 
use yii\base\Model;
use Yii;
use app\models\Crosswords;
 
/**
 * Signup form
 */
class EditCrosswordForm extends Model
{
    public $id;
    public $public_number;
    public $lang;
    public $crossword_name;
    public $description;
    public $type;    
    public $structure;
    public $questions;
    public $category;
    public $image;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questions', 'structure', 'public_number', 'description', 'type', 'category', 'lang', 'id'], 'required'],
            [['questions', 'structure'], 'string'],            
            
            [['public_number', 'id'], 'integer'],
                            
            [['crossword_name', 'lang', 'description', 'image', 'category'], 'string', 'max' => 255],
                        
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => 'ID',
            'public_number' => 'Public Number',
            'lang' => 'Lang',
            'crossword_name' => 'Crossword Name',
            'description' => 'Description',
            'type' => 'Type',
            'structure' => 'Structure',
            'questions' => 'Questions',
            'category' => 'Category',
            'image' => 'Image',
        ];
    }
    /**     
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {        
        if ($this->validate()) {
            $crossword = new Crosswords();
            $crossword = $crossword->findOne(['id' => $this->id]);
            $crossword->type = $this->type ? $this->type : 'medium';
            $crossword->category = $this->category ? $this->category : 'other';
            $crossword->lang = $this->lang ? $this->lang : 'arm';
            $crossword->questions = $this->questions;
            $crossword->structure = $this->structure;
            $crossword->description = $this->description;
            $crossword->crossword_name = $this->crossword_name ? $this->crossword_name : null;
            $crossword->public_number = $this->public_number;
            if($this->image)
            {
                $crossword ->image = $this->image;
            }

            if ($crossword->update()) {
                return $crossword;
            }
            else {
                var_dump($crossword->getErrors()); die;
            }
        }    
        return false;
    }

}