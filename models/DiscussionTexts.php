<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use app\components\DateTimeStampBehavior;

/**
 * This is the model class for table "discussion_texts".
 *
 * @property string $id
 * @property string $discussion_id
 * @property string $user_id
 * @property string $text
 * @property string $deleted
 * @property string $created
 * @property string $modified
 *
 * @property Discussion $discussion
 */
class DiscussionTexts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discussion_texts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discussion_id', 'text', 'deleted', 'user_id'], 'required'],
            [['discussion_id', 'user_id'], 'integer'],
            [['deleted'], 'string'],
            [['created', 'modified'], 'safe'],
            [['text'], 'string', 'max' => 500]
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => DateTimeStampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],                    
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'modified',
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discussion_id' => 'Discussion ID',
            'user_id' => 'User ID',
            'text' => 'Text',
            'deleted' => 'Deleted',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscussion()
    {
        return $this->hasOne(Discussion::className(), ['id' => 'discussion_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTexts($id)
    {
        $query = new Query();
        $result = [];

        $result = $query->select('dt.*, discussion.name, users.active, users.username')
                        ->from('discussion_texts AS dt')
                        ->leftJoin('discussion', 'discussion.id = :id', [':id' => $id])
                        ->leftJoin('users', 'users.id = dt.user_id')
                        ->where('dt.deleted = "no" AND dt.discussion_id = :id', [':id' => $id])
                        ->orderBy('dt.created DESC')
                        ->having('dt.discussion_id IS NOT NULL AND users.active = 1')
                        ->all();

        return $result;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscussions()
    {
        $query = new Query();
        $result = [];

        $result = $query->select('discussion.*, count(dt.id) AS count')
                        ->from('discussion')
                        ->leftJoin('discussion_texts as dt', 'dt.discussion_id = discussion.id AND dt.deleted = "no"')
                        ->orderBy('discussion.created DESC')
                        ->groupBy('discussion.id')
                        ->all();

        return $result;   
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllTexts($limit, $offset)
    {
        $query = new Query();
        $result = [];

        $result['discussions'] = $query->select('discussion_texts.*, discussion.name AS discname, users.username, users.active')
                        ->from('discussion_texts')
                        ->leftJoin('discussion', 'discussion.id = discussion_texts.discussion_id')
                        ->leftJoin('users', 'users.id = discussion_texts.user_id')
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy('discussion_texts.created DESC')
                        ->all();

        $result['count'] = Yii::$app->db->createCommand('SELECT COUNT(discussion_texts.id) FROM discussion_texts')
                                        ->queryScalar();

        return $result;   
    }
    public function getLastFive($user_id, $did)
    {
        $query = new Query();
        $result = [];

        $result = $query->select('discussion_texts.*')
                        ->from('discussion_texts')
                        ->where('discussion_texts.discussion_id = :did AND discussion_texts.user_id <> :user_id AND discussion_texts.deleted = "no"', ['did' => $did, 'user_id' => $user_id])
                        ->limit(5)
                        ->orderBy('discussion_texts.created DESC')
                        ->all();
        

        return $result;   
    }
}
