<?php
namespace app\models;
 
use yii\base\Model;
use app\components\ImageMaker;
use Yii;
 
/**
 * edit profile form
 */
class EditProfileForm extends Model
{
    public $username;
    public $email;    
    public $password; 
    public $password_repeat;
    public $first_name;
    public $last_name;
    public $address;    
    public $city;
    public $state;    
    public $country;
    public $zip_code;    
    public $sex;
    public $age;    
    public $facebook_id;
    public $street;
    public $lat_lng;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],            
            ['username', 'match', 'pattern' => '#^[\w_-d]+$#i'],            
            ['username', 'string', 'min' => 2, 'max' => 16],
 
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],            
             
            ['password', 'string', 'min' => 6, 'max' => 21],
            
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            
            ['first_name', 'string', 'max' => 255],                         
            ['last_name', 'string', 'max' => 255], 
            
            ['facebook_id', 'string', 'max' => 255],
            ['address', 'string', 'max' => 255],
            ['country', 'string', 'max' => 50],
            ['state', 'string', 'max' => 50],
            ['zip_code', 'string', 'max' => 50],
            ['street', 'string', 'max' => 50],
            ['lat_lng', 'string', 'max' => 50],
            
            ['age', 'integer'],
            ['sex', 'string'],
                                    
                ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => '#էլ հասցե',
            'password' => '#գաղնտաբառ',
            'password_repeat' => '#գաղնտաբառ կրկ.',
            'username' => '#մուտքանուն',
            'address' => '#հասցե',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'street' => 'Street',
            'zip_code' => 'Zip Code',
            'email_confirm_token' => 'Email Confirm Token',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'lat_lng' => 'Lat Lng',
            'facebook_id' => 'Facebook ID',
            'sex' => '#սեռ',
            'first_name' => '#անուն',
            'last_name' => '#ազգանուն',
            'age' => '#տարիք',            
        ];
    }
    /**
     * edit user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {        
        if ($this->validate()) {
            $user = new Users();
            $user = $user->findOne(['id' => Yii::$app->user->id]);
            $user->facebook_id = $this->facebook_id ? $this->facebook_id : null;
            $user->email = $this->email;
            if($user->username != $this->username)
            {
                $user->image = Yii::$app->ImageMaker->regenerate($this->username, $user->username);
            }
            $user->username = strtolower($this->username);
            $user->address = $this->address ? $this->address : null;
            $user->city = $this->city ? $this->city : null;
            $user->state = $this->state ? $this->state : null;
            $user->street = $this->street ? $this->street : null;
            $user->zip_code = $this->zip_code ? $this->zip_code : null;
            $user->lat_lng = $this->lat_lng ? $this->lat_lng : null;
            $user->first_name = $this->first_name ? $this->first_name : null;
            $user->last_name = $this->last_name ? $this->last_name : null;
            $user->age = $this->age ? $this->age : 18;
            $user->sex = $this->sex ? $this->sex : 'boy';            

            if($this->password) {
                $user->setPassword($this->password);
                Yii::$app->mailer->compose('changePassword', ['user' => $user, 'password' => $this->password])
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Crossword.am - Ծանուցումներ'])
                    ->setTo($this->email)
                    ->setSubject('Գաղտնաբառի փոփոխություն.')
                    ->send();
            }            

            if (!$user->update()) {
                var_dump($user->getErrors());
                die;
            }
    
            return $user;
        }        
        return false;
    }
}