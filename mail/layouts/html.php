<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Crossword.am</title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div style="width: 100%; margin: 0 auto; padding: 10px;">
		<div style="width: 80%;	margin: 0 auto;	text-align: center;">
			<h1 style="text-align: center;	color: #f7931e;	text-decoration: none;	font-size: 18px;">
				Crossword.am | Հայկական Օնլայն խաղեր
			</h1>
		</div>
		<?= $content ?>
	</div>
	<div style="color: #f7931e; font-size:12px;"> 
		<br><br>Շնորհակալություն!<br>
		Հարգանքներով <span style="color: rgb(161, 160, 161);">Crossword.am!</span>
	</div>
    <div style="width: 100%;	height: 80px;	margin: 10px auto;	text-align: center;">
    <div style="width: 100%; margin: 0 auto;">
   		<a href="http://crossword.am" title="Crossword.am" style="
   		margin: 0 auto;
		text-align: center;
		padding-left: 20px;
		padding-right: 20px;
		padding-top: 5px;
		padding-bottom: 5px;
		border: 3px solid #f7931e;
		color: #f7931e;
		font-weight: bold;
		cursor: pointer;
		text-decoration: none;">Բացել Կայքը</a>
	</div>
    <ul style="margin: 10px auto;	list-style: none;	font-size: 10px; padding: 0; color: rgb(161, 160, 161);">
    	<li>
    		<a style="color: rgb(161, 160, 161); text-decoration: none;" target="_blank" title="Գլխավոր" href="http://crossword.am">Գլխավոր</a> |
    		<a style="color: rgb(161, 160, 161); text-decoration: none;" target="_blank" title="Կապ" href="http://crossword.am/site/contact-us">Կապ</a> |
    		<a style="color: rgb(161, 160, 161); text-decoration: none;" target="_blank" title="Մեր Մասին" href="http://crossword.am/site/about">Մեր Մասին</a> |
    		<a style="color: rgb(161, 160, 161); text-decoration: none;" target="_blank" title="Խաղեր" href="http://crossword.am/site/games">Խաղեր</a> |
    		<a style="color: rgb(161, 160, 161); text-decoration: none;" target="_blank" title="ՀՏՀ<" href="http://crossword.am/site/faq">ՀՏՀ</a> |
    		<a style="color: rgb(161, 160, 161); text-decoration: none;" target="_blank" title="Կանոններ" href="http://crossword.am/site/privacy-policy">Կանոններ</a>
    	</li>
    </ul>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
