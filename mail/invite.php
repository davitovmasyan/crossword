<?php
use yii\helpers\Html;
use yii\helpers\Url;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
?>
<div style="color: rgb(161, 160, 161); margin: 0 auto; padding-left:50px; padding-right:50px; font-size:14px;">
Բարև <span style="color: #f7931e; font-weight: bold;"><?php echo Html::encode(ucfirst($name)); ?></span>,<br>

Սիրով տեղեկացնում ենք <span style="color: #f7931e; font-weight: bold;">Crossword.am</span> կայքի թարմացման մասին։<br>

Կայքում ավելացել են նաև ռուսերեն և անգլերեն լեզուներով խաչբառեր ու այլ հետաքրքիր խաղեր։<br><br>

Հիշեցնում ենք ձեր տվյալները՝<br>

<span style="padding-left:20px;">
	մուտքանուն։ էլ հասցե կամ <span style="color: #f7931e; font-weight: bold;"><?php echo $username;?></span>
</span><br>
<span style="padding-left:20px;">
	գաղտնաբառ։ <span style="color: #f7931e; font-weight: bold;"><?php echo $password;?></span>
</span>
<span style="padding-left:20px;">
	միավորներ։ <span style="color: #f7931e; font-weight: bold;"><?php echo $points;?></span>
</span>
</div>