<?php
use yii\helpers\Html;
use yii\helpers\Url;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
?>
<div style="color: rgb(161, 160, 161); margin: 0 auto; padding-left:50px; padding-right:50px; font-size:14px;">
Բարև <span style="color: #f7931e; font-weight: bold;"><?php echo Html::encode(ucfirst($user->username)); ?></span>,<br>

Գրանցումը հաջողությամբ կատարված է։

Ձեր տվյալներն են՝<br>

<span style="padding-left:20px;">
	մուտքանուն։ էլ հասցե կամ <span style="color: #f7931e; font-weight: bold;"><?php echo $user->username;?></span>
</span><br>
<span style="padding-left:20px;">
	գաղտնաբառ։ <span style="color: #f7931e; font-weight: bold;"><?php echo $password;?></span>
</span>
</div>