<?php
use yii\helpers\Html;
use yii\helpers\Url;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
?>
<div style="color: rgb(161, 160, 161); margin: 0 auto; padding-left:50px; padding-right:50px; font-size:14px;">
Հարգելի <span style="color: #f7931e; font-weight: bold;"><?php echo Html::encode(ucfirst($user->username)); ?></span>,<br>

Ձեր նոր գաղտնաբառը - <span style="color: #f7931e; font-weight: bold;"><?php echo $password; ?></span>:
</div>