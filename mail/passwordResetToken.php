<?php
use yii\helpers\Html;
use yii\helpers\Url;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password', 'token' => $user->password_reset_token]);
?>
<div style="color: rgb(161, 160, 161); margin: 0 auto; padding-left:50px; padding-right:50px; font-size:14px;">
Հարգելի <span style="color: #f7931e; font-weight: bold;"><?php echo Html::encode(ucfirst($user->username)); ?></span>,<br>

Ձեր գաղտնաբառը վերականգնելու համար սեղմեք <?php echo Html::a('այստեղ', $resetLink, ['style' => 'color: #f7931e; text-decoration:none;']); ?>:<br>

Իսկ եթե դուք չեք ուղարկել այս նամակը պարզապես անտեսեք այն։
</div>