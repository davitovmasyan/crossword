$(document).ready(function(){

});

var doing = false;

function enter(city)
{
    if(doing) {

    } else {
        doing = true;
        $.ajax({
            url: '/city/get-city',
            method:'GET',
            dataType: 'json',
            data: {
                city: city,
            },
            success: function(data){
                doing = false;
                if(typeof data.message != 'undefined' && (data.message == 'Invalid city.' || data.message == 'Invalid data.')) {
                    $('.answer').css('color', 'red');
                } else if(data.user.cityname) {
                    $('.citiesList').append('<span class="singleCity">'+data.user.cityname+'</span>');
                    $('.citiesList .singleCity').last().hide().fadeToggle(500);
                    if(typeof data.comp.compname != 'undefined') {
                        $('.citiesList').append('<span class="singleCity">'+data.comp.compname+'</span>');
                        $('.citiesList .singleCity').last().hide().delay(500).fadeToggle(500);
                        $('.answer').val('').focus();
                        $('.firstLetter').val(data.firstLetter);
                    }
                }
            }
        })
    }
}