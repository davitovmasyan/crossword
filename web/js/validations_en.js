
$(document).ready(function(){  
    
        function validateForm(formId) {
            var response = '';
            var form = $('#' + formId);            
            $.ajax({
                 async: false,
                 url: form.attr('action'),
                 type: 'post',
                 dataType: 'json',
                 data: form.serialize(),
                 success: function (data) {
                     response = data;
                 }
            });
            return response;
        }       

        $('#problem-form').validate({
            onkeyup: false,
            rules: {
                'email': {
                    required: true,
                    email: true,
                },
                'subject': {
                    required: true,
                    maxlength: 20,
                },
                'message' : {
                    required: true,
                    minlength: 50,
                    maxlength: 500,
                },
            },
            messages: {
                'email': {
                    required: 'Email is required.',
                    email: 'Enter a valid email.',
                },
                'subject': {
                    required: 'Subject is required.',
                    maxlength: 'Subject must be less than 20 symbols.',
                },
                'message' : {
                    required: 'Messages is required.',
                    minlength: 'Message must be more than 50 symbols.',
                    maxlength: 'Message must be less than 500 symbols.',
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#login-form").validate({
          onkeyup: false,
            rules: {
                'LoginForm[username]': {
                    required: true
                },
                'LoginForm[password]': {
                    required: true,
                    minlength: 6
                },
            },
            messages: {
                'LoginForm[username]': {
                    required: 'Username is required.'
                },
                'LoginForm[password]': {
                    required: 'Password is required.',
                    minlength: 'Password must be more than 6  symbols.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#request-password-reset-form").validate({
          onkeyup: false,
            rules: {
                'PasswordResetRequestForm[email]': {
                    required: true,
                    email:true,
                },
            },
            messages: {
                'PasswordResetRequestForm[email]': {
                    required: "Email is required.",
                    email: 'Enter a valid email.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#password-reset-form").validate({
          onkeyup: false,
            rules: {
                'ResetPasswordForm[password]': {
                    required: true,
                    minlength:6,
                },
                'ResetPasswordForm[password_repeat]': {
                    required:true,
                    equalTo: '#resetpasswordform-password'                    
                },                                
            },
            messages: {
                 'ResetPasswordForm[password]': {
                    required: 'Password is required.',
                    minlength: 'Password must be more than 6 symbols.'
                },
                'ResetPasswordForm[password_repeat]': {
                    required: 'Password confirmation is required.',
                    equalTo: 'Passwords must be equal.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#contact-form").validate({
          onkeyup: false,
            rules: {
                'ContactForm[name]': {
                    required: true
                },
                'ContactForm[email]': {
                    required: true,
                    email:true
                },
                'ContactForm[message]': {
                    required: true,                    
                },
                'ContactForm[verifyCode]': {
                    required: true,                    
                },
            },
            messages: {
                'ContactForm[name]': {
                    required: "First and Last names are required."
                },
                'ContactForm[email]': {
                    required: "Email is required.",
                    email: 'Enter a valid email.'
                },
                'ContactForm[message]': {
                    required: "Message is required."
                },
                'ContactForm[verifyCode]': {
                    required: "Captcha is required."
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $.validator.addMethod("unique_email", 
                function(value, element) {
                    var uniquie_email= true;
                    if($('#signupform-email').length) {
                        var email = $('#signupform-email').val();
                    }
                    if($('#editprofileform-email').length){
                        var email = $('#editprofileform-email').val();
                    }
                    if(email != ""){ 
                    $.ajax({
                        async: false,
                        url: '/user/unique-email',
                        type: 'get',
                        data: {                            
                            email: email
                        } ,
                        success: function(data) {
                            data = jQuery.parseJSON(data)
                           if(data['data']){
                               unquie_email = true;
                           }else{
                               unquie_email = false;
                           }
                        }
                    });
                }
                return unquie_email;
        }, "This email is already exists please try another one.");
        
        
        $.validator.addMethod("unique_username", 
                function(value, element) {
                    var unique_username= true;
                    if($('#signupform-username').length) {
                        var username = $('#signupform-username').val();
                    }
                    if($('#editprofileform-username').length){
                        var username = $('#editprofileform-username').val();
                    }
                    if(username != ""){ 
                    $.ajax({
                        async: false,
                        url: '/user/unique-username',
                        type: 'get',
                        data: {                            
                            username: username
                        } ,
                        success: function(data) {
                            data = jQuery.parseJSON(data)
                           if(data['data']){
                               unique_username = true;
                           }else{
                               unique_username = false;
                           }
                        }
                    });
                }
                return unique_username;
        }, "This username is already exists please try another one.");
        
        $.validator.addMethod("filter_username", function(){
            var username = '';
                if($('#signupform-username').length){
                    username = $('#signupform-username').val();
                }
                else {
                    username = $('#editprofileform-username').val();
                }
                
                if(username.match(/^[a-z][\w-d]+$/i)) {
                    return true;
                }
                return false;
        }, "Username must contain a-z 0-9 and - symbols and must start with letter.");
        
        
        
        $("#signupform").validate({
          onkeyup: false,          
            rules: {
                'SignUpForm[first_name]' : {
                    
                },
                'SignUpForm[last_name]' : {
                    
                },
                'SignUpForm[email]' : {
                    required: true,
                    email: true,
                    unique_email:true                    
                },
                'SignUpForm[address]' : {
                                    
                },
                'SignUpForm[username]': {
                    required: true,
                    filter_username:true,
                    minlength: 2,
                    maxlength: 15,
                    unique_username: true,
                },
                'SignUpForm[password]': {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                'SignUpForm[password_repeat]': {
                    required: true,
                    equalTo: '#signupform-password'
                },
            },
            messages: {
                'SignUpForm[first_name]' : {
                    
                },
                'SignUpForm[last_name]' : {
                    
                },
                'SignUpForm[email]' : {
                    required: 'Email is required.',
                    email: 'Enter a valid email.',
                },
                'SignUpForm[address]' : {
                    
                },
                'SignUpForm[username]': {
                    required: 'Username is required.',
                    minlength: 'Username must be more than 2 symbols.',
                    maxlength: 'Username must be less than 15 symbols.',
                },
                'SignUpForm[password]': {
                    required: 'Password is required.',
                    minlength: 'Password must be more than 6 symbols.',
                    maxlength: 'Password must be less than 20 symbols.',
                },
                'SignUpForm[password_repeat]': {
                    required: 'Password confirmation is required.',
                    equalTo: 'Passwords not equal.',
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').addClass('error');
                $(element).parents('.form-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').removeClass('error');
                $(element).parents('.form-group').addClass('success');
            }
        });
        
        $("#editprofileform").validate({
          onkeyup: false,          
            rules: {
                'EditProfileForm[first_name]' : {
                    
                },
                'EditProfileForm[last_name]' : {
                    
                },
                'EditProfileForm[email]' : {
                    required: true,
                    email: true,
                    unique_email:true
                },
                'EditProfileForm[address]' : {
                                    
                },
                'EditProfileForm[username]': {
                    required: true,
                    filter_username:true,
                    minlength: 2,
                    maxlength: 15,
                    unique_username: true,
                },
                'EditProfileForm[password]': {                    
                    minlength: 6,
                    maxlength: 20,
                },
                'EditProfileForm[password_repeat]': {                    
                    equalTo: '#editprofileform-password'
                },
            },
            messages: {
                'EditProfileForm[first_name]' : {
                    
                },
                'EditProfileForm[last_name]' : {
                    
                },
                'EditProfileForm[email]' : {
                    required: 'Email is required.',
                    email: 'Enter a valid email.',
                },
                'EditProfileForm[address]' : {
                    
                },
                'EditProfileForm[username]': {
                    required: 'Username is required.',
                    minlength: 'Username must be more than 2 symbols.',
                    maxlength: 'Username must be less than 15 symbols.',
                },
                'EditProfileForm[password]': {
                    minlength: 'Password must be more than 6 symbols.',
                    maxlength: 'Password must be less than 20 symbols.',
                },
                'EditProfileForm[password_repeat]': {                    
                    equalTo: 'Passwords not equal.',
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').addClass('error');
                $(element).parents('.form-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').removeClass('error');
                $(element).parents('.form-group').addClass('success');
            }
        });
});

