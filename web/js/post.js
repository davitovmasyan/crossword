$(document).ready(function(){

	$('button.toggle').on('click', function(){
		if(typeof $(this).attr('data-index') == 'undefined') {
			$('p.toggle').slideToggle();
		}
	});

	$('p.toggle').each(function(i,v) {
		$(this).hide();
	});

	$('button.toggle').on('click', function(){
		var index = $(this).attr('data-index');
		if(typeof $(this).attr('data-index') != 'undefined') {
			$('p.toggle').each(function(i,v) {
				if($(this).attr('data-index') == index) {
					$(this).slideToggle();
				} else {
					$(this).slideUp();
				}
			});
		}
	});
});