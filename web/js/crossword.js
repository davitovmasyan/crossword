$(document).ready(function(){

		var selected_i = '';
		var selected_j = '';
		var $keyboard = $('.keyboard');
		var empty = $('.empty').length;		
		var limit = parseInt(empty)*2;
		var save_time = 0;
		$('.limit').text(limit);
		$('div.steps').text(limit);


		window.saveTemp = true;
		// click on letter block
		$('td').on('click', function(e){
				var $this = $(this);
				if($this.attr('class') != 'empty') {
					return false;
				}
				$('td.selected').attr('class','empty');
				
				$this.attr('class','selected');
				$this.attr('tabindex', '1');
 				
				selected_i = $this.attr('data-i');
				selected_j = $this.attr('data-j');
				
				var offset = $this.offset();
				var kleft = offset.left+60+'px';
				var ktop = offset.top-100+'px';
				$keyboard.animate({
					left:kleft,
					top:ktop
				}, {
					duration:200
				})
		});

		// on keyboard letter click
		$('span.letter').on('click', function(){
			$this = $(this);
			var i = selected_i;
			var j = selected_j;
			if($('td#'+i+'-'+j).attr('class')=='selected') {
				limit = limit -1;
				$('.limit').text(limit).animate({
					'opacity' : 0.5
				}, {
					duration:200,
					queue:true
				}).animate({
					'opacity' : 0.9
				},{
					duration:200
				});
				
			if($this.text()=='-') {
				$('td#'+i+'-'+j).text('');
				return false;
			}
			$('td#'+i+'-'+j).text(alphabet($this.text(), inputlang));
			var l = c_letters.indexOf(i+'-'+j+'-'+$this.attr('data-letter'));
			if(l != -1) {
				$('td.selected').removeClass('empty');
				$('td#'+i+'-'+j).removeClass('selected');
			    $('td#'+i+'-'+j).attr('class','true').attr('data-letter', $this.text());
			    $('#save').text('պահպանել');
			    empty = empty - 1;
				save_time++;		
				if(!empty) {
					finish();
					return false;
				}
			    if(save_time > 5) {
			    	saveCrossword();
			    } else {
			    	$('#save').text('պահպանել');
			    }
			    var c1 = parseInt(i)+1;
			    var c2 = parseInt(j)+1;
			    var cm1 = parseInt(i)-1;
			    var cm2 = parseInt(j)-1;
			    if($('td#'+cm1+'-'+j).attr('class') == 'true' && $('td#'+i+'-'+cm2).attr('class') != 'true') {
				    if($('td#'+c1+'-'+j).attr('class') == 'empty') {
				    	$('td#'+c1+'-'+j).attr('class', 'selected').attr('tabindex', '1');
				    	var newoffset = $('td#'+c1+'-'+j).offset();
						var cleft = newoffset.left+60+'px';
						var ctop = newoffset.top-100+'px';
						$keyboard.animate({
							left:cleft,
							top:ctop
						}, {
							duration:200
						});
						selected_i = $('td#'+c1+'-'+j).attr('data-i');
						selected_j = $('td#'+c1+'-'+j).attr('data-j');
				    }
				    else if($('td#'+i+'-'+c2).attr('class') == 'empty') {
				    	$('td#'+i+'-'+c2).attr('class', 'selected').attr('tabindex', '1');
				    	var newoffset = $('td#'+i+'-'+c2).offset();
						var cleft = newoffset.left+60+'px';
						var ctop = newoffset.top-100+'px';
						$keyboard.animate({
							left:cleft,
							top:ctop
						}, {
							duration:200
						});
						selected_i = $('td#'+i+'-'+c2).attr('data-i');
						selected_j = $('td#'+i+'-'+c2).attr('data-j');
				    }
				    else {
				    	$keyboard.animate({
							left:'-500px'
						}, {
							duration:200
						});
				    }
				}
				else {
					 if($('td#'+i+'-'+c2).attr('class') == 'empty') {
				    	$('td#'+i+'-'+c2).attr('class', 'selected').attr('tabindex', '1');
				    	var newoffset = $('td#'+i+'-'+c2).offset();
						var cleft = newoffset.left+60+'px';
						var ctop = newoffset.top-100+'px';
						$keyboard.animate({
							left:cleft,
							top:ctop
						}, {
							duration:200
						});
						selected_i = $('td#'+i+'-'+c2).attr('data-i');
						selected_j = $('td#'+i+'-'+c2).attr('data-j');
				    }
				    else if($('td#'+c1+'-'+j).attr('class') == 'empty') {
				    	$('td#'+c1+'-'+j).attr('class', 'selected').attr('tabindex', '1');
				    	var newoffset = $('td#'+c1+'-'+j).offset();
						var cleft = newoffset.left+60+'px';
						var ctop = newoffset.top-100+'px';
						$keyboard.animate({
							left:cleft,
							top:ctop
						}, {
							duration:200
						});
						selected_i = $('td#'+c1+'-'+j).attr('data-i');
						selected_j = $('td#'+c1+'-'+j).attr('data-j');
				    }
				    else {
				    	$keyboard.animate({
							left:'-500px'
						}, {
							duration:200
						});
				    }
				}
			}
			if(limit == 0 && empty != 0) {
				noLimit();
			}
		}
		});

		function noLimit() {
			$keyboard.attr('class','no-keyboard');
			$('#nolimitbtn').trigger('click');
			saveCrossword();
		}

		function finish() {
			$keyboard.attr('class','no-keyboard');
			$('#finishbtn').trigger('click');
			saveCrossword();
		}

		// td question on click partial for touch events
		$('td.question').on('click', function(){
			$('div.questions').hide();
			$('td.selected').attr('class','empty');
			var $this = $(this);
			var i = $this.attr('data-i');
			var j = $this.attr('data-j');
			var ni = parseInt(i)+1;
			var nj = parseInt(j)+1;
			if($this.attr('class') == 'question bobble-bottom') {
				if($('td#' + ni + '-' + j).attr('class') == 'empty') {
					$('td#' + ni + '-' + j).attr('class','selected').attr('tabindex', '1');
					var newoffset = $('td#' + ni + '-' + j).offset();
					var cleft = newoffset.left+60+'px';
					var ctop = newoffset.top-100+'px';
					$keyboard.animate({
						left:cleft,
						top:ctop
						}, {
						duration:200
					});
					selected_i = $('td#' + ni + '-' + j).attr('data-i');
					selected_j = $('td#' + ni + '-' + j).attr('data-j');
				}
			}
			else if($this.attr('class') == 'question bobble-right') {
				if($('td#' + i + '-' + nj).attr('class') == 'empty') {
					$('td#' + i + '-' + nj).attr('class','selected').attr('tabindex', '1');
					var newoffset = $('td#' + i + '-' + nj).offset();
					var cleft = newoffset.left+60+'px';
					var ctop = newoffset.top-100+'px';
					$keyboard.animate({
						left:cleft,
						top:ctop
						}, {
						duration:200
					});
					selected_i = $('td#' + i + '-' + nj).attr('data-i');
					selected_j = $('td#' + i + '-' + nj).attr('data-j');
				}
			}
			var qId = $this.text();
			// var offset = $('td#'+selected_i+'-'+selected_j).position();
			var offset = $('td#'+i+'-'+j).offset();
			var x = offset.left;
			var y = offset.top;
			$('.questions').css('top', y+'px').css('left', x+'px').text(questions[qId]).show();			
			var width = $('.questions').css('width');
			x = x-parseInt(width);
			$('.questions').css('left', x+'px')
		});
	

		function saveCrossword(){
			if(saveTemp) {
				var letters = '';
				$('td.true').each(function(){
					if($.isNumeric($(this).attr('data-letter')))
					{
						letters = letters + $(this).attr('data-i')+'-'+$(this).attr('data-j')+'_'+$(this).attr('data-letter')+'|';
					}
					else {
						letters = letters + $(this).attr('data-i')+'-'+$(this).attr('data-j')+'_'+returnKey($(this).attr('data-letter'))+'|';
					}
				});
				$('#save-crossword-structure').val(letters);
				if(empty == 0) {
					$('#save-crossword-save-type').val('all');
				}

				if(letters != '' && save_time != 0)
				{
					saveTemp = false;
					$.ajax({
						url:'/cross-words/save',
						type:'POST',
						data: $('#save-crossword-form').serialize(),
						success: function(data){
							save_time = 0;
							$('#save').text('պահպանված է');
							saveTemp = true;
							return true;
						},
						error: function(data) {
							saveTemp = true;
						}
					});
				}
			}
		}

		$('#save').on('click', function()
		{
			saveCrossword();
		});

		$('td').bind('keyup' ,function(e){
			var code = (e.keyCode ? e.keyCode : e.which);
			var i = selected_i;
			var j = selected_j;
			if(code == 37) {
				// left arrow
				var ci = $('td.selected').attr('data-i');
				var cj = $('td.selected').attr('data-j');
				var ni = i;
				var nj = j;
				var max = 20;
				while($('td#'+ni+'-'+nj).attr('class') != 'empty' && max != 0) 
					{
						max--;
						nj--;
					}
				if(max != 0) { var $this = $('td#'+ni+'-'+nj); }
				else { var $this = $('td#'+ci+'-'+cj); }
				$('td.selected').attr('class','empty');
				
				$this.attr('class','selected').attr('tabindex', '1');
				var newoffset = $this.offset();
				var cleft = newoffset.left+60+'px';
				var ctop = newoffset.top-100+'px';
				$keyboard.animate({
					left:cleft,
					top:ctop
					}, {
					duration:200
				});
				selected_i = $this.attr('data-i');
				selected_j = $this.attr('data-j');
			}
			if(code == 39) {
				// right arrow
				var ci = $('td.selected').attr('data-i');
				var cj = $('td.selected').attr('data-j');
				var ni = i;
				var nj = j;
				var max = 20;
				while($('td#'+ni+'-'+nj).attr('class') != 'empty' && max != 0) 
					{
					max--;
					nj++;					
					}
				if(max != 0) { var $this = $('td#'+ni+'-'+nj); }
				else { var $this = $('td#'+ci+'-'+cj); }
				$('td.selected').attr('class','empty');
				$this.attr('class','selected').attr('tabindex', '1');
				var newoffset = $this.offset();
				var cleft = newoffset.left+60+'px';
				var ctop = newoffset.top-100+'px';
				$keyboard.animate({
					left:cleft,
					top:ctop
					}, {
					duration:200
				});
				selected_i = $this.attr('data-i');
				selected_j = $this.attr('data-j');
			}
			if(code == 38) {
				var ci = $('td.selected').attr('data-i');
				var cj = $('td.selected').attr('data-j');
				var ni = i-1;
				var nj = j;
				if($('td#'+ni+'-'+nj).attr('class') == 'empty') {
					var $this = $('td#'+ni+'-'+nj); 
					$('td.selected').attr('class','empty');
					
					$this.attr('class','selected').attr('tabindex', '1');
					var newoffset = $this.offset();
					var cleft = newoffset.left+60+'px';
					var ctop = newoffset.top-100+'px';
					$keyboard.animate({
						left:cleft,
						top:ctop
						}, {
						duration:200
					});
					selected_i = $this.attr('data-i');
					selected_j = $this.attr('data-j');
				}
			}
			if(code == 40) {
				var ci = $('td.selected').attr('data-i');
				var cj = $('td.selected').attr('data-j');
				var ni = i+1;
				var nj = j;
				if($('td#'+ni+'-'+nj).attr('class') == 'empty') {
					var $this = $('td#'+ni+'-'+nj); 

					$('td.selected').attr('class','empty');
					
					$this.attr('class','selected').attr('tabindex', '1');
					var newoffset = $this.offset();
					var cleft = newoffset.left+60+'px';
					var ctop = newoffset.top-100+'px';
					$keyboard.animate({
						left:cleft,
						top:ctop
						}, {
						duration:200
					});
					selected_i = $this.attr('data-i');
					selected_j = $this.attr('data-j');
				}
			}
			return false;
		});

		//keypress event
		$(document).bind('keypress', 'td, .letter', function(e){
			if($(this).hasClass('letter')) {
				var $this = $('td.selected');
			} else {
				var $this = $(this);
			}
			var code = (e.keyCode ? e.keyCode : e.which);
				var key = String.fromCharCode(code);
				if(limit!=0) {
					var i = selected_i;
					var j = selected_j;
					if($('td#'+i+'-'+j).attr('class')=='selected') {
						limit = limit -1;
						$('.limit').text(limit).animate({
							'opacity' : 0.3
						}, {
							duration:200,
							queue:true
						}).animate({
							'opacity' : 0.9
						},{
							duration:200
						});

						//letter convertation
						key = returnKey(key);
						key = key.trim();

						
						$('td#'+i+'-'+j).text(alphabet(key));
						var l = $('div.hidden').find('span#'+i+'-'+j).text();
						var l = c_letters.indexOf(i+'-'+j+'-'+key);
						if(l != -1) {
							$('td.selected').removeClass('empty');
							$('td#'+i+'-'+j).removeClass('selected');
						    $('td#'+i+'-'+j).attr('class','true').attr('data-letter', key);
						    empty = empty - 1;
						    save_time++;
							if(!empty) {
								finish();
								return false;
							}
						    if(save_time > 5) {
						    	saveCrossword();
						    }
						    else {
						    	$('#save').text('պահպանել');
						    }
						    var c1 = parseInt(i)+1;
						    var c2 = parseInt(j)+1;
						    var cm1 = parseInt(i)-1;
						    var cm2 = parseInt(j)-1;
						    if($('td#'+cm1+'-'+j).attr('class') == 'true' && $('td#'+i+'-'+cm2).attr('class') != 'true') {
							    if($('td#'+c1+'-'+j).attr('class') == 'empty') {
							    	$('td#'+c1+'-'+j).attr('class', 'selected').attr('tabindex', '1');
									selected_i = $('td#'+c1+'-'+j).attr('data-i');
									selected_j = $('td#'+c1+'-'+j).attr('data-j');
							    }
							    else if($('td#'+i+'-'+c2).attr('class') == 'empty') {
							    	$('td#'+i+'-'+c2).attr('class', 'selected').attr('tabindex', '1');
									selected_i = $('td#'+i+'-'+c2).attr('data-i');
									selected_j = $('td#'+i+'-'+c2).attr('data-j');
							    }
							}
							else {
								 if($('td#'+i+'-'+c2).attr('class') == 'empty') {
							    	$('td#'+i+'-'+c2).attr('class', 'selected').attr('tabindex', '1');
									selected_i = $('td#'+i+'-'+c2).attr('data-i');
									selected_j = $('td#'+i+'-'+c2).attr('data-j');
							    }
							    else if($('td#'+c1+'-'+j).attr('class') == 'empty') {
							    	$('td#'+c1+'-'+j).attr('class', 'selected').attr('tabindex', '1');
									selected_i = $('td#'+c1+'-'+j).attr('data-i');
									selected_j = $('td#'+c1+'-'+j).attr('data-j');
							    }
							}
							if($('td.selected').length) {
								var newoffset = $('td.selected').offset();
								var cleft = newoffset.left+60+'px';
								var ctop = newoffset.top-100+'px';
								$keyboard.animate({
									left:cleft,
									top:ctop
									}, {
									duration:200
								});
							}
							else {
								$keyboard.animate({
									left:'-500px'
								}, {
									duration:200
								});
							}
						}
					}
					if(limit == 0 && empty != 0) {
						noLimit();
					}
				}
		});

		$('.hand').on('click', function()
		{
			if($(this).attr('data-setup') == 'on')
			{
				$(this).addClass('handOff').attr('data-setup', 'off');
				$keyboard.attr('class', 'no-keyboard');
			}
			else {
				$('div.no-keyboard').attr('class', 'keyboard');
				$(this).removeClass('handOff').attr('data-setup', 'on');
			}
		});	


		$('.questions').on('click', function(){
			$('.questions').hide();
		});

		// close-keyboard
		$('.min-keyboard').on('click', function(){
				$keyboard.animate({
					left:'-500px'
				}, {
					duration:200
				});
				$('td.selected').attr('class','empty');
			})
		// close-keyboard
		$('.close-keyboard').on('click', function(){
				$('td.selected').attr('class','empty');				
				$keyboard.attr('class', 'no-keyboard');
				$('.hand').addClass('handOff').attr('data-setup', 'off');
				
		});

		function returnKey(key) {
			if(inputlang == 'arm') {
				switch(key) {
					case "a": key = "0"; break;
					case "b": key = "1"; break;
					case "g": key = "2"; break;
					case "d": key = "3"; break;
					case "e": key = "4"; break;
					case "z": key = "5"; break;
					case "1": key = "6"; break;
					case "y": key = "7"; break;
					case "2": key = "8"; break;
					case "=": key = "9"; break;
					case "i": key = "10"; break;
					case "l": key = "11"; break;
					case "[": key = "12"; break;
					case "]": key = "13"; break;
					case "k": key = "14"; break;
					case "h": key = "15"; break;
					case "4": key = "16"; break;
					case "x": key = "17"; break;
					case "0": key = "18"; break;
					case "m": key = "19"; break;
					case "j": key = "20"; break;
					case "n": key = "21"; break;
					case "\\": key = "22"; break;
					case "w": key = "23"; break;
					case "9": key = "24"; break;
					case "p": key = "25"; break;
					case "5": key = "26"; break;
					case "r": key = "27"; break;
					case "s": key = "28"; break;
					case "v": key = "29"; break;
					case "t": key = "30"; break;
					case "8": key = "31"; break;
					case "c": key = "32"; break;
					case "u": key = "33"; break;
					case "3": key = "34"; break;
					case "q": key = "35"; break;
					case "7": key = "36"; break;
					case "o": key = "37"; break;
					case "f": key = "38"; break;
					case "ա": key = "0"; break;
					case "բ": key = "1"; break;
					case "գ": key = "2"; break;
					case "դ": key = "3"; break;
					case "ե": key = "4"; break;
					case "զ": key = "5"; break;
					case "է": key = "6"; break;
					case "ը": key = "7"; break;
					case "թ": key = "8"; break;
					case "ժ": key = "9"; break;
					case "ի": key = "10"; break;
					case "լ": key = "11"; break;
					case "խ": key = "12"; break;
					case "ծ": key = "13"; break;
					case "կ": key = "14"; break;
					case "հ": key = "15"; break;
					case "ձ": key = "16"; break;
					case "ղ": key = "17"; break;
					case "ճ": key = "18"; break;
					case "մ": key = "19"; break;
					case "յ": key = "20"; break;
					case "ն": key = "21"; break;
					case "շ": key = "22"; break;
					case "ո": key = "23"; break;
					case "չ": key = "24"; break;
					case "պ": key = "25"; break;
					case "ջ": key = "26"; break;
					case "ռ": key = "27"; break;
					case "ս": key = "28"; break;
					case "վ": key = "29"; break;
					case "տ": key = "30"; break;
					case "ր": key = "31"; break;
					case "ց": key = "32"; break;
					case "ւ": key = "33"; break;
					case "փ": key = "34"; break;
					case "է": key = "35"; break;
					case "և": key = "36"; break;
					case "օ": key = "37"; break;
					case "ֆ": key = "38"; break;
				}
			}
			else if(inputlang == 'rus'){
				switch(key) {
					case 'а': key =  '0' ; break;
					case 'б': key =  '1' ; break;
					case 'в': key =  '2' ; break;
					case 'г': key =  '3' ; break;
					case 'д': key =  '4' ; break;
					case 'е': key =  '5' ; break;
					case 'ж': key =  '6' ; break;
					case 'з': key =  '7' ; break;
					case 'и': key =  '8' ; break;
					case 'й': key =  '9' ; break;
					case 'к' : key = '10' ; break;
					case 'л' : key = '11' ; break;
					case 'м' : key = '12' ; break;
					case 'н' : key = '13' ; break;
					case 'о' : key = '14' ; break;
					case 'п' : key = '15' ; break;
					case 'р' : key = '16' ; break;
					case 'с' : key = '17' ; break;
					case 'т' : key = '18' ; break;
					case 'у' : key = '19' ; break;
					case 'ф' : key = '20' ; break;
					case 'х' : key = '21' ; break;
					case 'ц' : key = '22' ; break;
					case 'ч' : key = '23' ; break;
					case 'ш' : key = '24' ; break;
					case 'щ' : key = '25' ; break;
					case 'ъ' : key = '26' ; break;
					case 'ы' : key = '27' ; break;
					case 'ь' : key = '28' ; break;
					case 'э' : key = '29' ; break;
					case 'ю' : key = '30' ; break;
					case 'я' : key = '31' ; break;
					case 'a': key =  '0' ; break;
					case 'b': key =  '1' ; break;
					case 'w': key =  '2' ; break;
					case 'g': key =  '3' ; break;
					case 'd': key =  '4' ; break;
					case 'e': key =  '5' ; break;
					case 'v': key =  '6' ; break;
					case 'z': key =  '7' ; break;
					case 'i': key =  '8' ; break;
					case 'j': key =  '9' ; break;
					case 'k' : key = '10' ; break;
					case 'l' : key = '11' ; break;
					case 'm' : key = '12' ; break;
					case 'n' : key = '13' ; break;
					case 'o' : key = '14' ; break;
					case 'p' : key = '15' ; break;
					case 'r' : key = '16' ; break;
					case 's' : key = '17' ; break;
					case 't' : key = '18' ; break;
					case 'u' : key = '19' ; break;
					case 'f' : key = '20' ; break;
					case 'h' : key = '21' ; break;
					case 'c' : key = '22' ; break;
					case '`' : key = '23' ; break;
					case '[' : key = '24' ; break;
					case ']' : key = '25' ; break;
					case '#' : key = '26' ; break;
					case 'y' : key = '27' ; break;
					case 'x' : key = '28' ; break;
					case '\\' : key = '29' ; break;
					case '=' : key = '30' ; break;
					case 'q' : key = '31' ; break;
				}
			}
			else if(inputlang == 'eng'){
				switch(key){
					case 'a': key =  '0' ; break;
					case 'b': key =  '1' ; break;
					case 'c': key =  '2' ; break;
					case 'd': key =  '3' ; break;
					case 'e': key =  '4' ; break;
					case 'f': key =  '5' ; break;
					case 'g': key =  '6' ; break;
					case 'h': key =  '7' ; break;
					case 'i': key =  '8' ; break;
					case 'j': key =  '9' ; break;
					case 'k' : key = '10';  break;
					case 'l' : key = '11';  break;
					case 'm' : key = '12';  break;
					case 'n' : key = '13';  break;
					case 'o' : key = '14';  break;
					case 'p' : key = '15';  break;
					case 'q' : key = '16';  break;
					case 'r' : key = '17';  break;
					case 's' : key = '18';  break;
					case 't' : key = '19';  break;
					case 'u' : key = '20';  break;
					case 'v' : key = '21';  break;
					case 'w' : key = '22';  break;
					case 'x' : key = '23';  break;
					case 'y' : key = '24';  break;
					case 'z' : key = '25';  break;
				}
			}

			return key;
		}

		function alphabet(key)
		{
			if(inputlang == 'arm')
			{
				switch(key) {
					case "0" : key = "ա";  break;
					case "1" : key = "բ";  break;
					case "2" : key = "գ";  break;
					case "3" : key = "դ";  break;
					case "4" : key = "ե";  break;
					case "5" : key = "զ";  break;
					case "6" : key = "է";  break;
					case "7" : key = "ը";  break;
					case "8" : key = "թ";  break;
					case "9" : key = "ժ";  break;
					case "10" : key = "ի";  break;
					case "11" : key = "լ";  break;
					case "12" : key = "խ";  break;
					case "13" : key = "ծ";  break;
					case "14" : key = "կ";  break;
					case "15" : key = "հ";  break;
					case "16" : key = "ձ";  break;
					case "17" : key = "ղ";  break;
					case "18" : key = "ճ";  break;
					case "19" : key = "մ";  break;
					case "20" : key = "յ";  break;
					case "21" : key = "ն";  break;
					case "22" : key = "շ";  break;
					case "23" : key = "ո";  break;
					case "24" : key = "չ";  break;
					case "25" : key = "պ";  break;
					case "26" : key = "ջ";  break;
					case "27" : key = "ռ";  break;
					case "28" : key = "ս";  break;
					case "29" : key = "վ";  break;
					case "30" : key = "տ";  break;
					case "31" : key = "ր";  break;
					case "32" : key = "ց";  break;
					case "33" : key = "ու";  break;
					case "34" : key = "փ";  break;
					case "35" : key = "ք";  break;
					case "36" : key = "և";  break;
					case "37" : key = "օ";  break;
					case "38" : key = "ֆ";  break;
				}
			}
			else if(inputlang == 'rus')
			{
				switch(key) {
					case '0' : key = 'а'; break;
					case '1' : key = 'б'; break;
					case '2' : key = 'в'; break;
					case '3' : key = 'г'; break;
					case '4' : key = 'д'; break;
					case '5' : key = 'е'; break;
					case '6' : key = 'ж'; break;
					case '7' : key = 'з'; break;
					case '8' : key = 'и'; break;
					case '9' : key = 'й'; break;
					case '10' : key = 'к'; break;
					case '11' : key = 'л'; break;
					case '12' : key = 'м'; break;
					case '13' : key = 'н'; break;
					case '14' : key = 'о'; break;
					case '15' : key = 'п'; break;
					case '16' : key = 'р'; break;
					case '17' : key = 'с'; break;
					case '18' : key = 'т'; break;
					case '19' : key = 'у'; break;
					case '20' : key = 'ф'; break;
					case '21' : key = 'х'; break;
					case '22' : key = 'ц'; break;
					case '23' : key = 'ч'; break;
					case '24' : key = 'ш'; break;
					case '25' : key = 'щ'; break;
					case '26' : key = 'ъ'; break;
					case '27' : key = 'ы'; break;
					case '28' : key = 'ь'; break;
					case '29' : key = 'э'; break;
					case '30' : key = 'ю'; break;
					case '31' : key = 'я'; break;
				}
			}
			else if(inputlang == 'eng')
			{
				switch(key) {
					case '0' : key = 'a'; break;
					case '1' : key = 'b'; break;
					case '2' : key = 'c'; break;
					case '3' : key = 'd'; break;
					case '4' : key = 'e'; break;
					case '5' : key = 'f'; break;
					case '6' : key = 'g'; break;
					case '7' : key = 'h'; break;
					case '8' : key = 'i'; break;
					case '9' : key = 'j'; break;
					case '10' : key = 'k'; break;
					case '11' : key = 'l'; break;
					case '12' : key = 'm'; break;
					case '13' : key = 'n'; break;
					case '14' : key = 'o'; break;
					case '15' : key = 'p'; break;
					case '16' : key = 'q'; break;
					case '17' : key = 'r'; break;
					case '18' : key = 's'; break;
					case '19' : key = 't'; break;
					case '20' : key = 'u'; break;
					case '21' : key = 'v'; break;
					case '22' : key = 'w'; break;
					case '23' : key = 'x'; break;
					case '24' : key = 'y'; break;
					case '25' : key = 'z'; break;
				}
			}
			return key;
		}

		$('.closePop').on('click', function(){
			$.magnificPopup.close();
		});
		// scroll button
		$(window).scroll(function () {
			
     	});
});
