$(document).ready(function(){
    
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var html = '<style type="text/css">@media only screen and (min-width: 500px) { '
                +'.mfp-bg { min-width: 1250px; }.mfp-wrap { width: 128%;'
                +'} .mgnSmallPopup { width: 800px!important; } }</style>';
        $('head').append(html);
    }

    $( window ).load(function() {
        var breakTime = 5000;
        if($('.notifMain p').length) {
            breakTime = $('.notifMain p').text().length*50;
        }                        
        
        if(breakTime < 5000) breakTime = 5000;
        
        $('.notifWrap').animate({ opacity: 0 }, breakTime, 'easeInBack', { queue: true }, function () {})
            .animate({ opacity: 0 }, 100, function(){});
        $('.notifMain').animate({ opacity: 0 }, breakTime, 'easeInBack', { queue: true }, function () {})
            .animate({ opacity: 0 }, 100, function(){});
        
    });

    $(document).on('click', '.whoSolvedCrossword', function(){
        $('#whoSolvedListScrollable').text('հեսա...');
        $.ajax({
            url: '/cross-words/all-solvers?id='+$(this).attr('data-id'),
            type: 'GET',
            dataType: 'JSON',
            success: function(data) {
                if(data) {
                    if(data.count == 0) {
                        $('#whoSolvedListScrollable').html('այս խաչբառը դեռ ոչ ոք չի լուծել...');
                    }
                    else {
                        var html = '<span class="count">այս խաչբառում ունենք '+data.count+' հաղթող</span>';
                        $.each(data.users, function(i, v){
                            html = html + '<li><ul class="singleSolver"><li><img src="/images/users/'+v.username+'.png"></li><li class="solverUsername" data-url="/user/page-view?id='+v.user_id+'">#'+v.username+'</li><li>'+v.modified+'</li></ul></li>';
                        });
                        $('#whoSolvedListScrollable').html(html);
                    }
                }
            },
        });
    });

    $(document).on('click', '.solverUsername', function(){
        window.location = $(this).attr('data-url');
    });

    $(document).on('click', '.notifMain, notifTitle, .notifWrap', function(){
        $('.notifMain').remove();
        $('.notifWrap').remove();
    });

    $('.imgPopup').magnificPopup({
        type: 'image',
        closeOnContentClick: true
    });
    $('.iframePopup').magnificPopup({
        type: 'iframe',
        mainClass: 'pdfPopup'
    });
    $('.mgnPopupBtn').magnificPopup({
        type: 'inline'
    });

    /////////////////////
    /// mCustomScroll ///
    /////////////////////
    $(".customScroll").mCustomScrollbar({
        scrollbarPosition: 'outside',
        scrollInertia: 1500,
        autoHideScrollbar: true
    });

    $('.addSmiley').on('click', function(){
        if($('#comment-text').length)
        {
            $('#comment-text').val($('#comment-text').val()+$(this).attr('data-smiley'));   
        }
        else if($('#discussion-text').length)
        {
            $('#discussion-text').val($('#discussion-text').val()+$(this).attr('data-smiley'));   
        }
    });
});

function str_replace( search, replace, subject ) { // Replace all occurrences of the search string with the replacement string
    // 
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Gabriel Paderni

    if(!(replace instanceof Array)){
        replace=new Array(replace);
        if(search instanceof Array){//If search is an array and replace is a string, then this replacement string is used for every value of search
            while(search.length>replace.length){
                replace[replace.length]=replace[0];
            }
        }
    }

    if(!(search instanceof Array))search=new Array(search);
    while(search.length>replace.length){//If replace    has fewer values than search , then an empty string is used for the rest of replacement values
        replace[replace.length]='';
    }

    if(subject instanceof Array){//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
        for(k in subject){
            subject[k]=str_replace(search,replace,subject[k]);
        }
        return subject;
    }

    for(var k=0; k<search.length; k++){
        var i = subject.indexOf(search[k]);
        while(i>-1){
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k],i);
        }
    }

    return subject;

}