
$(document).ready(function(){  
    
        function validateForm(formId) {
            var response = '';
            var form = $('#' + formId);            
            $.ajax({
                 async: false,
                 url: form.attr('action'),
                 type: 'post',
                 dataType: 'json',
                 data: form.serialize(),
                 success: function (data) {
                     response = data;
                 }
            });
            return response;
        }       

        $('#problem-form').validate({
            onkeyup: false,
            rules: {
                'email': {
                    required: true,
                    email: true,
                },
                'subject': {
                    required: true,
                    maxlength: 20,
                },
                'message' : {
                    required: true,
                    minlength: 50,
                    maxlength: 500,
                },
            },
            messages: {
                'email': {
                    required: 'էլ հասցեն պարտադիր է.',
                    email: 'էլ հասցեն սխալ է.',
                },
                'subject': {
                    required: 'Վերնագիրը պարտադիր է.',
                    maxlength: 'Վերնագիրը չպետք է գերազանցի 20 տառը.',
                },
                'message' : {
                    required: 'Բովանդակությունը պարտադիր է.',
                    minlength: 'Բովանդակությունը պետք էլ լինի առնվազն 50 տառ.',
                    maxlength: 'Բովանդակությունը պետք էլ լինի ոչ ավել քան 500 տառ.',
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#login-form").validate({
          onkeyup: false,
            rules: {
                'LoginForm[username]': {
                    required: true
                },
                'LoginForm[password]': {
                    required: true,
                    minlength: 6
                },
            },
            messages: {
                'LoginForm[username]': {
                    required: 'Մուտքանունը պարտադիր է.'
                },
                'LoginForm[password]': {
                    required: 'Գաղտնաբառը պարտադիր է.',
                    minlength: 'Գաղտնաբառը պետք է լինի 6 տառից ոչ պակաս.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#request-password-reset-form").validate({
          onkeyup: false,
            rules: {
                'PasswordResetRequestForm[email]': {
                    required: true,
                    email:true,
                },
            },
            messages: {
                'PasswordResetRequestForm[email]': {
                    required: "Էլ հասցեն պարտադիր է.",
                    email: 'Էլ հասցեն պետք է լինի ճշգրիտ.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#password-reset-form").validate({
          onkeyup: false,
            rules: {
                'ResetPasswordForm[password]': {
                    required: true,
                    minlength:6,
                },
                'ResetPasswordForm[password_repeat]': {
                    required:true,
                    equalTo: '#resetpasswordform-password'                    
                },                                
            },
            messages: {
                 'ResetPasswordForm[password]': {
                    required: 'Գաղտնաբառը պարտադիր է.',
                    minlength: 'Գաղտնաբառը պետք է լինի առնվազն 6 նիշ.'
                },
                'ResetPasswordForm[password_repeat]': {
                    required: 'Գաղտնաբառի կրկնությունը պարտադիր է.',
                    equalTo: 'Գաղտնաբառերը չեն համընկնում.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $("#contact-form").validate({
          onkeyup: false,
            rules: {
                'ContactForm[name]': {
                    required: true
                },
                'ContactForm[email]': {
                    required: true,
                    email:true
                },
                'ContactForm[message]': {
                    required: true,                    
                },
                'ContactForm[verifyCode]': {
                    required: true,                    
                },
            },
            messages: {
                'ContactForm[name]': {
                    required: "Անուն Ազգանունը պարտադիր է."
                },
                'ContactForm[email]': {
                    required: "Էլ հասցեն պարտադիր է.",
                    email: 'Էլ հասցեն պետք է լինի ճշգրիտ.'
                },
                'ContactForm[message]': {
                    required: "Հաղորդագրությունը պարտադիր է."
                },
                'ContactForm[verifyCode]': {
                    required: "Անվտանգության կոդը պարտադիր է."
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
        
        $.validator.addMethod("unique_email", 
                function(value, element) {
                    var uniquie_email= true;
                    if($('#signupform-email').length) {
                        var email = $('#signupform-email').val();
                    }
                    if($('#editprofileform-email').length){
                        var email = $('#editprofileform-email').val();
                    }
                    if(email != ""){ 
                    $.ajax({
                        async: false,
                        url: '/user/unique-email',
                        type: 'get',
                        data: {                            
                            email: email
                        } ,
                        success: function(data) {
                            data = jQuery.parseJSON(data)
                           if(data['data']){
                               unquie_email = true;
                           }else{
                               unquie_email = false;
                           }
                        }
                    });
                }
                return unquie_email;
        }, "Այս Էլ հասցեն զբաղված է. Փորձեք մեկ ուրիշը.");
        
        
        $.validator.addMethod("unique_username", 
                function(value, element) {
                    var unique_username= true;
                    if($('#signupform-username').length) {
                        var username = $('#signupform-username').val();
                    }
                    if($('#editprofileform-username').length){
                        var username = $('#editprofileform-username').val();
                    }
                    if(username != ""){ 
                    $.ajax({
                        async: false,
                        url: '/user/unique-username',
                        type: 'get',
                        data: {                            
                            username: username
                        } ,
                        success: function(data) {
                            data = jQuery.parseJSON(data)
                           if(data['data']){
                               unique_username = true;
                           }else{
                               unique_username = false;
                           }
                        }
                    });
                }
                return unique_username;
        }, "Այս մուտքանունը զբաղված է. Փորձեք մեկ ուրիշը.");
        
        $.validator.addMethod("filter_username", function(){
            var username = '';
                if($('#signupform-username').length){
                    username = $('#signupform-username').val();
                }
                else {
                    username = $('#editprofileform-username').val();
                }
                
                if(username.match(/^[a-z][\w-d]+$/i)) {
                    return true;
                }
                return false;
        }, "Մուտքանունը կարող է պարունակել a-z 0-9 և - նիշերը և պետք է սկսվի տառանշանով.");
        
        
        
        $("#signupform").validate({
          onkeyup: false,          
            rules: {
                'SignUpForm[first_name]' : {
                    
                },
                'SignUpForm[last_name]' : {
                    
                },
                'SignUpForm[email]' : {
                    required: true,
                    email: true,
                    unique_email:true                    
                },
                'SignUpForm[address]' : {
                                    
                },
                'SignUpForm[username]': {
                    required: true,
                    filter_username:true,
                    minlength: 2,
                    maxlength: 15,
                    unique_username: true,
                },
                'SignUpForm[password]': {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                'SignUpForm[password_repeat]': {
                    required: true,
                    equalTo: '#signupform-password'
                },
            },
            messages: {
                'SignUpForm[first_name]' : {
                    
                },
                'SignUpForm[last_name]' : {
                    
                },
                'SignUpForm[email]' : {
                    required: 'Էլ հասցեն պարտադիր է.',
                    email: 'Էլ հասցեն պարտադիր է.',
                },
                'SignUpForm[address]' : {
                    
                },
                'SignUpForm[username]': {
                    required: 'Մուտքանունը պարտադիր է.',
                    minlength: 'Մուտքանունը պետք է լինի առնվազն 2 նիշ.',
                    maxlength: 'Մուտքանունը պետք է լինի ոչ ավել քան 15 նիշ.',
                },
                'SignUpForm[password]': {
                    required: 'Գաղտնաբառը պարտադիր է.',
                    minlength: 'Գաղտնաբառը պետք է լինի առնվազն 6 նիշ.',
                    maxlength: 'Գաղտնաբառը պետք է լինի ոչ ավել քան 20 նիշ.',
                },
                'SignUpForm[password_repeat]': {
                    required: 'Գաղտնաբառի կրկնությունը պարտադիր է.',
                    equalTo: 'Գաղտնաբառերը չեն համընկնում.',
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').addClass('error');
                $(element).parents('.form-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').removeClass('error');
                $(element).parents('.form-group').addClass('success');
            }
        });
        
        $("#editprofileform").validate({
          onkeyup: false,          
            rules: {
                'EditProfileForm[first_name]' : {
                    
                },
                'EditProfileForm[last_name]' : {
                    
                },
                'EditProfileForm[email]' : {
                    required: true,
                    email: true,
                    unique_email:true
                },
                'EditProfileForm[address]' : {
                                    
                },
                'EditProfileForm[username]': {
                    required: true,
                    filter_username:true,
                    minlength: 2,
                    maxlength: 15,
                    unique_username: true,
                },
                'EditProfileForm[password]': {                    
                    minlength: 6,
                    maxlength: 20,
                },
                'EditProfileForm[password_repeat]': {                    
                    equalTo: '#editprofileform-password'
                },
            },
            messages: {
                'EditProfileForm[first_name]' : {
                    
                },
                'EditProfileForm[last_name]' : {
                    
                },
                'EditProfileForm[email]' : {
                    required: 'Էլ հասցեն պարտադիր է.',
                    email: 'Էլ հասցեն պարտադիր է.',
                },
                'EditProfileForm[address]' : {
                    
                },
                'EditProfileForm[username]': {
                    required: 'Մուտքանունը պարտադիր է.',
                    minlength: 'Մուտքանունը պետք է լինի առնվազն 2 նիշ.',
                    maxlength: 'Մուտքանունը պետք է լինի ոչ ավել քան 15 նիշ.',
                },
                'EditProfileForm[password]': {                    
                    minlength: 'Գաղտնաբառը պետք է լինի առնվազն 6 նիշ.',
                    maxlength: 'Գաղտնաբառը պետք է լինի ոչ ավել քան 20 նիշ.',
                },
                'EditProfileForm[password_repeat]': {                    
                    equalTo: 'Գաղտնաբառերը չեն համընկնում.'
                },
            },
            errorClass: "help-block-error",
            errorElement: "p",
            highlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').addClass('error');
                $(element).parents('.form-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).parents('.form-group').removeClass('error');
                $(element).parents('.form-group').addClass('success');
            }
        });
});

