$(document).ready(function(){
    
    $( window ).load(function() {
        var breakTime = 5000;
        if($('.notifMain p').length) {
            breakTime = $('.notifMain p').text().length*50;
        }                        
        
        if(breakTime < 5000) breakTime = 5000;
        
        $('.notifWrap').animate({ opacity: 0 }, breakTime, 'easeInBack', { queue: true }, function () {})
            .animate({ opacity: 0 }, 100, function(){});
        $('.notifMain').animate({ opacity: 0 }, breakTime, 'easeInBack', { queue: true }, function () {})
            .animate({ opacity: 0 }, 100, function(){});
        
    });

     $(document).on('click', '.notifMain, notifTitle, .notifWrap', function(){
        $('.notifMain').remove();
        $('.notifWrap').remove();
    });
	////////////////////////
	/// mCustomScrollbar ///
	////////////////////////
	$('.customScroll').mCustomScrollbar();

    //////////////////////
	/// magnific popup ///
	//////////////////////
	$('.mgnPopupBtn').magnificPopup({
		type: 'inline',
	});
	$('.mgnPopupImg').magnificPopup({
		type: 'image',
		closeOnContentClick: true
	});
        
});