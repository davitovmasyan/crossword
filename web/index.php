<?php



// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

if (stripos($_SERVER['SERVER_NAME'], 'am')) {
	$admin = (preg_match('/admin/', $_SERVER['REQUEST_URI'])) ? '_admin' : '';
    $config = require(__DIR__ . '/../config/web'.$admin.'.php');
} else {
    $admin = (preg_match('/admin/', $_SERVER['REQUEST_URI'])) ? '_admin' : '';    
    $config = require(__DIR__ . '/../config/web-local'.$admin.'.php');
}

(new yii\web\Application($config))->run();
